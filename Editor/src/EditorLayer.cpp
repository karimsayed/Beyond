// ReSharper disable CppClangTidyClangDiagnosticFloatConversion
#include "EditorLayer.h"

#include <Beyond/Editor/SceneHierarchyPanel.h>
#include <Beyond/Renderer/Renderer2D.h>
#include <Beyond/Scene/MeshExporter.h>
#include <Beyond/Core/AssimpUtils.h>
#include "ImGuiFileDialog.h"
#include <Beyond/ImGui/ImGuizmo.h>
#include <Beyond.h>
#include <imgui.h>

#include <ranges>
#include <variant>
#define GLFW_EXPOSE_NATIVE_WIN32
#include <GLFW/glfw3native.h>
#include "Beyond/ImGui/CustomImGui.h"

#define GL_GPU_MEM_INFO_TOTAL_AVAILABLE_MEM_NVX 0x9048
#define GL_GPU_MEM_INFO_CURRENT_AVAILABLE_MEM_NVX 0x9049

namespace bey {
	[[maybe_unused]] static void imGuiShowHelpMarker(const char* desc)
	{
		ImGui::TextDisabled("(?)");
		if (ImGui::IsItemHovered())
		{
			ImGui::BeginTooltip();
			ImGui::PushTextWrapPos(ImGui::GetFontSize() * 35.0f);
			ImGui::TextUnformatted(desc);
			ImGui::PopTextWrapPos();
			ImGui::EndTooltip();
		}
	}
	constexpr static const char* imageExtFilter = "Image Files(JPG, PNG, TGA, BMP, PSD, GIF, HDR, PIC)\0*.png;*.jpg;*.tga;*.tga;*.bmp;*.psd;*.gif;*.hdr;*.pic\0";


	Ref<FlyCamera> createCamera()
	{
		BEY_PROFILE_FUNCTION();


		return Ref<FlyCamera>::create(65.0f, 1280.0f, 720.0f, 0.1f, 1000.0f);
	}

	EditorLayer::EditorLayer()
		: mEditorCamera({ createCamera(), createCamera() })
	{
		mEditorCamera[1]->setDebug(true);
	}


	void EditorLayer::onAttach()
	{
		BEY_PROFILE_FUNCTION();


		// ImGui Colors
		ImVec4* colors = ImGui::GetStyle().Colors;
		auto& style = ImGui::GetStyle();
		style.FrameRounding = 4.0f;
		style.WindowBorderSize = 0.0f;
		style.PopupBorderSize = 0.0f;
		style.GrabRounding = 4.0f;

		colors[ImGuiCol_Text] = ImVec4(1.f, 1.f, 1.f, 1.00f);
		colors[ImGuiCol_TextDisabled] = ImVec4(0.35f, 0.35f, 0.35f, 1.00f);
		colors[ImGuiCol_WindowBg] = ImVec4(0.1f, 0.1f, 0.1f, 0.94f);
		colors[ImGuiCol_ChildBg] = ImVec4(0.1f, 0.1f, 0.1f, 0.50f);
		colors[ImGuiCol_PopupBg] = ImVec4(0.08f, 0.08f, 0.08f, 0.94f);
		colors[ImGuiCol_Border] = ImVec4(0.00f, 0.00f, 0.00f, 0.50f);
		colors[ImGuiCol_BorderShadow] = ImVec4(0.00f, 0.00f, 0.00f, 0.00f);
		colors[ImGuiCol_FrameBg] = ImVec4(0.00f, 0.00f, 0.00f, 0.54f);
		colors[ImGuiCol_FrameBgHovered] = ImVec4(0.37f, 0.14f, 0.14f, 0.67f);
		colors[ImGuiCol_FrameBgActive] = ImVec4(0.39f, 0.20f, 0.20f, 0.67f);
		colors[ImGuiCol_TitleBg] = ImVec4(0.04f, 0.04f, 0.04f, 1.00f);
		colors[ImGuiCol_TitleBgActive] = ImVec4(0.48f, 0.16f, 0.16f, 1.00f);
		colors[ImGuiCol_TitleBgCollapsed] = ImVec4(0.48f, 0.16f, 0.16f, 1.00f);
		colors[ImGuiCol_MenuBarBg] = ImVec4(0.14f, 0.14f, 0.14f, 1.00f);
		colors[ImGuiCol_ScrollbarBg] = ImVec4(0.3f, 0.3f, 0.3f, 0.63f);
		colors[ImGuiCol_ScrollbarGrab] = ImVec4(0.31f, 0.31f, 0.31f, 1.00f);
		colors[ImGuiCol_ScrollbarGrabHovered] = ImVec4(0.41f, 0.41f, 0.41f, 1.00f);
		colors[ImGuiCol_ScrollbarGrabActive] = ImVec4(0.51f, 0.51f, 0.51f, 1.00f);
		colors[ImGuiCol_CheckMark] = ImVec4(0.56f, 0.10f, 0.10f, 1.00f);
		colors[ImGuiCol_SliderGrab] = ImVec4(1.00f, 0.19f, 0.19f, 0.40f);
		colors[ImGuiCol_SliderGrabActive] = ImVec4(0.89f, 0.00f, 0.19f, 1.00f);
		colors[ImGuiCol_Button] = ImVec4(1.00f, 0.19f, 0.19f, 0.40f);
		colors[ImGuiCol_ButtonHovered] = ImVec4(0.80f, 0.17f, 0.00f, 1.00f);
		colors[ImGuiCol_ButtonActive] = ImVec4(0.89f, 0.00f, 0.19f, 1.00f);
		colors[ImGuiCol_Header] = ImVec4(0.33f, 0.35f, 0.36f, 0.53f);
		colors[ImGuiCol_HeaderHovered] = ImVec4(0.76f, 0.28f, 0.44f, 0.67f);
		colors[ImGuiCol_HeaderActive] = ImVec4(0.47f, 0.47f, 0.47f, 0.67f);
		colors[ImGuiCol_Separator] = ImVec4(0.32f, 0.32f, 0.32f, 1.00f);
		colors[ImGuiCol_SeparatorHovered] = ImVec4(0.32f, 0.32f, 0.32f, 1.00f);
		colors[ImGuiCol_SeparatorActive] = ImVec4(0.32f, 0.32f, 0.32f, 1.00f);
		colors[ImGuiCol_ResizeGrip] = ImVec4(1.00f, 1.00f, 1.00f, 0.85f);
		colors[ImGuiCol_ResizeGripHovered] = ImVec4(1.00f, 1.00f, 1.00f, 0.60f);
		colors[ImGuiCol_ResizeGripActive] = ImVec4(1.00f, 1.00f, 1.00f, 0.90f);
		colors[ImGuiCol_Tab] = ImVec4(0.07f, 0.07f, 0.07f, 0.51f);
		colors[ImGuiCol_TabHovered] = ImVec4(0.3f, 0.23f, 0.43f, 0.8f);
		colors[ImGuiCol_TabActive] = ImVec4(0.7f, 0.19f, 0.19f, 0.57f);
		colors[ImGuiCol_TabUnfocused] = ImVec4(0.07f, 0.07f, 0.07f, 0.51f);
		colors[ImGuiCol_TabUnfocusedActive] = ImVec4(0.5f, 0.05f, 0.05f, 1.f);
		colors[ImGuiCol_DockingPreview] = ImVec4(0.47f, 0.47f, 0.47f, 0.47f);
		colors[ImGuiCol_DockingEmptyBg] = ImVec4(0.20f, 0.20f, 0.20f, 1.00f);
		colors[ImGuiCol_PlotLines] = ImVec4(0.61f, 0.61f, 0.61f, 1.00f);
		colors[ImGuiCol_PlotLinesHovered] = ImVec4(1.00f, 0.43f, 0.35f, 1.00f);
		colors[ImGuiCol_PlotHistogram] = ImVec4(0.90f, 0.70f, 0.00f, 1.00f);
		colors[ImGuiCol_PlotHistogramHovered] = ImVec4(1.00f, 0.60f, 0.00f, 1.00f);
		colors[ImGuiCol_TableHeaderBg] = ImVec4(0.19f, 0.19f, 0.20f, 1.00f);
		colors[ImGuiCol_TableBorderStrong] = ImVec4(0.31f, 0.31f, 0.35f, 1.00f);
		colors[ImGuiCol_TableBorderLight] = ImVec4(0.23f, 0.23f, 0.25f, 1.00f);
		colors[ImGuiCol_TableRowBg] = ImVec4(0.00f, 0.00f, 0.00f, 0.00f);
		colors[ImGuiCol_TableRowBgAlt] = ImVec4(1.00f, 1.00f, 1.00f, 0.07f);
		colors[ImGuiCol_TextSelectedBg] = ImVec4(0.26f, 0.59f, 0.98f, 0.35f);
		colors[ImGuiCol_DragDropTarget] = ImVec4(1.00f, 1.00f, 0.00f, 0.90f);
		colors[ImGuiCol_NavHighlight] = ImVec4(0.26f, 0.59f, 0.98f, 1.00f);
		colors[ImGuiCol_NavWindowingHighlight] = ImVec4(1.00f, 1.00f, 1.00f, 0.70f);
		colors[ImGuiCol_NavWindowingDimBg] = ImVec4(0.80f, 0.80f, 0.80f, 0.20f);
		colors[ImGuiCol_ModalWindowDimBg] = ImVec4(0.80f, 0.80f, 0.80f, 0.35f);


		// Editor
		mCheckerboardTex = Texture2D::create("assets/editor/Checkerboard.tga", DataType::UNSIGNED_BYTE, false, false);

		mEditorScene = Ref<Scene>::create();
		mSceneHierarchyPanel = std::make_unique<SceneHierarchyPanel>(mEditorScene);
		mSceneHierarchyPanel->setSelectionChangedCallback([this](const Entity& entity) { selectEntity(entity); });
		mSceneHierarchyPanel->setEntityDeletedCallback([this](const Entity& entity) { onEntityDeleted(entity); });

		ImGuiFileDialog::Instance()->OpenDialog("Asset Browser", "Asset Browser", ".*", "./assets/");
		ImGuiFileDialog::Instance()->SetExtentionInfos(".png", ImVec4(1, 1, 0, 0.9f), "[Tex]");
		ImGuiFileDialog::Instance()->SetExtentionInfos(".jpg", ImVec4(1, 1, 0, 0.9f), "[Tex]");
		ImGuiFileDialog::Instance()->SetExtentionInfos(".tga", ImVec4(1, 1, 0, 0.9f), "[Tex]");
		ImGuiFileDialog::Instance()->SetExtentionInfos(".bmp", ImVec4(1, 1, 0, 0.9f), "[Tex]");
		ImGuiFileDialog::Instance()->SetExtentionInfos(".psd", ImVec4(1, 1, 0, 0.9f), "[Tex]");
		ImGuiFileDialog::Instance()->SetExtentionInfos(".gif", ImVec4(1, 1, 0, 0.9f), "[Tex]");
		ImGuiFileDialog::Instance()->SetExtentionInfos(".hdr", ImVec4(1, 1, 0, 0.9f), "[Tex]");
		ImGuiFileDialog::Instance()->SetExtentionInfos(".pic", ImVec4(1, 1, 0, 0.9f), "[Tex]");

		ImGuiFileDialog::Instance()->SetExtentionInfos(".fbx", ImVec4(0.f, 1, 1.f, 0.9f), "[MESH]");
		ImGuiFileDialog::Instance()->SetExtentionInfos(".obj", ImVec4(0.f, 1, 1.f, 0.9f), "[MESH]");
		//ImGuiFileDialog::Instance()->SetExtentionInfos(".", ImVec4(0.f, 1, 1.f, 0.9f));

	}

	void EditorLayer::onDetach()
	{
		BEY_PROFILE_FUNCTION();


	}


	std::pair<float, float> EditorLayer::getMouseViewportSpace() const
	{
		auto [mx, my] = ImGui::GetMousePos();

		mx -= mViewportBounds[0].x;
		my -= mViewportBounds[0].y;
		const auto viewportWidth = mViewportBounds[1].x - mViewportBounds[0].x;
		const auto viewportHeight = mViewportBounds[1].y - mViewportBounds[0].y;

		return { (mx / viewportWidth) * 2.0f - 1.0f, ((my / viewportHeight) * 2.0f - 1.0f) * -1.0f };
	}



	void EditorLayer::onUpdate(const TimeStep ts)
	{
		BEY_PROFILE_FUNCTION();

		mTimeStep = ts;

		if (mMouseRightInViewport || !mAllowImGuiMouse)
		{
			const glm::vec2 mouse = Input::getMousePosition();
			static glm::vec2 last;

			if (Input::isMouseButtonPressed(GLFW_MOUSE_BUTTON_RIGHT))
			{
				mAllowImGuiMouse = false;

				const glm::vec2 mouseDelta = (last - mouse) * 0.003f;
				mEditorCamera[mCurrentCamera]->onUpdate(ts, mouseDelta);
				glfwSetInputMode(Game::get().getWindow().getNativeWindow(), GLFW_CURSOR, GLFW_CURSOR_DISABLED);
			}
			else
			{
				mAllowImGuiMouse = true;

				glfwSetInputMode(Game::get().getWindow().getNativeWindow(), GLFW_CURSOR, GLFW_CURSOR_NORMAL);
			}



			last = mouse;
		}



		mEditorScene->onRenderEditor(ts, mEditorCamera[mCurrentCamera]);

	}

	void EditorLayer::setViewportFullScreen()
	{
		auto* viewport = ImGui::FindWindowByName("Viewport");
		if (mFullScreenViewport)
		{
			mBackupViewportDockID = mViewportDockID;

			mBackupViewportBounds[0].x = mViewportBounds[0].x;
			mBackupViewportBounds[0].y = mViewportBounds[0].y;
			mBackupViewportBounds[1].x = mBackupViewportBounds[1].x - mViewportBounds[0].x;
			mBackupViewportBounds[1].y = mBackupViewportBounds[1].y - mViewportBounds[0].y;
			auto [width, height] = Game::get().getWindow().getSize();
			ImGui::SetWindowPos(viewport, { 0, 0 });
			ImGui::SetWindowSize(viewport, { float(width), float(height) });
			ImGui::SetWindowDock(viewport, -1, ImGuiCond_Always);
		}
		else
		{
			ImGui::SetWindowPos(viewport, mBackupViewportBounds[0]);
			ImGui::SetWindowSize(viewport, mBackupViewportBounds[1]);
			ImGui::SetWindowDock(viewport, mBackupViewportDockID, ImGuiCond_Always);
		}
	}


	auto operator < (const ImVec2& lhs, const ImVec2& rhs)
	{
		return lhs.x < rhs.x && lhs.y < rhs.y;
	}

	template <typename charT>
	struct insensitiveChar {
		operator charT() const { return std::toupper(X); }
		charT X;
	};

	template <typename charT>
	static std::basic_string<insensitiveChar<charT>>* insensitiveString(const std::basic_string<charT>& s)
	{
		return (std::basic_string<insensitiveChar<charT> > *) & s;
	}

	template <typename charT>
	static insensitiveChar<charT>* insensitiveString(const charT* s)
	{
		return (insensitiveChar<charT>*)(s);
	}


	bool confirm(const LPCWSTR& text, const LPCWSTR& caption)
	{
		const int msgBoxID = MessageBox(
			glfwGetWin32Window(Game::get().getWindow().getNativeWindow()),
			text,
			caption,
			MB_ICONQUESTION | MB_YESNO
		);


		return msgBoxID == IDYES;
	}

	void EditorLayer::openScene(const std::string& filepath)
	{
		Ref<Scene> newScene = Ref<Scene>::create();
		SceneSerializer serializer(newScene);
		serializer.deserialize(filepath);
		mEditorScene = newScene;
		mSceneHierarchyPanel->setContext(mEditorScene);

		mEditorScene->setSelectedEntity({});
		mSelectionContext.clear();
	}


	void EditorLayer::onImGuiRender()
	{
		static bool pOpen = true;

		static bool optFullscreenPersistant = true;
		static ImGuiDockNodeFlags optFlags = ImGuiDockNodeFlags_None;
		bool optFullscreen = optFullscreenPersistant;

		// We are using the ImGuiWindowFlags_NoDocking flag to make the parent window not dockable into,
		// because it would be confusing to have two docking targets within each others.
		ImGuiWindowFlags windowFlags = ImGuiWindowFlags_MenuBar | ImGuiWindowFlags_NoDocking;
		if (optFullscreen)
		{
			ImGuiViewport* viewport = ImGui::GetMainViewport();
			ImGui::SetNextWindowPos(viewport->Pos);
			ImGui::SetNextWindowSize(viewport->Size);
			ImGui::SetNextWindowViewport(viewport->ID);
			ImGui::PushStyleVar(ImGuiStyleVar_WindowRounding, 0.0f);
			ImGui::PushStyleVar(ImGuiStyleVar_WindowBorderSize, 0.0f);
			windowFlags |= ImGuiWindowFlags_NoTitleBar | ImGuiWindowFlags_NoCollapse | ImGuiWindowFlags_NoResize | ImGuiWindowFlags_NoMove;
			windowFlags |= ImGuiWindowFlags_NoBringToFrontOnFocus | ImGuiWindowFlags_NoNavFocus;
		}

		//When using ImGuiDockNodeFlags_PassthruDockspace, DockSpace() will render our background and handle the pass-thru hole, so we ask Begin() to not render a background.
		if (optFlags & ImGuiDockNodeFlags_PassthruCentralNode)
			windowFlags |= ImGuiWindowFlags_NoBackground;

		ImGui::PushStyleVar(ImGuiStyleVar_WindowPadding, ImVec2(0.0f, 0.0f));
		ImGui::Begin("DockSpace Demo", &pOpen, windowFlags);
		ImGui::PopStyleVar();

		if (optFullscreen)
			ImGui::PopStyleVar(2);

		// Dockspace
		ImGuiIO& io = ImGui::GetIO();
		if (io.ConfigFlags & ImGuiConfigFlags_DockingEnable)
		{
			ImGuiID dockspaceID = ImGui::GetID("MyDockspace");
			ImGui::DockSpace(dockspaceID, ImVec2(0.0f, 0.0f), optFlags);
		}

		mAllowImGuiMouse ? io.ConfigFlags &= ~ImGuiConfigFlags_NoMouse : io.ConfigFlags |= ImGuiConfigFlags_NoMouse;


		// Editor Panel ------------------------------------------------------------------------------
		ImGui::Begin("Model");

		ImGui::Begin("Miscellaneous");
		ImGui::Columns(2);
		ImGui::AlignTextToFramePadding();

		static const char* items[] = { "Camera 1", "Camera 2" };
		property("Camera Selection", mCurrentCamera, items, 2);
		property("Exposure", mEditorCamera[mCurrentCamera]->getExposure(), 0.0f, 5.0f);
		property("Camera Speed", mEditorCamera[mCurrentCamera]->getCameraSpeed(), 0.0005f, 2.f);
		property("Radiance Prefiltering", mRadiancePrefilter);
		property("Env Map Rotation", mEnvMapRotation, -360.0f, 360.0f);

		if (property("Show Bounding Boxes", mUiShowBoundingBoxes))
			showBoundingBoxes(mUiShowBoundingBoxes, mUiShowBoundingBoxesOnTop);
		if (mUiShowBoundingBoxes && property("On Top", mUiShowBoundingBoxesOnTop))
			showBoundingBoxes(mUiShowBoundingBoxes, mUiShowBoundingBoxesOnTop);

		if (property("Instrumentation", BEY_PROFILE_ENABLED))
			BEY_PROFILE_REFRESH(Instrumentor::get().getName());

		static const char* selectionModes[] = { "Entity", "Mesh" };
		auto mode = static_cast<unsigned>(mSelectionMode);
		property("Selection Mode", mode, selectionModes, IM_ARRAYSIZE(selectionModes));
		mSelectionMode = static_cast<SelectionMode>(mode);
		ImGui::Columns(1);


		ImGui::End();



		if (!mSelectionContext.empty())
		{
			using T = std::decay_t<decltype(mSelectionContext[0].Selectable)>;
			if constexpr (std::is_same_v<T, Submesh>)
			{
				auto& selection = mSelectionContext[0];
				auto& mesh = selection.Entity.getComponent<MeshComponent>().Mesh;
				{
					if (mSelectionMode == SelectionMode::ENTITY)
					{
						ImGui::Separator();
						ImGui::Text("Mesh");
						std::string fullpath = mesh ? mesh->getFilePath() : "None";
						size_t found = fullpath.find_last_of("/\\");
						std::string path = found != std::string::npos ? fullpath.substr(found + 1) : fullpath;
						ImGui::Text("Full Path: %s", fullpath.c_str());
						ImGui::Text("Entity Name: %s", path.c_str()); ImGui::SameLine();
						if (ImGui::Button("Replace File##Entity"))
						{
							std::string filename = Game::get().openFile("");
							if (!filename.empty())
								selection.Entity.getComponent<MeshComponent>().Mesh = Ref<Mesh>::create(filename);
						}
					}
					ImGui::Separator();
				}
			}
		}


		ImGui::Separator();

		if (ImGui::TreeNode("Shaders"))
		{
			auto& shaders = Shader::s_AllShaders;
			for (auto& shader : shaders)
			{
				if (ImGui::TreeNode(fmt::format("{}{}", shader->getName(), shader->getRendererID()).c_str()))
				{
					std::string buttonName = fmt::format("Reload##{}{}", shader->getName(), shader->getRendererID());
					if (ImGui::Button(buttonName.c_str()))
						shader->reload();
					ImGui::TreePop();
				}
			}


			ImGui::TreePop();
		}
		ImGui::End();



		ImGui::PushStyleVar(ImGuiStyleVar_WindowPadding, ImVec2(0, 0));

		auto viewportFlags = mFullScreenViewport ? ImGuiWindowFlags_NoDecoration | ImGuiWindowFlags_NoMove : 0;
		ImGui::Begin("Viewport", nullptr, viewportFlags);

		mViewportPanelMouseOver = ImGui::IsWindowHovered();
		mViewportPanelFocused = ImGui::IsWindowFocused();
		mRenderTargetsWindowFocused = !ImGui::IsWindowAppearing();

		mViewportDockID = ImGui::GetWindowDockID();
		if (!mAllowImGuiMouse)
			ImGui::FocusWindow(mRenderTargetsWindowFocused ? ImGui::FindWindowByID(ImGui::GetID("Render Targets")) : ImGui::GetCurrentWindow());

		//ImGui::SetWindowSize({ 1920, 1104 });


		auto viewportOffset = ImGui::GetCursorPos(); // includes tab bar
		auto viewportSize = ImGui::GetContentRegionAvail();
		if (viewportSize.y > 0)
		{
			SceneRenderer::setViewportSize(uint32_t(viewportSize.x), uint32_t(viewportSize.y));
			mEditorScene->setViewportSize(uint32_t(viewportSize.x), uint32_t(viewportSize.y));
			mEditorCamera[mCurrentCamera]->setViewportSize(uint32_t(viewportSize.x), uint32_t(viewportSize.y));
			mEditorCamera[mCurrentCamera]->setProjectionMatrix(glm::perspectiveFov(glm::radians(65.f), viewportSize.x, viewportSize.y, 0.1f, 1000.f), 0.1f, 1000.f);
		}

		ImGui::Image(reinterpret_cast<void*>(SceneRenderer::getFinalColorBufferRendererID()), viewportSize, { 0, 1 }, { 1, 0 });

		ImVec2 windowSize = ImGui::GetWindowSize();
		ImVec2 minBound = ImGui::GetWindowPos();
		minBound.x += viewportOffset.x;
		minBound.y += viewportOffset.y;

		ImVec2 maxBound = { minBound.x + windowSize.x, minBound.y + windowSize.y };
		mViewportBounds[0] = { minBound.x, minBound.y };
		mViewportBounds[1] = { maxBound.x, maxBound.y };

		mMouseHoveringViewport = ImGui::IsWindowHovered(ImGuiHoveredFlags_None);
		mMouseRightInViewport = minBound < io.MouseClickedPos[1] && io.MouseClickedPos[1] < maxBound && mMouseHoveringViewport;

	
		if (mMouseHoveringViewport)
		{
			auto str = ui::getPayloadString("Mesh");
			if (!str.empty())
			{
				const auto newEntity = mEditorScene->createEntity("new entity");
				newEntity.addComponent<MeshComponent>().Mesh = Ref<Mesh>::create(str);
			}
			str = ui::getPayloadString("Scene");
			if (!str.empty())
			{
				openScene(str);
			}
		}

		// Gizmos
		if (mGizmoType != -1 && !mSelectionContext.empty())
		{
			auto& selection = mSelectionContext[0];

			auto rw = ImGui::GetWindowWidth();
			auto rh = ImGui::GetWindowHeight();
			ImGuizmo::SetOrthographic(false);
			ImGuizmo::SetDrawlist();
			ImGuizmo::SetRect(ImGui::GetWindowPos().x, ImGui::GetWindowPos().y, rw, rh);
			bool snap = Input::isKeyPressed(BEY_KEY_J);

			auto& entityTransform = selection.Entity.transform();
			float snapValue[3] = { mSnapValue, mSnapValue, mSnapValue };


			if (mSelectionMode == SelectionMode::ENTITY || !std::get_if<Submesh*>(&selection.Selectable))
			{
				ImGuizmo::Manipulate(glm::value_ptr(mEditorCamera[mCurrentCamera]->getViewMatrix()),
					glm::value_ptr(mEditorCamera[mCurrentCamera]->getProjectionMatrix()),
					ImGuizmo::OPERATION(mGizmoType),
					ImGuizmo::LOCAL,
					glm::value_ptr(entityTransform),
					nullptr,
					snap ? snapValue : nullptr);
			}
			else
			{
				glm::mat4 transformBase = entityTransform * std::get<Submesh*>(selection.Selectable)->Transform;
				ImGuizmo::Manipulate(glm::value_ptr(mEditorCamera[mCurrentCamera]->getViewMatrix()),
					glm::value_ptr(mEditorCamera[mCurrentCamera]->getProjectionMatrix()),
					ImGuizmo::OPERATION(mGizmoType),
					ImGuizmo::LOCAL,
					glm::value_ptr(transformBase),
					nullptr,
					snap ? snapValue : nullptr);

				std::get<Submesh*>(selection.Selectable)->Transform = glm::inverse(entityTransform) * transformBase;
			}
		}

		ImGui::End();

		ImGui::PopStyleVar();

		if (ImGui::BeginMenuBar())
		{

			if (ImGui::BeginMenu("File"))
			{
				if (ImGui::MenuItem("New Scene", "Ctrl+N"))
				{
					mEditorScene = Ref<Scene>::create();
					mSceneHierarchyPanel->setContext(mEditorScene);

				}
				else if (ImGui::MenuItem("Open Scene...", "Ctrl+O"))
				{
					auto& app = Game::get();
					std::string filepath = app.openFile("Beyond Scene (*.hsc)\0*.hsc\0");
					if (!filepath.empty())
					{
						openScene(filepath);
					}
				}
				else if (ImGui::MenuItem("Save Scene...", "Ctrl+S"))
				{
					std::string filepath = Game::get().saveFile("Beyond Scene (*.hsc)\0*.hsc\0");
					if (!filepath.empty())
					{
						SceneSerializer serializer(mEditorScene);
						serializer.serialize(filepath);
					}
				}
				else if (ImGui::MenuItem("Export Scene...", "Ctrl+E"))
				{
					std::string exportPath = Game::get().saveFile("Beyond Scene (*.hsc)\0*.hsc\0");
					if (!exportPath.empty())
						MeshExporter::exportScene(mEditorScene, exportPath);
				}
				ImGui::Separator();
				if (ImGui::MenuItem("Exit"))
				{
					pOpen = false;
				}
				ImGui::EndMenu();
			}
			if (ImGui::BeginMenu("load default"))
			{
				if (ImGui::MenuItem("Open My new Sponza Palace"))
				{
					std::string filepath = "assets/scenes/newSponza.hsc";

					Ref<Scene> newScene = Ref<Scene>::create();
					SceneSerializer serializer(newScene);
					serializer.deserialize(filepath);
					mEditorScene = newScene;
					mSceneHierarchyPanel->setContext(mEditorScene);

					mEditorScene->setSelectedEntity({});
					mSelectionContext.clear();

				}
				if (ImGui::MenuItem("Open Old Sponza Palace"))
				{
					std::string filepath = "assets/scenes/OldSponza.hsc";

					Ref<Scene> newScene = Ref<Scene>::create();
					SceneSerializer serializer(newScene);
					serializer.deserialize(filepath);
					mEditorScene = newScene;
					mSceneHierarchyPanel->setContext(mEditorScene);

					mEditorScene->setSelectedEntity({});
					mSelectionContext.clear();

				}

				if (ImGui::MenuItem("Open Test Scene"))
				{
					std::string filepath = "assets/scenes/TestScene.hsc";
					Ref<Scene> newScene = Ref<Scene>::create();
					SceneSerializer serializer(newScene);
					serializer.deserialize(filepath);
					mEditorScene = newScene;
					mSceneHierarchyPanel->setContext(mEditorScene);

					mEditorScene->setSelectedEntity({});
					mSelectionContext.clear();
				}
				ImGui::EndMenu();
			}
			static bool showDemo = false;
			if (ImGui::BeginMenu("Settings"))
			{
				if (ImGui::MenuItem("Reset Camera", "Ctrl+R"))
					mEditorCamera[mCurrentCamera]->reset();
				ImGui::Checkbox("Show Demo", &showDemo);
				ImGui::Checkbox("Statistics", &mStatsWindow);
				if (ImGui::Checkbox("Fullscreen Viewport", &mFullScreenViewport))
					setViewportFullScreen();

				ImGui::EndMenu();
			}
			ImGui::EndMenuBar();
			if (showDemo)
				ImGui::ShowDemoWindow();
		}

		mSceneHierarchyPanel->onImGuiRender();


		ImGui::Begin("Materials");
		if (!mSelectionContext.empty())
		{
			Entity selectedEntity = mSelectionContext.front().Entity;
			if (selectedEntity.hasComponent<MeshComponent>())
			{
				Ref<Mesh> mesh = selectedEntity.getComponent<MeshComponent>().Mesh;
				if (mesh)
				{
					auto& unFilteredMaterials = mesh->getMaterials();
					static std::vector<Ref<MaterialInstance>> materials;
					static uint32_t selectedMaterialIndex = 0;
					static char searchStr[128] = "";
					
					if (ImGui::InputTextWithHint("Search materials", "Search Materials", searchStr, 1024,
						ImGuiInputTextFlags_AutoSelectAll))
					{

						materials.clear();
						std::ranges::copy(unFilteredMaterials | std::views::filter([](Ref<MaterialInstance> mat)
							{
								return insensitiveString(mat->getName())->find(insensitiveString(searchStr)) != std::string::npos;
							}), std::back_inserter(materials));

					}
					if (strlen(searchStr) == 0)
						materials = unFilteredMaterials;


					for (uint32_t i = 0; i < materials.size(); i++)
					{
						auto& materialInstance = materials[i];
						ImGuiTreeNodeFlags nodeFlags = (selectedMaterialIndex == i ? ImGuiTreeNodeFlags_Selected : 0) | ImGuiTreeNodeFlags_Leaf;
						bool opened = ImGui::TreeNodeEx((void*)(&materialInstance), nodeFlags, materialInstance->getName().c_str());
						if (ImGui::IsItemClicked())
							selectedMaterialIndex = i;
						if (opened)
							ImGui::TreePop();
					}
					ImGui::Separator();
					// Selected material
					if (selectedMaterialIndex < materials.size())
					{
						auto& materialInstance = materials[selectedMaterialIndex];
						ImGui::Text("Shader: %s", materialInstance->getShader()->getName().c_str());

						// Textures -- Albedo
						{
							if (ImGui::CollapsingHeader("Albedo", nullptr, ImGuiTreeNodeFlags_DefaultOpen))
							{
								ImGui::PushStyleVar(ImGuiStyleVar_FramePadding, ImVec2(10, 10));
								auto& albedoColor = materialInstance->get<glm::vec4>("u_AlbedoColor", ShaderDomain::Pixel);
								bool useAlbedoMap = materialInstance->get<float>("u_AlbedoTexToggle", ShaderDomain::Pixel);
								auto albedoMap = materialInstance->tryGetResource<Texture2D>("u_AlbedoTexture");
								ImGui::Image(albedoMap ? reinterpret_cast<void*>(static_cast<size_t>(albedoMap->getRendererID()))
									: reinterpret_cast<void*>(static_cast<size_t>(mCheckerboardTex->getRendererID())), ImVec2(64, 64));

								ImGui::PopStyleVar();

								static bool importingTexture = false;
								bool transparent;
								bool answered = false;
								static std::string fileName;


								if (ImGui::IsItemHovered())
								{
									std::string path = ui::getPayloadString("Image");
									if (!path.empty())
									{
										fileName = path;
										importingTexture = true;
									}
									if (albedoMap)
									{
										ImGui::BeginTooltip();
										ImGui::PushTextWrapPos(ImGui::GetFontSize() * 35.0f);
										ImGui::TextUnformatted(albedoMap->getPath().c_str());
										ImGui::PopTextWrapPos();
										ImGui::Image(reinterpret_cast<void*>(static_cast<size_t>(albedoMap->getRendererID())), ImVec2(384, 384));
										ImGui::EndTooltip();
									}


									if (ImGui::IsItemClicked())
									{
										fileName = Game::get().openFile(imageExtFilter);

										if (!fileName.empty())
											importingTexture = true;
									}
								}


								if (!answered && importingTexture)
								{
									ImGui::OpenPopup("Transparent?");

									// Always center this window when appearing
									ImVec2 center(ImGui::GetIO().DisplaySize.x * 0.5f, ImGui::GetIO().DisplaySize.y * 0.5f);
									ImGui::SetNextWindowPos(center, ImGuiCond_Appearing, ImVec2(0.5f, 0.5f));
									if (ImGui::BeginPopupModal("Transparent?", nullptr, ImGuiWindowFlags_AlwaysAutoResize))
									{
										ImGui::Text("Is this texture transparent?\n");
										ImGui::Separator();

										ImGui::PushStyleVar(ImGuiStyleVar_FramePadding, ImVec2(0, 0));
										ImGui::PopStyleVar();

										if (ImGui::Button("Yes", ImVec2(120, 0))) { answered = true; importingTexture = false; transparent = true; ImGui::CloseCurrentPopup(); }
										ImGui::SetItemDefaultFocus();
										ImGui::SameLine();
										if (ImGui::Button("No", ImVec2(120, 0))) { answered = true; importingTexture = false; transparent = false; ImGui::CloseCurrentPopup(); }
										ImGui::EndPopup();
									}
								}
								if (answered && !importingTexture)
								{
									albedoMap = Texture2D::create(fileName, DataType::UNSIGNED_BYTE, true, transparent, aiTextureType_DIFFUSE/*m_AlbedoInput.SRGB*/);
									materialInstance->set("u_AlbedoTexture", albedoMap);
									materialInstance->set<float>("u_AlbedoTexToggle", 1.0f, ShaderDomain::Pixel);
									MeshExporter::updateTexture(fileName, mesh, selectedMaterialIndex, _AI_MATKEY_TEXTURE_BASE, aiTextureType_DIFFUSE);
								}




								ImGui::SameLine();
								ImGui::BeginGroup();
								if (ImGui::Checkbox("Use##AlbedoMap", &useAlbedoMap))
								{
									materialInstance->set<float>("u_AlbedoTexToggle", useAlbedoMap ? 1.0f : 0.0f, ShaderDomain::Pixel);
									MeshExporter::updateTexture(useAlbedoMap && albedoMap ? albedoMap->getPath() : "", mesh, selectedMaterialIndex, _AI_MATKEY_TEXTURE_BASE, aiTextureType_DIFFUSE);
								}
								ImGui::EndGroup();
								ImGui::SameLine();
								if (ImGui::ColorEdit4("Color##Albedo", glm::value_ptr(albedoColor), ImGuiColorEditFlags_NoInputs))
								{
									MeshExporter::updateFactor({ albedoColor.x, albedoColor.y, albedoColor.z, albedoColor.w, }, mesh, selectedMaterialIndex, "$clr.diffuse");
								}
							}
						}
						{
							// Normals
							if (ImGui::CollapsingHeader("Normals", nullptr, ImGuiTreeNodeFlags_DefaultOpen))
							{
								ImGui::PushStyleVar(ImGuiStyleVar_FramePadding, ImVec2(10, 10));
								bool useNormalMap = materialInstance->get<float>("u_NormalTexToggle", ShaderDomain::Pixel);
								Ref<Texture2D> normalMap = materialInstance->tryGetResource<Texture2D>("u_NormalTexture");
								ImGui::Image(normalMap ? reinterpret_cast<void*>(static_cast<size_t>(normalMap->getRendererID())) :
									reinterpret_cast<void*>(static_cast<size_t>(mCheckerboardTex->getRendererID())), ImVec2(64, 64));
								ImGui::PopStyleVar();
								if (ImGui::IsItemHovered())
								{
									std::string fileName;
									std::string path = ui::getPayloadString("Image");
									if (!path.empty())
										fileName = path;
									if (normalMap)
									{
										ImGui::BeginTooltip();
										ImGui::PushTextWrapPos(ImGui::GetFontSize() * 35.0f);
										ImGui::TextUnformatted(normalMap->getPath().c_str());
										ImGui::PopTextWrapPos();
										ImGui::Image(reinterpret_cast<void*>(static_cast<size_t>(normalMap->getRendererID())), ImVec2(384, 384));
										ImGui::EndTooltip();
									}
									if (ImGui::IsItemClicked())
										fileName = Game::get().openFile(imageExtFilter);
									if (!fileName.empty())
									{
										normalMap = Texture2D::create(fileName, DataType::UNSIGNED_BYTE, false, false, aiTextureType_NORMALS);
										materialInstance->set("u_NormalTexture", normalMap);
										materialInstance->set<float>("u_NormalTexToggle", 1.0f, ShaderDomain::Pixel);
										MeshExporter::updateTexture(fileName, mesh, selectedMaterialIndex, _AI_MATKEY_TEXTURE_BASE, aiTextureType_NORMALS);
									}
								}
								ImGui::SameLine();
								if (ImGui::Checkbox("Use##NormalMap", &useNormalMap))
								{
									materialInstance->set<float>("u_NormalTexToggle", useNormalMap ? 1.0f : 0.0f, ShaderDomain::Pixel);
									MeshExporter::updateTexture(useNormalMap && normalMap ? normalMap->getPath() : "", mesh, selectedMaterialIndex, _AI_MATKEY_TEXTURE_BASE, aiTextureType_NORMALS);
								}
							}
						}
						{
							// Metalness
							if (ImGui::CollapsingHeader("Metalness", nullptr, ImGuiTreeNodeFlags_DefaultOpen))
							{
								ImGui::PushStyleVar(ImGuiStyleVar_FramePadding, ImVec2(10, 10));
								auto& metalnessValue = materialInstance->get<float>("u_Metalness", ShaderDomain::Pixel);
								bool useMetalnessMap = materialInstance->get<float>("u_MetalnessTexToggle", ShaderDomain::Pixel);
								Ref<Texture2D> metalnessMap = materialInstance->tryGetResource<Texture2D>("u_MetalnessTexture");
								ImGui::Image(metalnessMap ? reinterpret_cast<void*>(static_cast<size_t>(metalnessMap->getRendererID())) :
									reinterpret_cast<void*>(static_cast<size_t>(mCheckerboardTex->getRendererID())), ImVec2(64, 64));
								ImGui::PopStyleVar();
								if (ImGui::IsItemHovered())
								{
									std::string fileName;
									std::string path = ui::getPayloadString("Image");
									if (!path.empty())
										fileName = path;
									if (metalnessMap)
									{
										ImGui::BeginTooltip();
										ImGui::PushTextWrapPos(ImGui::GetFontSize() * 35.0f);
										ImGui::TextUnformatted(metalnessMap->getPath().c_str());
										ImGui::PopTextWrapPos();
										ImGui::Image(reinterpret_cast<void*>(static_cast<size_t>(metalnessMap->getRendererID())), ImVec2(384, 384));
										ImGui::EndTooltip();
									}
									if (ImGui::IsItemClicked())
										fileName = Game::get().openFile(imageExtFilter);
									if (!fileName.empty())
									{
										metalnessMap = Texture2D::create(fileName, DataType::UNSIGNED_BYTE, false, false, aiTextureType_METALNESS);
										materialInstance->set("u_MetalnessTexture", metalnessMap);
										materialInstance->set<float>("u_MetalnessTexToggle", 1.0f, ShaderDomain::Pixel);
										MeshExporter::updateMetalnessTexture(fileName, mesh, selectedMaterialIndex);

									}
								}
								ImGui::SameLine();
								if (ImGui::Checkbox("Use##MetalnessMap", &useMetalnessMap))
								{
									materialInstance->set<float>("u_MetalnessTexToggle", useMetalnessMap ? 1.0f : 0.0f, ShaderDomain::Pixel);
									MeshExporter::updateMetalnessTexture(useMetalnessMap && metalnessMap ? metalnessMap->getPath() :
										"", mesh, selectedMaterialIndex);

								}
								ImGui::SameLine();
								if (ImGui::SliderFloat("Value##MetalnessInput", &metalnessValue, 0.0f, 1.0f))
								{
									//MeshExporter::updateFactor(metalnessValue, mesh, selectedMaterialIndex, AI_MATKEY_FBX_MAYA_METALNESS_FACTOR);
									MeshExporter::updateFactor(metalnessValue, mesh, selectedMaterialIndex, "$mat.reflectivity");
									MeshExporter::updateFactor(metalnessValue, mesh, selectedMaterialIndex, "$clr.reflective");
									MeshExporter::updateFactor(metalnessValue, mesh, selectedMaterialIndex, "$raw.Reflectivity");
								}
							}
						}
						{
							// Roughness
							if (ImGui::CollapsingHeader("Roughness", nullptr, ImGuiTreeNodeFlags_DefaultOpen))
							{
								ImGui::PushStyleVar(ImGuiStyleVar_FramePadding, ImVec2(10, 10));
								auto& roughnessValue = materialInstance->get<float>("u_Roughness", ShaderDomain::Pixel);
								bool useRoughnessMap = materialInstance->get<float>("u_RoughnessTexToggle", ShaderDomain::Pixel);
								Ref<Texture2D> roughnessMap = materialInstance->tryGetResource<Texture2D>("u_RoughnessTexture");
								ImGui::Image(roughnessMap ? reinterpret_cast<void*>(static_cast<size_t>(roughnessMap->getRendererID())) :
									reinterpret_cast<void*>(static_cast<size_t>(mCheckerboardTex->getRendererID())), ImVec2(64, 64));
								ImGui::PopStyleVar();
								if (ImGui::IsItemHovered())
								{
									std::string fileName;
									std::string path = ui::getPayloadString("Image");
									if (!path.empty())
										fileName = path;
									if (roughnessMap)
									{
										ImGui::BeginTooltip();
										ImGui::PushTextWrapPos(ImGui::GetFontSize() * 35.0f);
										ImGui::TextUnformatted(roughnessMap->getPath().c_str());
										ImGui::PopTextWrapPos();
										ImGui::Image(reinterpret_cast<void*>(static_cast<size_t>(roughnessMap->getRendererID())), ImVec2(384, 384));
										ImGui::EndTooltip();
									}
									if (ImGui::IsItemClicked())
										fileName = Game::get().openFile(imageExtFilter);
									if (!fileName.empty())
									{
										roughnessMap = Texture2D::create(fileName, DataType::UNSIGNED_BYTE, false, false, aiTextureType_SHININESS);
										materialInstance->set("u_RoughnessTexture", roughnessMap);
										materialInstance->set<float>("u_RoughnessTexToggle", 1.0f, ShaderDomain::Pixel);
										MeshExporter::updateTexture(fileName, mesh, selectedMaterialIndex, _AI_MATKEY_TEXTURE_BASE, aiTextureType_SHININESS);

									}
								}
								ImGui::SameLine();
								if (ImGui::Checkbox("Use##RoughnessMap", &useRoughnessMap))
								{
									materialInstance->set<float>("u_RoughnessTexToggle", useRoughnessMap ? 1.0f : 0.0f, ShaderDomain::Pixel);
									MeshExporter::updateTexture(useRoughnessMap && roughnessMap ? roughnessMap->getPath() : "", mesh, selectedMaterialIndex, _AI_MATKEY_TEXTURE_BASE, aiTextureType_SHININESS);

								}
								ImGui::SameLine();
								if (ImGui::SliderFloat("Value##RoughnessInput", &roughnessValue, 0.0f, 1.0f))
								{
									MeshExporter::updateFactor(100.f * std::powf(1.f - roughnessValue, 2), mesh, selectedMaterialIndex, "$raw.Shininess");
									MeshExporter::updateFactor(100.f * std::powf(1.f - roughnessValue, 2), mesh, selectedMaterialIndex, "$mat.shininess");
								}
							}
						}
						{
							// Displacement
							if (ImGui::CollapsingHeader("Displacement -- More Expensive", nullptr, ImGuiTreeNodeFlags_DefaultOpen))
							{
								ImGui::PushStyleVar(ImGuiStyleVar_FramePadding, ImVec2(10, 10));
								bool useDisplacementMap = materialInstance->get<float>("u_DisplacementTexToggle", ShaderDomain::TessES);
								auto& DisplacementScale = materialInstance->get<float>("u_DisplacementScale", ShaderDomain::TessES);
								Ref<Texture2D> displacementMap = materialInstance->tryGetResource<Texture2D>("u_DisplacementTexture");
								ImGui::Image(displacementMap ? reinterpret_cast<void*>(static_cast<size_t>(displacementMap->getRendererID())) :
									reinterpret_cast<void*>(static_cast<size_t>(mCheckerboardTex->getRendererID())), ImVec2(64, 64));
								ImGui::PopStyleVar();
								if (ImGui::IsItemHovered())
								{
									std::string fileName;
									std::string path = ui::getPayloadString("Image");
									if (!path.empty())
										fileName = path;
									if (displacementMap)
									{
										ImGui::BeginTooltip();
										ImGui::PushTextWrapPos(ImGui::GetFontSize() * 35.0f);
										ImGui::TextUnformatted(displacementMap->getPath().c_str());
										ImGui::PopTextWrapPos();
										ImGui::Image(reinterpret_cast<void*>(size_t(displacementMap->getRendererID())), ImVec2(384, 384));
										ImGui::EndTooltip();
									}
									if (ImGui::IsItemClicked())
										fileName = Game::get().openFile(imageExtFilter);
									if (!fileName.empty())
									{
										displacementMap = Texture2D::create(fileName, DataType::UNSIGNED_BYTE, false, false, aiTextureType_DISPLACEMENT);
										materialInstance->set("u_DisplacementTexture", displacementMap);
										materialInstance->set<float>("u_DisplacementTexToggle", 1.0f, ShaderDomain::TessES);
										materialInstance->set<float>("u_DisplacementScale", 1.0f, ShaderDomain::TessES);
										materialInstance->set<bool>("u_Tesselate", true, ShaderDomain::TessCS);
										MeshExporter::updateTexture(displacementMap->getPath(), mesh, selectedMaterialIndex, _AI_MATKEY_TEXTURE_BASE, aiTextureType_DISPLACEMENT);

									}
								}
								ImGui::SameLine();
								if (ImGui::Checkbox("Use##DisplacementMap", &useDisplacementMap))
								{
									materialInstance->set<float>("u_DisplacementTexToggle", useDisplacementMap ? 1.0f : 0.0f, ShaderDomain::TessES);
									materialInstance->set<bool>("u_Tesselate", useDisplacementMap ? true : false, ShaderDomain::TessCS);
									MeshExporter::updateTexture(useDisplacementMap && displacementMap ? displacementMap->getPath() : "", mesh, selectedMaterialIndex, _AI_MATKEY_TEXTURE_BASE, aiTextureType_DISPLACEMENT);

								}
								ImGui::SameLine();
								if (ImGui::SliderFloat("Value##DisplacementInput", &DisplacementScale, -1000.f, 1000.f))
									MeshExporter::updateFactor(DisplacementScale, mesh, selectedMaterialIndex, AI_MATKEY_FBX_MAYA_STINGRAY_DISPLACEMENT_SCALING_FACTOR);

							}
						}
						{
							// height
							if (ImGui::CollapsingHeader("Height", nullptr, ImGuiTreeNodeFlags_DefaultOpen))
							{
								ImGui::PushStyleVar(ImGuiStyleVar_FramePadding, ImVec2(10, 10));
								bool useHeightMap = materialInstance->get<float>("u_HeightTexToggle", ShaderDomain::Pixel);
								auto& heightScale = materialInstance->get<float>("u_HeightScale", ShaderDomain::Pixel);
								Ref<Texture2D> heightMap = materialInstance->tryGetResource<Texture2D>("u_HeightTexture");
								ImGui::Image(heightMap ? reinterpret_cast<void*>(static_cast<size_t>(heightMap->getRendererID())) :
									reinterpret_cast<void*>(static_cast<size_t>(mCheckerboardTex->getRendererID())), ImVec2(64, 64));
								ImGui::PopStyleVar();
								if (ImGui::IsItemHovered())
								{
									std::string fileName;
									std::string path = ui::getPayloadString("Image");
									if (!path.empty())
										fileName = path;
									if (heightMap)
									{
										ImGui::BeginTooltip();
										ImGui::PushTextWrapPos(ImGui::GetFontSize() * 35.0f);
										ImGui::TextUnformatted(heightMap->getPath().c_str());
										ImGui::PopTextWrapPos();
										ImGui::Image(reinterpret_cast<void*>(size_t(heightMap->getRendererID())), ImVec2(384, 384));
										ImGui::EndTooltip();
									}
									if (ImGui::IsItemClicked())
										fileName = Game::get().openFile(imageExtFilter);
									if (!fileName.empty())
									{
										heightMap = Texture2D::create(fileName, DataType::UNSIGNED_BYTE, false, false, aiTextureType_DISPLACEMENT);
										materialInstance->set("u_HeightTexture", heightMap);
										materialInstance->set<float>("u_HeightTexToggle", 1.0f, ShaderDomain::Pixel);
										MeshExporter::updateTexture(heightMap->getPath(), mesh, selectedMaterialIndex, _AI_MATKEY_TEXTURE_BASE, aiTextureType_DISPLACEMENT);

									}
								}
								ImGui::SameLine();
								if (ImGui::Checkbox("Use##HeightMap", &useHeightMap))
								{
									materialInstance->set<float>("u_HeightTexToggle", useHeightMap ? 1.0f : 0.0f, ShaderDomain::Pixel);
									MeshExporter::updateTexture(useHeightMap && heightMap ? heightMap->getPath() : "", mesh, selectedMaterialIndex, _AI_MATKEY_TEXTURE_BASE, aiTextureType_HEIGHT);

								}
								ImGui::SameLine();
								ImGui::SliderFloat("Value##HeightInput", &heightScale, -0.05f, 0.05f);
								//MeshExporter::updateFactor(metalnessValue, mesh, selectedMaterialIndex, AI_MATKEY_FBX_MAYA_STINGRAY_);

							}
						}
					}

				}
			}
		}
		ImGui::End();//Materials

		if (ImGuiFileDialog::Instance()->Display("Asset Browser"))
		{
		}

		SceneRenderer::onImGuiRender();

		ImGui::End();


		GLint totalMemKb = 0;
		glGetIntegerv(GL_GPU_MEM_INFO_TOTAL_AVAILABLE_MEM_NVX, &totalMemKb);

		GLint curAvailMemKb = 0;
		glGetIntegerv(GL_GPU_MEM_INFO_CURRENT_AVAILABLE_MEM_NVX, &curAvailMemKb);

		constexpr ImGuiWindowFlags statWindowFlags = ImGuiWindowFlags_NoTitleBar | ImGuiWindowFlags_NoDocking;
		if (mStatsWindow)
		{
			ImGui::Begin("Renderer", nullptr, statWindowFlags);
			auto& caps = RendererAPI::getCapabilities();
			ImGui::Text("Vendor: %s", caps.Vendor.c_str());
			ImGui::Text("Renderer: %s", caps.Renderer.c_str());
			ImGui::Text("Version: %s", caps.Version.c_str());
			ImGui::Text("Max Anisotropy: %d", caps.MaxAnisotropy);
			ImGui::Text("Max Texture Units: %d", caps.MaxTextureUnits);
			ImGui::Text("Max Samples: %d", caps.MaxSamples);
			ImGui::Text("Max Patch Vertices: %d", caps.MaxPatchVertices);
			ImGui::Text("Memory Usage: %d MB", (totalMemKb - curAvailMemKb) / 1024);
			ImGui::Text("Available Memory: %d MB", curAvailMemKb / 1024);
			ImGui::Text("Frame Time: %.3fms", mTimeStep.getMilliseconds());
			ImGui::Text("FPS: %d\n", Game::get().getFPS());
			bool vsync = Game::get().getWindow().isVSync();
			if (ImGui::Checkbox("Vsync", &vsync))
				Game::get().getWindow().setVSync(vsync);
			ImGui::End();
		}


	}

	void EditorLayer::onEvent(Event& e)
	{
		if (mMouseRightInViewport || !mAllowImGuiMouse)
			mEditorCamera[mCurrentCamera]->onEvent(e);

		mEditorScene->onEvent(e);

		EventDispatcher dispatcher(e);
		dispatcher.dispatch<KeyPressedEvent>([this](Event& event)
			{ return onKeyPressedEvent(static_cast<KeyPressedEvent&>(event)); });  // NOLINT(cppcoreguidelines-pro-type-static-cast-downcast)
		dispatcher.dispatch<MouseButtonPressedEvent>([this](Event& event)
			{ return onMouseButtonPressed(static_cast<MouseButtonPressedEvent&>(event)); });  // NOLINT(cppcoreguidelines-pro-type-static-cast-downcast)
		dispatcher.dispatch<MouseButtonReleasedEvent>([this](Event& event)
			{ return onMouseButtonReleased(static_cast<MouseButtonReleasedEvent&>(event)); });  // NOLINT(cppcoreguidelines-pro-type-static-cast-downcast)
	}


	bool EditorLayer::onKeyPressedEvent(KeyPressedEvent& e)
	{
		if (Game::get().getWindow().isFocused())
		{
			switch (e.getKeyCode()) // NOLINT(clang-diagnostic-switch)
			{

			case KeyCode::Escape:
			{
				const auto& data = *static_cast<WindowData*>(glfwGetWindowUserPointer(Game::get().getWindow().getNativeWindow()));
				WindowCloseEvent event;
				data.EventCallback(event);
				break;
			}
			case KeyCode::F11:
			{
				Game::get().getWindow().toggleFullscreen();
				break;
			}
			case KeyCode::F10:
			{
				mFullScreenViewport = !mFullScreenViewport;
				setViewportFullScreen();
				break;
			}
			[[likely]] default: break;

			}
		}

		if (mViewportPanelFocused && mAllowImGuiMouse)
		{
			switch (e.getKeyCode()) // NOLINT(clang-diagnostic-switch-enum)
			{

			case KeyCode::Q: mGizmoType = -1;
				break;
			case KeyCode::W: mGizmoType = ImGuizmo::OPERATION::TRANSLATE;
				break;
			case KeyCode::E: mGizmoType = ImGuizmo::OPERATION::ROTATE;
				break;
			case KeyCode::R: mGizmoType = ImGuizmo::OPERATION::SCALE;
				break;
			case KeyCode::Delete:
				if (!mSelectionContext.empty())
				{
					const Entity selectedEntity = mSelectionContext[0].Entity;
					mEditorScene->destroyEntity(selectedEntity);
					mSelectionContext.clear();
					mEditorScene->setSelectedEntity({});
					mSceneHierarchyPanel->setSelected({});
				}
				break;
			[[likely]] default: break;
			}
		}

		if (Input::isKeyPressed(BEY_KEY_LEFT_CONTROL))
		{
			switch (e.getKeyCode())  // NOLINT(clang-diagnostic-switch-enum)
			{
			case KeyCode::G:
				// Toggle grid
				SceneRenderer::getOptions().ShowGrid = !SceneRenderer::getOptions().ShowGrid;
				break;
			case KeyCode::B:
				// Toggle bounding boxes 
				mUiShowBoundingBoxes = !mUiShowBoundingBoxes;
				showBoundingBoxes(mUiShowBoundingBoxes, mUiShowBoundingBoxesOnTop);
				break;
			case KeyCode::D:
				if (!mSelectionContext.empty())
				{
					const Entity selectedEntity = mSelectionContext[0].Entity;
					mEditorScene->duplicateEntity(selectedEntity);
				}
				break;
			[[likely]] default: break;
			}
		}

		return false;
	}

	void EditorLayer::intersectMouse(const float mouseX, const float mouseY)
	{

		auto [mx, my] = ImGui::GetMousePos();
		mx -= mViewportBounds[0].x;
		my -= mViewportBounds[0].y;

		const auto height = mViewportBounds[1].y - mViewportBounds[0].y;
		my = height - my;

		const auto hoveredID = SceneRenderer::getLightPixelIDAt({ mx, my });
		if (!mSelectionContext.empty())
			return;
		for (auto ent : mEditorScene->getAllEntitiesWith<PointLightComponent>())
		{
			Entity entity = { ent, mEditorScene.raw() };

			auto& lightComponent = entity.getComponent<PointLightComponent>();
			if (hoveredID == lightComponent.UUID)
			{
				mSelectionContext.push_back({ entity, &lightComponent, });
				return;
			}

		}

		for (auto ent : mEditorScene->getAllEntitiesWith<DirLightComponent>())
		{
			Entity entity = { ent, mEditorScene.raw() };
			auto& lightComponent = entity.getComponent<DirLightComponent>();
			if (hoveredID == lightComponent.UUID)
			{
				mSelectionContext.push_back({ entity, &lightComponent, });
				return;
			}

		}

		{
			auto meshEntities = mEditorScene->getAllEntitiesWith<MeshComponent>();
			for (auto ent : meshEntities)
			{

				Entity entity = { ent, mEditorScene.raw() };
				auto mesh = entity.getComponent<MeshComponent>().Mesh;
				if (!mesh)
					continue;
				auto [origin, direction] = castRay(mouseX, mouseY);

				auto& submeshes = mesh->getSubMeshes();
				for (uint32_t i = 0; i < submeshes.size(); i++)
				{

					auto& submesh = submeshes[i];
					Ray ray = {
						glm::inverse(entity.transform() * submesh.Transform) * glm::vec4(origin, 1.0f),
						glm::inverse(glm::mat3(entity.transform()) * glm::mat3(submesh.Transform)) * direction
					};

					float t;
					const bool intersects = ray.intersectsAABB(submesh.BoundingBox, t);
					if (intersects)
					{
						const auto& triangleCache = mesh->getTriangleCache(i);
						for (const auto& triangle : triangleCache)
						{
							if (ray.intersectsTriangle(triangle.V0.Position, triangle.V1.Position, triangle.V2.Position, t))
							{
								BEY_TRACE("INTERSECTION: {0}, t={1}", submesh.NodeName, t);
								mSelectionContext.push_back({ entity, &submesh, t });
								break;
							}
						}
					}
				}
			}
		}


	}



	bool EditorLayer::onMouseButtonPressed(MouseButtonPressedEvent& e)
	{
		if (e.getMouseButton() == BEY_MOUSE_BUTTON_RIGHT && mMouseHoveringViewport)
		{
			//mViewportPanelFocused = true;
		}
		if (e.getMouseButton() == BEY_MOUSE_BUTTON_LEFT && !Input::isMouseButtonPressed(BEY_MOUSE_BUTTON_RIGHT) && !ImGuizmo::IsOver() && mViewportPanelMouseOver
			&& !mEditorScene->getEntityMap().empty())
		{
			auto [mouseX, mouseY] = getMouseViewportSpace();
			if (mouseX > -1.0f && mouseX < 1.0f && mouseY > -1.0f && mouseY < 1.0f)
			{

				mSelectionContext.clear();
				mEditorScene->setSelectedEntity({});

				intersectMouse(mouseX, mouseY);


				const auto cameraPos = mEditorCamera[mCurrentCamera]->getViewMatrix()[3];
				for (auto& selected : mSelectionContext)
				{
					using T = std::decay_t<decltype(selected)>;
					if constexpr (std::is_same_v<T, Submesh>)
						selected.Distance = glm::distance(cameraPos, std::get<Submesh*>(selected.Selectable)->Transform[3]);
				}
				//std::ranges::sort(mSelectionContext, [](auto& a, auto& b) { return a.Distance > b.Distance; });

				if (!mSelectionContext.empty())
					onSelected(mSelectionContext[0]);

			}
		}
		return false;
	}



	bool EditorLayer::onMouseButtonReleased(MouseButtonReleasedEvent& e)
	{
		if (e.getMouseButton() == BEY_MOUSE_BUTTON_RIGHT && mMouseHoveringViewport)
		{

		}

		return false;
	}




	std::pair<glm::vec3, glm::vec3> EditorLayer::castRay(const float mx, const float my) const
	{
		const glm::vec4 mouseClipPos = { mx, my, -1.0f, 1.0f };

		const auto inverseProj = glm::inverse(mEditorCamera[mCurrentCamera]->getProjectionMatrix());
		const auto inverseView = glm::inverse(glm::mat3(mEditorCamera[mCurrentCamera]->getViewMatrix()));

		const glm::vec4 ray = inverseProj * mouseClipPos;
		glm::vec3 rayPos = mEditorCamera[mCurrentCamera]->getPosition();
		glm::vec3 rayDir = inverseView * glm::vec3(ray);

		return { rayPos, rayDir };
	}

	void EditorLayer::onSelected(const SelectedObject& selectionContext)
	{
		mSceneHierarchyPanel->setSelected(selectionContext.Entity);
		mEditorScene->setSelectedEntity(static_cast<entt::entity>(selectionContext.Entity));
	}

	void EditorLayer::onEntityDeleted(const Entity e)
	{
		if (mSelectionContext[0].Entity == e)
		{
			mSelectionContext.clear();
			mEditorScene->setSelectedEntity({});
		}
	}



	void EditorLayer::showBoundingBoxes(const bool show, const bool onTop)
	{
		BEY_PROFILE_FUNCTION();

		SceneRenderer::getOptions().ShowBoundingBoxes = show && !onTop;
		mDrawOnTopBoundingBoxes = show && onTop;
	}

	void EditorLayer::selectEntity(Entity entity)
	{
		BEY_PROFILE_FUNCTION();
		SelectedObject selection;
		if (entity.hasComponent<MeshComponent>())
		{
			auto mesh = entity.getComponent<MeshComponent>().Mesh;
			if (mesh)
				selection.Selectable = &mesh->getSubMeshes()[0];
		}
		selection.Entity = entity;
		mSelectionContext.clear();
		mSelectionContext.push_back(selection);

		mEditorScene->setSelectedEntity(entity);
	}


	Ray EditorLayer::castMouseRay() const
	{
		auto [mouseX, mouseY] = getMouseViewportSpace();
		if (mouseX > -1.0f && mouseX < 1.0f && mouseY > -1.0f && mouseY < 1.0f)
		{
			auto [origin, direction] = castRay(mouseX, mouseY);
			return Ray(origin, direction);
		}
		return Ray::zero();
	}

	bool EditorLayer::property(const std::string& name, bool& value)
	{
		ImGui::Text("%s", name.c_str());
		ImGui::NextColumn();
		ImGui::PushItemWidth(-1);

		const std::string id = "##" + name;
		const bool result = ImGui::Checkbox(id.c_str(), &value);

		ImGui::PopItemWidth();
		ImGui::NextColumn();

		return result;
	}

	void EditorLayer::property(const std::string& name, float& value, const float min, const float max, PropertyFlag flags)
	{
		ImGui::Text("%s", name.c_str());
		ImGui::NextColumn();
		ImGui::PushItemWidth(-1);

		const std::string id = "##" + name;
		ImGui::SliderFloat(id.c_str(), &value, min, max);

		ImGui::PopItemWidth();
		ImGui::NextColumn();
	}

	template<typename T> requires std::is_same_v<T, unsigned>
		void EditorLayer::property(const std::string& name, T& value, const char* const items[], const unsigned count) const
		{
			ImGui::Text("%s", name.c_str());
			ImGui::NextColumn();
			ImGui::PushItemWidth(-1);
			ImGuiStyle& style = ImGui::GetStyle();
			const float w = ImGui::CalcItemWidth();
			const float spacing = style.ItemInnerSpacing.x;
			const float buttonSz = ImGui::GetFrameHeight();
			ImGui::PushItemWidth(w - spacing * 2.0f - buttonSz * 2.0f);

			if (ImGui::BeginCombo(fmt::format("##{} custom combo", name).c_str(), items[unsigned(value)], ImGuiComboFlags_NoArrowButton))
			{
				for (unsigned int n = 0; n < count; n++)
				{
					const bool isSelected = (items[unsigned(value)] == items[n]);
					if (ImGui::Selectable(items[n], isSelected))
						value = static_cast<T>(n);
					if (isSelected)
						ImGui::SetItemDefaultFocus();
				}
				ImGui::EndCombo();
			}
			ImGui::SameLine(0, spacing);
			if (ImGui::ArrowButton(fmt::format("##l{}", name).c_str(), ImGuiDir_Left) && unsigned(value) > 0)
				value = static_cast<T>((unsigned)value - 1);
			ImGui::SameLine(0, spacing);
			if (ImGui::ArrowButton(fmt::format("##r{}", name).c_str(), ImGuiDir_Right) && unsigned(value) < count - 1)
				value = static_cast<T>((unsigned)value + 1);

			ImGui::SameLine(0, style.ItemInnerSpacing.x);

			ImGui::PopItemWidth();
			ImGui::NextColumn();

		}




		void EditorLayer::property(const std::string& name, glm::vec2& value, const PropertyFlag flags) const
		{
			property(name, value, -1.0f, 1.0f, flags);
		}

		void EditorLayer::property(const std::string& name, glm::vec2& value, const float min, const float max, PropertyFlag flags) const
		{
			ImGui::Text("%s", name.c_str());
			ImGui::NextColumn();
			ImGui::PushItemWidth(-1);

			const std::string id = "##" + name;
			ImGui::SliderFloat2(id.c_str(), glm::value_ptr(value), min, max);

			ImGui::PopItemWidth();
			ImGui::NextColumn();
		}

		void EditorLayer::property(const std::string& name, glm::vec3& value, const PropertyFlag flags) const
		{
			property(name, value, -1.0f, 1.0f, flags);
		}

		void EditorLayer::property(const std::string& name, glm::vec3& value, const float min, const float max, PropertyFlag flags) const
		{
			ImGui::Text("%s", name.c_str());
			ImGui::NextColumn();
			ImGui::PushItemWidth(-1);

			const std::string id = "##" + name;
			if (static_cast<int>(flags) & static_cast<int>(PropertyFlag::COLOR_PROPERTY))
				ImGui::ColorEdit3(id.c_str(), glm::value_ptr(value), ImGuiColorEditFlags_NoInputs);
			else
				ImGui::SliderFloat3(id.c_str(), glm::value_ptr(value), min, max);

			ImGui::PopItemWidth();
			ImGui::NextColumn();
		}

		void EditorLayer::property(const std::string& name, glm::vec4& value, const PropertyFlag flags) const
		{
			property(name, value, -1.0f, 1.0f, flags);
		}

		void EditorLayer::property(const std::string& name, glm::vec4& value, const float min, const float max, PropertyFlag flags) const
		{
			ImGui::Text("%s", name.c_str());
			ImGui::NextColumn();
			ImGui::PushItemWidth(-1);

			const std::string id = "##" + name;
			if (static_cast<int>(flags) & static_cast<int>(PropertyFlag::COLOR_PROPERTY))
				ImGui::ColorEdit4(id.c_str(), glm::value_ptr(value), ImGuiColorEditFlags_NoInputs);
			else
				ImGui::SliderFloat4(id.c_str(), glm::value_ptr(value), min, max);

			ImGui::PopItemWidth();
			ImGui::NextColumn();
		}


}
