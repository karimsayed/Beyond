#include <Beyond.h>
#include <Beyond/EntryPoint.h>

#include "EditorLayer.h"

#ifdef BEY_DEBUG
constexpr auto* title = "Beyond Engine (Debug) Forward +";
#else
constexpr auto* title = "Beyond Engine (Release) Forward +";
#endif

class BeyondGame final : public bey::Game
{
public:
	explicit BeyondGame(const bey::ApplicationProps& props)
		: Game(props)
	{
	}

	void onInit() override
	{
		BEY_PROFILE_FUNCTION();
		pushLayer(new bey::EditorLayer());
	}
};

bey::Game* bey::createGame()
{
	BEY_PROFILE_FUNCTION();
	return new BeyondGame({ title, 1600, 900 } );
}