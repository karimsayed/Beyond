#pragma once

#undef __gl_h_
#include <imgui.h>
#include "Beyond.h"


namespace bey {

	class EditorLayer final : public Layer
	{
	public:
		enum class PropertyFlag
		{
			NONE = 0, COLOR_PROPERTY = 1
		};


	public:
		EditorLayer();


		
		void onAttach() override;
		void onDetach() override;
		void onUpdate(TimeStep ts) override;

		void setViewportFullScreen();
		void openScene(const std::string& filepath);
		void onImGuiRender() override;
		void onEvent(Event& e) override;
		bool onKeyPressedEvent(KeyPressedEvent& e);
		void intersectMouse(float mouseX, float mouseY);
		bool onMouseButtonPressed(MouseButtonPressedEvent& e);
		bool onMouseButtonReleased(MouseButtonReleasedEvent& e);

		// ImGui UI helpers
		static bool property(const std::string& name, bool& value);
		static void property(const std::string& name, float& value, float min = -1.0f, float max = 1.0f, PropertyFlag flags = PropertyFlag::NONE);
		void property(const std::string& name, glm::vec2& value, PropertyFlag flags) const;
		void property(const std::string& name, glm::vec2& value, float min = -1.0f, float max = 1.0f, PropertyFlag flags = PropertyFlag::NONE) const;
		void property(const std::string& name, glm::vec3& value, PropertyFlag flags) const;
		void property(const std::string& name, glm::vec3& value, float min = -1.0f, float max = 1.0f, PropertyFlag flags = PropertyFlag::NONE) const;
		void property(const std::string& name, glm::vec4& value, PropertyFlag flags) const;
		void property(const std::string& name, glm::vec4& value, float min = -1.0f, float max = 1.0f, PropertyFlag flags = PropertyFlag::NONE) const;

		template<typename T> requires std::is_same_v<T, unsigned>
		void property(const std::string& name, T& value, const char* const items[], unsigned count) const;

		void showBoundingBoxes(bool show, bool onTop = false);
		void selectEntity(Entity entity);
	private:
		[[nodiscard]] std::pair<float, float> getMouseViewportSpace() const;
		[[nodiscard]] std::pair<glm::vec3, glm::vec3> castRay(float mx, float my) const;

		struct SelectedObject
		{
			Entity Entity;
			std::variant<Submesh*, DirLightComponent*, PointLightComponent*> Selectable;
			float Distance = 0.0f;
		};

		void onSelected(const SelectedObject& selectionContext);
		void onEntityDeleted(Entity e);
		[[nodiscard]] Ray castMouseRay() const;

	private:
		std::unique_ptr<SceneHierarchyPanel> mSceneHierarchyPanel;

		Ref<Scene> mActiveScene;
		Ref<Scene> mEditorScene;

		std::array<Ref<FlyCamera>, 2> mEditorCamera;
		unsigned int mCurrentCamera { 0 };

		
		// PBR params
		bool mRadiancePrefilter = false;

		float mEnvMapRotation = 0.0f;

		// Editor resources
		Ref<Texture2D> mCheckerboardTex;

		glm::vec2 mViewportBounds[2] {};
		ImVec2 mBackupViewportBounds[2] {};
		
		int mGizmoType = -1; // -1 = no gizmo
		float mSnapValue = 0.5f;
		bool mDrawOnTopBoundingBoxes = false;

		bool mUiShowBoundingBoxes = false;
		bool mUiShowBoundingBoxesOnTop = false;

		bool mMouseRightInViewport = false;
		bool mAllowImGuiMouse = true;
		bool mMouseHoveringViewport = false;
		bool mViewportPanelMouseOver = false;
		bool mViewportPanelFocused = false;
		ImGuiID mViewportDockID;
		ImGuiID mBackupViewportDockID;



	


		enum class SelectionMode : unsigned
		{
			ENTITY = 0, SUB_MESH = 1
			
		} mSelectionMode = SelectionMode::ENTITY;


		std::vector<SelectedObject> mSelectionContext;
		TimeStep mTimeStep;
		bool mStatsWindow {false};
		bool mRenderTargetsWindowFocused;
		bool mFullScreenViewport { false };
		//glm::mat4* mRelativeTransform = nullptr;
		//glm::mat4* mCurrentlySelectedTransform = nullptr;
	};

}
