Scene: Scene Name
Environment:
  AssetPath: assets\env\birchwood_4k.hdr
  Light:
    Direction: [-0.5, -0.5, 1]
    Radiance: [1, 1, 1]
    Multiplier: 1
Entities:
  - Entity: 0841038
    TagComponent:
      Tag: Sky Light
    TransformComponent:
      Position: [0, 0, 0]
      Rotation: [1, 0, 0, 0]
      Scale: [1, 1, 1]
    SkyLightComponent:
      EnvironmentAssetPath: assets\env\pink_sunrise_4k.hdr
      Intensity: 0.460000008
      Angle: 0
  - Entity: 2347593
    TagComponent:
      Tag: Test Entity
    TransformComponent:
      Position: [4.1358757, 3.15137386, 11.4047718]
      Rotation: [1, 0, 0, 0]
      Scale: [17.9037628, 17.9037628, 17.9037628]
    MeshComponent:
      AssetPath: assets\models\m1911\m1911.fbx
  - Entity: 2347594
    TagComponent:
      Tag: Test Entity
    TransformComponent:
      Position: [1.12979317, -1.90734863e-06, -0.268640995]
      Rotation: [1, 0, 0, 0]
      Scale: [1, 1, 1]
    MeshComponent:
      AssetPath: assets\meshes\TestScene.fbx
  - Entity: 434728
    TagComponent:
      Tag: Camera
    TransformComponent:
      Position: [0, 14.75, 79.75]
      Rotation: [0.995602965, -0.0936739072, 0, 0]
      Scale: [1, 0.999999821, 0.999999821]
    CameraComponent:
      Camera: some camera data...
      Primary: true
  - Entity: 1289165
    TagComponent:
      Tag: Sphere
    TransformComponent:
      Position: [0, 21.9805069, -1.64006281]
      Rotation: [1, 0, 0, 0]
      Scale: [0.100000024, 0.100000024, 0.100000024]
    MeshComponent:
      AssetPath: assets\meshes\Sphere1m.fbx