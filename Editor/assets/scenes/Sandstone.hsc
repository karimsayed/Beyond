Scene: Scene Name
Environment:
  AssetPath: assets\env\birchwood_4k.hdr
Entities:
  - Entity: 3551211736
    TagComponent:
      Tag: new entity
    TransformComponent:
      Position: [-2.67893124, 25.9083843, 236.206528]
      Rotation: [0.718125761, 0, -0.695913315, 0]
      Scale: [1.00000024, 1, 1.00000024]
    MeshComponent:
      AssetPath: C:\Users\karim\Desktop\Beyond\Editor\assets\meshes\SandStone\Rock_Sandstone_uk5sccn\uk5sccn_lod0.fbx
  - Entity: 1634776283
    TagComponent:
      Tag: new entity
    TransformComponent:
      Position: [44.636158, 24.8438644, 166.189972]
      Rotation: [0.718125761, 0, -0.695913315, 0]
      Scale: [1.00000024, 1, 1.00000024]
    MeshComponent:
      AssetPath: C:\Users\karim\Desktop\Beyond\Editor\assets\meshes\SandStone\Rock_Sandstone_uk5sccn\uk5sccn_lod0.fbx
  - Entity: 2695541524
    TagComponent:
      Tag: new entity
    TransformComponent:
      Position: [1.17803764, 0.464668274, 156.61908]
      Rotation: [0.718125761, 0, -0.695913315, 0]
      Scale: [1.00000024, 1, 1.00000024]
    MeshComponent:
      AssetPath: C:\Users\karim\Desktop\Beyond\Editor\assets\meshes\SandStone\Rock_Sandstone_uk5sccn\uk5sccn_lod0.fbx
  - Entity: 4262329557
    TagComponent:
      Tag: new entity
    TransformComponent:
      Position: [67.1590042, -5.81360817, 109.449181]
      Rotation: [-0.464009374, 0, 0.885830283, 0]
      Scale: [0.669999838, 0.670000017, 0.669999838]
    MeshComponent:
      AssetPath: C:\Users\karim\Desktop\Beyond\Editor\assets\meshes\SandStone\Rock_Sandstone_ulsjcazga\ulsjcazga_lod0.fbx
  - Entity: 1805259918
    TagComponent:
      Tag: new entity
    TransformComponent:
      Position: [-30.2469654, -1.12305641, 121.908257]
      Rotation: [0.956969082, -0.0894768462, -0.24346143, -0.130117863]
      Scale: [0.219999865, 0.219999909, 0.219999924]
    MeshComponent:
      AssetPath: C:\Users\karim\Desktop\Beyond\Editor\assets\meshes\SandStone\Rock_Sandstone_uk4seg3\uk4seg3_lod0.fbx
  - Entity: 1423589420
    TagComponent:
      Tag: new entity
    TransformComponent:
      Position: [44.9122162, -2.74495506, 186.562943]
      Rotation: [0.179467097, 0, 0.983763993, 0]
      Scale: [0.386399925, 0.386399955, 0.386399895]
    MeshComponent:
      AssetPath: C:\Users\karim\Desktop\Beyond\Editor\assets\meshes\SandStone\Nature_Rock_uk5ofde\uk5ofde_lod0.fbx
  - Entity: 2085833533
    TagComponent:
      Tag: new entity
    TransformComponent:
      Position: [49.7550583, -1.36909211, 64.2562561]
      Rotation: [0.914277375, 0.17164585, -0.331640899, 0.156999841]
      Scale: [0.219999835, 0.21999985, 0.219999894]
    MeshComponent:
      AssetPath: C:\Users\karim\Desktop\Beyond\Editor\assets\meshes\SandStone\Rock_Sandstone_uk4seg3\uk4seg3_lod0.fbx
  - Entity: 2273514591
    TagComponent:
      Tag: new entity
    TransformComponent:
      Position: [26.6706753, 25.9083843, 271.332977]
      Rotation: [0.718125761, 0, -0.695913315, 0]
      Scale: [1.00000024, 1, 1.00000024]
    MeshComponent:
      AssetPath: C:\Users\karim\Desktop\Beyond\Editor\assets\meshes\SandStone\Rock_Sandstone_uk5sccn\uk5sccn_lod0.fbx
  - Entity: 669156530
    TagComponent:
      Tag: new entity
    TransformComponent:
      Position: [0, 0, 181.545944]
      Rotation: [1, 0, 0, 0]
      Scale: [1, 1, 1]
    MeshComponent:
      AssetPath: C:\Users\karim\Desktop\Beyond\Editor\assets\meshes\SandStone\Rock_Sandstone_uk5sccn\uk5sccn_lod0.fbx
  - Entity: 1382267984
    TagComponent:
      Tag: new entity
    TransformComponent:
      Position: [-20.1204128, 0, 157.117783]
      Rotation: [-0.165381446, 0, 0.986229658, 0]
      Scale: [1.00000012, 1, 1.00000012]
    MeshComponent:
      AssetPath: C:\Users\karim\Desktop\Beyond\Editor\assets\meshes\SandStone\Rock_Sandstone_uk5sccn\uk5sccn_lod0.fbx
  - Entity: 871483327
    TagComponent:
      Tag: new entity
    TransformComponent:
      Position: [-30.1659718, -2.74495506, 205.278656]
      Rotation: [0.969330251, 0, 0.245761812, 0]
      Scale: [0.386399925, 0.386399955, 0.386399925]
    MeshComponent:
      AssetPath: C:\Users\karim\Desktop\Beyond\Editor\assets\meshes\SandStone\Nature_Rock_uk5ofde\uk5ofde_lod0.fbx
  - Entity: 666801623
    TagComponent:
      Tag: new entity
    TransformComponent:
      Position: [26.1654186, -6.74578714, 197.33049]
      Rotation: [1, 0, 0, 0]
      Scale: [0.379999995, 0.379999995, 0.379999995]
    MeshComponent:
      AssetPath: C:\Users\karim\Desktop\Beyond\Editor\assets\meshes\SandStone\Rock_Sandstone_uk4tej2\uk4tej2_lod0.fbx
  - Entity: 2787366594
    TagComponent:
      Tag: new entity
    TransformComponent:
      Position: [3.27656841, -6.74578714, 133.933945]
      Rotation: [0.533980489, -0.137662753, 0.829494059, 0.0886193439]
      Scale: [0.379999936, 0.379999936, 0.379999936]
    MeshComponent:
      AssetPath: C:\Users\karim\Desktop\Beyond\Editor\assets\meshes\SandStone\Rock_Sandstone_uk4tej2\uk4tej2_lod0.fbx
  - Entity: 1306133420
    TagComponent:
      Tag: new entity
    TransformComponent:
      Position: [21.0565548, -2.74495506, 232.003693]
      Rotation: [0.794686913, 0, 0.607019484, 0]
      Scale: [0.386399925, 0.386399955, 0.386399925]
    MeshComponent:
      AssetPath: C:\Users\karim\Desktop\Beyond\Editor\assets\meshes\SandStone\Nature_Rock_uk5ofde\uk5ofde_lod0.fbx
  - Entity: 647941704
    TagComponent:
      Tag: new entity
    TransformComponent:
      Position: [65.2621155, -2.74495506, 139.364075]
      Rotation: [0.598447263, 0, 0.801162243, 0]
      Scale: [0.386399895, 0.386399955, 0.386399895]
    MeshComponent:
      AssetPath: C:\Users\karim\Desktop\Beyond\Editor\assets\meshes\SandStone\Nature_Rock_uk5ofde\uk5ofde_lod0.fbx
  - Entity: 377911196
    TagComponent:
      Tag: new entity
    TransformComponent:
      Position: [-67.3058929, -6.26069355, 147.035629]
      Rotation: [1, 0, 0, 0]
      Scale: [0.219999999, 0.219999999, 0.219999999]
    MeshComponent:
      AssetPath: C:\Users\karim\Desktop\Beyond\Editor\assets\meshes\SandStone\Rock_Sandstone_uk4seg3\uk4seg3_lod0.fbx
  - Entity: 3037776839
    TagComponent:
      Tag: new entity
    TransformComponent:
      Position: [-92.0556946, 35.1596069, 186.982635]
      Rotation: [0.895152152, -0.349997759, -0.198119551, -0.192230999]
      Scale: [0.219999865, 0.21999982, 0.219999924]
    MeshComponent:
      AssetPath: C:\Users\karim\Desktop\Beyond\Editor\assets\meshes\SandStone\Rock_Sandstone_uk4seg3\uk4seg3_lod0.fbx
  - Entity: 188450588
    TagComponent:
      Tag: new entity
    TransformComponent:
      Position: [-44.5304222, -6.26069355, 170.121536]
      Rotation: [1, 0, 0, 0]
      Scale: [0.219999999, 0.219999999, 0.219999999]
    MeshComponent:
      AssetPath: C:\Users\karim\Desktop\Beyond\Editor\assets\meshes\SandStone\Rock_Sandstone_uk4seg3\uk4seg3_lod0.fbx
  - Entity: 2372900033
    TagComponent:
      Tag: new entity
    TransformComponent:
      Position: [-78.3263016, -9.42790127, 127.670639]
      Rotation: [0.661385655, -0.00420795707, 0.736464381, 0.142026693]
      Scale: [0.21999988, 0.219999865, 0.219999909]
    MeshComponent:
      AssetPath: C:\Users\karim\Desktop\Beyond\Editor\assets\meshes\SandStone\Rock_Sandstone_uk4seg3\uk4seg3_lod0.fbx
  - Entity: 2180615181
    TagComponent:
      Tag: Plane
    TransformComponent:
      Position: [62.3459244, 0, 93.5241776]
      Rotation: [1, 0, 0, 0]
      Scale: [0.430000007, 0.430000007, 0.430000007]
    MeshComponent:
      AssetPath: assets/primitives/Plane_100x100.fbx
  - Entity: 8132
    TagComponent:
      Tag: Point Light
    TransformComponent:
      Position: [-41.8357887, 13.0005426, 15.4264793]
      Rotation: [1, 1.69102491e-16, 0, 0]
      Scale: [0.99999994, 0.99999994, 1]
    PointLightComponent:
      Radiance: [1, 0, 0]
      Intensity: 355.5
      CastShadows: false
      SoftShadows: true
      FarPlane: 25
      LightSize: 0.5
  - Entity: 2007569
    TagComponent:
      Tag: Point Light
    TransformComponent:
      Position: [-41.5466042, 28.2926445, 15.3687248]
      Rotation: [1, 1.69102491e-16, 0, 0]
      Scale: [0.99999994, 0.99999994, 1]
    PointLightComponent:
      Radiance: [0.690196097, 0, 1]
      Intensity: 355.5
      CastShadows: false
      SoftShadows: true
      FarPlane: 25
      LightSize: 0.5
  - Entity: 6978287
    TagComponent:
      Tag: Point Light
    TransformComponent:
      Position: [43.6846962, 13.2182474, 15.3687248]
      Rotation: [1, 1.69102491e-16, 0, 0]
      Scale: [0.99999994, 0.99999994, 1]
    PointLightComponent:
      Radiance: [0, 1, 1]
      Intensity: 355.5
      CastShadows: false
      SoftShadows: true
      FarPlane: 25
      LightSize: 0.5
  - Entity: 957663
    TagComponent:
      Tag: Point Light
    TransformComponent:
      Position: [43.7148476, 14.1338463, -15.2327557]
      Rotation: [1, 1.69102491e-16, 0, 0]
      Scale: [0.99999994, 0.99999994, 1]
    PointLightComponent:
      Radiance: [0, 1, 0.0274509806]
      Intensity: 355.5
      CastShadows: false
      SoftShadows: true
      FarPlane: 25
      LightSize: 0.5
  - Entity: 272547
    TagComponent:
      Tag: Point Light
    TransformComponent:
      Position: [43.218277, 25.9598255, -16.903038]
      Rotation: [1, 1.69102491e-16, 0, 0]
      Scale: [0.99999994, 0.99999994, 1]
    PointLightComponent:
      Radiance: [0, 0.718146682, 0.526825845]
      Intensity: 355.5
      CastShadows: false
      SoftShadows: true
      FarPlane: 25
      LightSize: 0.5
  - Entity: 554321
    TagComponent:
      Tag: Point Light
    TransformComponent:
      Position: [-21.8922462, 12.8008204, 3.97182035]
      Rotation: [1, 1.69102491e-16, 0, 0]
      Scale: [0.99999994, 0.99999994, 1]
    PointLightComponent:
      Radiance: [1, 0.545098066, 0]
      Intensity: 355.5
      CastShadows: false
      SoftShadows: true
      FarPlane: 25
      LightSize: 0.5
  - Entity: 9694607
    TagComponent:
      Tag: Point Light
    TransformComponent:
      Position: [-41.5466042, 28.2926445, -15.7151394]
      Rotation: [1, 1.69102491e-16, 0, 0]
      Scale: [0.99999994, 0.99999994, 1]
    PointLightComponent:
      Radiance: [1, 0.880308688, 0]
      Intensity: 355.5
      CastShadows: false
      SoftShadows: true
      FarPlane: 25
      LightSize: 0.5
  - Entity: 773688
    TagComponent:
      Tag: Point Light
    TransformComponent:
      Position: [20.4332829, 11.4064159, -6.79038239]
      Rotation: [1, 1.69102491e-16, 0, 0]
      Scale: [0.99999994, 0.99999994, 1]
    PointLightComponent:
      Radiance: [0, 0.239215687, 1]
      Intensity: 355.5
      CastShadows: false
      SoftShadows: true
      FarPlane: 25
      LightSize: 0.5
  - Entity: 4772389
    TagComponent:
      Tag: Point Light
    TransformComponent:
      Position: [-41.8357887, 13.0005426, -16.057106]
      Rotation: [1, 1.69102491e-16, 0, 0]
      Scale: [0.99999994, 0.99999994, 1]
    PointLightComponent:
      Radiance: [1, 1, 1]
      Intensity: 355.5
      CastShadows: true
      SoftShadows: true
      FarPlane: 25
      LightSize: 0.5
  - Entity: 1134347
    TagComponent:
      Tag: Directional Light
    TransformComponent:
      Position: [-14.885766, 21.3358288, 2.33239102]
      Rotation: [-0.492082059, -0.0570405573, -0.182319894, 0.84932977]
      Scale: [0.999969602, 0.999966443, 0.999976277]
    DirectionalLightComponent:
      Radiance: [1, 1, 1]
      Intensity: 12.1000004
      CastShadows: true
      SoftShadows: true
      LightSize: 0.5
  - Entity: 378215
    TagComponent:
      Tag: Sponza
    TransformComponent:
      Position: [0, 0, 0]
      Rotation: [1, 0, 0, 0]
      Scale: [0.999999106, 1, 0.999999106]
    MeshComponent:
      AssetPath: assets\meshes\My New Sponza\Sponza.fbx
  - Entity: 34728
    TagComponent:
      Tag: Camera
    TransformComponent:
      Position: [0, 14.75, 79.75]
      Rotation: [0.995602965, -0.0936739072, 0, 0]
      Scale: [1, 0.999999821, 0.999999821]
    CameraComponent:
      Camera: some camera data...
      Primary: true
  - Entity: 1038
    TagComponent:
      Tag: Sky Light
    TransformComponent:
      Position: [0, 0, 0]
      Rotation: [1, 0, 0, 0]
      Scale: [1, 1, 1]
    SkyLightComponent:
      EnvironmentAssetPath: assets\env\birchwood_4k.hdr
      Intensity: 0.460000008
      Angle: 0
  - Entity: 152283
    TagComponent:
      Tag: Point Light
    TransformComponent:
      Position: [43.218277, 25.9598255, 16.1392117]
      Rotation: [1, 1.69102491e-16, 0, 0]
      Scale: [0.99999994, 0.99999994, 1]
    PointLightComponent:
      Radiance: [0.294117659, 1, 0.196078435]
      Intensity: 355.5
      CastShadows: false
      SoftShadows: true
      FarPlane: 25
      LightSize: 0.5