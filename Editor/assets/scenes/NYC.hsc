Scene: Scene Name
Environment:
  AssetPath: C:\Users\karim\Desktop\Beyond\Editor\assets\env\venice_dawn_1_4k.hdr
Entities:
  - Entity: 496704152
    TagComponent:
      Tag: Point Light
    TransformComponent:
      Position: [31.790926, 8.47852325, -19.3313599]
      Rotation: [1, 0, 0, 0]
      Scale: [1, 1, 1]
    PointLightComponent:
      Radiance: [1, 0, 0]
      Intensity: 213.300003
      CastShadows: true
      SoftShadows: true
      FarPlane: 25
      LightSize: 0.5
  - Entity: 3231749355
    TagComponent:
      Tag: Point Light
    TransformComponent:
      Position: [50.2076225, 3.92801142, 61.3852844]
      Rotation: [1, 0, 0, 0]
      Scale: [1, 1, 1]
    PointLightComponent:
      Radiance: [1, 0.999995768, 0.999989986]
      Intensity: 55.0999985
      CastShadows: false
      SoftShadows: true
      FarPlane: 25
      LightSize: 0.5
  - Entity: 2900650830
    TagComponent:
      Tag: Point Light
    TransformComponent:
      Position: [67.2365952, 3.92801142, 73.413681]
      Rotation: [1, 0, 0, 0]
      Scale: [1, 1, 1]
    PointLightComponent:
      Radiance: [1, 0.999995768, 0.999989986]
      Intensity: 55.0999985
      CastShadows: false
      SoftShadows: true
      FarPlane: 25
      LightSize: 0.5
  - Entity: 2821631358
    TagComponent:
      Tag: Point Light
    TransformComponent:
      Position: [64.3212738, 3.43706155, 67.6443787]
      Rotation: [1, 0, 0, 0]
      Scale: [1, 1, 1]
    PointLightComponent:
      Radiance: [1, 0.999995768, 0.999989986]
      Intensity: 55.0999985
      CastShadows: false
      SoftShadows: true
      FarPlane: 25
      LightSize: 0.5
  - Entity: 4056609901
    TagComponent:
      Tag: Point Light
    TransformComponent:
      Position: [57.3334694, 3.92801142, 75.3034821]
      Rotation: [1, 0, 0, 0]
      Scale: [1, 1, 1]
    PointLightComponent:
      Radiance: [1, 0.999995768, 0.999989986]
      Intensity: 55.0999985
      CastShadows: false
      SoftShadows: true
      FarPlane: 25
      LightSize: 0.5
  - Entity: 158437150
    TagComponent:
      Tag: Point Light
    TransformComponent:
      Position: [68.4255371, 3.43706155, 47.752758]
      Rotation: [1, 0, 0, 0]
      Scale: [1, 1, 1]
    PointLightComponent:
      Radiance: [1, 0.999995768, 0.999989986]
      Intensity: 38.5
      CastShadows: false
      SoftShadows: true
      FarPlane: 25
      LightSize: 0.5
  - Entity: 3324762753
    TagComponent:
      Tag: Point Light
    TransformComponent:
      Position: [47.9575081, 3.43706155, 47.400219]
      Rotation: [1, 0, 0, 0]
      Scale: [1, 1, 1]
    PointLightComponent:
      Radiance: [1, 0.999995768, 0.999989986]
      Intensity: 185.600006
      CastShadows: false
      SoftShadows: true
      FarPlane: 25
      LightSize: 0.5
  - Entity: 3621480310
    TagComponent:
      Tag: Point Light
    TransformComponent:
      Position: [35.069519, 10.3788214, 20.1737289]
      Rotation: [1, 0, 0, 0]
      Scale: [1, 1, 1]
    PointLightComponent:
      Radiance: [1, 0.577377498, 0]
      Intensity: 185.600006
      CastShadows: false
      SoftShadows: true
      FarPlane: 25
      LightSize: 0.5
  - Entity: 1497422241
    TagComponent:
      Tag: Point Light
    TransformComponent:
      Position: [64.3212738, 3.43706155, 55.2253494]
      Rotation: [1, 0, 0, 0]
      Scale: [1, 1, 1]
    PointLightComponent:
      Radiance: [1, 0.999995768, 0.999989986]
      Intensity: 185.600006
      CastShadows: false
      SoftShadows: true
      FarPlane: 25
      LightSize: 0.5
  - Entity: 1779197120
    TagComponent:
      Tag: Point Light
    TransformComponent:
      Position: [35.069519, 10.3788214, -7.95614624]
      Rotation: [1, 0, 0, 0]
      Scale: [1, 1, 1]
    PointLightComponent:
      Radiance: [1, 0.577377498, 0]
      Intensity: 185.600006
      CastShadows: false
      SoftShadows: true
      FarPlane: 25
      LightSize: 0.5
  - Entity: 417709656
    TagComponent:
      Tag: Point Light
    TransformComponent:
      Position: [37.7922821, 10.3788214, -7.95614624]
      Rotation: [1, 0, 0, 0]
      Scale: [1, 1, 1]
    PointLightComponent:
      Radiance: [1, 0.577377498, 0]
      Intensity: 185.600006
      CastShadows: false
      SoftShadows: true
      FarPlane: 25
      LightSize: 0.5
  - Entity: 3549875823
    TagComponent:
      Tag: Point Light
    TransformComponent:
      Position: [39.5998878, 10.3788214, -14.8359985]
      Rotation: [1, 0, 0, 0]
      Scale: [1, 1, 1]
    PointLightComponent:
      Radiance: [1, 0.577377498, 0]
      Intensity: 185.600006
      CastShadows: false
      SoftShadows: true
      FarPlane: 25
      LightSize: 0.5
  - Entity: 3151559192
    TagComponent:
      Tag: Point Light
    TransformComponent:
      Position: [36.9483795, 10.3788214, -16.3881798]
      Rotation: [1, 0, 0, 0]
      Scale: [1, 1, 1]
    PointLightComponent:
      Radiance: [1, 0.577377498, 0]
      Intensity: 185.600006
      CastShadows: false
      SoftShadows: true
      FarPlane: 25
      LightSize: 0.5
  - Entity: 1347139906
    TagComponent:
      Tag: Point Light
    TransformComponent:
      Position: [45.6518745, 10.3788214, -18.2573013]
      Rotation: [1, 0, 0, 0]
      Scale: [1, 1, 1]
    PointLightComponent:
      Radiance: [1, 0.577377498, 0]
      Intensity: 185.600006
      CastShadows: false
      SoftShadows: true
      FarPlane: 25
      LightSize: 0.5
  - Entity: 1372726102
    TagComponent:
      Tag: Point Light
    TransformComponent:
      Position: [45.6518745, 10.3788214, -15.3149986]
      Rotation: [1, 0, 0, 0]
      Scale: [1, 1, 1]
    PointLightComponent:
      Radiance: [1, 0.577377498, 0]
      Intensity: 185.600006
      CastShadows: false
      SoftShadows: true
      FarPlane: 25
      LightSize: 0.5
  - Entity: 683041276
    TagComponent:
      Tag: Point Light
    TransformComponent:
      Position: [76.2615967, 10.3788214, -15.3149986]
      Rotation: [1, 0, 0, 0]
      Scale: [1, 1, 1]
    PointLightComponent:
      Radiance: [1, 0.577377498, 0]
      Intensity: 185.600006
      CastShadows: false
      SoftShadows: true
      FarPlane: 25
      LightSize: 0.5
  - Entity: 712189943
    TagComponent:
      Tag: Point Light
    TransformComponent:
      Position: [76.2615967, 10.3788214, -18.1868782]
      Rotation: [1, 0, 0, 0]
      Scale: [1, 1, 1]
    PointLightComponent:
      Radiance: [1, 0.577377498, 0]
      Intensity: 185.600006
      CastShadows: false
      SoftShadows: true
      FarPlane: 25
      LightSize: 0.5
  - Entity: 511319985
    TagComponent:
      Tag: Point Light
    TransformComponent:
      Position: [106.544525, 10.3788214, -18.1868782]
      Rotation: [1, 0, 0, 0]
      Scale: [1, 1, 1]
    PointLightComponent:
      Radiance: [1, 0.577377498, 0]
      Intensity: 185.600006
      CastShadows: false
      SoftShadows: true
      FarPlane: 25
      LightSize: 0.5
  - Entity: 985707897
    TagComponent:
      Tag: Point Light
    TransformComponent:
      Position: [106.544525, 10.3788214, -15.305809]
      Rotation: [1, 0, 0, 0]
      Scale: [1, 1, 1]
    PointLightComponent:
      Radiance: [1, 0.577377498, 0]
      Intensity: 185.600006
      CastShadows: false
      SoftShadows: true
      FarPlane: 25
      LightSize: 0.5
  - Entity: 1545314523
    TagComponent:
      Tag: Point Light
    TransformComponent:
      Position: [113.679535, 10.3788214, -13.7684059]
      Rotation: [1, 0, 0, 0]
      Scale: [1, 1, 1]
    PointLightComponent:
      Radiance: [1, 0.577377498, 0]
      Intensity: 185.600006
      CastShadows: false
      SoftShadows: true
      FarPlane: 25
      LightSize: 0.5
  - Entity: 2356904667
    TagComponent:
      Tag: Point Light
    TransformComponent:
      Position: [114.49028, 10.3788214, -16.8818798]
      Rotation: [1, 0, 0, 0]
      Scale: [1, 1, 1]
    PointLightComponent:
      Radiance: [1, 0.577377498, 0]
      Intensity: 185.600006
      CastShadows: false
      SoftShadows: true
      FarPlane: 25
      LightSize: 0.5
  - Entity: 4196920427
    TagComponent:
      Tag: Point Light
    TransformComponent:
      Position: [116.769638, 10.3788214, -8.02162647]
      Rotation: [1, 0, 0, 0]
      Scale: [1, 1, 1]
    PointLightComponent:
      Radiance: [1, 0.577377498, 0]
      Intensity: 185.600006
      CastShadows: false
      SoftShadows: true
      FarPlane: 25
      LightSize: 0.5
  - Entity: 2880176542
    TagComponent:
      Tag: Point Light
    TransformComponent:
      Position: [114.112373, 10.3788214, -8.02162647]
      Rotation: [1, 0, 0, 0]
      Scale: [1, 1, 1]
    PointLightComponent:
      Radiance: [1, 0.577377498, 0]
      Intensity: 185.600006
      CastShadows: false
      SoftShadows: true
      FarPlane: 25
      LightSize: 0.5
  - Entity: 3291679841
    TagComponent:
      Tag: Point Light
    TransformComponent:
      Position: [114.112373, 10.3788214, 20.1963501]
      Rotation: [1, 0, 0, 0]
      Scale: [1, 1, 1]
    PointLightComponent:
      Radiance: [1, 0.577377498, 0]
      Intensity: 185.600006
      CastShadows: false
      SoftShadows: true
      FarPlane: 25
      LightSize: 0.5
  - Entity: 2399075336
    TagComponent:
      Tag: Point Light
    TransformComponent:
      Position: [116.732689, 10.3788214, 20.1963501]
      Rotation: [1, 0, 0, 0]
      Scale: [1, 1, 1]
    PointLightComponent:
      Radiance: [1, 0.577377498, 0]
      Intensity: 185.600006
      CastShadows: false
      SoftShadows: true
      FarPlane: 25
      LightSize: 0.5
  - Entity: 2690955795
    TagComponent:
      Tag: Point Light
    TransformComponent:
      Position: [116.732689, 10.3788214, 48.5978661]
      Rotation: [1, 0, 0, 0]
      Scale: [1, 1, 1]
    PointLightComponent:
      Radiance: [1, 0.577377498, 0]
      Intensity: 185.600006
      CastShadows: false
      SoftShadows: true
      FarPlane: 25
      LightSize: 0.5
  - Entity: 3589811672
    TagComponent:
      Tag: Point Light
    TransformComponent:
      Position: [114.173363, 10.3788214, 48.5978661]
      Rotation: [1, 0, 0, 0]
      Scale: [1, 1, 1]
    PointLightComponent:
      Radiance: [1, 0.577377498, 0]
      Intensity: 185.600006
      CastShadows: false
      SoftShadows: true
      FarPlane: 25
      LightSize: 0.5
  - Entity: 4221036523
    TagComponent:
      Tag: Point Light
    TransformComponent:
      Position: [114.173363, 10.3788214, 76.5252838]
      Rotation: [1, 0, 0, 0]
      Scale: [1, 1, 1]
    PointLightComponent:
      Radiance: [1, 0.577377498, 0]
      Intensity: 185.600006
      CastShadows: false
      SoftShadows: true
      FarPlane: 25
      LightSize: 0.5
  - Entity: 2975279440
    TagComponent:
      Tag: Point Light
    TransformComponent:
      Position: [116.766472, 10.3788214, 76.5252838]
      Rotation: [1, 0, 0, 0]
      Scale: [1, 1, 1]
    PointLightComponent:
      Radiance: [1, 0.577377498, 0]
      Intensity: 185.600006
      CastShadows: false
      SoftShadows: true
      FarPlane: 25
      LightSize: 0.5
  - Entity: 2925923347
    TagComponent:
      Tag: Point Light
    TransformComponent:
      Position: [106.745811, 10.3788214, 84.7875748]
      Rotation: [1, 0, 0, 0]
      Scale: [1, 1, 1]
    PointLightComponent:
      Radiance: [1, 0.577377498, 0]
      Intensity: 185.600006
      CastShadows: false
      SoftShadows: true
      FarPlane: 25
      LightSize: 0.5
  - Entity: 3724384718
    TagComponent:
      Tag: Point Light
    TransformComponent:
      Position: [38.1763954, 10.3788214, 20.1737289]
      Rotation: [1, 0, 0, 0]
      Scale: [1, 1, 1]
    PointLightComponent:
      Radiance: [1, 0.577377498, 0]
      Intensity: 185.600006
      CastShadows: false
      SoftShadows: true
      FarPlane: 25
      LightSize: 0.5
  - Entity: 1924761250
    TagComponent:
      Tag: Point Light
    TransformComponent:
      Position: [76.1982727, 10.3788214, 87.3308029]
      Rotation: [1, 0, 0, 0]
      Scale: [1, 1, 1]
    PointLightComponent:
      Radiance: [1, 0.577380538, 0]
      Intensity: 185.600006
      CastShadows: false
      SoftShadows: true
      FarPlane: 25
      LightSize: 0.5
  - Entity: 439708048
    TagComponent:
      Tag: Point Light
    TransformComponent:
      Position: [76.1982727, 10.3788214, 84.2398376]
      Rotation: [1, 0, 0, 0]
      Scale: [1, 1, 1]
    PointLightComponent:
      Radiance: [1, 0.577380538, 0]
      Intensity: 185.600006
      CastShadows: false
      SoftShadows: true
      FarPlane: 25
      LightSize: 0.5
  - Entity: 2187778088
    TagComponent:
      Tag: Point Light
    TransformComponent:
      Position: [45.4161339, 10.3788214, 84.2398376]
      Rotation: [1, 0, 0, 0]
      Scale: [1, 1, 1]
    PointLightComponent:
      Radiance: [1, 0.577380538, 0]
      Intensity: 185.600006
      CastShadows: false
      SoftShadows: true
      FarPlane: 25
      LightSize: 0.5
  - Entity: 391195024
    TagComponent:
      Tag: Point Light
    TransformComponent:
      Position: [45.4161339, 10.3788214, 87.2419128]
      Rotation: [1, 0, 0, 0]
      Scale: [1, 1, 1]
    PointLightComponent:
      Radiance: [1, 0.577380538, 0]
      Intensity: 185.600006
      CastShadows: false
      SoftShadows: true
      FarPlane: 25
      LightSize: 0.5
  - Entity: 2535336382
    TagComponent:
      Tag: Point Light
    TransformComponent:
      Position: [37.6373253, 10.3788214, 85.9771652]
      Rotation: [1, 0, 0, 0]
      Scale: [1, 1, 1]
    PointLightComponent:
      Radiance: [1, 0.577380538, 0]
      Intensity: 185.600006
      CastShadows: false
      SoftShadows: true
      FarPlane: 25
      LightSize: 0.5
  - Entity: 2395109601
    TagComponent:
      Tag: Point Light
    TransformComponent:
      Position: [38.3187408, 10.3788214, 82.6470947]
      Rotation: [1, 0, 0, 0]
      Scale: [1, 1, 1]
    PointLightComponent:
      Radiance: [1, 0.577380538, 0]
      Intensity: 185.600006
      CastShadows: false
      SoftShadows: true
      FarPlane: 25
      LightSize: 0.5
  - Entity: 1898348647
    TagComponent:
      Tag: Point Light
    TransformComponent:
      Position: [37.7212334, 10.3788214, 76.9692917]
      Rotation: [1, 0, 0, 0]
      Scale: [1, 1, 1]
    PointLightComponent:
      Radiance: [1, 0.577380538, 0]
      Intensity: 185.600006
      CastShadows: false
      SoftShadows: true
      FarPlane: 25
      LightSize: 0.5
  - Entity: 1380253771
    TagComponent:
      Tag: Point Light
    TransformComponent:
      Position: [35.2841148, 10.3788214, 76.9692917]
      Rotation: [1, 0, 0, 0]
      Scale: [1, 1, 1]
    PointLightComponent:
      Radiance: [1, 0.577380538, 0]
      Intensity: 185.600006
      CastShadows: false
      SoftShadows: true
      FarPlane: 25
      LightSize: 0.5
  - Entity: 3217593775
    TagComponent:
      Tag: Point Light
    TransformComponent:
      Position: [35.2841148, 10.3788214, 48.8987846]
      Rotation: [1, 0, 0, 0]
      Scale: [1, 1, 1]
    PointLightComponent:
      Radiance: [1, 0.577380538, 0]
      Intensity: 185.600006
      CastShadows: false
      SoftShadows: true
      FarPlane: 25
      LightSize: 0.5
  - Entity: 1165065322
    TagComponent:
      Tag: new entity
    TransformComponent:
      Position: [83.9829407, 0, 0]
      Rotation: [1, 0, 0, 0]
      Scale: [1, 1, 1]
    MeshComponent:
      AssetPath: C:\Users\karim\Desktop\Beyond\Editor\assets\meshes\NYC\nyc.fbx
  - Entity: 3573461753
    TagComponent:
      Tag: Point Light
    TransformComponent:
      Position: [106.745811, 10.3788214, 87.3308029]
      Rotation: [1, 0, 0, 0]
      Scale: [1, 1, 1]
    PointLightComponent:
      Radiance: [1, 0.577377498, 0]
      Intensity: 185.600006
      CastShadows: false
      SoftShadows: true
      FarPlane: 25
      LightSize: 0.5
  - Entity: 180650149
    TagComponent:
      Tag: Sky Light
    TransformComponent:
      Position: [0, 0, 0]
      Rotation: [1, 0, 0, 0]
      Scale: [1, 1, 1]
    SkyLightComponent:
      EnvironmentAssetPath: C:\Users\karim\Desktop\Beyond\Editor\assets\env\venice_dawn_1_4k.hdr
      Intensity: 0.150000006
      Angle: 0
  - Entity: 1192642427
    TagComponent:
      Tag: Point Light
    TransformComponent:
      Position: [38.1583481, 10.3788214, 48.8987846]
      Rotation: [1, 0, 0, 0]
      Scale: [1, 1, 1]
    PointLightComponent:
      Radiance: [1, 0.577380538, 0]
      Intensity: 185.600006
      CastShadows: false
      SoftShadows: true
      FarPlane: 25
      LightSize: 0.5