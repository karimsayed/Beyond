Scene: Scene Name
Environment:
  AssetPath: assets\env\birchwood_4k.hdr
Entities:
  - Entity: 152283
    TagComponent:
      Tag: Point Light
    TransformComponent:
      Position: [43.218277, 25.9598255, 16.1392117]
      Rotation: [1, 1.69102491e-16, 0, 0]
      Scale: [0.99999994, 0.99999994, 1]
    PointLightComponent:
      Radiance: [0.294117659, 1, 0.196078435]
      Intensity: 355.5
      CastShadows: false
      SoftShadows: true
      FarPlane: 25
      LightSize: 0.5
  - Entity: 1038
    TagComponent:
      Tag: Sky Light
    TransformComponent:
      Position: [0, 0, 0]
      Rotation: [1, 0, 0, 0]
      Scale: [1, 1, 1]
    SkyLightComponent:
      EnvironmentAssetPath: assets\env\birchwood_4k.hdr
      Intensity: 0.460000008
      Angle: 0
  - Entity: 9434728
    TagComponent:
      Tag: Camera
    TransformComponent:
      Position: [0, 14.75, 79.75]
      Rotation: [0.995602965, -0.0936739072, 0, 0]
      Scale: [1, 0.999999821, 0.999999821]
    CameraComponent:
      Camera: some camera data...
      Primary: true
  - Entity: 637815
    TagComponent:
      Tag: Sponza
    TransformComponent:
      Position: [0, 0, 0]
      Rotation: [1, 0, 0, 0]
      Scale: [0.999999106, 1, 0.999999106]
    MeshComponent:
      AssetPath: assets\meshes\SponzaCombined\Sponza.fbx
  - Entity: 1134347
    TagComponent:
      Tag: Directional Light
    TransformComponent:
      Position: [-14.885766, 21.3358288, 2.33239102]
      Rotation: [-0.492082059, -0.0570405573, -0.182319909, 0.84932977]
      Scale: [0.999969721, 0.999966443, 0.999976277]
    DirectionalLightComponent:
      Radiance: [1, 1, 1]
      Intensity: 12.1000004
      CastShadows: true
      SoftShadows: true
      LightSize: 0.5
  - Entity: 2472389
    TagComponent:
      Tag: Point Light
    TransformComponent:
      Position: [-41.8357887, 13.0005426, -16.057106]
      Rotation: [1, 1.69102491e-16, 0, 0]
      Scale: [0.99999994, 0.99999994, 1]
    PointLightComponent:
      Radiance: [1, 1, 1]
      Intensity: 355.5
      CastShadows: true
      SoftShadows: true
      FarPlane: 25
      LightSize: 0.5
  - Entity: 1073688
    TagComponent:
      Tag: Point Light
    TransformComponent:
      Position: [20.4332829, 11.4064159, -6.79038239]
      Rotation: [1, 1.69102491e-16, 0, 0]
      Scale: [0.99999994, 0.99999994, 1]
    PointLightComponent:
      Radiance: [0, 0.239215687, 1]
      Intensity: 355.5
      CastShadows: false
      SoftShadows: true
      FarPlane: 25
      LightSize: 0.5
  - Entity: 94607
    TagComponent:
      Tag: Point Light
    TransformComponent:
      Position: [-41.5466042, 28.2926445, -15.7151394]
      Rotation: [1, 1.69102491e-16, 0, 0]
      Scale: [0.99999994, 0.99999994, 1]
    PointLightComponent:
      Radiance: [1, 0.880308688, 0]
      Intensity: 355.5
      CastShadows: false
      SoftShadows: true
      FarPlane: 25
      LightSize: 0.5
  - Entity: 554321
    TagComponent:
      Tag: Point Light
    TransformComponent:
      Position: [-21.8922462, 12.8008204, 3.97182035]
      Rotation: [1, 1.69102491e-16, 0, 0]
      Scale: [0.99999994, 0.99999994, 1]
    PointLightComponent:
      Radiance: [1, 0.545098066, 0]
      Intensity: 355.5
      CastShadows: false
      SoftShadows: true
      FarPlane: 25
      LightSize: 0.5
  - Entity: 672547
    TagComponent:
      Tag: Point Light
    TransformComponent:
      Position: [43.218277, 25.9598255, -16.903038]
      Rotation: [1, 1.69102491e-16, 0, 0]
      Scale: [0.99999994, 0.99999994, 1]
    PointLightComponent:
      Radiance: [0, 0.718146682, 0.526825845]
      Intensity: 355.5
      CastShadows: false
      SoftShadows: true
      FarPlane: 25
      LightSize: 0.5
  - Entity: 307663
    TagComponent:
      Tag: Point Light
    TransformComponent:
      Position: [43.7148476, 14.1338463, -15.2327557]
      Rotation: [1, 1.69102491e-16, 0, 0]
      Scale: [0.99999994, 0.99999994, 1]
    PointLightComponent:
      Radiance: [0, 1, 0.0274509806]
      Intensity: 355.5
      CastShadows: false
      SoftShadows: true
      FarPlane: 25
      LightSize: 0.5
  - Entity: 697287
    TagComponent:
      Tag: Point Light
    TransformComponent:
      Position: [43.6846962, 13.2182474, 15.3687248]
      Rotation: [1, 1.69102491e-16, 0, 0]
      Scale: [0.99999994, 0.99999994, 1]
    PointLightComponent:
      Radiance: [0, 1, 1]
      Intensity: 355.5
      CastShadows: false
      SoftShadows: true
      FarPlane: 25
      LightSize: 0.5
  - Entity: 7569
    TagComponent:
      Tag: Point Light
    TransformComponent:
      Position: [-41.5466042, 28.2926445, 15.3687248]
      Rotation: [1, 1.69102491e-16, 0, 0]
      Scale: [0.99999994, 0.99999994, 1]
    PointLightComponent:
      Radiance: [0.690196097, 0, 1]
      Intensity: 355.5
      CastShadows: false
      SoftShadows: true
      FarPlane: 25
      LightSize: 0.5
  - Entity: 9838132
    TagComponent:
      Tag: Point Light
    TransformComponent:
      Position: [-41.8357887, 13.0005426, 15.4264793]
      Rotation: [1, 1.69102491e-16, 0, 0]
      Scale: [0.99999994, 0.99999994, 1]
    PointLightComponent:
      Radiance: [1, 0, 0]
      Intensity: 355.5
      CastShadows: false
      SoftShadows: true
      FarPlane: 25
      LightSize: 0.5