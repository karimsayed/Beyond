Scene: Scene Name
Environment:
  AssetPath: assets\env\birchwood_4k.hdr
  Light:
    Direction: [-0.838999987, -1, -0.375]
    Radiance: [1, 0, 0]
    Multiplier: 5
Entities:
  - Entity: 9434728
    TagComponent:
      Tag: Camera
    TransformComponent:
      Position: [0, 14.75, 79.75]
      Rotation: [0.995602965, -0.0936739072, 0, 0]
      Scale: [1, 0.999999821, 0.999999821]
    CameraComponent:
      Camera: some camera data...
      Primary: true
  - Entity: 1289165777996378215
    TagComponent:
      Tag: Sphere
    TransformComponent:
      Position: [0, -0.966204643, -1.64006281]
      Rotation: [1, 0, 0, 0]
      Scale: [3.00487876, 2.47365046, 2.47365046]
    MeshComponent:
      AssetPath: assets\meshes\my room.fbx