Scene: Scene Name
Environment:
  AssetPath: assets\env\birchwood_4k.hdr
Entities:
  - Entity: 4772389
    TagComponent:
      Tag: Point Light
    TransformComponent:
      Position: [-15.5998497, 13.0005426, 1.70560741]
      Rotation: [1, 1.69102491e-16, 0, 0]
      Scale: [0.99999994, 0.99999994, 1]
    PointLightComponent:
      Radiance: [1, 1, 1]
      Intensity: 2.4000001
      CastShadows: true
      SoftShadows: true
      FarPlane: 124.400002
      LightSize: 0.5
  - Entity: 1134347
    TagComponent:
      Tag: Directional Light
    TransformComponent:
      Position: [-14.885766, 21.3358288, 2.33239102]
      Rotation: [-0.492082059, -0.0570405573, -0.182319894, 0.84932977]
      Scale: [0.999969602, 0.999966443, 0.999976277]
    DirectionalLightComponent:
      Radiance: [1, 1, 1]
      Intensity: 1.2
      SoftShadows: true
      LightSize: 0.5
  - Entity: 378215
    TagComponent:
      Tag: Sponza
    TransformComponent:
      Position: [0, 0, 0]
      Rotation: [1, 0, 0, 0]
      Scale: [0.999999106, 1, 0.999999106]
    MeshComponent:
      AssetPath: assets\meshes\My New Sponza\Sponza.fbx
  - Entity: 34728
    TagComponent:
      Tag: Camera
    TransformComponent:
      Position: [0, 14.75, 79.75]
      Rotation: [0.995602965, -0.0936739072, 0, 0]
      Scale: [1, 0.999999821, 0.999999821]
    CameraComponent:
      Camera: some camera data...
      Primary: true
  - Entity: 1038
    TagComponent:
      Tag: Sky Light
    TransformComponent:
      Position: [0, 0, 0]
      Rotation: [1, 0, 0, 0]
      Scale: [1, 1, 1]
    SkyLightComponent:
      EnvironmentAssetPath: assets\env\birchwood_4k.hdr
      Intensity: 0.460000008
      Angle: 0