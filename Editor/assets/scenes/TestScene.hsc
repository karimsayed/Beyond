Scene: Scene Name
Environment:
  AssetPath: assets\env\birchwood_4k.hdr
Entities:
  - Entity: 5571055
    TagComponent:
      Tag: Directional Light
    TransformComponent:
      Position: [0, 0, 0]
      Rotation: [-0.136509165, -0.484946549, -0.40411526, 0.763467729]
      Scale: [0.999974251, 0.999979138, 0.999969244]
    DirectionalLightComponent:
      Radiance: [1, 1, 1]
      Intensity: 35.5
      CastShadows: true
      SoftShadows: true
      LightSize: 14.6000004
  - Entity: 717233
    TagComponent:
      Tag: Sky Light
    TransformComponent:
      Position: [0, 0, 0]
      Rotation: [1, 0, 0, 0]
      Scale: [1, 1, 1]
    SkyLightComponent:
      EnvironmentAssetPath: assets\env\birchwood_4k.hdr
      Intensity: 1.05999994
      Angle: 0
  - Entity: 128916
    TagComponent:
      Tag: Sphere
    TransformComponent:
      Position: [16.145977, 28.2194023, 0]
      Rotation: [1, 0, 0, 0]
      Scale: [8.02996826, 8.02999973, 8.02997017]
    MeshComponent:
      AssetPath: assets\meshes\Sphere1m.fbx
  - Entity: 517886
    TagComponent:
      Tag: Camera
    TransformComponent:
      Position: [0, 14.75, 79.75]
      Rotation: [0.995602965, -0.0936739072, 0, 0]
      Scale: [6.5, 0.999999821, 0.999999821]
    CameraComponent:
      Camera: some camera data...
      Primary: true
  - Entity: 9055
    TagComponent:
      Tag: Test Entity
    TransformComponent:
      Position: [0, 0, 0]
      Rotation: [1, 0, 0, 0]
      Scale: [1, 1, 1]
    MeshComponent:
      AssetPath: assets\meshes\TestScene.fbx
  - Entity: 909545
    TagComponent:
      Tag: Barn
    TransformComponent:
      Position: [4.1358757, 3.15137291, -40.8446846]
      Rotation: [1, 0, 0, 0]
      Scale: [0.0286460202, 0.0286460202, 0.0286460202]
    MeshComponent:
      AssetPath: assets\meshes\Barn\Rbarn15.FBX