#version 460 core



uniform sampler2D u_NormalScene;
uniform sampler2D G_Depth;
uniform sampler2D G_Normal;
uniform sampler2D G_MetalnessRoughness;

uniform float u_DepthTolerance = 0.1;
uniform float u_FadeIn = 0.4;
uniform float u_DistanceFade = 0.6;
uniform int u_MaxSteps;


struct PointLight {
	vec3 Position;
	float Multiplier;
	vec3 Radiance;
	float NearPlane;
	float FarPlane;
	bool CastsShadows;
};

layout(std140, binding = 0) uniform light_data {
	float u_BloomThreshold;
	float u_LightSize;
	float u_MaxShadowDistance;
	float u_ShadowFade;

	float u_CascadeTransitionFade;
	float u_IBLContribution;
	vec2 u_PixelSize;

	vec3 u_CameraPosition;
	float u_padding1;

	vec4 u_CascadeSplits;
	mat4 u_LightView;
    mat4 u_ProjectionMatrix;
    mat4 u_ProjectionMatrixInv;
	mat4 u_ViewMatrix;
    mat4 u_ViewMatrixInv;
	vec4 u_ProjectionParams; //x = near, y = far
	vec2 u_ViewportSize;
	vec2 u_Padding2;
    mat4 u_ViewProjectionMatrix;
	int u_PointLightsCount;
    PointLight pointLightsUniform[1024];
};


layout(location=0,index=0) out vec4 outColor;

noperspective in vec2 vs_TexCoords;



vec2 viewToScreen(vec3 viewPos, out float w) {
	vec4 projected = u_ProjectionMatrix * vec4(viewPos, 1.0);
	projected.xy /= projected.w;
	projected.xy = projected.xy * 0.5 + 0.5;
	w = projected.w;
	return projected.xy;
}


void main() {


	vec3 normal = texture(G_Normal, vs_TexCoords).rgb;

	if (normal == vec3(0)) discard;

	float depthTex = texture(G_Depth, vs_TexCoords).r;


	vec4 viewPos = u_ProjectionMatrixInv * vec4(vs_TexCoords * 2.0 - 1.0, depthTex * 2.0 - 1.0, 1.0);

	vec3 vertex = viewPos.xyz / viewPos.w;

	vec3 viewDir = normalize(vertex);
	
	vec2 metalnessRoughness = texture(G_MetalnessRoughness, vs_TexCoords).rg;


	vec3 rayDir = normalize(reflect(normalize(viewDir), normalize(normal)));


	if (dot(rayDir, normal) < 0.001) {
		outColor = texture(u_NormalScene, vs_TexCoords);
		return;
	}
	//rayDir = normalize(viewDir - normal * dot(normal,viewDir) * 2.0);

	////////////////

	// make ray length and clip it against the near plane (don't want to trace beyond visible)
	float rayLength = (vertex.z + rayDir.z * u_ProjectionParams.y) > -u_ProjectionParams.x ? (-u_ProjectionParams.x - vertex.z) / rayDir.z : u_ProjectionParams.y;
	vec3 rayEnd = vertex + rayDir * rayLength;

	float wBegin;
	vec2 vpLineBegin = viewToScreen(vertex, wBegin);
	float wEnd;
	vec2 vpLineEnd = viewToScreen(rayEnd, wEnd);
	vec2 vpLineDir = vpLineEnd - vpLineBegin;

	// we need to interpolate w along the ray, to generate perspective correct reflections
	wBegin = 1.0 / wBegin;
	wEnd = 1.0 / wEnd;

	float zBegin = vertex.z * wBegin;
	float zEnd = rayEnd.z * wEnd;

	vec2 lineBegin = vpLineBegin / u_PixelSize;
	vec2 lineDir = vpLineDir / u_PixelSize;
	float zDir = zEnd - zBegin;
	float wDir = wEnd - wBegin;

	// clip the line to the viewport edges

	float scaleMaxX = min(1.0, 0.99 * (1.0 - vpLineBegin.x) / max(1e-5, vpLineDir.x));
	float scaleMaxY = min(1.0, 0.99 * (1.0 - vpLineBegin.y) / max(1e-5, vpLineDir.y));
	float scaleMinX = min(1.0, 0.99 * vpLineBegin.x / max(1e-5, -vpLineDir.x));
	float scaleMinY = min(1.0, 0.99 * vpLineBegin.y / max(1e-5, -vpLineDir.y));
	float lineClip = min(scaleMaxX, scaleMaxY) * min(scaleMinX, scaleMinY);
	lineDir *= lineClip;
	zDir *= lineClip;
	wDir *= lineClip;

	// clip z and w advance to line advance
	vec2 lineAdvance = normalize(lineDir); // down to pixel
	float stepSize = length(lineAdvance) / length(lineDir);
	float zAdvance = zDir * stepSize; // adapt z advance to line advance
	float wAdvance = wDir * stepSize; // adapt w advance to line advance

	// make line advance faster if direction is closer to pixel edges (this avoids sampling the same pixel twice)
	float advanceAngleAdj = 1.0 / max(abs(lineAdvance.x), abs(lineAdvance.y));
	lineAdvance *= advanceAngleAdj; // adapt z advance to line advance
	zAdvance *= advanceAngleAdj;
	wAdvance *= advanceAngleAdj;

	vec4 pos = vec4(lineBegin, zBegin, wBegin);
	float zFrom = pos.z / pos.w;
	float zTo = zFrom;
	float depth;

	bool found = false;



	float stepsTaken = 0;

	for (; stepsTaken < u_MaxSteps; stepsTaken++) 
	{

		pos += vec4(lineAdvance, zAdvance, wAdvance);
		// convert to linear depth

		depth = texture(G_Depth, pos.xy * u_PixelSize).r * 2.0 - 1.0;

#ifdef USE_ORTHOGONAL_PROJECTION
		depth = ((depth + (u_ProjectionParams.y + u_ProjectionParams.x) / (u_ProjectionParams.y - u_ProjectionParams.x)) * (u_ProjectionParams.y - u_ProjectionParams.x)) / 2.0;
#else
		depth = 2.0 * u_ProjectionParams.x * u_ProjectionParams.y / (u_ProjectionParams.y + u_ProjectionParams.x - depth * (u_ProjectionParams.y - u_ProjectionParams.x));
#endif
		depth = -depth;

		zFrom = zTo;
		zTo = pos.z / pos.w;

		if (depth > zTo) {
			// if depth was surpassed
			if (depth <= max(zTo, zFrom) + u_DepthTolerance) {
				// check the depth tolerance
				found = true;
			}
			break;
		}

	} 

	if (found) {

		float marginBlend = 1.0;

		vec2 margin = vec2((u_ViewportSize.x + u_ViewportSize.y) * 0.5 * 0.05); 
		if (any(bvec4(lessThan(pos.xy, -margin), greaterThan(pos.xy, u_ViewportSize + margin)))) {
			// clip outside screen + margin
			outColor = texture(u_NormalScene, vs_TexCoords);
			return;
		}

		{
			//blend fading out towards external margin
			vec2 marginGrad = mix(pos.xy - u_ViewportSize, -pos.xy, lessThan(pos.xy, vec2(0.0)));
			marginBlend = 1.0 - smoothstep(0.0, margin.x, max(marginGrad.x, marginGrad.y));
		}

		vec2 finalPos;
		float grad;
		grad = stepsTaken / float(u_MaxSteps);
		float initial_fade = u_FadeIn == 0.0 ? 1.0 : pow(clamp(grad, 0.0, 1.0), u_FadeIn);
		float fade = pow(clamp(1.0 - grad, 0.0, 1.0), u_DistanceFade) * initial_fade;
		finalPos = pos.xy;




		outColor = mix(texture(u_NormalScene, vs_TexCoords), texture(u_NormalScene, finalPos * u_PixelSize, mix(0.0, 2.5, metalnessRoughness.g)), max(metalnessRoughness.r, 1 - metalnessRoughness.g) * fade * marginBlend);
	}
	else
		outColor = texture(u_NormalScene, vs_TexCoords);

}
