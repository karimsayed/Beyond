#version 460 core

layout (location = 0) out vec4 FragColor;

in vec2 v_TexCoord;

uniform sampler2D u_Position;
uniform sampler2D u_Normal;
uniform sampler2D u_texNoise;

uniform vec3 u_samples[8];

uniform int u_kernelSize = 8;
uniform float u_radius = 0.5;
uniform float u_bias = 0.5;

// tile noise texture over screen based on screen dimensions divided by noise size
uniform vec2 u_NoiseScale;


struct PointLight {
	vec3 Position;
	float Multiplier;
	vec3 Radiance;
	float NearPlane;
	float FarPlane;
	bool CastsShadows;
};

layout(std140, binding = 0) uniform light_data {
	float u_BloomThreshold;
	float u_LightSize;
	float u_MaxShadowDistance;
	float u_ShadowFade;

	float u_CascadeTransitionFade;
	float u_IBLContribution;
	vec2 u_PixelSize;

	vec3 u_CameraPosition;
	float u_padding1;

	vec4 u_CascadeSplits;
	mat4 u_LightView;
    mat4 u_ProjectionMatrix;
    mat4 u_ProjectionMatrixInv;
	mat4 u_ViewMatrix;
    mat4 u_ViewMatrixInv;
	vec4 u_ProjectionParams; //x = near, y = far
	vec2 u_ViewportSize;
	vec2 u_Padding2;
    mat4 u_ViewProjectionMatrix;
	int u_PointLightsCount;
    PointLight pointLightsUniform[1024];
};

void main()
{
    // get input for SSAO algorithm
    const vec3 fragPos = texture(u_Position, v_TexCoord).xyz;
	vec3 normalMatrix = texture(u_Normal, v_TexCoord).xyz;
    		
    vec3 randomVec = normalize(texture(u_texNoise, v_TexCoord * u_NoiseScale).xyz);
    // create TBN change-of-basis matrix: from tangent-space to view-space
    vec3 tangent = normalize(randomVec - normalMatrix * dot(randomVec, normalMatrix));
    vec3 bitangent = cross(normalMatrix, tangent);
    mat3 TBN = mat3(tangent, bitangent, normalMatrix);
    // iterate over the sample kernel and calculate occlusion factor
    float occlusion = 0.0;
    for(int i = 0; i < u_kernelSize; ++i)
    {
        // get sample position
        vec3 samplePos = TBN * u_samples[i]; // from tangent to view-space
        samplePos = fragPos + samplePos * u_radius; 
        
        // project sample position (to sample texture) (to get position on screen/texture)
        vec4 offset = vec4(samplePos, 1.0);
        offset = u_ProjectionMatrix * offset; // from view to clip-space
        offset.xyz /= offset.w; // perspective divide
        offset.xyz = offset.xyz * 0.5 + 0.5; // transform to range 0.0 - 1.0
        
        // get sample depth
        float sampleDepth = (textureLod(u_Position, offset.xy, 2)).z; // get depth value of kernel sample
        
        // range check & accumulate
        float rangeCheck = smoothstep(0.0, 1.0, u_radius / abs(fragPos.z - sampleDepth));
        occlusion += (sampleDepth >= samplePos.z + u_bias ? 1.0 : 0.0) * rangeCheck;           
    }
    
    FragColor = vec4(vec3(1.0 - (occlusion / u_kernelSize)), 1);
}
