#version 460 core

layout(std140, binding = 1) uniform light_matrices {
	mat4 ShadowMatrices[600];
};


layout (location = 0) in vec3 aPos;


struct PointLight {
	vec3 Position;
	float Multiplier;
	vec3 Radiance;
	float NearPlane;
	float FarPlane;
	bool CastsShadows;
};

layout(std140, binding = 0) uniform light_data {
	float u_BloomThreshold;
	float u_LightSize;
	float u_MaxShadowDistance;
	float u_ShadowFade;

	float u_CascadeTransitionFade;
	float u_IBLContribution;
	vec2 u_PixelSize;

	vec3 u_CameraPosition;
	float u_padding1;

	vec4 u_CascadeSplits;
	mat4 u_LightView;
    mat4 u_ProjectionMatrix;
    mat4 u_ProjectionMatrixInv;
	mat4 u_ViewMatrix;
    mat4 u_ViewMatrixInv;
	vec4 u_ProjectionParams; //x = near, y = far
	vec2 u_ViewportSize;
	vec2 u_Padding2;
    mat4 u_ViewProjectionMatrix;
	int u_PointLightsCount;
    PointLight pointLightsUniform[1024];
};


uniform mat4 u_Transform;
uniform int u_LayerIndex;

out vec3 WorldPos;



void main(void) 
{

    vec4 Pos4 = vec4(aPos, 1.0);
    gl_Position = ShadowMatrices[u_LayerIndex] * u_Transform * Pos4;
    WorldPos = (u_Transform * Pos4).xyz;    

}
