// Copyright Epic Games, Inc. All Rights Reserved.
#version 450 core
#define MAX_MIP_BATCH_SIZE 4
#define GROUP_TILE_SIZE 8

shared float SharedFurthestDeviceZ[GROUP_TILE_SIZE * GROUP_TILE_SIZE];
shared float SharedClosestDeviceZ[GROUP_TILE_SIZE * GROUP_TILE_SIZE];
#define COMPILER_GLSL 1


#define DIM_MIP_LEVEL_COUNT 4
#define DIM_FURTHEST 1




uniform vec4 DispatchThreadIdToBufferUV;
uniform vec2 InvSize;
uniform vec2 InputViewportMaxBound;

layout(binding = 0) uniform sampler2D ParentTextureMip;
layout(binding = 1, r32f) uniform writeonly image2D FurthestHZBOutput_0;
layout(binding = 2, r32f) uniform writeonly image2D FurthestHZBOutput_1;
layout(binding = 3, r32f) uniform writeonly image2D FurthestHZBOutput_2;
layout(binding = 4, r32f) uniform writeonly image2D FurthestHZBOutput_3;

vec4 Gather4(sampler2D Texture, vec2 BufferUV)
#if COMPILER_GLSL || FEATURE_LEVEL < FEATURE_LEVEL_SM5
{
	vec2 UV[4];
	
	// min(..., InputViewportMaxBound) because we don't want to sample outside of the viewport
	// when the view size has odd dimensions on X/Y axis.
	UV[0] = min(BufferUV + vec2(-0.25f, -0.25f) * InvSize, InputViewportMaxBound);
	UV[1] = min(BufferUV + vec2( 0.25f, -0.25f) * InvSize, InputViewportMaxBound);
	UV[2] = min(BufferUV + vec2(-0.25f,  0.25f) * InvSize, InputViewportMaxBound);
	UV[3] = min(BufferUV + vec2( 0.25f,  0.25f) * InvSize, InputViewportMaxBound);

	vec4 Out;
	Out.x = textureLod(Texture, UV[0], 0).r;
	Out.y = textureLod(Texture, UV[1], 0).r;
	Out.z = textureLod(Texture, UV[2], 0).r;
	Out.w = textureLod(Texture, UV[3], 0).r;

	return Out;
}
#else
{
	vec2 UV = min(BufferUV + vec2(-0.25f, -0.25f) * InvSize, InputViewportMaxBound - InvSize);
	return vec4(texture(Texture, UV, 0).r);
}
#endif
#define PIXELSHADER 1

#if COMPUTESHADER




#if 0
void OutputMipLevel(uint MipLevel, uvec2 OutputPixelPos, float FurthestDeviceZ, float ClosestDeviceZ)
{
	#if DIM_MIP_LEVEL_COUNT >= 2
	if (MipLevel == 1)
	{
		#if DIM_FURTHEST
			imageStore(FurthestHZBOutput_1, ivec2(OutputPixelPos), vec4(FurthestDeviceZ));
		#endif
	}
	#endif
	#if DIM_MIP_LEVEL_COUNT >= 3
	else if (MipLevel == 2)
	{
		#if DIM_FURTHEST
			imageStore(FurthestHZBOutput_2, ivec2(OutputPixelPos), vec4(FurthestDeviceZ));
		#endif
	}
	#endif
	#if DIM_MIP_LEVEL_COUNT >= 4
	else if (MipLevel == 3)
	{
		#if DIM_FURTHEST
			imageStore(FurthestHZBOutput_3, ivec2(OutputPixelPos), vec4(FurthestDeviceZ));
		#endif
	}		
	#endif
}

// Returns the pixel pos [[0; N[[^2 in a two dimensional tile size of N=2^TileSizeLog2, to
// store at a given SharedArrayId in [[0; N^2[[, so that a following recursive 2x2 pixel
// block reduction stays entirely LDS memory banks coherent.
uvec2 InitialTilePixelPositionForReduction2x2(const uint TileSizeLog2, uint SharedArrayId)
{
	uint x = 0;
	uint y = 0;

	for (uint i = 0; i < TileSizeLog2; i++)
	{
		const uint DestBitId = TileSizeLog2 - 1 - i;
		const uint DestBitMask = 1 << DestBitId;
		x |= DestBitMask & (SharedArrayId >> int(DestBitId) - int(i * 2 + 0));
		y |= DestBitMask & (SharedArrayId >> int(DestBitId) - int(i * 2 + 1));
	}

	return uvec2(x, y);
}


void main()
{
	uvec2 GroupId = gl_WorkGroupID.xy;
	uint GroupThreadIndex = gl_LocalInvocationIndex;
	#if DIM_MIP_LEVEL_COUNT == 1
		uvec2 GroupThreadId = uvec2(GroupThreadIndex % GROUP_TILE_SIZE, GroupThreadIndex / GROUP_TILE_SIZE);
	#else
		uvec2 GroupThreadId = InitialTilePixelPositionForReduction2x2(MAX_MIP_BATCH_SIZE - 1, GroupThreadIndex);
	#endif

	uvec2 DispatchThreadId = GROUP_TILE_SIZE * GroupId + GroupThreadId;

	vec2 BufferUV = (DispatchThreadId + vec2(0.5)) * DispatchThreadIdToBufferUV.xy + DispatchThreadIdToBufferUV.zw;
	vec4 DeviceZ = Gather4(ParentTextureMip, BufferUV);

	float FurthestDeviceZ = min(min(DeviceZ.x, DeviceZ.y), min(DeviceZ.z, DeviceZ.w));
	float ClosestDeviceZ = max(max(DeviceZ.x, DeviceZ.y), max(DeviceZ.z, DeviceZ.w));
	
	uvec2 OutputPixelPos = DispatchThreadId;
	
	#if DIM_FURTHEST
			imageStore(FurthestHZBOutput_0, ivec2(OutputPixelPos), vec4(FurthestDeviceZ));
	#endif
	
	#if DIM_MIP_LEVEL_COUNT == 1
	{
		// NOP
	}
	#else
	{
		SharedFurthestDeviceZ[GroupThreadIndex] = FurthestDeviceZ;
		SharedClosestDeviceZ[GroupThreadIndex] = ClosestDeviceZ;
	
		for (uint MipLevel = 1; MipLevel < DIM_MIP_LEVEL_COUNT; MipLevel++)
		{
			const uint TileSize = GROUP_TILE_SIZE / (1 << MipLevel);
			const uint ReduceBankSize = TileSize * TileSize;
			
			// LDS has been written before.
			if (MipLevel == 1)
				barrier();

			if (GroupThreadIndex < ReduceBankSize)
			{
				vec4 ParentFurthestDeviceZ;
				vec4 ParentClosestDeviceZ;
				ParentFurthestDeviceZ[0] = FurthestDeviceZ;
				ParentClosestDeviceZ[0] = ClosestDeviceZ;

				for (uint i = 1; i < 4; i++)
				{
					uint LDSIndex = GroupThreadIndex + i * ReduceBankSize;
					ParentFurthestDeviceZ[i] = SharedFurthestDeviceZ[LDSIndex];
					ParentClosestDeviceZ[i] = SharedClosestDeviceZ[LDSIndex];
				}
				
				FurthestDeviceZ = min(min(ParentFurthestDeviceZ.x, ParentFurthestDeviceZ.y), min(ParentFurthestDeviceZ.z, ParentFurthestDeviceZ.w));
				ClosestDeviceZ =  max(max(ParentClosestDeviceZ.x,  ParentClosestDeviceZ.y),  max(ParentClosestDeviceZ.z,  ParentClosestDeviceZ.w));
	
				OutputPixelPos = OutputPixelPos >> 1;
				OutputMipLevel(MipLevel, OutputPixelPos, FurthestDeviceZ, ClosestDeviceZ);
				
				SharedFurthestDeviceZ[GroupThreadIndex] = FurthestDeviceZ;
				SharedClosestDeviceZ[GroupThreadIndex] = ClosestDeviceZ;
			}
		} // for (uint MipLevel = 1; MipLevel < DIM_MIP_LEVEL_COUNT; MipLevel++)
	}
	#endif
} // HZBBuildCS


#elif PIXELSHADER

in vec2 vs_TexCoords;

out vec4 OutColor;

void main()
{
	vec2 BufferUV = vs_TexCoords.xy * DispatchThreadIdToBufferUV.xy + DispatchThreadIdToBufferUV.zw;
	vec4 DeviceZ = Gather4(ParentTextureMip, BufferUV);

	float FurthestDeviceZ = min(min(DeviceZ.x, DeviceZ.y), min(DeviceZ.z, DeviceZ.w));

	OutColor = vec4(FurthestDeviceZ);
}


#else
	#error Unknown shader frequency

#endif

#else
layout(local_size_x = GROUP_TILE_SIZE, local_size_y = GROUP_TILE_SIZE, local_size_z = 1u) in;

void main()
{
    int _74 = int(2u);
    int _75 = int(0u);
    int _76 = _74 - _75;
    uint _93;
    do
    {
        if (_76 > 0)
        {
            _93 = gl_LocalInvocationIndex << (uint(_76) & 31u);
            break;
        }
        else
        {
            if (_76 < 0)
            {
                _93 = gl_LocalInvocationIndex >> (uint(-_76) & 31u);
                break;
            }
        }
        _93 = gl_LocalInvocationIndex;
        break;
    } while(false);
    uint _94 = 4u & _93;
    int _96 = int(1u);
    int _97 = _74 - _96;
    uint _114;
    do
    {
        if (_97 > 0)
        {
            _114 = gl_LocalInvocationIndex << (uint(_97) & 31u);
            break;
        }
        else
        {
            if (_97 < 0)
            {
                _114 = gl_LocalInvocationIndex >> (uint(-_97) & 31u);
                break;
            }
        }
        _114 = gl_LocalInvocationIndex;
        break;
    } while(false);
    uint _115 = 4u & _114;
    int _117 = _96 - _74;
    uint _134;
    do
    {
        if (_117 > 0)
        {
            _134 = gl_LocalInvocationIndex << (uint(_117) & 31u);
            break;
        }
        else
        {
            if (_117 < 0)
            {
                _134 = gl_LocalInvocationIndex >> (uint(-_117) & 31u);
                break;
            }
        }
        _134 = gl_LocalInvocationIndex;
        break;
    } while(false);
    uint _135 = 2u & _134;
    int _138 = _96 - int(3u);
    uint _155;
    do
    {
        if (_138 > 0)
        {
            _155 = gl_LocalInvocationIndex << (uint(_138) & 31u);
            break;
        }
        else
        {
            if (_138 < 0)
            {
                _155 = gl_LocalInvocationIndex >> (uint(-_138) & 31u);
                break;
            }
        }
        _155 = gl_LocalInvocationIndex;
        break;
    } while(false);
    uint _156 = 2u & _155;
    int _159 = _75 - int(4u);
    uint _176;
    do
    {
        if (_159 > 0)
        {
            _176 = gl_LocalInvocationIndex << (uint(_159) & 31u);
            break;
        }
        else
        {
            if (_159 < 0)
            {
                _176 = gl_LocalInvocationIndex >> (uint(-_159) & 31u);
                break;
            }
        }
        _176 = gl_LocalInvocationIndex;
        break;
    } while(false);
    uint _177 = 1u & _176;
    int _180 = _75 - int(5u);
    uint _197;
    do
    {
        if (_180 > 0)
        {
            _197 = gl_LocalInvocationIndex << (uint(_180) & 31u);
            break;
        }
        else
        {
            if (_180 < 0)
            {
                _197 = gl_LocalInvocationIndex >> (uint(-_180) & 31u);
                break;
            }
        }
        _197 = gl_LocalInvocationIndex;
        break;
    } while(false);
    uint _198 = 1u & _197;
    uvec2 _202 = (uvec2(8u) * gl_WorkGroupID.xy) + uvec2(((0u | _94) | _135) | _177, ((0u | _115) | _156) | _198);
    vec4 _222 = textureGatherOffset(sampler2D(ParentTextureMip), min((((vec2(_202) + vec2(0.5)) * DispatchThreadIdToBufferUV.xy) + DispatchThreadIdToBufferUV.zw) + (vec2(-0.25) * InvSize), InputViewportMaxBound - InvSize), ivec2(0), 0);
    float _223 = _222.x;
    float _224 = _222.y;
    float _226 = _222.z;
    float _227 = _222.w;
    float _229 = min(min(_223, _224), min(_226, _227));
    float _232 = max(max(_223, _224), max(_226, _227));
    imageStore(FurthestHZBOutput_0, ivec2(_202), 1 - vec4(_229));
    //imageStore(ClosestHZBOutput_0, ivec2(_202), vec4(_232));
    SharedFurthestDeviceZ[gl_LocalInvocationIndex] = _229;
    SharedClosestDeviceZ[gl_LocalInvocationIndex] = _232;
    barrier();
    uvec2 _265;
    float _266;
    float _267;
    if (gl_LocalInvocationIndex < 16u)
    {
        uint _240 = gl_LocalInvocationIndex + 16u;
        float _242 = SharedFurthestDeviceZ[_240];
        float _244 = SharedClosestDeviceZ[_240];
        uint _245 = gl_LocalInvocationIndex + 32u;
        float _247 = SharedFurthestDeviceZ[_245];
        float _249 = SharedClosestDeviceZ[_245];
        uint _250 = gl_LocalInvocationIndex + 48u;
        float _252 = SharedFurthestDeviceZ[_250];
        float _254 = SharedClosestDeviceZ[_250];
        float _257 = min(min(_229, _242), min(_247, _252));
        float _260 = max(max(_232, _244), max(_249, _254));
        uvec2 _262 = _202 >> (uvec2(1u) & uvec2(31u));
        imageStore(FurthestHZBOutput_1, ivec2(_262),1 -  vec4(_257));
        //imageStore(ClosestHZBOutput_1, ivec2(_262), vec4(_260));
        SharedFurthestDeviceZ[gl_LocalInvocationIndex] = _257;
        SharedClosestDeviceZ[gl_LocalInvocationIndex] = _260;
        _265 = _262;
        _266 = _260;
        _267 = _257;
    }
    else
    {
        _265 = _202;
        _266 = _232;
        _267 = _229;
    }
    uvec2 _296;
    float _297;
    float _298;
    if (gl_LocalInvocationIndex < 4u)
    {
        uint _271 = gl_LocalInvocationIndex + 4u;
        float _273 = SharedFurthestDeviceZ[_271];
        float _275 = SharedClosestDeviceZ[_271];
        uint _276 = gl_LocalInvocationIndex + 8u;
        float _278 = SharedFurthestDeviceZ[_276];
        float _280 = SharedClosestDeviceZ[_276];
        uint _281 = gl_LocalInvocationIndex + 12u;
        float _283 = SharedFurthestDeviceZ[_281];
        float _285 = SharedClosestDeviceZ[_281];
        float _288 = min(min(_267, _273), min(_278, _283));
        float _291 = max(max(_266, _275), max(_280, _285));
        uvec2 _293 = _265 >> (uvec2(1u) & uvec2(31u));
        imageStore(FurthestHZBOutput_2, ivec2(_293), 1 - vec4(_288));
        //imageStore(ClosestHZBOutput_2, ivec2(_293), vec4(_291));
        SharedFurthestDeviceZ[gl_LocalInvocationIndex] = _288;
        SharedClosestDeviceZ[gl_LocalInvocationIndex] = _291;
        _296 = _293;
        _297 = _291;
        _298 = _288;
    }
    else
    {
        _296 = _265;
        _297 = _266;
        _298 = _267;
    }
    if (gl_LocalInvocationIndex < 1u)
    {
        uint _302 = gl_LocalInvocationIndex + 1u;
        uint _307 = gl_LocalInvocationIndex + 2u;
        uint _312 = gl_LocalInvocationIndex + 3u;
        float _319 = min(min(_298, SharedFurthestDeviceZ[_302]), min(SharedFurthestDeviceZ[_307], SharedFurthestDeviceZ[_312]));
        float _322 = max(max(_297, SharedClosestDeviceZ[_302]), max(SharedClosestDeviceZ[_307], SharedClosestDeviceZ[_312]));
        uvec2 _324 = _296 >> (uvec2(1u) & uvec2(31u));
        imageStore(FurthestHZBOutput_3, ivec2(_324), 1 - vec4(_319));
        //imageStore(ClosestHZBOutput_3, ivec2(_324), vec4(_322));
        SharedFurthestDeviceZ[gl_LocalInvocationIndex] = _319;
        SharedClosestDeviceZ[gl_LocalInvocationIndex] = _322;
    }
}




#endif




