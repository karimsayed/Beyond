#version 460 core

layout(location = 0) in vec3 a_Position;
layout(location = 1) in vec3 a_Normal;
layout(location = 2) in vec3 a_Tangent;
layout(location = 3) in vec3 a_Binormal;
layout(location = 4) in vec2 a_TexCoord;

uniform mat4 u_Transform;

struct PointLight {
	vec3 Position;
	float Multiplier;
	vec3 Radiance;
	float NearPlane;
	float FarPlane;
	bool CastsShadows;
};

layout(std140, binding = 0) uniform light_data {
	float u_BloomThreshold;
	float u_LightSize;
	float u_MaxShadowDistance;
	float u_ShadowFade;

	float u_CascadeTransitionFade;
	float u_IBLContribution;
	vec2 u_PixelSize;

	vec3 u_CameraPosition;
	float u_padding1;

	vec4 u_CascadeSplits;
	mat4 u_LightView;
    mat4 u_ProjectionMatrix;
    mat4 u_ProjectionMatrixInv;
	mat4 u_ViewMatrix;
    mat4 u_ViewMatrixInv;
	vec4 u_ProjectionParams; //x = near, y = far
	vec2 u_ViewportSize;
	vec2 u_Padding2;
    mat4 u_ViewProjectionMatrix;
	int u_PointLightsCount;
    PointLight pointLightsUniform[1024];
};
out VertexOutput
{
	vec3 WorldPosition;
	vec3 TangentViewPosition;
    vec3 Normal;
	vec2 TexCoord;
	mat3 WorldNormals;
} vs_Output;




void main()
{
	vs_Output.WorldPosition = vec3(u_Transform * vec4(a_Position, 1.0));
	vs_Output.TexCoord = vec2(a_TexCoord.x, 1.0 - a_TexCoord.y);
	mat3 TBN = mat3(a_Tangent, a_Binormal, a_Normal);
	vs_Output.WorldNormals = mat3(u_Transform) * TBN;
	vs_Output.TangentViewPosition = TBN * u_CameraPosition;
    vs_Output.Normal = mat3(u_Transform) * a_Normal;

	//gl_Position = u_ViewProjectionMatrix * u_Transform * vec4(a_Position, 1.0);
}
