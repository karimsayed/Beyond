#version 460 core                                                                               
																								
// define the number of CPs in the output patch                                                 
layout (vertices = 3) out;                                                                      
																								
struct PointLight {
	vec3 Position;
	float Multiplier;
	vec3 Radiance;
	float NearPlane;
	float FarPlane;
	bool CastsShadows;
};

layout(std140, binding = 0) uniform light_data {
	float u_BloomThreshold;
	float u_LightSize;
	float u_MaxShadowDistance;
	float u_ShadowFade;

	float u_CascadeTransitionFade;
	float u_IBLContribution;
	vec2 u_PixelSize;

	vec3 u_CameraPosition;
	float u_padding1;

	vec4 u_CascadeSplits;
	mat4 u_LightView;
    mat4 u_ProjectionMatrix;
    mat4 u_ProjectionMatrixInv;
	mat4 u_ViewMatrix;
    mat4 u_ViewMatrixInv;
	vec4 u_ProjectionParams; //x = near, y = far
	vec2 u_ViewportSize;
	vec2 u_Padding2;
    mat4 u_ViewProjectionMatrix;
	int u_PointLightsCount;
    PointLight pointLightsUniform[1024];
};																							
// attributes of the input CPs                                                                  
in VertexOutput
{
	vec3 WorldPosition;
	vec3 TangentViewPosition;
	vec3 Normal;
	vec2 TexCoord;
	mat3 WorldNormals;
} vs_Input[];                                                                       
																								
out CSOutput
{
	vec3 WorldPosition;
	vec3 TangentViewPosition;
	vec3 Normal;
	vec2 TexCoord;
	mat3 WorldNormals;
} sc_es[];                                                                  
																								
float GetTessLevel(float Distance0, float Distance1)                                            
{                                                                                               
	float AvgDistance = (Distance0 + Distance1) / 2.0;      
	
	if (AvgDistance <= 5.f)                                                                  
        return 10.f;                                                                            
    else if (AvgDistance <= 10.f)                                                            
        return 7.0;    
	else if (AvgDistance <= 50.f)                                                            
        return 3.f;  
    else                                                                                     
        return 1.f;                                                                             
		
}    

uniform bool u_Tesselate;
																								
void main()                                                                                     
{                                                                                               
	// Set the control points of the output patch                                               
	sc_es[gl_InvocationID].TexCoord = vs_Input[gl_InvocationID].TexCoord;                          
	sc_es[gl_InvocationID].Normal = vs_Input[gl_InvocationID].Normal;                            
	sc_es[gl_InvocationID].WorldPosition = vs_Input[gl_InvocationID].WorldPosition;                          
	sc_es[gl_InvocationID].TangentViewPosition = vs_Input[gl_InvocationID].TangentViewPosition;                          
	sc_es[gl_InvocationID].WorldNormals = vs_Input[gl_InvocationID].WorldNormals; 
	
																								
	// Calculate the distance from the camera to the three control points  
	float eyeToVertexDist[3];
	eyeToVertexDist[0] = distance(u_CameraPosition, vs_Input[0].WorldPosition);                     
	eyeToVertexDist[1] = distance(u_CameraPosition, vs_Input[1].WorldPosition);                     
	eyeToVertexDist[2] = distance(u_CameraPosition, vs_Input[2].WorldPosition);                     
							
	// Calculate the tessellation levels                                                        
	gl_TessLevelOuter[0] = u_Tesselate ? GetTessLevel(eyeToVertexDist[1], eyeToVertexDist[2]) : 1.f;            
	gl_TessLevelOuter[1] = u_Tesselate ? GetTessLevel(eyeToVertexDist[2], eyeToVertexDist[0]) : 1.f;            
	gl_TessLevelOuter[2] = u_Tesselate ? GetTessLevel(eyeToVertexDist[0], eyeToVertexDist[1]) : 1.f;            
	gl_TessLevelInner[0] = gl_TessLevelOuter[2];                                                
}                                                                                               