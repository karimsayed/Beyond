// Icon Shader
#version 460

layout(location = 0) out vec4 color;
layout(location = 1) out uint iconID;

uniform sampler2D u_IconTexture;
uniform uint u_IconID;


in vec2 v_TexCoord;

void main()
{
	color = texture(u_IconTexture, v_TexCoord);
	iconID = u_IconID;
}