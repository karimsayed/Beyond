#version 430

layout(location = 0) out vec4 o_Color;

in vec2 v_TexCoord;

uniform sampler2D u_NormalScene;
//uniform sampler2D u_Skybox;

uniform float u_Exposure;
//uniform bool u_EnableBloom;
//uniform int u_TextureSamples;


void main()
{
	const float gamma     = 2.2;
	const float pureWhite = 1.0;
	
	vec4 msColor = texture(u_NormalScene, v_TexCoord);

	//vec3 msBright = u_EnableBloom ? texture(u_BlurredScene, v_TexCoord).rgb : vec3(0);

//	msColor = (msColor);//texture(u_Texture, v_TexCoord).rgb * u_Exposure;

	msColor.rgb = vec3(1.0) - exp(-msColor.rgb * u_Exposure);
	// Reinhard tonemapping operator.
	// see: "Photographic Tone Reproduction for Digital Images", eq. 4
	float luminance = dot(msColor.rgb, vec3(0.2126, 0.7152, 0.0722));
	float mappedLuminance = (luminance * (1.0 + luminance / (pureWhite * pureWhite))) / (1.0 + luminance);

	// Scale color by ratio of average luminances. 
	vec4 mappedColor = (mappedLuminance / luminance) * msColor;

	// Gamma correction.
	//if(msColor.a != 6)
		o_Color = vec4(pow(mappedColor.rgb, vec3(1.0 / gamma)), 1);
	//else 
	//	o_Color = textureLod(u_Skybox, v_TexCoord, 0);
}