﻿// -----------------------------
// -- Beyond's PBR shader --
// -----------------------------
// 
//       
//
// References upon which this is based:
// - Unreal Engine 4 PBR notes (https://blog.selfshadow.com/publications/s2013-shading-course/karis/s2013_pbs_epic_notes_v2.pdf)
// - Frostbite's SIGGRAPH 2014 paper (https://seblagarde.wordpress.com/2015/07/14/siggraph-2014-moving-frostbite-to-physically-based-rendering/)
// - Michał Siejak's PBR project (https://github.com/Nadrin)
// - The Cherno's implimentation from years ago in the Sparky engine (https://github.com/TheCherno/Sparky)
#version 460 core

const float PI = 3.141592;
const float Epsilon = 0.00001;
const float MINIMUM_SHADOW_BIAS = 0.002f;

const int LightCount = 1;

float u_light_zNear = 0.0; // 0.01 gives artifacts? maybe because of ortho proj?
float u_light_zFar = 10000.0;
vec2 u_lightRadiusUV = vec2(0.05);


uniform sampler2D G_SSRReflections;
uniform sampler2D G_Position;
uniform sampler2D G_Normal;
uniform sampler2D G_Albedo;
uniform sampler2D G_MetalnessRoughness;
uniform sampler2D G_SSAO;

struct DirectionalLight {
	vec3 Direction;
	vec3 Radiance;
	float Multiplier;
	//bool CastsShadows;
};
uniform DirectionalLight u_DirectionalLight;

layout(std140, binding = 1) uniform light_matrices {
	mat4 ShadowMatrices[600];
};

struct PointLight {
	vec3 Position;
	float Multiplier;
	vec3 Radiance;
	float NearPlane;
	float FarPlane;
	bool CastsShadows;
};

layout(std140, binding = 0) uniform light_data {
	float u_BloomThreshold;
	float u_LightSize;
	float u_MaxShadowDistance;
	float u_ShadowFade;

	float u_CascadeTransitionFade;
	float u_IBLContribution;
	vec2 u_PixelSize;

	vec3 u_CameraPosition;
	float u_padding1;

	vec4 u_CascadeSplits;
	mat4 u_LightView;
    mat4 u_ProjectionMatrix;
    mat4 u_ProjectionMatrixInv;
	mat4 u_ViewMatrix;
    mat4 u_ViewMatrixInv;
	vec4 u_ProjectionParams; //x = near, y = far
	vec2 u_ViewportSize;
	vec2 u_Padding2;
    mat4 u_ViewProjectionMatrix;
	int u_PointLightsCount;
    PointLight pointLightsUniform[1024];
};

uniform mat4 lightSpaceMatrix0;
uniform mat4 lightSpaceMatrix1;
uniform mat4 lightSpaceMatrix2;
uniform mat4 lightSpaceMatrix3;


uniform bool u_SoftShadows;
uniform bool u_CascadeFading;
uniform bool u_ShowCascades;

uniform bool u_EnableBloom;
uniform bool u_EnableSSR;
uniform bool u_SSAOToggle;
uniform bool u_UseIrradianceReflections;
uniform bool u_EnableWireframe;



// Constant normal incidence Fresnel factor for all dielectrics.
const vec3 Fdielectric = vec3(0.04);



in vec2 vs_TexCoords;

layout(location = 0) out vec4 FragColor;
layout(location = 1) out vec4 BrightColor;




uniform sampler2D u_shadowMap0;
uniform sampler2D u_shadowMap1;
uniform sampler2D u_shadowMap2;
uniform sampler2D u_shadowMap3;

uniform samplerCube u_PointShadowMap;




// Environment maps
uniform samplerCube u_EnvRadianceTex;
uniform samplerCube u_EnvIrradianceTex;

// BRDF LUT
uniform sampler2D u_BRDFLUTTexture;


uniform float u_EnvMapRotation;



struct PBRParameters
{
	vec3 Position;
	vec3 Albedo;
	float Roughness;
	float Metalness;
	float SSAO;

	vec3 Normal;
	vec3 View;
	float NdotV;

	vec3 F;
};


PBRParameters m_Params;
float shadowBias;



// GGX/Towbridge-Reitz normal distribution function.
// Uses Disney's reparametrization of alpha = roughness^2
float ndfGGX(in float cosLh, in float roughness)
{
	const float alpha = roughness * roughness;
	const float alphaSq = alpha * alpha;

	const float denom = (cosLh * cosLh) * (alphaSq - 1.0) + 1.0;
	return alphaSq / (PI * denom * denom);
}

// Single term for separable Schlick-GGX below.
float gaSchlickG1(in float cosTheta, in float k)
{
	return cosTheta / (cosTheta * (1.0 - k) + k);
}

// Schlick-GGX approximation of geometric attenuation function using Smith's method.
float gaSchlickGGX(in float cosLi, in float NdotV, in float roughness)
{
	const float r = roughness + 1.0f;
	const float k = (r * r) / 8.0f; // Epic suggests using this roughness remapping for analytic lights.
	return gaSchlickG1(cosLi, k) * gaSchlickG1(NdotV, k);
}



// Shlick's approximation of the Fresnel factor.
vec3 fresnelSchlick(in vec3 F0, in float cosTheta)
{
	return F0 + (1.0f - F0) * pow(1.0f - cosTheta, 5.0f);
}

vec3 fresnelSchlickRoughness(in vec3 F0, in float cosTheta, in float roughness)
{
    return F0 + (max(vec3(1.0 - roughness), F0) - F0) * pow(1.0 - cosTheta, 5.0);
} 

vec3 RotateVectorAboutY(in float angle, in vec3 vec)
{
    angle = radians(angle);
    mat3x3 rotationMatrix = { vec3(cos(angle),0.0,sin(angle)),
                              vec3(0.0,1.0,0.0),
                              vec3(-sin(angle),0.0,cos(angle))};
    return rotationMatrix * vec;
}

vec3 DirLighting(in vec3 F0)
{
	vec3 result = vec3(0.0);
	for(int i = 0; i < LightCount; i++)
	{
		vec3 Li = u_DirectionalLight.Direction;
		vec3 Lradiance = u_DirectionalLight.Radiance * u_DirectionalLight.Multiplier;
		vec3 Lh = normalize(Li + m_Params.View);

		// Calculate angles between surface normal and various light vectors.
		float cosLi = max(0.0, dot(m_Params.Normal, Li));
		float cosLh = max(0.0, dot(m_Params.Normal, Lh));

		m_Params.F = fresnelSchlick(F0, max(0.0, dot(Lh, m_Params.View)));
		float D = ndfGGX(cosLh, m_Params.Roughness);
		float G = gaSchlickGGX(cosLi, m_Params.NdotV, m_Params.Roughness);

		vec3 kd = (1.0 - m_Params.F) * (1.0 - m_Params.Metalness);
		vec3 diffuseBRDF = kd * m_Params.Albedo;

		// Cook-Torrance
		vec3 specularBRDF = (m_Params.F * D * G) / max(Epsilon, 4.0 * cosLi * m_Params.NdotV);

		result += (diffuseBRDF + specularBRDF) * Lradiance * cosLi;
	}
	return result;
}

const vec3 gridSamplingDisk[20] = vec3[]
(
   vec3(1, 1,  1), vec3( 1, -1,  1), vec3(-1, -1,  1), vec3(-1, 1,  1), 
   vec3(1, 1, -1), vec3( 1, -1, -1), vec3(-1, -1, -1), vec3(-1, 1, -1),
   vec3(1, 1,  0), vec3( 1, -1,  0), vec3(-1, -1,  0), vec3(-1, 1,  0),
   vec3(1, 0,  1), vec3(-1,  0,  1), vec3( 1,  0, -1), vec3(-1, 0, -1),
   vec3(0, 1,  1), vec3( 0, -1,  1), vec3( 0, -1, -1), vec3( 0, 1, -1)
);


float calculatePointShadows(in int lightIndex)
{
    // get vector between fragment position and light position
    const vec3 fragToLight = m_Params.Position - pointLightsUniform[lightIndex].Position;
    const float currentDepth = length(fragToLight);
	const float farPlane = pointLightsUniform[lightIndex].FarPlane;
    float shadow = 0.0;
    const float bias = 0.15;
    const float viewDistance = length(m_Params.Position - u_CameraPosition);
    const float diskRadius = (1.0 + (viewDistance / farPlane)) / farPlane;
    for(int i = 0; i < 5; ++i) // 5 in number of samples
    {
        float closestDepth = texture(u_PointShadowMap, fragToLight + gridSamplingDisk[i] * diskRadius).r;
        closestDepth *= farPlane;
        if(currentDepth - bias > closestDepth)
            shadow += 1.0;
    }
    shadow /= 5.f; // 5 in number of samples
	return 1.f - shadow;
}

vec3 PointLighting(in vec3 F0)
{
	vec3 result = vec3(0.0);
	for(int i = 0; i < u_PointLightsCount; i++)
	{

		vec3 Li = normalize(pointLightsUniform[i].Position - m_Params.Position);
		float lightDistance = length(pointLightsUniform[i].Position - m_Params.Position);
		vec3 Lh = normalize(Li + m_Params.View);
		float attenuation = 1.0 / (lightDistance * lightDistance);

		vec3 Lradiance = pointLightsUniform[i].Radiance * pointLightsUniform[i].Multiplier * attenuation;

		// Calculate angles between surface normal and various light vectors.
		float cosLi = max(0.0, dot(m_Params.Normal, Li));
		float cosLh = max(0.0, dot(m_Params.Normal, Lh));

		m_Params.F = fresnelSchlick(F0, max(0.0, dot(Lh, m_Params.View)));
		float D = ndfGGX(cosLh, m_Params.Roughness);
		float G = gaSchlickGGX(cosLi, m_Params.NdotV, m_Params.Roughness);

		vec3 kd = (1.0 - m_Params.F) * (1.0 - m_Params.Metalness);
		vec3 diffuseBRDF = kd * m_Params.Albedo;

		// Cook-Torrance
		vec3 specularBRDF = (m_Params.F * D * G) / max(Epsilon, 4.0 * cosLi * m_Params.NdotV);
		result += (diffuseBRDF + specularBRDF) * Lradiance * cosLi;
	}
	return result;

}





vec3 IBL(in vec3 F0, in vec3 Lr)
{
	vec3 irradiance = texture(u_EnvIrradianceTex, m_Params.Normal).rgb;


	vec3 F = fresnelSchlickRoughness(F0, m_Params.NdotV, m_Params.Roughness);
	vec3 kd = (1.0f - F) * (1.0f - m_Params.Metalness);

	vec3 diffuseIBL = m_Params.Albedo * irradiance;

	int u_EnvRadianceTexLevels = textureQueryLevels(u_EnvRadianceTex);
	float NoV = clamp(m_Params.NdotV, 0.0f, 1.0f);
	vec3 R = 2.0f * dot(m_Params.View, m_Params.Normal) * m_Params.Normal - m_Params.View;


	vec3 specularIrradiance = u_UseIrradianceReflections ? textureLod(u_EnvRadianceTex, RotateVectorAboutY(u_EnvMapRotation, Lr), (m_Params.Roughness) * u_EnvRadianceTexLevels).rgb : vec3(0);



	// Sample BRDF Lut, 1.0 - roughness for y-coord because texture was generated (in Sparky) for gloss model
	vec2 specularBRDF = texture(u_BRDFLUTTexture, vec2(m_Params.NdotV, 1.0 - m_Params.Roughness)).rg;
	vec3 specularIBL = (specularIrradiance) * (F0 * specularBRDF.x + specularBRDF.y);
	
	return kd * diffuseIBL + specularIBL;
}

/////////////////////////////////////////////
// PCSS
/////////////////////////////////////////////

uint CascadeIndex = 0;
float ShadowFade = 1.0;


float HardShadows_DirectionalLight(in sampler2D shadowMap, in vec3 shadowCoords)
{
	return 1.0 - step( texture(shadowMap, shadowCoords.xy).r + shadowBias, shadowCoords.z) * ShadowFade;
}

// Penumbra

// this search point estimation comes from the following article: 
// http://developer.download.nvidia.com/whitepapers/2008/PCSS_DirectionalLight_Integration.pdf
float SearchWidth(in float uvLightSize, in float receiverDistance)
{
	const float NEAR = 0.1;
	return uvLightSize * (receiverDistance - NEAR) / u_CameraPosition.z;
}



vec2 searchRegionRadiusUV(in float zWorld)
{
    return u_lightRadiusUV * (zWorld - u_light_zNear) / zWorld;
}

const vec2 PoissonDistribution[64] = vec2[](
	vec2(-0.884081, 0.124488),
	vec2(-0.714377, 0.027940),
	vec2(-0.747945, 0.227922),
	vec2(-0.939609, 0.243634),
	vec2(-0.985465, 0.045534),
	vec2(-0.861367, -0.136222),
	vec2(-0.881934, 0.396908),
	vec2(-0.466938, 0.014526),
	vec2(-0.558207, 0.212662),
	vec2(-0.578447, -0.095822),
	vec2(-0.740266, -0.095631),
	vec2(-0.751681, 0.472604),
	vec2(-0.553147, -0.243177),
	vec2(-0.674762, -0.330730),
	vec2(-0.402765, -0.122087),
	vec2(-0.319776, -0.312166),
	vec2(-0.413923, -0.439757),
	vec2(-0.979153, -0.201245),
	vec2(-0.865579, -0.288695),
	vec2(-0.243704, -0.186378),
	vec2(-0.294920, -0.055748),
	vec2(-0.604452, -0.544251),
	vec2(-0.418056, -0.587679),
	vec2(-0.549156, -0.415877),
	vec2(-0.238080, -0.611761),
	vec2(-0.267004, -0.459702),
	vec2(-0.100006, -0.229116),
	vec2(-0.101928, -0.380382),
	vec2(-0.681467, -0.700773),
	vec2(-0.763488, -0.543386),
	vec2(-0.549030, -0.750749),
	vec2(-0.809045, -0.408738),
	vec2(-0.388134, -0.773448),
	vec2(-0.429392, -0.894892),
	vec2(-0.131597, 0.065058),
	vec2(-0.275002, 0.102922),
	vec2(-0.106117, -0.068327),
	vec2(-0.294586, -0.891515),
	vec2(-0.629418, 0.379387),
	vec2(-0.407257, 0.339748),
	vec2(0.071650, -0.384284),
	vec2(0.022018, -0.263793),
	vec2(0.003879, -0.136073),
	vec2(-0.137533, -0.767844),
	vec2(-0.050874, -0.906068),
	vec2(0.114133, -0.070053),
	vec2(0.163314, -0.217231),
	vec2(-0.100262, -0.587992),
	vec2(-0.004942, 0.125368),
	vec2(0.035302, -0.619310),
	vec2(0.195646, -0.459022),
	vec2(0.303969, -0.346362),
	vec2(-0.678118, 0.685099),
	vec2(-0.628418, 0.507978),
	vec2(-0.508473, 0.458753),
	vec2(0.032134, -0.782030),
	vec2(0.122595, 0.280353),
	vec2(-0.043643, 0.312119),
	vec2(0.132993, 0.085170),
	vec2(-0.192106, 0.285848),
	vec2(0.183621, -0.713242),
	vec2(0.265220, -0.596716),
	vec2(-0.009628, -0.483058),
	vec2(-0.018516, 0.435703)
);

vec2 SamplePoisson(in int index)
{
   return PoissonDistribution[index % 32];
}

float FindBlockerDistance_DirectionalLight(in sampler2D shadowMap, in vec3 shadowCoords, in float uvLightSize)
{

	const int numBlockerSearchSamples = 32;
	int blockers = 0;
	float avgBlockerDistance = 0;
	
	const float zEye = -(u_LightView * vec4(m_Params.Position, 1.0)).z;
	const vec2 searchWidth = searchRegionRadiusUV(zEye);
	for (int i = 0; i < numBlockerSearchSamples; i++) // samples
	{
		float z = texture(shadowMap, shadowCoords.xy + (SamplePoisson(i).xy * searchWidth.xy)).r;
		if (z < (shadowCoords.z - shadowBias))
		{
			blockers++;
			avgBlockerDistance += z;
		}
	}

	if (blockers > 0)
		return avgBlockerDistance / float(blockers);

	return -1;
}


float PCF_DirectionalLight(in sampler2D shadowMap, in vec3 shadowCoords, in float uvRadius)
{
	int numPCFSamples = 32 ;
	float sum = 0;
	for (int i = 0; i < numPCFSamples; i++)
	{
		float z = texture(shadowMap, shadowCoords.xy + SamplePoisson(i)  * uvRadius).r;
		sum += (z < (shadowCoords.z - shadowBias)) ? 1 : 0;
	}
	return sum / numPCFSamples;
}

float PCSS_DirectionalLight(in sampler2D shadowMap, in vec3 shadowCoords, in float uvLightSize)
{
	float blockerDistance = FindBlockerDistance_DirectionalLight(shadowMap, shadowCoords, uvLightSize);
	if (blockerDistance == -1)
		return 1.f;		

	float penumbraWidth = (shadowCoords.z - blockerDistance) / blockerDistance;

	float NEAR = 0.01; // Should this value be tweakable?
	float uvRadius = penumbraWidth * uvLightSize * NEAR / shadowCoords.z;
	return 1.0 - PCF_DirectionalLight(shadowMap, shadowCoords, uvRadius) * ShadowFade;
}

sampler2D getShadowMap(uint index)
{
	switch (index)
	{
	case 0:
		return u_shadowMap0;
	case 1:
		return u_shadowMap1;
	case 2:
		return u_shadowMap2;
	case 3:
		return u_shadowMap3;
	}
}

vec4 getLightPos(in uint index)
{
	switch (index)
	{
	case 0: return lightSpaceMatrix0 * vec4(m_Params.Position, 1.0);
	case 1: return lightSpaceMatrix1 * vec4(m_Params.Position, 1.0);
	case 2:	return lightSpaceMatrix2 * vec4(m_Params.Position, 1.0);
	case 3:	return lightSpaceMatrix3 * vec4(m_Params.Position, 1.0);
	}
}

float calculateDirShadows()
{

	const uint SHADOW_MAP_CASCADE_COUNT = 4;
	for(uint i = 0; i < SHADOW_MAP_CASCADE_COUNT - 1; i++)
	{
		if(u_CameraPosition.z <= u_CascadeSplits[i])
			CascadeIndex = i + 1;
	}

	shadowBias = max(MINIMUM_SHADOW_BIAS * (1.0 - dot(m_Params.Normal, u_DirectionalLight.Direction)), MINIMUM_SHADOW_BIAS);

	const float shadowDistance = u_MaxShadowDistance;
	const float transitionDistance = u_ShadowFade;
	const float distance = length(u_CameraPosition);
	ShadowFade = distance - (shadowDistance - transitionDistance);
	ShadowFade /= transitionDistance;
	ShadowFade = clamp(1.0 - ShadowFade, 0.0, 1.0);


	const bool fadeCascades = u_CascadeFading;
	float shadowAmount = 1.0;
	if (fadeCascades)
	{
		float cascadeTransitionFade = u_CascadeTransitionFade;
		float viewDistance = u_CameraPosition.z;

		float c0 = smoothstep(cascadeTransitionFade * 0.5f + u_CascadeSplits[0], u_CascadeSplits[0] - cascadeTransitionFade * 0.5f, viewDistance);
		float c1 = smoothstep(cascadeTransitionFade * 0.5f + u_CascadeSplits[1], u_CascadeSplits[1] - cascadeTransitionFade * 0.5f, viewDistance);
		float c2 = smoothstep(cascadeTransitionFade * 0.5f + u_CascadeSplits[2], u_CascadeSplits[2] - cascadeTransitionFade * 0.5f, viewDistance);
		if (c0 > 0.0 && c0 < 1.0)
		{
			// Sample 0 & 1
			vec4 ShadowMapCoords0 = lightSpaceMatrix0 * vec4(m_Params.Position, 1.0);
			vec4 ShadowMapCoords1 = lightSpaceMatrix1 * vec4(m_Params.Position, 1.0);
			vec3 shadowMapCoords = (ShadowMapCoords0.xyz / ShadowMapCoords0.w);
			float shadowAmount0 = u_SoftShadows ? PCSS_DirectionalLight(u_shadowMap0, shadowMapCoords, u_LightSize) : HardShadows_DirectionalLight(u_shadowMap0, shadowMapCoords);
			shadowMapCoords = (ShadowMapCoords1.xyz / ShadowMapCoords1.w);
			float shadowAmount1 = u_SoftShadows ? PCSS_DirectionalLight(u_shadowMap1, shadowMapCoords, u_LightSize) : HardShadows_DirectionalLight(u_shadowMap1, shadowMapCoords);

			shadowAmount = mix(shadowAmount0, shadowAmount1, c0);
		}
		else if (c1 > 0.0 && c1 < 1.0)
		{
			// Sample 1 & 2
			vec4 ShadowMapCoords1 = lightSpaceMatrix0 * vec4(m_Params.Position, 1.0);
			vec4 ShadowMapCoords2 = lightSpaceMatrix1 * vec4(m_Params.Position, 1.0);
			vec3 shadowMapCoords = (ShadowMapCoords1.xyz / ShadowMapCoords1.w);
			float shadowAmount1 = u_SoftShadows ? PCSS_DirectionalLight(u_shadowMap1, shadowMapCoords, u_LightSize) : HardShadows_DirectionalLight(u_shadowMap1, shadowMapCoords);
			shadowMapCoords = (ShadowMapCoords2.xyz / ShadowMapCoords2.w);
			float shadowAmount2 = u_SoftShadows ? PCSS_DirectionalLight(u_shadowMap2, shadowMapCoords, u_LightSize) : HardShadows_DirectionalLight(u_shadowMap2, shadowMapCoords);

			shadowAmount = mix(shadowAmount1, shadowAmount2, c1);
		}
		else if (c2 > 0.0 && c2 < 1.0)
		{
			// Sample 2 & 3
			vec4 ShadowMapCoords2 = lightSpaceMatrix2 * vec4(m_Params.Position, 1.0);
			vec4 ShadowMapCoords3 = lightSpaceMatrix3 * vec4(m_Params.Position, 1.0);
			vec3 shadowMapCoords = (ShadowMapCoords2.xyz / ShadowMapCoords2.w);
			float shadowAmount2 = u_SoftShadows ? PCSS_DirectionalLight(u_shadowMap2, shadowMapCoords, u_LightSize) : HardShadows_DirectionalLight(u_shadowMap2, shadowMapCoords);
			shadowMapCoords = (ShadowMapCoords3.xyz / ShadowMapCoords3.w);
			float shadowAmount3 = u_SoftShadows ? PCSS_DirectionalLight(u_shadowMap3, shadowMapCoords, u_LightSize) : HardShadows_DirectionalLight(u_shadowMap3, shadowMapCoords);

			shadowAmount = mix(shadowAmount2, shadowAmount3, c2);
		}
		else
		{
			vec4 pos = getLightPos(CascadeIndex);
			vec3 shadowMapCoords = (pos.xyz / pos.w);
			shadowAmount = u_SoftShadows ? PCSS_DirectionalLight(getShadowMap(CascadeIndex), shadowMapCoords, u_LightSize) : HardShadows_DirectionalLight(getShadowMap(CascadeIndex), shadowMapCoords);
		}
	}
	else
	{
		vec4 pos = getLightPos(CascadeIndex);
			vec3 shadowMapCoords = (pos.xyz / pos.w);
			shadowAmount = u_SoftShadows ? PCSS_DirectionalLight(getShadowMap(CascadeIndex), shadowMapCoords, u_LightSize) : HardShadows_DirectionalLight(getShadowMap(CascadeIndex), shadowMapCoords);
	}


	float NdotL = dot(m_Params.Normal, u_DirectionalLight.Direction);
	NdotL = smoothstep(0.0, 0.4, NdotL + 0.2);
	shadowAmount *= (NdotL * 1.0);

	return shadowAmount;
}



void main()
{
	// Standard PBR inputs
	m_Params.Position = texture(G_Position, vs_TexCoords).rgb;
	if(m_Params.Position == vec3(0))
		discard;

	if (u_EnableWireframe)
	{
		FragColor = vec4(m_Params.Position, 1.f);
		return;
	}

	m_Params.Normal = texture(G_Normal, vs_TexCoords).rgb; 
	m_Params.Albedo = texture(G_Albedo, vs_TexCoords).rgb;
	vec2 metalnessRoughness = texture(G_MetalnessRoughness, vs_TexCoords).rg;
	m_Params.Metalness = metalnessRoughness.r;
	m_Params.Roughness = metalnessRoughness.g;
	m_Params.SSAO = texture(G_SSAO, vs_TexCoords).r;
	


	// Normals (either from vertex or map)
	m_Params.View = normalize(u_CameraPosition - m_Params.Position);
	m_Params.NdotV = max(dot(m_Params.Normal, m_Params.View), 0.0);
		
	// Specular reflection vector
	vec3 Lr = (2.0f * m_Params.NdotV * m_Params.Normal) - m_Params.View;
	
	// Fresnel reflectance, metals use albedo
	vec3 F0 = mix(Fdielectric, m_Params.Albedo, m_Params.Metalness);


	vec3 iblContribution = IBL(F0, Lr) * u_IBLContribution;
	float dirShadows = calculateDirShadows();

	vec3 lightContribution = u_DirectionalLight.Multiplier > 0.0f ? (DirLighting(F0) * dirShadows) : vec3(0.0f);
	
	lightContribution += PointLighting(F0);

	iblContribution *= u_SSAOToggle ? m_Params.SSAO : 1.f;

	vec4 ssrReflections = u_EnableSSR ? texture(G_SSRReflections, vs_TexCoords) : vec4(0.f, 0.f, 0.f, 1.f);
	
	vec3 result = (lightContribution + iblContribution) + (ssrReflections.xyz * ssrReflections.w) * 0.6f;


	result *= u_SSAOToggle ? m_Params.SSAO : 1.f;

	float brightness = dot(result, vec3(0.2126, 0.7152, 0.0722));

	BrightColor = u_EnableBloom && brightness > u_BloomThreshold ? vec4(result, 1.0) : vec4(0.0, 0.0, 0.0, 1.0);

	FragColor = vec4(result, 1.0);
	
	if (u_ShowCascades)
	{
		switch(CascadeIndex)
		{
		case 0:
			FragColor.rgb *= vec3(1.0f, 0.25f, 0.25f);
			break;
		case 1:
			FragColor.rgb *= vec3(0.25f, 1.0f, 0.25f);
			break;
		case 2:
			FragColor.rgb *= vec3(0.25f, 0.25f, 1.0f);
			break;
		case 3:
			FragColor.rgb *= vec3(1.0f, 1.0f, 0.25f);
			break;
		}
	}

}
