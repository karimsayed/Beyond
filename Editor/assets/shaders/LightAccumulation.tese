#version 460 core                                                                               
                                                                                                
layout(triangles, equal_spacing, ccw) in;                                                       
                   
                   
uniform sampler2D u_DisplacementTexture;                                                             
uniform float u_DisplacementScale;  
uniform float u_DisplacementTexToggle;  

struct PointLight {
	vec3 Position;
	float Multiplier;
	vec3 Radiance;
	float NearPlane;
	float FarPlane;
	bool CastsShadows;
};

layout(std140, binding = 0) uniform light_data {
	float u_BloomThreshold;
	float u_LightSize;
	float u_MaxShadowDistance;
	float u_ShadowFade;

	float u_CascadeTransitionFade;
	float u_IBLContribution;
	vec2 u_PixelSize;

	vec3 u_CameraPosition;
	float u_padding1;

	vec4 u_CascadeSplits;
	mat4 u_LightView;
    mat4 u_ProjectionMatrix;
    mat4 u_ProjectionMatrixInv;
	mat4 u_ViewMatrix;
    mat4 u_ViewMatrixInv;
	vec4 u_ProjectionParams; //x = near, y = far
	vec2 u_ViewportSize;
	vec2 u_Padding2;
    mat4 u_ViewProjectionMatrix;
	int u_PointLightsCount;
    PointLight pointLightsUniform[1024];
};

in CSOutput
{
	vec3 WorldPosition;
	vec3 TangentViewPosition;
    vec3 Normal;
	vec2 TexCoord;
	mat3 WorldNormals;
} sc_Input[];                                                                 

out ESOutput
{
	vec3 WorldPosition;
	vec3 TangentViewPosition;
    vec3 Normal;
	vec2 TexCoord;
	mat3 WorldNormals;
} es_fs;                                                                         
                                                                                                
vec2 interpolate2D(vec2 v0, vec2 v1, vec2 v2)                                                   
{                                                                                               
    return vec2(gl_TessCoord.x) * v0 + vec2(gl_TessCoord.y) * v1 + vec2(gl_TessCoord.z) * v2;   
}                                                                                               
                                                                                                
vec3 interpolate3D(vec3 v0, vec3 v1, vec3 v2)                                                   
{                                                                                               
    return vec3(gl_TessCoord.x) * v0 + vec3(gl_TessCoord.y) * v1 + vec3(gl_TessCoord.z) * v2;   
}      

mat3 interpolate3D(mat3 v0, mat3 v1, mat3 v2)                                                   
{                                                                                               
    return mat3(gl_TessCoord.x) * v0 + mat3(gl_TessCoord.y) * v1 + mat3(gl_TessCoord.z) * v2;   
}                                                                                               
                                                                                                
void main()                                                                                     
{                                                                                               
    // Interpolate the attributes of the output vertex using the barycentric coordinates        
    es_fs.TexCoord = interpolate2D(sc_Input[0].TexCoord, sc_Input[1].TexCoord, sc_Input[2].TexCoord);    
    es_fs.Normal = normalize(interpolate3D(sc_Input[0].Normal, sc_Input[1].Normal, sc_Input[2].Normal));            
    es_fs.WorldPosition = interpolate3D(sc_Input[0].WorldPosition, sc_Input[1].WorldPosition, sc_Input[2].WorldPosition);    
    es_fs.WorldNormals = interpolate3D(sc_Input[0].WorldNormals, sc_Input[1].WorldNormals, sc_Input[2].WorldNormals);    
    es_fs.TangentViewPosition = interpolate3D(sc_Input[0].TangentViewPosition, sc_Input[1].TangentViewPosition, sc_Input[2].TangentViewPosition);    

    if (bool(u_DisplacementTexToggle))
        es_fs.WorldPosition +=  es_fs.Normal * texture(u_DisplacementTexture, es_fs.TexCoord).r * u_DisplacementScale; 

    gl_Position = u_ViewProjectionMatrix * vec4(es_fs.WorldPosition, 1.0);                                              
}                                                                                               
