#version 460 core

layout(location = 0) in vec3 a_Position;
layout(location = 1) in vec2 a_TexCoord;

out vec2 vs_TexCoords;
out vec2 vs_pos;

void main()
{
	vs_TexCoords = a_TexCoord;
	gl_Position = vec4(a_Position, 1.0);
	vs_pos = gl_Position.xy;
}
