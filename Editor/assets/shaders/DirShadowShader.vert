﻿// Shadow Depth shader

#version 460


layout(location = 0) in vec3 position;

uniform mat4 u_Transform;


void main()
{
    gl_Position = u_Transform * vec4(position, 1.0);
}

