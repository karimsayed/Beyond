#version 460 core
layout(location = 0) out vec4 FragColor;
layout(location = 1) out vec4 Position;
layout(location = 2) out vec4 Albedo;
layout(location = 3) out vec4 Normal;
layout(location = 4) out vec4 MetalnessRoughness;
layout(location = 5) out float LinearizedDepth;



in ESOutput
{
	vec3 WorldPosition;
	vec3 TangentViewPosition;
    vec3 Normal;
	vec2 TexCoord;
	mat3 WorldNormals;
} vs_Input;

const float PI = 3.141592;
const float Epsilon = 0.00001;



uniform vec3 u_AlbedoColor;
uniform float u_Metalness;
uniform float u_Roughness;
uniform float u_HeightScale;


//uniform bool u_UseIrradianceReflections;
//uniform bool u_EnableBloom;
//uniform bool u_EnableSSR;
uniform bool u_SSAOToggle;
//uniform bool u_EnableWireframe;

uniform bool u_ShowCascades;



layout(std430, binding = 2) readonly buffer VisibleLightIndicesBuffer {
	int indices[];
} visibleLightIndicesBuffer;

uniform int numberOfTilesX;



uniform sampler2D u_AlbedoTexture;
uniform sampler2D u_NormalTexture;
uniform sampler2D u_MetalnessTexture;
uniform sampler2D u_RoughnessTexture;
uniform sampler2D u_HeightTexture;


uniform float u_AlbedoTexToggle;
uniform float u_NormalTexToggle;
uniform float u_MetalnessTexToggle;
uniform float u_RoughnessTexToggle;
uniform float u_HeightTexToggle;


// Environment maps
uniform samplerCube u_EnvRadianceTex;
uniform samplerCube u_EnvIrradianceTex;

// BRDF LUT
uniform sampler2D u_BRDFLUTTexture;


uniform float u_EnvMapRotation;

struct PointLight {
	vec3 Position;
	float Multiplier;
	vec3 Radiance;
	float NearPlane;
	float FarPlane;
	bool CastsShadows;
};

layout(std140, binding = 0) uniform light_data {
	float u_BloomThreshold;
	float u_LightSize;
	float u_MaxShadowDistance;
	float u_ShadowFade;

	float u_CascadeTransitionFade;
	float u_IBLContribution;
	vec2 u_PixelSize;

	vec3 u_CameraPosition;
	float u_padding1;

	vec4 u_CascadeSplits;
	mat4 u_LightView;
    mat4 u_ProjectionMatrix;
    mat4 u_ProjectionMatrixInv;
	mat4 u_ViewMatrix;
    mat4 u_ViewMatrixInv;
	vec4 u_ProjectionParams; //x = near, y = far
	vec2 u_ViewportSize;
	vec2 u_Padding2;
    mat4 u_ViewProjectionMatrix;
	int u_PointLightsCount;
    PointLight pointLightsUniform[1024];
};

struct PBRParameters
{
	vec3 Albedo;
	float Roughness;
	float Metalness;
	vec3 Normal;
	vec3 View;
	float NdotV;
} m_Params;

const vec3 Fdielectric = vec3(0.04);


struct DirectionalLight {
	vec3 Direction;
	vec3 Radiance;
	float Multiplier;
};
uniform DirectionalLight u_DirectionalLight;


vec2 ParallaxMapping(vec2 texCoords, vec3 viewDir)
{ 
    // number of depth layers
    const float minLayers = 8;
    const float maxLayers = 32;
    float numLayers = mix(maxLayers, minLayers, abs(dot(vec3(0.0, 0.0, 1.0), viewDir)));  
    // calculate the size of each layer
    float layerDepth = 1.0 / numLayers;
    // depth of current layer
    float currentLayerDepth = 0.0;
    // the amount to shift the texture coordinates per layer (from vector P)
    vec2 P = viewDir.xy / viewDir.z * u_HeightScale; 
    vec2 deltaTexCoords = P / numLayers;
  
    // get initial values
    vec2  currentTexCoords     = texCoords;
    float currentDepthMapValue = texture(u_HeightTexture, currentTexCoords).r;
      
    while(currentLayerDepth < currentDepthMapValue)
    {
        // shift texture coordinates along direction of P
        currentTexCoords -= deltaTexCoords;
        // get depthmap value at current texture coordinates
        currentDepthMapValue = texture(u_HeightTexture, currentTexCoords).r;  
        // get depth of next layer
        currentLayerDepth += layerDepth;  
    }
    
    // get texture coordinates before collision (reverse operations)
    vec2 prevTexCoords = currentTexCoords + deltaTexCoords;

    // get depth after and before collision for linear interpolation
    float afterDepth  = currentDepthMapValue - currentLayerDepth;
    float beforeDepth = texture(u_HeightTexture, prevTexCoords).r - currentLayerDepth + layerDepth;
 
    // interpolation of texture coordinates
    float weight = afterDepth / (afterDepth - beforeDepth);
    vec2 finalTexCoords = prevTexCoords * weight + currentTexCoords * (1.0 - weight);

    return finalTexCoords;
}

vec3 fresnelSchlickRoughness(vec3 F0, float cosTheta, float roughness)
{
    return F0 + (max(vec3(1.0 - roughness), F0) - F0) * pow(1.0 - cosTheta, 5.0);
} 

vec3 RotateVectorAboutY(in float angle, in vec3 vec)
{
    angle = radians(angle);
    mat3x3 rotationMatrix = { vec3(cos(angle),0.0,sin(angle)),
                              vec3(0.0,1.0,0.0),
                              vec3(-sin(angle),0.0,cos(angle))};
    return rotationMatrix * vec;
}

vec3 IBL(in vec3 F0, in vec3 Lr)
{
	vec3 irradiance = texture(u_EnvIrradianceTex, m_Params.Normal).rgb;


	vec3 F = fresnelSchlickRoughness(F0, m_Params.NdotV, m_Params.Roughness);
	vec3 kd = (1.0f - F) * (1.0f - m_Params.Metalness);

	vec3 diffuseIBL = m_Params.Albedo * irradiance;

	int u_EnvRadianceTexLevels = textureQueryLevels(u_EnvRadianceTex);
	//float NoV = clamp(m_Params.NdotV, 0.0f, 1.0f);
	//vec3 R = 2.0f * dot(m_Params.View, m_Params.Normal) * m_Params.Normal - m_Params.View;


	vec3 specularIrradiance = textureLod(u_EnvRadianceTex, RotateVectorAboutY(u_EnvMapRotation, Lr), (m_Params.Roughness) * u_EnvRadianceTexLevels).rgb;



	// Sample BRDF Lut, 1.0 - roughness for y-coord because texture was generated (in Sparky) for gloss model
	vec2 specularBRDF = texture(u_BRDFLUTTexture, vec2(m_Params.NdotV, 1.0 - m_Params.Roughness)).rg;
	vec3 specularIBL = (specularIrradiance) * (F0 * specularBRDF.x + specularBRDF.y);
	
	return kd * diffuseIBL + specularIBL;
}

// Shlick's approximation of the Fresnel factor.
vec3 fresnelSchlick(vec3 F0, float cosTheta)
{
	return F0 + (1.0 - F0) * pow(1.0 - cosTheta, 5.0);
}

const int LightCount = 1;
float ndfGGX(in float cosLh, in float roughness)
{
	const float alpha = roughness * roughness;
	const float alphaSq = alpha * alpha;

	const float denom = (cosLh * cosLh) * (alphaSq - 1.0) + 1.0;
	return alphaSq / (PI * denom * denom);
}

// Single term for separable Schlick-GGX below.
float gaSchlickG1(in float cosTheta, in float k)
{
	return cosTheta / (cosTheta * (1.0 - k) + k);
}

// Schlick-GGX approximation of geometric attenuation function using Smith's method.
float gaSchlickGGX(in float cosLi, in float NdotV, in float roughness)
{
	const float r = roughness + 1.0f;
	const float k = (r * r) / 8.0f; // Epic suggests using this roughness remapping for analytic lights.
	return gaSchlickG1(cosLi, k) * gaSchlickG1(NdotV, k);
}

vec3 DirLighting(in vec3 F0)
{
	vec3 result = vec3(0.0);
	for(int i = 0; i < LightCount; i++)
	{
		vec3 Li = u_DirectionalLight.Direction;
		vec3 Lradiance = u_DirectionalLight.Radiance * u_DirectionalLight.Multiplier;
		vec3 Lh = normalize(Li + m_Params.View);

		// Calculate angles between surface normal and various light vectors.
		float cosLi = max(0.0, dot(m_Params.Normal, Li));
		float cosLh = max(0.0, dot(m_Params.Normal, Lh));

		vec3 F = fresnelSchlick(F0, max(0.0, dot(Lh, m_Params.View)));
		float D = ndfGGX(cosLh, m_Params.Roughness);
		float G = gaSchlickGGX(cosLi, m_Params.NdotV, m_Params.Roughness);

		vec3 kd = (1.0 - F) * (1.0 - m_Params.Metalness);
		vec3 diffuseBRDF = kd * m_Params.Albedo;

		// Cook-Torrance
		vec3 specularBRDF = (F * D * G) / max(Epsilon, 4.0 * cosLi * m_Params.NdotV);

		result += (diffuseBRDF + specularBRDF) * Lradiance * cosLi;
	}
	return result;
}



vec3 PointLighting(in vec3 F0)
{

	vec3 result = vec3(0.0);

	ivec2 tileID = ivec2(gl_FragCoord) / ivec2(16, 16);
	uint index = tileID.y * numberOfTilesX + tileID.x;

	uint offset = index * 1024;
	for(int i = 0; i < u_PointLightsCount && visibleLightIndicesBuffer.indices[offset + i] != -1; i++)
	{
		uint lightIndex = visibleLightIndicesBuffer.indices[offset + i];
		PointLight light = pointLightsUniform[lightIndex];
		vec3 Li = normalize(light.Position - vs_Input.WorldPosition);
		float lightDistance = length(light.Position - vs_Input.WorldPosition);
		vec3 Lh = normalize(Li + m_Params.View);
		float attenuation = clamp(1.0 - lightDistance * lightDistance / (light.FarPlane * light.FarPlane), 0.0, 1.0); 
		attenuation *= attenuation;

		vec3 Lradiance = light.Radiance * light.Multiplier * attenuation;

		// Calculate angles between surface normal and various light vectors.
		float cosLi = max(0.0, dot(m_Params.Normal, Li));
		float cosLh = max(0.0, dot(m_Params.Normal, Lh));

		vec3 F = fresnelSchlick(F0, max(0.0, dot(Lh, m_Params.View)));
		float D = ndfGGX(cosLh, m_Params.Roughness);
		float G = gaSchlickGGX(cosLi, m_Params.NdotV, m_Params.Roughness);

		vec3 kd = (1.0 - F) * (1.0 - m_Params.Metalness);
		vec3 diffuseBRDF = kd * m_Params.Albedo;

		// Cook-Torrance
		vec3 specularBRDF = (F * D * G) / max(Epsilon, 4.0 * cosLi * m_Params.NdotV);
		result += (diffuseBRDF + specularBRDF) * Lradiance * cosLi;
	}
	return result;

}

uint CascadeIndex = 0;

float reconstructCSZ(float d, vec4 clipInfo) {
  if (clipInfo[3] != 0) {
    return (clipInfo[0] / (clipInfo[1] * d + clipInfo[2]));
  }
  else {
    return (clipInfo[1] + clipInfo[2] - d * clipInfo[1]);
  }
}


void main()
{    


	vec2 texCoords = vs_Input.TexCoord;
	vec3 viewDir = normalize(vs_Input.TangentViewPosition - (vs_Input.WorldPosition));
    if (bool(u_HeightTexToggle))
        texCoords = ParallaxMapping(vs_Input.TexCoord,  viewDir);       


    // Standard PBR inputs
	m_Params.Albedo = u_AlbedoTexToggle > 0.5 ? texture(u_AlbedoTexture, texCoords).rgb : u_AlbedoColor; 
	m_Params.Metalness = u_MetalnessTexToggle > 0.5 ? texture(u_MetalnessTexture, texCoords).r - u_Metalness : u_Metalness;
	m_Params.Roughness = u_RoughnessTexToggle > 0.5 ?  texture(u_RoughnessTexture, texCoords).r + u_Roughness : u_Roughness;

	MetalnessRoughness = vec4(m_Params.Metalness, m_Params.Roughness, 0.f, 1.f);

	m_Params.Roughness = max(m_Params.Roughness, 0.05); // Minimum roughness of 0.05 to keep specular highlight
	// Normals (either from vertex or map)
	m_Params.Normal = normalize(vs_Input.Normal);
	if (u_NormalTexToggle > 0.5)
	{
		m_Params.Normal = normalize(2.0 * texture(u_NormalTexture, texCoords).rgb - 1.0);
		m_Params.Normal = normalize(vs_Input.WorldNormals * m_Params.Normal);
	}

	

	// Normals (either from vertex or map)
	m_Params.View = normalize(u_CameraPosition - vs_Input.WorldPosition);
	m_Params.NdotV = max(dot(m_Params.Normal, m_Params.View), 0.0);
		
	// Specular reflection vector
	vec3 Lr = (2.0f * m_Params.NdotV * m_Params.Normal) - m_Params.View;
	
	// Fresnel reflectance, metals use albedo
	vec3 F0 = mix(Fdielectric, m_Params.Albedo, m_Params.Metalness);


	vec3 iblContribution = IBL(F0, Lr) * u_IBLContribution;

	vec3 lightContribution = u_DirectionalLight.Multiplier > 0.0f ? (DirLighting(F0)) : vec3(0.0f);
	
	lightContribution += PointLighting(F0);

	iblContribution *= u_SSAOToggle ? 2 : 1.f;

	
	vec3 result = (lightContribution + iblContribution);


	result *= u_SSAOToggle ? 2 : 1.f;

	float brightness = dot(result, vec3(0.2126, 0.7152, 0.0722));


	FragColor = vec4(result, 1.0);
	Position = u_ViewMatrix * vec4(vs_Input.WorldPosition, 1.f);
	Albedo = vec4(m_Params.Albedo, 1.f);
	Normal = vec4( normalize(mat3(u_ViewMatrix) * normalize(m_Params.Normal).rgb), 1.f);

//#if DEPTHLINEARIZE_MSAA
//	float depth = texelFetch(u_PreDepth, ivec2(gl_FragCoord.xy), sampleIndex).x;
//#else
//	float depth = texelFetch(u_PreDepth, ivec2(gl_FragCoord.xy), 0).x;
//#endif
  LinearizedDepth = -Position.z;

	
	if (u_ShowCascades)
	{
		switch(CascadeIndex)
		{
		case 0:
			FragColor.rgb *= vec3(1.0f, 0.25f, 0.25f);
			break;
		case 1:
			FragColor.rgb *= vec3(0.25f, 1.0f, 0.25f);
			break;
		case 2:
			FragColor.rgb *= vec3(0.25f, 0.25f, 1.0f);
			break;
		case 3:
			FragColor.rgb *= vec3(1.0f, 1.0f, 0.25f);
			break;
		}
	}


}