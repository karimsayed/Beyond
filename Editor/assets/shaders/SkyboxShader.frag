// Skybox shader
#version 460

layout(location = 0) out vec4 FragColor;
layout(location = 1) out vec4 Position;
layout(location = 2) out vec4 Albedo;
layout(location = 3) out vec4 Normal;
layout(location = 4) out vec4 MetalnessRoughness;

uniform samplerCube u_Texture;
uniform float u_TextureLod;
uniform float u_SkyIntensity;
uniform float u_Angle;
in vec3 v_Position;

vec3 RotateVectorAboutY(in float angle, in vec3 vec)
{
    angle = radians(angle);
    mat3x3 rotationMatrix = { vec3(cos(angle),0.0,sin(angle)),
                              vec3(0.0,1.0,0.0),
                              vec3(-sin(angle),0.0,cos(angle))};
    return rotationMatrix * vec;
}

void main()
{
	
	FragColor = textureLod(u_Texture, RotateVectorAboutY(u_Angle, v_Position), u_TextureLod) * u_SkyIntensity;
	Position = vec4(0);
} 