#include "pchs.h"
#include "Game.h"
#include "MyWinDef.hpp"

#include "Beyond/Renderer/Renderer.h"



#include <imgui.h>

#define GLFW_EXPOSE_NATIVE_WIN32
#include <ranges>
#include <GLFW/glfw3native.h>
#include "Instrumentor.h"



namespace bey {


	static int nbFrames = 0;
	static  int temp;
	static float lastTime = Game::getTime();

	Game* Game::sInstance = nullptr;

	Game::Game(const ApplicationProps& props)
	{
		BEY_PROFILE_FUNCTION();

		sInstance = this;

		mWindow = std::unique_ptr<Window>(Window::create(WindowProps(props.Name, props.WindowWidth, props.WindowHeight)));
		mWindow->setEventCallback([this](Event& event) { return Game::onEvent(event); });
		mWindow->setVSync(true);

		mImGuiLayer = new ImGuiLayer("ImGui");
		pushOverlay(mImGuiLayer);


		Renderer::init();
		Renderer::waitAndRender();

	}

	Game::~Game()
	{
		BEY_PROFILE_FUNCTION();
		Renderer::terminate();
	}

	void Game::pushLayer(Layer* layer)
	{
		BEY_PROFILE_FUNCTION();
		mLayerStack.pushLayer(layer);
		layer->onAttach();

	}

	void Game::pushOverlay(Layer* layer)
	{
		BEY_PROFILE_FUNCTION();
		mLayerStack.pushOverlay(layer);
		layer->onAttach();

	}

	unsigned Game::getFPS() const
	{
		++nbFrames;

		if (mLastFrameTime - lastTime >= 0.0f)
		{
			temp = nbFrames;
			nbFrames = 0;
			++lastTime;
		}
		return temp;
	}


	void Game::renderImGui()
	{
		BEY_PROFILE_FUNCTION();
		ImGuiLayer::begin();


		for (Layer* layer : mLayerStack)
			layer->onImGuiRender();


		ImGuiLayer::end();
	}

	void Game::run()
	{
		BEY_PROFILE_FUNCTION();

		onInit();
		while (mRunning)
		{
			if (!mMinimized)
			{
				for (Layer* layer : mLayerStack)
					layer->onUpdate(mTimeStep);

				// Render ImGui on render thread
				Game* app = this;
				Renderer::submit([app]() { app->renderImGui(); });
				Renderer::waitAndRender();
			}
			mWindow->onUpdate();

			const float time = getTime();
			mTimeStep = time - mLastFrameTime;
			mLastFrameTime = time;
		}
		onShutdown();
	}

	void Game::onEvent(Event& event)
	{
		BEY_PROFILE_FUNCTION();
		EventDispatcher dispatcher(event);
		dispatcher.dispatch<WindowMovedEvent>([this](WindowMovedEvent& e) { return  onWindowMove(e); });
		dispatcher.dispatch<WindowResizeEvent>([this](WindowResizeEvent& e) { return  onWindowResize(e); });
		dispatcher.dispatch<WindowCloseEvent>([this](WindowCloseEvent& e) {return onWindowClose(e); });

		for (auto it = mLayerStack.end(); it != mLayerStack.begin(); )
		{
			(*--it)->onEvent(event);
			if (event.Handled)
				break;
		}
	}

	bool Game::onWindowMove(WindowMovedEvent& e)
	{
		BEY_PROFILE_FUNCTION();

		return false;
	}

	bool Game::onWindowResize(WindowResizeEvent& e)
	{
		BEY_PROFILE_FUNCTION();
		const unsigned width = e.getWidth(), height = e.getHeight();
		if (width == 0 || height == 0)
		{
			mMinimized = true;
			return false;
		}
		mMinimized = false;
		Renderer::submit([=]() { glViewport(0, 0, width, height); });
		auto& fbs = FramebufferPool::getGlobal()->getAll();
		for (auto& framebuffer : fbs | std::ranges::views::filter([](Ref<FrameBuffer> fb) {return fb->getSpecification().IsResizable; }))
			framebuffer->resize(width, height);

		return false;
	}

	bool Game::onWindowClose(WindowCloseEvent&)
	{
		mRunning = false;
		return true;
	}

	std::string Game::openFile(const char* filter) const
	{
		BEY_PROFILE_FUNCTION();
		OPENFILENAMEA ofn;       // common dialog box structure
		CHAR szFile[260] = { 0 };       // if using TCHAR macros

		// Initialize OPENFILENAME
		ZeroMemory(&ofn, sizeof(OPENFILENAME));
		ofn.lStructSize = sizeof(OPENFILENAME);
		ofn.hwndOwner = glfwGetWin32Window(mWindow->getNativeWindow());
		ofn.lpstrFile = szFile;
		ofn.nMaxFile = sizeof(szFile);
		ofn.lpstrFilter = filter;
		ofn.nFilterIndex = 1;
		ofn.Flags = OFN_PATHMUSTEXIST | OFN_FILEMUSTEXIST | OFN_NOCHANGEDIR | OFN_DONTADDTORECENT;

		if (GetOpenFileNameA(&ofn) != FALSE)
			return ofn.lpstrFile;
		return std::string();
	}

	int Game::deleteFile(const char* path) const
	{
		BEY_PROFILE_FUNCTION();
		SHFILEOPSTRUCTA  SHFileOp;       // common dialog box structure

		// Initialize OPENFILENAME
		ZeroMemory(&SHFileOp, sizeof(SHFILEOPSTRUCT));
		SHFileOp.hwnd = glfwGetWin32Window(mWindow->getNativeWindow());
		SHFileOp.fFlags = FOF_ALLOWUNDO;
		SHFileOp.pFrom = path;
		SHFileOp.wFunc = FO_DELETE;

		return SHFileOperationA(&SHFileOp);
	}

	std::string Game::saveFile(const char* filter) const
	{
		BEY_PROFILE_FUNCTION();
		OPENFILENAMEA ofn;       // common dialog box structure
		CHAR szFile[FILENAME_MAX] = { 0 };       // if using TCHAR macros

		// Initialize OPENFILENAME
		ZeroMemory(&ofn, sizeof(OPENFILENAME));
		ofn.lStructSize = sizeof(OPENFILENAME);
		ofn.hwndOwner = glfwGetWin32Window(mWindow->getNativeWindow());
		ofn.lpstrFile = szFile;
		ofn.nMaxFile = sizeof(szFile);
		ofn.lpstrFilter = filter;
		ofn.nFilterIndex = 1;
		ofn.Flags = OFN_PATHMUSTEXIST | OFN_FILEMUSTEXIST | OFN_NOCHANGEDIR;

		if (GetSaveFileNameA(&ofn))
		{
			return ofn.lpstrFile;
		}
		return std::string();
	}

	float Game::getTime()
	{
		return float(glfwGetTime());
	}

}
