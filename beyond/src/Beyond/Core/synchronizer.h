#pragma once
#include <atomic>
class Synchronizer
{

	inline static std::atomic_bool mSwappedBuffers = true;

public: 
	static void setSwapped(const bool enabled)
	{
		mSwappedBuffers = enabled;
	}

	static bool getSwapped()
	{
		return mSwappedBuffers;
	}
};

