#pragma once

#include "Instrumentor.h"
#include "Beyond/Core/Base.h"

namespace bey {

	struct Buffer
	{
		std::uint8_t* Data;
		uint64_t Size;

		constexpr Buffer() noexcept
			: Data(nullptr), Size(0)
		{
		}

		Buffer(std::uint8_t* data, const uint32_t size) noexcept
			: Data(data), Size(size)
		{
		}

		static Buffer copy(void* data, const uint32_t size) noexcept
		{
			BEY_PROFILE_FUNCTION();
			Buffer buffer;
			buffer.allocate(size);
			memcpy(buffer.Data, data, size);
			return buffer;
		}

		void allocate(const uint32_t size) noexcept
		{
			BEY_PROFILE_FUNCTION();
			delete[] Data;
			Data = nullptr;

			if (size == 0)
				return;

			Data = new std::uint8_t[size_t(size)];
			Size = size;
		}

		void zeroInitialize() const noexcept
		{
			BEY_PROFILE_FUNCTION();
			if (Data)
				memset(Data, 0, Size);
		}

		template<typename T>
		T& read(const uint32_t offset = 0)
		{
			return *reinterpret_cast<T*>(Data + offset);
		}

		void write(void* data, const uint64_t size, const uint64_t offset = 0) const noexcept
		{
			BEY_PROFILE_FUNCTION();
			BEY_CORE_ASSERT(offset + size <= Size, "Buffer overflow!");
			memcpy(Data + offset, data, size);
		}

		explicit operator bool() const noexcept
		{
			return Data;
		}

		std::uint8_t& operator[](const size_t index) noexcept
		{
			return Data[index];
		}

		std::uint8_t operator[](const size_t index) const noexcept
		{
			return Data[index];
		}

		template<typename T>
		T* as() noexcept
		{
			return reinterpret_cast<T*>(Data);
		}

		[[nodiscard]] uint64_t getSize() const noexcept { return Size; }
	};

}