#include "pchs.h"
#include "Base.h"

#include "Log.h"


namespace bey {

	void InitializeCore()
	{
		Log::init();

		BEY_CORE_TRACE("Beyond Engine");
		BEY_CORE_TRACE("Initializing...");
	}

	void ShutdownCore()
	{
		BEY_CORE_TRACE("Shutting down...");
	}

}