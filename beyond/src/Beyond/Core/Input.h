#pragma once

#include "KeyCodes.h"
#include <imgui.h>
#include <glm/vec2.hpp>

namespace bey {

	class Input
	{
	public:
		static bool isKeyPressed(KeyCode keycode);
		static bool isMouseButtonPressed(int button);
		static bool isMouseButtonReleased(int button);
		static float getMouseX();
		static float getMouseY();
		static ImVec2 getMousePositionImGui();
		static glm::vec2 getMousePosition();
	};

}