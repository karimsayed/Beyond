#pragma once

#include "Beyond/Core/TimeStep.h"
#include "Beyond/Core/Events/Event.h"
#include <string>

namespace bey {

	class Layer
	{
	public:
		explicit Layer(const std::string& debugName = "Layer");
		virtual ~Layer();

		virtual void onAttach() {}
		virtual void onDetach() {}
		virtual void onUpdate(TimeStep) {}
		virtual void onImGuiRender() {}
		virtual void onEvent(Event&) {}

		[[nodiscard]] const std::string& getName() const { return mDebugName; }
	protected:
		std::string mDebugName;
	};

}