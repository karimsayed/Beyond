#pragma once

#include "Base.h"
#include <xhash>

namespace bey {

	// "UUID" (universally unique identifier) or GUID is (usually) a 128-bit integer
	// used to "uniquely" identify information. In Beyond, even though we use the term
	// GUID and UUID, at the moment we're simply using a randomly generated 64-bit
	// integer, as the possibility of a clash is low enough for now.
	// This may change in the future.
	class UUID
	{
	public:
		UUID();
		UUID(uint32_t uuid);
		UUID(const UUID& other);

		UUID& operator= (const UUID& uuid);

		operator uint32_t& () { return mUUID; }
		operator const uint32_t() const { return mUUID; }
	private:
		uint32_t mUUID;
	};

}

namespace std {

	template <>
	struct hash<bey::UUID>
	{
		size_t operator()(const bey::UUID& uuid) const noexcept
		{
			return hash<uint32_t>()(uint32_t(uuid));
		}
	};
}