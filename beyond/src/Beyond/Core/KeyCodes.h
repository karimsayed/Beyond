#pragma once

namespace bey
{
	typedef enum class KeyCode : uint16_t
	{
		// From glfw3.h
		Space = 32,
		Apostrophe = 39, /* ' */
		Comma = 44, /* , */
		Minus = 45, /* - */
		Period = 46, /* . */
		Slash = 47, /* / */

		D0 = 48, /* 0 */
		D1 = 49, /* 1 */
		D2 = 50, /* 2 */
		D3 = 51, /* 3 */
		D4 = 52, /* 4 */
		D5 = 53, /* 5 */
		D6 = 54, /* 6 */
		D7 = 55, /* 7 */
		D8 = 56, /* 8 */
		D9 = 57, /* 9 */

		Semicolon = 59, /* ; */
		Equal = 61, /* = */

		A = 65,
		B = 66,
		C = 67,
		D = 68,
		E = 69,
		F = 70,
		G = 71,
		H = 72,
		I = 73,
		J = 74,
		K = 75,
		L = 76,
		M = 77,
		N = 78,
		O = 79,
		P = 80,
		Q = 81,
		R = 82,
		S = 83,
		T = 84,
		U = 85,
		V = 86,
		W = 87,
		X = 88,
		Y = 89,
		Z = 90,

		LeftBracket = 91,  /* [ */
		Backslash = 92,  /* \ */
		RightBracket = 93,  /* ] */
		GraveAccent = 96,  /* ` */

		World1 = 161, /* non-US #1 */
		World2 = 162, /* non-US #2 */

		/* Function keys */
		Escape = 256,
		Enter = 257,
		Tab = 258,
		Backspace = 259,
		Insert = 260,
		Delete = 261,
		Right = 262,
		Left = 263,
		Down = 264,
		Up = 265,
		PageUp = 266,
		PageDown = 267,
		Home = 268,
		End = 269,
		CapsLock = 280,
		ScrollLock = 281,
		NumLock = 282,
		PrintScreen = 283,
		Pause = 284,
		F1 = 290,
		F2 = 291,
		F3 = 292,
		F4 = 293,
		F5 = 294,
		F6 = 295,
		F7 = 296,
		F8 = 297,
		F9 = 298,
		F10 = 299,
		F11 = 300,
		F12 = 301,
		F13 = 302,
		F14 = 303,
		F15 = 304,
		F16 = 305,
		F17 = 306,
		F18 = 307,
		F19 = 308,
		F20 = 309,
		F21 = 310,
		F22 = 311,
		F23 = 312,
		F24 = 313,
		F25 = 314,

		/* Keypad */
		KP0 = 320,
		KP1 = 321,
		KP2 = 322,
		KP3 = 323,
		KP4 = 324,
		KP5 = 325,
		KP6 = 326,
		KP7 = 327,
		KP8 = 328,
		KP9 = 329,
		KPDecimal = 330,
		KPDivide = 331,
		KPMultiply = 332,
		KPSubtract = 333,
		KPAdd = 334,
		KPEnter = 335,
		KPEqual = 336,

		LeftShift = 340,
		LeftControl = 341,
		LeftAlt = 342,
		LeftSuper = 343,
		RightShift = 344,
		RightControl = 345,
		RightAlt = 346,
		RightSuper = 347,
		Menu = 348
	} Key;

	inline std::ostream& operator<<(std::ostream& os, KeyCode keyCode)
	{
		os << static_cast<int32_t>(keyCode);
		return os;
	}
}

// From glfw3.h
#define BEY_KEY_SPACE           ::bey::Key::Space
#define BEY_KEY_APOSTROPHE      ::bey::Key::Apostrophe    /* ' */
#define BEY_KEY_COMMA           ::bey::Key::Comma         /* , */
#define BEY_KEY_MINUS           ::bey::Key::Minus         /* - */
#define BEY_KEY_PERIOD          ::bey::Key::Period        /* . */
#define BEY_KEY_SLASH           ::bey::Key::Slash         /* / */
#define BEY_KEY_0               ::bey::Key::D0
#define BEY_KEY_1               ::bey::Key::D1
#define BEY_KEY_2               ::bey::Key::D2
#define BEY_KEY_3               ::bey::Key::D3
#define BEY_KEY_4               ::bey::Key::D4
#define BEY_KEY_5               ::bey::Key::D5
#define BEY_KEY_6               ::bey::Key::D6
#define BEY_KEY_7               ::bey::Key::D7
#define BEY_KEY_8               ::bey::Key::D8
#define BEY_KEY_9               ::bey::Key::D9
#define BEY_KEY_SEMICOLON       ::bey::Key::Semicolon     /* ; */
#define BEY_KEY_EQUAL           ::bey::Key::Equal         /* = */
#define BEY_KEY_A               ::bey::Key::A
#define BEY_KEY_B               ::bey::Key::B
#define BEY_KEY_C               ::bey::Key::C
#define BEY_KEY_D               ::bey::Key::D
#define BEY_KEY_E               ::bey::Key::E
#define BEY_KEY_F               ::bey::Key::F
#define BEY_KEY_G               ::bey::Key::G
#define BEY_KEY_H               ::bey::Key::H
#define BEY_KEY_I               ::bey::Key::I
#define BEY_KEY_J               ::bey::Key::J
#define BEY_KEY_K               ::bey::Key::K
#define BEY_KEY_L               ::bey::Key::L
#define BEY_KEY_M               ::bey::Key::M
#define BEY_KEY_N               ::bey::Key::N
#define BEY_KEY_O               ::bey::Key::O
#define BEY_KEY_P               ::bey::Key::P
#define BEY_KEY_Q               ::bey::Key::Q
#define BEY_KEY_R               ::bey::Key::R
#define BEY_KEY_S               ::bey::Key::S
#define BEY_KEY_T               ::bey::Key::T
#define BEY_KEY_U               ::bey::Key::U
#define BEY_KEY_V               ::bey::Key::V
#define BEY_KEY_W               ::bey::Key::W
#define BEY_KEY_X               ::bey::Key::X
#define BEY_KEY_Y               ::bey::Key::Y
#define BEY_KEY_Z               ::bey::Key::Z
#define BEY_KEY_LEFT_BRACKET    ::bey::Key::LeftBracket   /* [ */
#define BEY_KEY_BACKSLASH       ::bey::Key::Backslash     /* \ */
#define BEY_KEY_RIGHT_BRACKET   ::bey::Key::RightBracket  /* ] */
#define BEY_KEY_GRAVE_ACCENT    ::bey::Key::GraveAccent   /* ` */
#define BEY_KEY_WORLD_1         ::bey::Key::World1        /* non-US #1 */
#define BEY_KEY_WORLD_2         ::bey::Key::World2        /* non-US #2 */

/* Function keys */
#define BEY_KEY_ESCAPE          ::bey::Key::Escape
#define BEY_KEY_ENTER           ::bey::Key::Enter
#define BEY_KEY_TAB             ::bey::Key::Tab
#define BEY_KEY_BACKSPACE       ::bey::Key::Backspace
#define BEY_KEY_INSERT          ::bey::Key::Insert
#define BEY_KEY_DELETE          ::bey::Key::Delete
#define BEY_KEY_RIGHT           ::bey::Key::Right
#define BEY_KEY_LEFT            ::bey::Key::Left
#define BEY_KEY_DOWN            ::bey::Key::Down
#define BEY_KEY_UP              ::bey::Key::Up
#define BEY_KEY_PAGE_UP         ::bey::Key::PageUp
#define BEY_KEY_PAGE_DOWN       ::bey::Key::PageDown
#define BEY_KEY_HOME            ::bey::Key::Home
#define BEY_KEY_END             ::bey::Key::End
#define BEY_KEY_CAPS_LOCK       ::bey::Key::CapsLock
#define BEY_KEY_SCROLL_LOCK     ::bey::Key::ScrollLock
#define BEY_KEY_NUM_LOCK        ::bey::Key::NumLock
#define BEY_KEY_PRINT_SCREEN    ::bey::Key::PrintScreen
#define BEY_KEY_PAUSE           ::bey::Key::Pause
#define BEY_KEY_F1              ::bey::Key::F1
#define BEY_KEY_F2              ::bey::Key::F2
#define BEY_KEY_F3              ::bey::Key::F3
#define BEY_KEY_F4              ::bey::Key::F4
#define BEY_KEY_F5              ::bey::Key::F5
#define BEY_KEY_F6              ::bey::Key::F6
#define BEY_KEY_F7              ::bey::Key::F7
#define BEY_KEY_F8              ::bey::Key::F8
#define BEY_KEY_F9              ::bey::Key::F9
#define BEY_KEY_F10             ::bey::Key::F10
#define BEY_KEY_F11             ::bey::Key::F11
#define BEY_KEY_F12             ::bey::Key::F12
#define BEY_KEY_F13             ::bey::Key::F13
#define BEY_KEY_F14             ::bey::Key::F14
#define BEY_KEY_F15             ::bey::Key::F15
#define BEY_KEY_F16             ::bey::Key::F16
#define BEY_KEY_F17             ::bey::Key::F17
#define BEY_KEY_F18             ::bey::Key::F18
#define BEY_KEY_F19             ::bey::Key::F19
#define BEY_KEY_F20             ::bey::Key::F20
#define BEY_KEY_F21             ::bey::Key::F21
#define BEY_KEY_F22             ::bey::Key::F22
#define BEY_KEY_F23             ::bey::Key::F23
#define BEY_KEY_F24             ::bey::Key::F24
#define BEY_KEY_F25             ::bey::Key::F25

/* Keypad */
#define BEY_KEY_KP_0            ::bey::Key::KP0
#define BEY_KEY_KP_1            ::bey::Key::KP1
#define BEY_KEY_KP_2            ::bey::Key::KP2
#define BEY_KEY_KP_3            ::bey::Key::KP3
#define BEY_KEY_KP_4            ::bey::Key::KP4
#define BEY_KEY_KP_5            ::bey::Key::KP5
#define BEY_KEY_KP_6            ::bey::Key::KP6
#define BEY_KEY_KP_7            ::bey::Key::KP7
#define BEY_KEY_KP_8            ::bey::Key::KP8
#define BEY_KEY_KP_9            ::bey::Key::KP9
#define BEY_KEY_KP_DECIMAL      ::bey::Key::KPDecimal
#define BEY_KEY_KP_DIVIDE       ::bey::Key::KPDivide
#define BEY_KEY_KP_MULTIPLY     ::bey::Key::KPMultiply
#define BEY_KEY_KP_SUBTRACT     ::bey::Key::KPSubtract
#define BEY_KEY_KP_ADD          ::bey::Key::KPAdd
#define BEY_KEY_KP_ENTER        ::bey::Key::KPEnter
#define BEY_KEY_KP_EQUAL        ::bey::Key::KPEqual

#define BEY_KEY_LEFT_SHIFT      ::bey::Key::LeftShift
#define BEY_KEY_LEFT_CONTROL    ::bey::Key::LeftControl
#define BEY_KEY_LEFT_ALT        ::bey::Key::LeftAlt
#define BEY_KEY_LEFT_SUPER      ::bey::Key::LeftSuper
#define BEY_KEY_RIGHT_SHIFT     ::bey::Key::RightShift
#define BEY_KEY_RIGHT_CONTROL   ::bey::Key::RightControl
#define BEY_KEY_RIGHT_ALT       ::bey::Key::RightAlt
#define BEY_KEY_RIGHT_SUPER     ::bey::Key::RightSuper
#define BEY_KEY_MENU            ::bey::Key::Menu

// Mouse (TODO: move into separate file probably)
#define BEY_MOUSE_BUTTON_LEFT    0
#define BEY_MOUSE_BUTTON_RIGHT   1
#define BEY_MOUSE_BUTTON_MIDDLE  2