#pragma once

#ifdef BEY_DEBUG
#define BEY_ENABLE_ASSERTS
#endif

#ifdef BEY_ENABLE_ASSERTS
#define BEY_ASSERT_NO_MESSAGE(condition) { \
	if(!(condition)) {					   \
		BEY_ERROR("Assertion Failed"); 	   \
	if (!IsDebuggerPresent()) 			   \
		system("pause"); 				   \
	__debugbreak(); 					   \
	} 									   \
}

#define BEY_ASSERT_MESSAGE(condition, ...) {				\
	if(!(condition)) {										\
		BEY_ERROR("Assertion Failed: {0}", __VA_ARGS__);	\
	if (!IsDebuggerPresent())								\
		system("pause");									\
	else                                                    \
		__debugbreak();										\
	} 														\
}

#define BEY_ASSERT_RESOLVE(arg1, arg2, macro, ...) macro
#define BEY_GET_ASSERT_MACRO(...) BEY_EXPAND_VARGS(BEY_ASSERT_RESOLVE(__VA_ARGS__, BEY_ASSERT_MESSAGE, BEY_ASSERT_NO_MESSAGE))

#define BEY_ASSERT(...) BEY_EXPAND_VARGS( BEY_GET_ASSERT_MACRO(__VA_ARGS__)(__VA_ARGS__) )
#define BEY_CORE_ASSERT(...) BEY_EXPAND_VARGS( BEY_GET_ASSERT_MACRO(__VA_ARGS__)(__VA_ARGS__) )
#else
#define BEY_ASSERT(...)
#define BEY_CORE_ASSERT(...)
#endif