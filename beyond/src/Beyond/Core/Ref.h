#pragma once


namespace bey {

	class RefCounted
	{
	public:
		void incRefCount() const
		{
			mRefCount++;
		}
		void decRefCount() const
		{
			mRefCount--;
		}

		uint32_t getRefCount() const { return mRefCount; }
	private:
		mutable uint32_t mRefCount = 0; // TODO: atomic
	};

	template<typename T>
	class Ref
	{
	public:
		Ref()
			: mInstance(nullptr)
		{
		}

		explicit Ref(std::nullptr_t n)
			: mInstance(nullptr)
		{
		}

		Ref(T* instance) requires std::is_base_of<RefCounted, T>::value
			: mInstance(instance)
		{
			incRef();
		}

		template<typename T2>
		Ref(const Ref<T2>& other) requires std::is_base_of<RefCounted, T>::value
		{
			mInstance = static_cast<T*>(other.mInstance);
			incRef();
		}

		template<typename T2>
		Ref(Ref<T2>&& other) requires std::is_base_of<RefCounted, T>::value
		{
			mInstance = static_cast<T*>(other.mInstance);
			other.mInstance = nullptr;
		}

		~Ref()
		{
			decRef();
		}

		Ref(const Ref<T>& other) requires std::is_base_of<RefCounted, T>::value
			: mInstance(other.mInstance)
		{
			incRef();
		}

		bool operator==(const Ref& other) const requires std::is_base_of<RefCounted, T>::value
		{
			return mInstance == other.mInstance;
		}

		Ref& operator=(std::nullptr_t)
		{
			decRef();
			mInstance = nullptr;
			return *this;
		}

		Ref& operator=(const Ref<T>& other) requires std::is_base_of<RefCounted, T>::value
		{
			other.incRef();
			decRef();

			mInstance = other.mInstance;
			return *this;
		}

		template<typename T2>
		Ref& operator=(const Ref<T2>& other) requires std::is_base_of<RefCounted, T>::value
		{
			other.incRef();
			decRef();

			mInstance = other.mInstance;
			return *this;
		}

		template<typename T2>
		Ref& operator=(Ref<T2>&& other) requires std::is_base_of<RefCounted, T>::value
		{
			decRef();

			mInstance = other.mInstance;
			other.mInstance = nullptr;
			return *this;
		}

		explicit operator bool() { return mInstance != nullptr; }
		explicit operator bool() const { return mInstance != nullptr; }

		T* operator->() { return mInstance; }
		const T* operator->() const { return mInstance; }

		T& operator*() { return *mInstance; }
		const T& operator*() const { return *mInstance; }

		T* raw() { return  mInstance; }
		[[nodiscard]] const T* raw() const { return  mInstance; }

		void reset(T* instance = nullptr)
		{
			decRef();
			mInstance = instance;
		}

		template<typename... Args>
		static Ref<T> create(Args&&... args)
		{
			return Ref<T>(new T(std::forward<Args>(args)...));
		}
	private:
		void incRef() const
		{
			if (mInstance)
				mInstance->incRefCount();
		}

		void decRef() const
		{
			if (mInstance)
			{
				mInstance->decRefCount();
				if (mInstance->getRefCount() == 0)
				{
					delete mInstance;
				}
			}
		}

		template<class T2>
		friend class Ref;
		T* mInstance;
	};

	// TODO: WeakRef

}