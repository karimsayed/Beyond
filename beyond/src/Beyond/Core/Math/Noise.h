#pragma once
#include "FastNoiseSIMD.h"

namespace bey {

	class Noise
	{
	public:
		explicit Noise(const FastNoiseSIMD::NoiseType type);
		static float* perlinNoise(const float x0, const float y0, const float x1, const float y1);
	};

}