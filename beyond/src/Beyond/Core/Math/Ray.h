#pragma once


#include <glm/common.hpp>
#include <glm/geometric.hpp>
#include <glm/vec3.hpp>

#include "AABB.h"
#include "Beyond/Core/Instrumentor.h"

namespace bey {

    struct Ray
    {
        glm::vec3 Origin, Direction;

        Ray(const glm::vec3& origin, const glm::vec3& direction)
        {
            BEY_PROFILE_FUNCTION();
            Origin = origin;
            Direction = direction;
        }

        static Ray zero()
        {
            return { {0.0f, 0.0f, 0.0f},{0.0f, 0.0f, 0.0f} };
        }

        bool intersectsAABB(const AABB& aabb, float& t) const
        {
            BEY_PROFILE_FUNCTION();
            glm::vec3 dirfrac;
            // r.dir is unit direction vector of ray
            dirfrac.x = 1.0f / Direction.x;
            dirfrac.y = 1.0f / Direction.y;
            dirfrac.z = 1.0f / Direction.z;
            // lb is the corner of AABB with minimal coordinates - left bottom, rt is maximal corner
            // r.org is origin of ray
            const glm::vec3& lb = aabb.Min;
            const glm::vec3& rt = aabb.Max;
            const float t1 = (lb.x - Origin.x) * dirfrac.x;
            const float t2 = (rt.x - Origin.x) * dirfrac.x;
            const float t3 = (lb.y - Origin.y) * dirfrac.y;
            const float t4 = (rt.y - Origin.y) * dirfrac.y;
            const float t5 = (lb.z - Origin.z) * dirfrac.z;
            const float t6 = (rt.z - Origin.z) * dirfrac.z;

            const float tmin = glm::max(glm::max(glm::min(t1, t2), glm::min(t3, t4)), glm::min(t5, t6));
            const float tmax = glm::min(glm::min(glm::max(t1, t2), glm::max(t3, t4)), glm::max(t5, t6));

            // if tmax < 0, ray (line) is intersecting AABB, but the whole AABB is behind us
            if (tmax < 0)
            {
                t = tmax;
                return false;
            }

            // if tmin > tmax, ray doesn't intersect AABB
            if (tmin > tmax)
            {
                t = tmax;
                return false;
            }

            t = tmin;
            return true;
        }

		bool intersectsTriangle(const glm::vec3& a, const glm::vec3& b, const glm::vec3& c, float& t) const
		{
            BEY_PROFILE_FUNCTION();
	        const glm::vec3 E1 = b - a;
	        const glm::vec3 E2 = c - a;
	        const glm::vec3 N = cross(E1, E2);
	        const float det = -glm::dot(Direction, N);
			const float invDet = 1.0f / det;
	        const glm::vec3 AO = Origin - a;
	        const glm::vec3 DAO = glm::cross(AO, Direction);
			const float u = glm::dot(E2, DAO) * invDet;
			const float v = -glm::dot(E1, DAO) * invDet;
			t = dot(AO, N) * invDet;
			return (det >= 1e-6f && t >= 0.0f && u >= 0.0f && v >= 0.0f && (u + v) <= 1.0f);
		}

    };

}