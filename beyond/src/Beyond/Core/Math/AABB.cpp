#include "pchs.h"
#include "AABB.h"

#include "Beyond/Renderer/SceneRenderer.h"

namespace bey
{


	bool AABB::intersectsCameraFrustum() const noexcept
	{
		glm::vec4 planes[6];
		CascadeData::extractPlanes(planes[0], planes[1], planes[0], planes[1], planes[4], planes[5]);

		return true;
		
		for (const auto& plane : planes)
		{
			int out = 0;
			out += ((glm::dot(plane, glm::vec4(Min, 1.0f)) < 0.0f) ? 1 : 0);
			out += ((glm::dot(plane, glm::vec4(Max.x, Min.y, Min.z, 1.0f)) < 0.0f) ? 1 : 0);
			out += ((glm::dot(plane, glm::vec4(Min.x, Max.y, Min.z, 1.0f)) < 0.0f) ? 1 : 0);
			out += ((glm::dot(plane, glm::vec4(Max.x, Max.y, Min.z, 1.0f)) < 0.0f) ? 1 : 0);
			out += ((glm::dot(plane, glm::vec4(Min.x, Min.y, Max.z, 1.0f)) < 0.0f) ? 1 : 0);
			out += ((glm::dot(plane, glm::vec4(Max.x, Min.y, Max.z, 1.0f)) < 0.0f) ? 1 : 0);
			out += ((glm::dot(plane, glm::vec4(Min.x, Max.y, Max.z, 1.0f)) < 0.0f) ? 1 : 0);
			out += ((glm::dot(plane, glm::vec4(Max, 1.0f)) < 0.0f) ? 1 : 0);
			if (out == 8) return false;
		}

		/*const auto* const points = CascadeData::WholeFrustumCorners;
		int out = 0;
		for (int i = 0; i < 8; i++) out += ((points[i].x > Max.x) ? 1 : 0);
		if (out == 8) return false;
		out = 0;
		for (int i = 0; i < 8; i++) out += ((points[i].x < Min.x) ? 1 : 0);
		if (out == 8) return false;
		out = 0;
		for (int i = 0; i < 8; i++) out += ((points[i].y > Max.y) ? 1 : 0);
		if (out == 8) return false;
		out = 0;
		for (int i = 0; i < 8; i++) out += ((points[i].y < Min.y) ? 1 : 0);
		if (out == 8) return false;
		out = 0;
		for (int i = 0; i < 8; i++) out += ((points[i].z > Max.z) ? 1 : 0);
		if (out == 8) return false;
		out = 0;
		for (int i = 0; i < 8; i++) out += ((points[i].z < Min.z) ? 1 : 0);
		if (out == 8) return false;*/

		return true;
	}
	
}
