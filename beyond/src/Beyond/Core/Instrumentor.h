#pragma once

#include <chrono>
#include <fstream>
#include <string>
#include <thread>
#include <mutex>


namespace bey {

	using FloatingPointMicroseconds = std::chrono::duration<double, std::micro>;
	struct ProfileResult
	{
		std::string Name;
		
		FloatingPointMicroseconds Start;
		std::chrono::microseconds ElapsedTime;
		std::thread::id ThreadID;
	};

	struct InstrumentationSession
	{
		std::string Name;
	};

	class Instrumentor
	{
	public:
		Instrumentor(const Instrumentor&) = delete;
		Instrumentor(Instrumentor&&) = delete;
		auto operator=(Instrumentor&) = delete;
		auto operator=(Instrumentor&&) = delete;
		[[nodiscard]] const char* getName() const;

		static void toggle(const char* name);
		static void refresh(const char* name);

		void beginSession(const std::string& name, const std::string& filepath) noexcept;

		void endSession() noexcept;

		void writeProfile(const ProfileResult& result) noexcept;

		static Instrumentor& get() noexcept
		{
			static Instrumentor instance;
			return instance;
		}
		inline static bool Enabled = false;

	private:
		Instrumentor() noexcept
			: mCurrentSession(nullptr)
		{
		}

		~Instrumentor() noexcept;

		void writeHeader() noexcept;

		void writeFooter() noexcept;

		// Note: you must already own lock on m_Mutex before
		// calling InternalEndSession()
		void internalEndSession() noexcept;

	private:
		InstrumentationSession* mCurrentSession;
		std::mutex mMutex;
		std::ofstream mOutputStream;

		struct OutputBuffer
		{
			OutputBuffer();

			~OutputBuffer();

			void* allocate(const char* func, const size_t size);


			[[nodiscard]] char* get() const { return mBuffer; }
			void reset()
			{
				mCommandCount = 0;
				mCommandBufferPtr = mBuffer;
			}

			bool canFit(const std::string_view& stringView) const;

		private:
			char* const mBuffer = new char[100 * 1024 * 1024];
			char* mCommandBufferPtr;
			const char* const mCommandBufferEnd = mBuffer + 100 * 1024 * 1024;
			uint32_t mCommandCount = 0;
		} inline static mOutputBuffer;

	};



	class InstrumentationTimer
	{
	public:
		explicit InstrumentationTimer(const char* name) noexcept
			: mName(name), mStopped(false)
		{
			mStartTimepoint = std::chrono::steady_clock::now();
			if (!Instrumentor::Enabled) mStopped = true;

		}

		~InstrumentationTimer() noexcept
		{
			if (!mStopped)
				stop();
		}

		void stop() noexcept;
	private:
		const char* mName;
		std::chrono::time_point<std::chrono::steady_clock> mStartTimepoint;
		bool mStopped;
	};

	namespace instrumentor_utils {

		template <size_t N>
		struct ChangeResult
		{
			char Data[N];
		};

		template <size_t N, size_t K>
		constexpr auto cleanupOutputString(const char(&expr)[N], const char(&remove)[K]) noexcept
		{
			ChangeResult<N> result = {};

			size_t srcIndex = 0;
			size_t dstIndex = 0;
			while (srcIndex < N)
			{
				size_t matchIndex = 0;
				while (matchIndex < K - 1 && srcIndex + matchIndex < N - 1 && expr[srcIndex + matchIndex] == remove[matchIndex])
					matchIndex++;
				if (matchIndex == K - 1)
					srcIndex += matchIndex;
				result.Data[dstIndex++] = expr[srcIndex] == '"' ? '\'' : expr[srcIndex];
				srcIndex++;
			}
			return result;
		}


	}
}

#define BEY_PROFILE 1
#if BEY_PROFILE
// Resolve which function signature macro will be used. Note that this only
// is resolved when the (pre)compiler starts, so the syntax highlighting
// could mark the wrong one in your editor!
#if defined(__GNUC__) || (defined(__MWERKS__) && (__MWERKS__ >= 0x3000)) || (defined(__ICC) && (__ICC >= 600)) || defined(__ghs__)
#define BEY_FUNC_SIG __PRETTY_FUNCTION__
#elif defined(__DMC__) && (__DMC__ >= 0x810)
#define BEY_FUNC_SIG __PRETTY_FUNCTION__
#elif (defined(__FUNCSIG__) || (_MSC_VER))
#define BEY_FUNC_SIG __FUNCSIG__
#elif (defined(__INTEL_COMPILER) && (__INTEL_COMPILER >= 600)) || (defined(__IBMCPP__) && (__IBMCPP__ >= 500))
#define BEY_FUNC_SIG __FUNCTION__
#elif defined(__BORLANDC__) && (__BORLANDC__ >= 0x550)
#define BEY_FUNC_SIG __FUNC__
#elif defined(__STDC_VERSION__) && (__STDC_VERSION__ >= 199901)
#define BEY_FUNC_SIG __func__
#elif defined(__cplusplus) && (__cplusplus >= 201103)
#define BEY_FUNC_SIG __func__
#else
#define BEY_FUNC_SIG "BEY_FUNC_SIG unknown!"
#endif

#define BEY_PROFILE_ENABLED ::bey::Instrumentor::Enabled
#define BEY_PROFILE_TOGGLE(...) ::bey::Instrumentor::toggle(__VA_ARGS__)
#define BEY_PROFILE_REFRESH(...) ::bey::Instrumentor::refresh(__VA_ARGS__)



#define BEY_PROFILE_BEGIN_SESSION(name, filepath) ::bey::Instrumentor::get().beginSession(name, filepath)
#define BEY_PROFILE_END_SESSION() ::bey::Instrumentor::get().endSession()





#define BEY_PROFILE_SCOPE_LINE(name, line) BEY_PROFILE_SCOPE_LINE2(name, line)
#define BEY_PROFILE_SCOPE_LINE2(name, line)  constexpr auto fixedName##line = ::bey::instrumentor_utils::cleanupOutputString(name, "__cdecl ");\
											   ::bey::InstrumentationTimer timer##line(fixedName##line.Data)




#define BEY_PROFILE_SCOPE(name)   BEY_PROFILE_SCOPE_LINE(name, __LINE__)
#define BEY_PROFILE_FUNCTION() BEY_PROFILE_SCOPE(BEY_FUNC_SIG)
#else

#define BEY_PROFILE_BEGIN_SESSION(name, filepath)
#define BEY_PROFILE_END_SESSION()
#define BEY_PROFILE_SCOPE(name)
#define BEY_PROFILE_FUNCTION()
#define BEY_PROFILE_TOGGLE()
#define BEY_PROFILE_ENABLED (false)

#endif