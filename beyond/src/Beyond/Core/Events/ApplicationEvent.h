#pragma once

#include "Event.h"

#include <sstream>

namespace bey {

	// TODO: Should this store previous size?
	class WindowResizeEvent final : public Event
	{
	public:
		WindowResizeEvent(const unsigned int width, const unsigned int height)
			: mWidth(width), mHeight(height) {}

		[[nodiscard]] unsigned int getWidth() const { return mWidth; }
		[[nodiscard]] unsigned int getHeight() const { return mHeight; }

		[[nodiscard]] std::string toString() const override
		{
			std::stringstream ss;
			ss << "WindowResizeEvent: " << mWidth << ", " << mHeight;
			return ss.str();
		}

		EVENT_CLASS_TYPE(WindowResize)
			EVENT_CLASS_CATEGORY(EventCategory::EventCategoryApplication)
	private:
		unsigned int mWidth, mHeight;
	};

	class WindowMovedEvent final : public Event
	{
	public:
		WindowMovedEvent(const unsigned int posX, const unsigned int posY)
			: mPosX(posX), mPosY(posY) {}

		[[nodiscard]] unsigned int getPosX() const { return mPosX; }
		[[nodiscard]] unsigned int getPosY() const { return mPosY; }

		[[nodiscard]] std::string toString() const override
		{
			std::stringstream ss;
			ss << "WindowMovedEvent: " << mPosX << ", " << mPosY;
			return ss.str();
		}

		EVENT_CLASS_TYPE(WindowMoved)
			EVENT_CLASS_CATEGORY(EventCategory::EventCategoryApplication)
	private:
		unsigned int mPosX, mPosY;
	};

	class WindowFocusEvent final : public Event
	{
	public:
		explicit WindowFocusEvent(const bool focused)
			: mFocused(focused) {}

		[[nodiscard]] unsigned int getFocused() const { return mFocused; }

		[[nodiscard]] std::string toString() const override
		{
			std::stringstream ss;
			ss << "WindowFocusEvent: " << (mFocused ? "Focused" : "UnFocused");
			return ss.str();
		}

		EVENT_CLASS_TYPE(WindowFocus)
			EVENT_CLASS_CATEGORY(EventCategory::EventCategoryApplication)
	private:
		bool mFocused;
	};



	class WindowCloseEvent final : public Event
	{
	public:
		WindowCloseEvent() = default;

		EVENT_CLASS_TYPE(WindowClose)
			EVENT_CLASS_CATEGORY(EventCategory::EventCategoryApplication)
	};

	class AppTickEvent final : public Event
	{
	public:
		AppTickEvent() = default;

		EVENT_CLASS_TYPE(AppTick)
			EVENT_CLASS_CATEGORY(EventCategory::EventCategoryApplication)
	};

	class AppUpdateEvent final : public Event
	{
	public:
		AppUpdateEvent() = default;

		EVENT_CLASS_TYPE(AppUpdate)
			EVENT_CLASS_CATEGORY(EventCategory::EventCategoryApplication)
	};

	class AppRenderEvent final : public Event
	{
	public:
		AppRenderEvent() = default;

		EVENT_CLASS_TYPE(AppRender)
			EVENT_CLASS_CATEGORY(EventCategory::EventCategoryApplication)
	};
}