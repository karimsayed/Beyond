#pragma once
#include <string>
#include <functional>
#include "Beyond/Core/Base.h"

namespace bey {

	// Events in Beyond are currently blocking, meaning when an event occurs it
	// immediately gets dispatched and must be dealt with right then an there.
	// For the future, a better strategy might be to buffer events in an event
	// bus and process them during the "event" part of the update stage.

	enum class EventType
	{
		NONE = 0,
		WindowClose, WindowResize, WindowFocus, WindowLostFocus, WindowMoved,
		AppTick, AppUpdate, AppRender,
		KeyPressed, KeyReleased, KeyTyped,
		MouseButtonPressed, MouseButtonReleased, MouseMoved, MouseScrolled
	};

	enum class EventCategory
	{
		None = 0,
		EventCategoryApplication    = BIT(0),
		EventCategoryInput          = BIT(1),
		EventCategoryKeyboard       = BIT(2),
		EventCategoryMouse          = BIT(3),
		EventCategoryMouseButton    = BIT(4),
	};
	static bool operator&(const int& lhs, const EventCategory& rhs) { return lhs & rhs; }
	static constexpr EventCategory operator|(const EventCategory& lhs, const EventCategory& rhs) { return static_cast<EventCategory>(static_cast<int>(lhs) | static_cast<int>(rhs)); }

#define EVENT_CLASS_TYPE(type) static EventType getStaticType() { return EventType::##type; }\
								virtual EventType getEventType() const override { return getStaticType(); }\
								virtual const char* getName() const override { return #type; }

#define EVENT_CLASS_CATEGORY(category) virtual int getCategoryFlags() const override { return static_cast<std::underlying_type_t<decltype(category)>>(category); }

	
	class Event
	{
	public:
		virtual ~Event() = default;
		bool Handled{ false };

		[[nodiscard]] virtual EventType getEventType() const = 0;
		[[nodiscard]] virtual const char* getName() const = 0;
		[[nodiscard]] virtual int getCategoryFlags() const = 0;
		[[nodiscard]] virtual std::string toString() const { return getName(); }

		[[nodiscard]] bool isInCategory(const EventCategory category) const
		{
			return getCategoryFlags() & category;
		}
	};

	class EventDispatcher
	{
		template<typename T> 
		using EventFn = std::function<bool(T&)>;
	public:
		explicit EventDispatcher(Event& event)
			: mEvent(event)
		{
		}
		

		template<typename T> 
		bool dispatch(EventFn<T> func)
		{
			if (mEvent.getEventType() == T::getStaticType())
			{
				mEvent.Handled = func(*static_cast<T*>(&mEvent));
				return true;
			}
			return false;
		}
	private:
		Event& mEvent;
	};

	inline std::ostream& operator<<(std::ostream& os, const Event& e)
	{
		return os << e.toString();
	}
}

