#include "pchs.h"
#include "Beyond/Core/Input.h"
#include "Beyond/Platform/Windows/Window.hpp"

#include "Beyond/Core/Game.h"


namespace bey {

	bool Input::isKeyPressed(KeyCode keycode)
	{
		BEY_PROFILE_FUNCTION();
		const auto state = glfwGetKey(Game::get().getWindow(), static_cast<int32_t>(keycode));
		return state == GLFW_PRESS || state == GLFW_REPEAT;
	}

	bool Input::isMouseButtonPressed(const int button)
	{
		BEY_PROFILE_FUNCTION();
		const auto state = glfwGetMouseButton(Game::get().getWindow(), button);
		return state == GLFW_PRESS;
	}

	bool Input::isMouseButtonReleased(const int button)
	{
		BEY_PROFILE_FUNCTION();
		const auto state = glfwGetMouseButton(Game::get().getWindow(), button);
		return state == GLFW_RELEASE;
	}

	float Input::getMouseX()
	{
		return getMousePosition().x;
	}

	float Input::getMouseY()
	{
		return getMousePosition().y;
	}

	glm::vec2 Input::getMousePosition()
	{
		BEY_PROFILE_FUNCTION();
		
		double x, y;
		glfwGetCursorPos(Game::get().getWindow(), &x, &y);
		return { float(x), float(y) };

	}

	ImVec2 Input::getMousePositionImGui()
	{
		BEY_PROFILE_FUNCTION();

		double x, y;
		glfwGetCursorPos(Game::get().getWindow(), &x, &y);
		return { float(x), float(y) };

	}

}