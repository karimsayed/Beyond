#pragma once

#include "Ref.h"
#include <memory>
namespace bey {

	void InitializeCore();
	void ShutdownCore();

}

#ifndef BEY_PLATFORM_WINDOWS
	#error Beyond only supports Windows!
#endif

// __VA_ARGS__ expansion to get past MSVC "bug"
#define BEY_EXPAND_VARGS(x) x

#define BIT(x) (1 << (x))


#include "Assert.h"

// Pointer wrappers
namespace bey {

	template<typename T>
	using Scope = std::unique_ptr<T>;
	template<typename T, typename ... Args>
	constexpr Scope<T> createScope(Args&& ... args)
	{
		return std::make_unique<T>(std::forward<Args>(args)...);
	}


}