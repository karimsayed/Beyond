#include "pchs.h"
#include "Instrumentor.h"
#include <memory_resource>
#include "fmt/ostream.h"
#include "Log.h"


namespace bey
{
	const char* Instrumentor::getName() const
	{
		return mCurrentSession ? mCurrentSession->Name.c_str() : "Runtime";
	}

	void Instrumentor::toggle(const char * name)
	{
		Enabled = !Enabled;
		if (Enabled)
		{
			const auto fileName = fmt::format("BeyondProfile-{}.json", name);
			BEY_PROFILE_BEGIN_SESSION(name, fileName);
		}
		else
			BEY_PROFILE_END_SESSION();
	}


	void Instrumentor::refresh(const char* name)
	{
		if (Enabled)
			BEY_PROFILE_BEGIN_SESSION(name, fmt::format("BeyondProfile-{}.json", name));
		else
			BEY_PROFILE_END_SESSION();
	}


	static inline std::array<std::uint8_t, 1024> buffer{};
	static inline std::pmr::monotonic_buffer_resource memResource(buffer.data(), buffer.size());

	void Instrumentor::beginSession(const std::string& name, const std::string& filepath) noexcept
	{
		std::lock_guard<std::mutex> lock(mMutex);
		if (mCurrentSession)
		{
			//BEY_CORE_ERROR("Instrumentor::BeginSession('{0}') when session '{1}' already open.", name, mCurrentSession->Name);
			internalEndSession();
		}
		mOutputStream.open(filepath);
		if (mOutputStream.is_open())
		{
			mCurrentSession = new InstrumentationSession({ name });
			writeHeader();
		}
		else
			BEY_CORE_ERROR("Instrumentor could not open results file '{0}'.", filepath);
		std::pmr::set_default_resource(&memResource);

	}

	void Instrumentor::endSession() noexcept
	{
		std::lock_guard<std::mutex> lock(mMutex);
		internalEndSession();
	}
	bool Instrumentor::OutputBuffer::canFit(const std::string_view& stringView) const
	{
		return (mCommandBufferEnd - (mCommandBufferPtr + stringView.size())) >= 3;
	}




	void Instrumentor::writeProfile(const ProfileResult& result) noexcept
	{
		const std::pmr::string json(fmt::format(
			FMT_STRING(R"(,{{"cat":"function","dur":{0},"name":"{1}","ph":"X","pid":0,"tid":{2},"ts":{3}}})"),
			result.ElapsedTime.count(), result.Name, result.ThreadID, result.Start.count()), &memResource);

		if (!mOutputBuffer.canFit(json))
		{
			toggle(mCurrentSession->Name.c_str());
		}
		else if (mCurrentSession)
		{
			std::lock_guard<std::mutex> lock(mMutex);
			mOutputBuffer.allocate(const_cast<char*>(json.c_str()), json.size());
		}
	}

	Instrumentor::~Instrumentor() noexcept
	{
		endSession();
	}

	void Instrumentor::writeHeader() noexcept
	{
		mOutputStream << R"({"otherData": {},"traceEvents":[{})";
		mOutputStream.flush();
	}

	inline void bey::Instrumentor::writeFooter() noexcept
	{
		mOutputStream << "]}";
		mOutputStream.flush();
	}

	void Instrumentor::internalEndSession() noexcept
	{
		if (mCurrentSession)
		{
			mOutputStream << mOutputBuffer.get();
			mOutputBuffer.reset();
			writeFooter();
			mOutputStream.close();
			delete mCurrentSession;
			mCurrentSession = nullptr;
		}
	}

	Instrumentor::OutputBuffer::OutputBuffer()
	{
		mCommandBufferPtr = mBuffer;
		mBuffer[0] = '\0';
		//memset(mBuffer, 0, 100 * 1024 * 1024);
	}

	Instrumentor::OutputBuffer::~OutputBuffer()
	{
		delete[] mBuffer;
	}

	void* Instrumentor::OutputBuffer::allocate(const char* func, const size_t size)
	{
		std::memcpy(mCommandBufferPtr, func, size + 1);
		mCommandBufferPtr += size;
		mCommandCount++;
		return mCommandBufferPtr;
	}

	void InstrumentationTimer::stop() noexcept
	{
		Instrumentor::get().writeProfile({
			mName, FloatingPointMicroseconds{mStartTimepoint.time_since_epoch()},
			std::chrono::time_point_cast<std::chrono::microseconds>(std::chrono::steady_clock::now()).
			time_since_epoch() -
			std::chrono::time_point_cast<std::chrono::microseconds>(mStartTimepoint).time_since_epoch(),
			std::this_thread::get_id()
			});

		mStopped = true;
	}
}
