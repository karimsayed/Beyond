#pragma once

#include "Beyond/Core/TimeStep.h"
#include "Beyond/Core/LayerStack.h"

#include "Beyond/Core/Events/ApplicationEvent.h"

#include "Beyond/ImGui/ImGuiLayer.h"
#include "Beyond/Platform/Windows/Window.hpp"

namespace bey {

	struct ApplicationProps
	{
		std::string Name;
		uint32_t WindowWidth, WindowHeight;
	};

	class Game
	{
	public:
		explicit Game(const ApplicationProps& props = { "Beyond Game World Editor", 1600, 900 });
		virtual ~Game();

		void run();

		virtual void onInit() {}
		virtual void onShutdown() {}
		virtual void onUpdate(TimeStep) {}

		virtual void onEvent(Event& event);

		void pushLayer(Layer* layer);
		void pushOverlay(Layer* layer);
		[[nodiscard]] unsigned getFPS() const;
		void renderImGui();

		std::string openFile(const char* filter = "All\0*.*\0") const;
		int deleteFile(const char* path) const;
		std::string saveFile(const char* filter = "All\0*.*\0") const;

		[[nodiscard]] Window& getWindow() const { return *mWindow; }
		
		static Game& get() { return *sInstance; }

		[[nodiscard]] static float getTime();
	private:
		bool onWindowMove(WindowMovedEvent& e);
		bool onWindowResize(WindowResizeEvent& e);
		bool onWindowClose(WindowCloseEvent& e);

		std::unique_ptr<Window> mWindow;
		bool mRunning = true;
		bool mMinimized = false;
		LayerStack mLayerStack;
		ImGuiLayer* mImGuiLayer;
		TimeStep mTimeStep;

		float mLastFrameTime = 0.0f;

		static Game* sInstance;
		
	};

	// Implemented by CLIENT
	Game* createGame();
}
