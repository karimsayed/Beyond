#include "pchs.h"
#include "LayerStack.h"

namespace bey {

	LayerStack::LayerStack() = default;

	LayerStack::~LayerStack()
	{
		BEY_PROFILE_FUNCTION();
		for (Layer* layer : mLayers)
			delete layer;
	}

	void LayerStack::pushLayer(Layer* layer)
	{
		BEY_PROFILE_FUNCTION();
		mLayers.emplace(mLayers.begin() + mLayerInsertIndex, layer);
		mLayerInsertIndex++;
	}

	void LayerStack::pushOverlay(Layer* overlay)
	{
		BEY_PROFILE_FUNCTION();
		mLayers.emplace_back(overlay);
	}

	void LayerStack::popLayer(Layer* layer)
	{
		BEY_PROFILE_FUNCTION();
		const auto it = std::find(mLayers.begin(), mLayers.end(), layer);
		if (it != mLayers.end())
		{
			mLayers.erase(it);
			mLayerInsertIndex--;
		}

	}

	void LayerStack::popOverlay(Layer* overlay)
	{
		BEY_PROFILE_FUNCTION();
		const auto it = std::find(mLayers.begin(), mLayers.end(), overlay);
		if (it != mLayers.end())
			mLayers.erase(it);
	}

}