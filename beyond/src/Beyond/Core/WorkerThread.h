#pragma once

namespace  bey
{

	class WorkerThread
	{
	public:
		WorkerThread(const char* threadName);

		~WorkerThread();

		bool createThread();

		void ExitThread();

		std::thread::id getThreadId();

		static std::thread::id getCurrentThreadId();

		void postMsg(std::shared_ptr<UserData> msg);

	private:
		WorkerThread(const WorkerThread&) = delete;
		WorkerThread& operator=(const WorkerThread&) = delete;

		void process();

		void timerThread();

		std::unique_ptr<std::thread> mThread;
		std::queue<std::shared_ptr<ThreadMsg>> mQueue;
		std::mutex mMutex;
		std::condition_variable mCv;
		std::atomic<bool> mTimerExit;
		const char* THREAD_NAME;
	};
}