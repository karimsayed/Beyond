#pragma once

#include <assimp/matrix4x4.h>
#include <spdlog/logger.h>
#include <spdlog/fmt/ostr.h>

#include "Beyond/Core/Base.h"

#include <glm/glm.hpp>
#include <glm/gtx/string_cast.hpp>
namespace bey {

	class Log
	{
	public:
		static void init();

		inline static std::shared_ptr<spdlog::logger>& getCoreLogger() { return s_CoreLogger; }
		inline static std::shared_ptr<spdlog::logger>& getClientLogger() { return s_ClientLogger; }
	private:
		static std::shared_ptr<spdlog::logger> s_CoreLogger;
		static std::shared_ptr<spdlog::logger> s_ClientLogger;
	};

}

template <typename T>
concept isGLM = std::is_same_v<T, glm::mat2> || std::is_same_v<T, glm::mat3> || std::is_same_v<T, glm::mat4> ||
std::is_same_v<T, glm::vec1> || std::is_same_v<T, glm::vec2> || std::is_same_v<T, glm::vec3> || std::is_same_v<T, glm::vec4>;


template<typename genType> requires (isGLM<genType>)
std::ostream& operator<<(std::ostream& os, const auto& mat) noexcept
{
	return os << glm::to_string(mat);
}

template<typename OStream>
OStream& operator<<(OStream& os, const aiMatrix4x4& mat) noexcept
{
	return os <<
		 "			" << '[' << mat.a1 << ", " << mat.b1 << ", " << mat.c1 << ", " << mat.d1 << "]\n" <<
		 "			" << '[' << mat.a2 << ", " << mat.b2 << ", " << mat.c2 << ", " << mat.d2 << "]\n" <<
		 "			" << '[' << mat.a3 << ", " << mat.b3 << ", " << mat.c3 << ", " << mat.d3 << "]\n" <<
		 "			" << '[' << mat.a4 << ", " << mat.b4 << ", " << mat.c4 << ", " << mat.d4 << "]\n";
}

// Core Logging Macros
#define BEY_CORE_TRACE(...)	bey::Log::getCoreLogger()->trace(__VA_ARGS__)
#define BEY_CORE_INFO(...)	bey::Log::getCoreLogger()->info(__VA_ARGS__)
#define BEY_CORE_WARN(...)	bey::Log::getCoreLogger()->warn(__VA_ARGS__)
#define BEY_CORE_ERROR(...)	bey::Log::getCoreLogger()->error(__VA_ARGS__)
#define BEY_CORE_FATAL(...)	bey::Log::getCoreLogger()->critical(__VA_ARGS__)

// Client Logging Macros
#define BEY_TRACE(...)	bey::Log::getClientLogger()->trace(__VA_ARGS__)
#define BEY_INFO(...)	bey::Log::getClientLogger()->info(__VA_ARGS__)
#define BEY_WARN(...)	bey::Log::getClientLogger()->warn(__VA_ARGS__)
#define BEY_ERROR(...)	bey::Log::getClientLogger()->error(__VA_ARGS__)
#define BEY_FATAL(...)	bey::Log::getClientLogger()->critical(__VA_ARGS__)