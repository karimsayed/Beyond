#include "pchs.h"
#include "WorkerThread.h"


namespace bey
{
    bool WorkerThread::createThread()
    {
        if (!mThread)
            mThread = std::make_unique<std::thread>(&WorkerThread::process, this);
        return true;
    }

    void WorkerThread::process()
    {
        mTimerExit = false;
        std::thread timerThread(&WorkerThread::TimerThread, this);

        while (true)
        {
            std::shared_ptr<ThreadMsg> msg;
            {
                // Wait for a message to be added to the queue
                std::unique_lock<std::mutex> lk(mMutex);
                while (mQueue.empty())
                    mCv.wait(lk);

                if (mQueue.empty())
                    continue;

                msg = mQueue.front();
                mQueue.pop();
            }

            switch (msg->id)
            {
            case MSG_POST_USER_DATA:
            {
                BEY_CORE_ASSERT(msg->msg != NULL);

                auto userData = std::static_pointer_cast<UserData>(msg->msg);
                cout << userData->msg.c_str() << " " << userData->year << " on " << THREAD_NAME << endl;

                break;
            }

            case MSG_TIMER:
                cout << "Timer expired on " << THREAD_NAME << endl;
                break;

            case MSG_EXIT_THREAD:
            {
                m_timerExit = true;
                timerThread.join();
                return;
            }

            default:
                BEY_CORE_ASSERT(false);
            }
        }
    }

    void WorkerThread::postMsg(std::shared_ptr<UserData> data)
    {
        BEY_CORE_ASSERT(mThread);

        // Create a new ThreadMsg
        std::shared_ptr<ThreadMsg> threadMsg(new ThreadMsg(MSG_POST_USER_DATA, data));

        // Add user data msg to queue and notify worker thread
        std::unique_lock<std::mutex> lk(m_mutex);
        m_queue.push(threadMsg);
        m_cv.notify_one();
    }
}