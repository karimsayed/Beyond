#include "pchs.h"
#include "UUID.h"


namespace bey {

	static std::random_device s_RandomDevice;
	static std::mt19937_64 eng(s_RandomDevice());
	static std::uniform_int_distribution<uint32_t> s_UniformDistribution;

	UUID::UUID()
		: mUUID(s_UniformDistribution(eng))
	{
	}

	UUID::UUID(const uint32_t uuid)
		: mUUID(uuid)
	{
	}

	UUID::UUID(const UUID& other) = default;

	UUID& UUID::operator=(const UUID& uuid)
	{
		mUUID = uuid.mUUID;
		return *this;
	}

}
