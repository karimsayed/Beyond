#pragma once

namespace bey {

	class TimeStep
	{
	public:
		TimeStep() {}
		TimeStep(float time);

		float getSeconds() const { return mTime; }
		float getMilliseconds() const { return mTime * 1000.0f; }

		operator float() { return mTime; }
		operator float() const { return mTime; }
	private:
		float mTime = 0.0f;
	};

}