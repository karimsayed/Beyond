#pragma once

#include "Core/Synchronizer.h"

extern bey::Game* bey::createGame();

int main(int argc, char** argv)
{

	bey::InitializeCore();
	BEY_PROFILE_BEGIN_SESSION("Startup", "BeyondProfile-Startup.json");
	bey::Game* app = bey::createGame();
	BEY_PROFILE_END_SESSION();

	//BEY_PROFILE_ENABLED = false;
	BEY_CORE_ASSERT(app, "Client Game is null!");

	BEY_PROFILE_BEGIN_SESSION("Runtime", "BeyondProfile-Runtime.json");
	app->run();
	BEY_PROFILE_END_SESSION();
	BEY_PROFILE_BEGIN_SESSION("Destruction", "BeyondProfile-Destruction.json");
	delete app;
	BEY_PROFILE_END_SESSION();

	bey::ShutdownCore();
}

