#pragma once

#include "Beyond/Core/TimeStep.h"
#include "Beyond/Core/Events/MouseEvent.h"
#include "Beyond/Renderer/Camera.h"

namespace bey {
	/*
	class TempCamera final : public Camera
	{
	public:
		TempCamera() = default;
		explicit TempCamera(const glm::mat4& projectionMatrix) noexcept;

		static void focus();
		void onUpdate(TimeStep ts);
		void onEvent(Event& e);

		[[nodiscard]] float getDistance() const noexcept { return mDistance; }
		void setDistance(const float distance)  noexcept { mDistance = distance; }

		void setViewportSize(const uint32_t width, const uint32_t height) noexcept { mViewportWidth = width; mViewportHeight = height; }

		[[nodiscard]] const glm::mat4& getViewMatrix() const noexcept { return mViewMatrix; }
		[[nodiscard]] glm::mat4 getViewProjection() const noexcept { return mProjectionMatrix * mViewMatrix; }

		[[nodiscard]] glm::vec3 getUpDirection() const noexcept;
		[[nodiscard]] glm::vec3 getRightDirection() const noexcept;
		[[nodiscard]] glm::vec3 getForwardDirection() const noexcept;
		[[nodiscard]] const glm::vec3& getPosition() const noexcept { return mPosition; }
		[[nodiscard]] glm::quat getOrientation() const noexcept;

		[[nodiscard]] float getExposure() const override { return mExposure; }
		[[nodiscard]] float& getExposure() override { return mExposure; }

		[[nodiscard]] float getPitch() const noexcept { return mPitch; }
		[[nodiscard]] float getYaw() const noexcept { return mYaw; }
	private:
		void updateCameraView() noexcept;

		bool onMouseScroll(MouseScrolledEvent& e) noexcept;

		void mousePan(const glm::vec2& delta) noexcept;
		void mouseRotate(const glm::vec2& delta) noexcept;
		void mouseZoom(float delta) noexcept;

		[[nodiscard]] glm::vec3 calculatePosition() const noexcept;

		[[nodiscard]] std::pair<float, float> panSpeed() const noexcept;
		constexpr static float rotationSpeed() noexcept;
		[[nodiscard]] float zoomSpeed() const noexcept;
	private:
		glm::mat4 mViewMatrix{};
		glm::vec3 mPosition{}, mRotation{}, mFocalPoint{};

		bool mPanning{}, mRotating{};
		glm::vec2 mInitialMousePosition{};
		glm::vec3 mInitialFocalPoint{}, mInitialRotation{};

		float mDistance;
		float mPitch, mYaw;

		
		uint32_t mViewportWidth = 1280, mViewportHeight = 720;
	};
	*/
}
