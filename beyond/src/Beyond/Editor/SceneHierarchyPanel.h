#pragma once

#include "Beyond/Scene/Scene.h"
#include "Beyond/Scene/Entity.h"
#include "Beyond/Renderer/Mesh.h"

namespace bey {

	class SceneHierarchyPanel
	{
	public:
		explicit SceneHierarchyPanel(const Ref<Scene>& scene);

		void setContext(const Ref<Scene>& scene);
		void setSelected(const Entity & entity);
		void setSelectionChangedCallback(std::function<void(Entity)>&& func) { mSelectionChangedCallback = func; }
		void setEntityDeletedCallback(std::function<void(Entity)>&& func) { mEntityDeletedCallback = func; }

		void onImGuiRender();
	private:
		void drawEntityNode(Entity entity);
		void drawMeshNode(Ref<Mesh> mesh, uint32_t& imguiMeshID) const;
		void meshNodeHierarchy(const Ref<Mesh> mesh, aiNode* node, uint32_t level = 0) const;
		void drawComponents(Entity entity) const;
	private:
		Ref<Scene> mContext;
		Entity mSelectionContext;

		std::function<void(Entity)> mSelectionChangedCallback, mEntityDeletedCallback;
	};

}