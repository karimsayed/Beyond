#include "pchs.h"
#include "SceneHierarchyPanel.h"

#include <assimp/scene.h>

#include "Beyond/Core/Game.h"
#include "Beyond/Renderer/Mesh.h"
#include "Beyond/ImGui/CustomImGui.h"

#include <imgui.h>

#include "Beyond/Scene/MeshExporter.h"


// TODO:
// - Eventually change imgui node IDs to be entity/asset GUID

namespace bey {

	glm::mat4 mat4FromAssimpMat4(const aiMatrix4x4& matrix);



	
	SceneHierarchyPanel::SceneHierarchyPanel(const Ref<Scene>& scene)
		: mContext(scene)
	{



	}

	void SceneHierarchyPanel::setContext(const Ref<Scene>& scene)
	{
		BEY_PROFILE_FUNCTION();
		mContext = scene;
		mSelectionContext = {};
		if (mSelectionContext && false)
		{
			// Try and find same entity in new scene
			const auto& entityMap = mContext->getEntityMap();
			const UUID selectedEntityID = mSelectionContext.getUUID();
			if (entityMap.find(selectedEntityID) != entityMap.end())
				mSelectionContext = entityMap.at(selectedEntityID);
		}
	}

	void SceneHierarchyPanel::setSelected(const Entity& entity) { mSelectionContext = entity; }

	const glm::vec3 LIGHT_MIN_BOUNDS = glm::vec3(-135.0f, -50.0f, -60.0f);
	const glm::vec3 LIGHT_MAX_BOUNDS = glm::vec3(135.0f, 30.0f, 60.0f);

	glm::vec3 RandomPosition(std::uniform_real_distribution<float> dis, std::mt19937 gen) {
		glm::vec3 position = glm::vec3(0.0);
		for (int i = 0; i < 3; i++) {
			float min = LIGHT_MIN_BOUNDS[i];
			float max = LIGHT_MAX_BOUNDS[i];
			position[i] = (GLfloat)dis(gen) * (max - min) + min;
		}

		return position;
	}
	
	void SceneHierarchyPanel::onImGuiRender()
	{
		BEY_PROFILE_FUNCTION();
		ImGui::Begin("Scene Hierarchy");
		if (mContext)
		{
			int lightCount = 0;
			mContext->mRegistry.each([&](auto entity)
				{
					const Entity e(entity, mContext.raw());
					if (e.hasComponent<IDComponent>())
						drawEntityNode(e);
					//if (e.hasComponent<PointLightComponent>())
					//	lightCount++;
				});
			mContext->mRegistry.each([=, &lightCount](auto entity)
				{
					const Entity e(entity, mContext.raw());
					if (e.hasComponent<PointLightComponent>())
						lightCount++;
				});
			
			ImGui::Text("Light Count: %i", lightCount);

			if (ImGui::BeginPopupContextWindow(nullptr, 1, false))
			{
				if (ImGui::BeginMenu("Create"))
				{
					if (ImGui::MenuItem("Empty Entity"))
					{
						const auto newEntity = mContext->createEntity("Empty Entity");
						setSelected(newEntity);
					}
					if (ImGui::MenuItem("Mesh"))
					{
						const auto newEntity = mContext->createEntity("Mesh");
						newEntity.addComponent<MeshComponent>();
						setSelected(newEntity);
					}
					ImGui::Separator();
					if (ImGui::MenuItem("Directional Light"))
					{
						auto newEntity = mContext->createEntity("Directional Light");
						newEntity.addComponent<DirLightComponent>();
						newEntity.getComponent<TransformComponent>().Transform = glm::toMat4(glm::quat(glm::radians(glm::vec3{ 80.0f, 10.0f, 0.0f })));
						setSelected(newEntity);
					}
					if (ImGui::MenuItem("Point Light"))
					{
						auto newEntity = mContext->createEntity("Point Light");
						newEntity.addComponent<PointLightComponent>();
						newEntity.getComponent<TransformComponent>().Transform = glm::toMat4(glm::quat(glm::radians(glm::vec3{ 0.f, 0.f, 0.f })));
						setSelected(newEntity);
					}
					if (ImGui::MenuItem("Sky Light"))
					{
						const auto newEntity = mContext->createEntity("Sky Light");
						newEntity.addComponent<SkyLightComponent>();
						setSelected(newEntity);
					}
					int count = 0;
					if (ImGui::MenuItem("1000 Point Lights"))
						count = 1000;
					if (ImGui::MenuItem("900 Point Lights"))
						count = 900;
					if (ImGui::MenuItem("750 Point Lights"))
						count = 750;
					if (ImGui::MenuItem("500 Point Lights"))
						count = 500;
					if (ImGui::MenuItem("250 Point Lights"))
						count = 250;
					if (ImGui::MenuItem("100 Point Lights"))
						count = 100;

					if (count)
					{
						for (int i = 0; i < count; i++)
						{
							auto newEntity = mContext->createEntity("Point Light");
							newEntity.addComponent<PointLightComponent>();
							newEntity.getComponent<TransformComponent>().Transform = glm::mat4(1);
							auto& component = newEntity.getComponent<PointLightComponent>();
							component.CastsShadows = false;
							component.FarPlane = 25.f;
							component.Intensity = 3.f;
							component.LightSize = 0.5f;
							component.NearPlane = 0.1f;
							component.Radiance = glm::vec3(1);
						}
					}

					ImGui::Separator();
					if (ImGui::MenuItem("Plane 1000x1000"))
					{
						const auto newEntity = mContext->createEntity("Plane");
						newEntity.addComponent<MeshComponent>("assets/primitives/Plane_1000x1000.fbx");
						setSelected(newEntity);
					}
					if (ImGui::MenuItem("Plane 100x100"))
					{
						const auto newEntity = mContext->createEntity("Plane");
						newEntity.addComponent<MeshComponent>("assets/primitives/Plane_100x100.fbx");
						setSelected(newEntity);
					}
					if (ImGui::MenuItem("Plane 50x50"))
					{
						const auto newEntity = mContext->createEntity("Plane");
						newEntity.addComponent<MeshComponent>("assets/primitives/Plane_50x50.fbx");
						setSelected(newEntity);
					}
					if (ImGui::MenuItem("Plane 10x10"))
					{
						const auto newEntity = mContext->createEntity("Plane");
						newEntity.addComponent<MeshComponent>("assets/primitives/Plane_10x10.fbx");
						setSelected(newEntity);
					}
					if (ImGui::MenuItem("Torus"))
					{
						const auto newEntity = mContext->createEntity("Torus");
						newEntity.addComponent<MeshComponent>("assets/primitives/Torus.fbx");
						setSelected(newEntity);
					}
					if (ImGui::MenuItem("Disc"))
					{
						const auto newEntity = mContext->createEntity("Disc");
						newEntity.addComponent<MeshComponent>("assets/primitives/Polygon_Disc.fbx");
						setSelected(newEntity);
					}
					if (ImGui::MenuItem("Cube"))
					{
						const auto newEntity = mContext->createEntity("Cube");
						newEntity.addComponent<MeshComponent>("assets/primitives/Cube.fbx");
						setSelected(newEntity);
					}
					if (ImGui::MenuItem("Cylinder"))
					{
						const auto newEntity = mContext->createEntity("Cylinder");
						newEntity.addComponent<MeshComponent>("assets/primitives/Cylinder.fbx");
						setSelected(newEntity);
					}
					if (ImGui::MenuItem("Sphere"))
					{
						const auto newEntity = mContext->createEntity("Sphere");
						newEntity.addComponent<MeshComponent>("assets/primitives/Sphere.fbx");
						setSelected(newEntity);
					}

					ImGui::EndMenu();
				}
				ImGui::EndPopup();
			}

			ImGui::End();

			ImGui::Begin("Properties");

			if (mSelectionContext)
				drawComponents(mSelectionContext);
		}
		ImGui::End();

#if TODO
		ImGui::Begin("Mesh Debug");
		if (ImGui::CollapsingHeader(mesh->m_FilePath.c_str()))
		{
			if (mesh->m_IsAnimated)
			{
				if (ImGui::CollapsingHeader("Animation"))
				{
					if (ImGui::Button(mesh->m_AnimationPlaying ? "Pause" : "Play"))
						mesh->m_AnimationPlaying = !mesh->m_AnimationPlaying;

					ImGui::SliderFloat("##AnimationTime", &mesh->m_AnimationTime, 0.0f, (float)mesh->m_Scene->mAnimations[0]->mDuration);
					ImGui::DragFloat("Time Scale", &mesh->m_TimeMultiplier, 0.05f, 0.0f, 10.0f);
				}
			}
		}
		ImGui::End();
#endif
	}

	void SceneHierarchyPanel::drawEntityNode(Entity entity)
	{
		const char* name = "Unnamed Entity";
		if (entity.hasComponent<TagComponent>())
			name = entity.getComponent<TagComponent>().Tag.c_str();

		const ImGuiTreeNodeFlags nodeFlags = (entity == mSelectionContext ? ImGuiTreeNodeFlags_Selected : 0) | ImGuiTreeNodeFlags_OpenOnArrow;
		const bool opened = ImGui::TreeNodeEx(reinterpret_cast<void*>(uint64_t(entity)), nodeFlags, "%s", name);

		if (opened && entity.hasComponent<MeshComponent>())
		{
			auto id = ImGui::GetItemID();
			drawMeshNode(entity.getComponent<MeshComponent>().Mesh, id);
		}
		if (ImGui::IsItemClicked())
		{
			mSelectionContext = entity;
			if (mSelectionChangedCallback)
				mSelectionChangedCallback(mSelectionContext);
		}

		bool entityDeleted = false;
		if (ImGui::BeginPopupContextItem())
		{

			if (ImGui::MenuItem("Export"))
			{
				const auto transform = mSelectionContext.getComponent<TransformComponent>().Transform;
				const auto mesh = mSelectionContext.getComponent<MeshComponent>().Mesh;
				MeshExporter::exportMesh(Game::get().saveFile("FBX\0*.fbx\0"), mesh, transform);
			}
			else if (ImGui::MenuItem("Delete"))
				entityDeleted = true;

			ImGui::EndPopup();
		}
		if (opened)
			ImGui::TreePop();

		// Defer deletion until end of node ui
		if (entityDeleted)
		{
			mContext->destroyEntity(entity);
			if (entity == mSelectionContext)
				mSelectionContext = {};

			mEntityDeletedCallback(entity);
		}
	}

	void SceneHierarchyPanel::drawMeshNode(const Ref<Mesh> mesh, uint32_t& imguiMeshID) const
	{
		static char imguiName[128];
		std::memset(imguiName, 0, 128);
		sprintf_s(imguiName, "Mesh##%o", imguiMeshID++);

		// Mesh Hierarchy
		if (ImGui::TreeNode(imguiName) && mesh)
		{
			auto* const rootNode = mesh->mScene->mRootNode;
			meshNodeHierarchy(mesh, rootNode);
			ImGui::TreePop();
		}
	}

	static std::tuple<glm::vec3, glm::quat, glm::vec3> getTransformDecomposition(const glm::mat4& transform)
	{
		glm::vec3 scale, translation, skew;
		glm::vec4 perspective;
		glm::quat orientation;
		glm::decompose(transform, scale, orientation, translation, skew, perspective);

		return { translation, orientation, scale };
	}

	void SceneHierarchyPanel::meshNodeHierarchy(const Ref<Mesh> mesh, aiNode* node, const uint32_t level) const
	{

		if (ImGui::TreeNode(node->mName.C_Str()))
		{

			for (size_t i = 0; unsigned(i) < node->mNumChildren; i++)
				meshNodeHierarchy(mesh, node->mChildren[i], level + 1);

			ImGui::TreePop();
		}

	}


	template<typename T, typename UIFunction>
	static void drawComponent(const std::string& name, Entity entity, UIFunction uiFunction)
	{
		const ImGuiTreeNodeFlags treeNodeFlags = ImGuiTreeNodeFlags_DefaultOpen | ImGuiTreeNodeFlags_Framed | ImGuiTreeNodeFlags_SpanAvailWidth | 
			ImGuiTreeNodeFlags_AllowItemOverlap | ImGuiTreeNodeFlags_FramePadding;
		if (entity.hasComponent<T>())
		{
			auto& component = entity.getComponent<T>();
			const ImVec2 contentRegionAvailable = ImGui::GetContentRegionAvail();

			ImGui::PushStyleVar(ImGuiStyleVar_FramePadding, ImVec2{ 4, 4 });
			const float lineHeight = GImGui->Font->FontSize + GImGui->Style.FramePadding.y * 2.0f;
			ImGui::Separator();
			const bool open = ImGui::TreeNodeEx(reinterpret_cast<void*>(typeid(T).hash_code()), treeNodeFlags, name.c_str());
			ImGui::PopStyleVar();
			ImGui::SameLine(contentRegionAvailable.x - lineHeight * 0.5f);
			if (ImGui::Button("+", ImVec2{ lineHeight, lineHeight }))
				ImGui::OpenPopup("ComponentSettings");

			bool removeComponent = false;
			if (ImGui::BeginPopup("ComponentSettings"))
			{
				if (ImGui::MenuItem("Remove component"))
					removeComponent = true;

				ImGui::EndPopup();
			}

			if (open)
			{
				uiFunction(component);
				ImGui::TreePop();
			}

			if (removeComponent)
				entity.removeComponent<T>();
		}
	}

	static bool drawVec3Control(const std::string& label, glm::vec3& values, const float resetValue = 0.0f, const float columnWidth = 100.0f)
	{
		bool modified = false;

		ImGuiIO& io = ImGui::GetIO();
		auto* const boldFont = io.Fonts->Fonts[0];

		ImGui::PushID(label.c_str());

		ImGui::Columns(2);
		ImGui::SetColumnWidth(0, columnWidth);
		ImGui::Text(label.c_str());
		ImGui::NextColumn();

		ImGui::PushMultiItemsWidths(3, ImGui::CalcItemWidth());
		ImGui::PushStyleVar(ImGuiStyleVar_ItemSpacing, ImVec2{ 0, 0 });

		const float lineHeight = GImGui->Font->FontSize + GImGui->Style.FramePadding.y * 2.0f;
		const ImVec2 buttonSize = { lineHeight + 3.0f, lineHeight };

		ImGui::PushStyleColor(ImGuiCol_Button, ImVec4{ 0.8f, 0.1f, 0.15f, 1.0f });
		ImGui::PushStyleColor(ImGuiCol_ButtonHovered, ImVec4{ 0.9f, 0.2f, 0.2f, 1.0f });
		ImGui::PushStyleColor(ImGuiCol_ButtonActive, ImVec4{ 0.8f, 0.1f, 0.15f, 1.0f });
		ImGui::PushFont(boldFont);
		if (ImGui::Button("X", buttonSize))
		{
			values.x = resetValue;
			modified = true;
		}

		ImGui::PopFont();
		ImGui::PopStyleColor(3);

		ImGui::SameLine();
		modified |= ImGui::DragFloat("##X", &values.x, 0.1f, 0.0f, 0.0f, "%.2f");
		ImGui::PopItemWidth();
		ImGui::SameLine();

		ImGui::PushStyleColor(ImGuiCol_Button, ImVec4{ 0.2f, 0.7f, 0.2f, 1.0f });
		ImGui::PushStyleColor(ImGuiCol_ButtonHovered, ImVec4{ 0.3f, 0.8f, 0.3f, 1.0f });
		ImGui::PushStyleColor(ImGuiCol_ButtonActive, ImVec4{ 0.2f, 0.7f, 0.2f, 1.0f });
		ImGui::PushFont(boldFont);
		if (ImGui::Button("Y", buttonSize))
		{
			values.y = resetValue;
			modified = true;
		}

		ImGui::PopFont();
		ImGui::PopStyleColor(3);

		ImGui::SameLine();
		modified |= ImGui::DragFloat("##Y", &values.y, 0.1f, 0.0f, 0.0f, "%.2f");
		ImGui::PopItemWidth();
		ImGui::SameLine();

		ImGui::PushStyleColor(ImGuiCol_Button, ImVec4{ 0.1f, 0.25f, 0.8f, 1.0f });
		ImGui::PushStyleColor(ImGuiCol_ButtonHovered, ImVec4{ 0.2f, 0.35f, 0.9f, 1.0f });
		ImGui::PushStyleColor(ImGuiCol_ButtonActive, ImVec4{ 0.1f, 0.25f, 0.8f, 1.0f });
		ImGui::PushFont(boldFont);
		if (ImGui::Button("Z", buttonSize))
		{
			values.z = resetValue;
			modified = true;
		}

		ImGui::PopFont();
		ImGui::PopStyleColor(3);

		ImGui::SameLine();
		modified |= ImGui::DragFloat("##Z", &values.z, 0.1f, 0.0f, 0.0f, "%.2f");
		ImGui::PopItemWidth();

		ImGui::PopStyleVar();

		ImGui::Columns(1);

		ImGui::PopID();

		return modified;
	}


	void SceneHierarchyPanel::drawComponents(Entity entity) const
	{
		ImGui::AlignTextToFramePadding();

		const auto& id = entity.getComponent<IDComponent>().ID;
		const ImVec2 contentRegionAvailable = ImGui::GetContentRegionAvail();


		if (entity.hasComponent<TagComponent>())
		{
			auto& tag = entity.getComponent<TagComponent>().Tag;
			char buffer[256];
			memset(buffer, 0, 256);
			memcpy(buffer, tag.c_str(), tag.length());
			ImGui::PushItemWidth(contentRegionAvailable.x * 0.5f);
			if (ImGui::InputText("##Tag", buffer, 256))
			{
				tag = std::string(buffer);
			}
			ImGui::PopItemWidth();
		}

		// ID
		//ImGui::SameLine();
		ImGui::TextDisabled("%llx", id);

		const ImVec2 textSize = ImGui::CalcTextSize("Add Component");
		ImGui::SameLine(contentRegionAvailable.x - (textSize.x + GImGui->Style.FramePadding.y));
		if (ImGui::Button("Add Component"))
			ImGui::OpenPopup("AddComponentPanel");

		if (ImGui::BeginPopup("AddComponentPanel"))
		{
			if (!mSelectionContext.hasComponent<CameraComponent>())
			{
				if (ImGui::Button("Camera"))
				{
					mSelectionContext.addComponent<CameraComponent>();
					ImGui::CloseCurrentPopup();
				}
			}
			if (!mSelectionContext.hasComponent<MeshComponent>())
			{
				if (ImGui::Button("Mesh"))
				{
					mSelectionContext.addComponent<MeshComponent>();
					ImGui::CloseCurrentPopup();
				}
			}
			if (!mSelectionContext.hasComponent<DirLightComponent>())
			{
				if (ImGui::Button("Directional Light"))
				{
					mSelectionContext.addComponent<DirLightComponent>();
					ImGui::CloseCurrentPopup();
				}
			}
			if (!mSelectionContext.hasComponent<PointLightComponent>())
			{
				if (ImGui::Button("Point Light"))
				{
					mSelectionContext.addComponent<PointLightComponent>();
					ImGui::CloseCurrentPopup();
				}
			}
			ImGui::EndPopup();
		}

		drawComponent<TransformComponent>("Transform", entity, [](auto& component)
			{
				auto [translation, rotationQuat, scale] = getTransformDecomposition(component);

				bool updateTransform = false;
				updateTransform |= drawVec3Control("Translation", translation);
				glm::vec3 rotation = glm::degrees(glm::eulerAngles(rotationQuat));
				updateTransform |= drawVec3Control("Rotation", rotation);
				updateTransform |= drawVec3Control("Scale", scale, 1.0f);

				if (updateTransform)
				{
					component.Transform = glm::translate(glm::mat4(1.0f), translation) *
						glm::toMat4(glm::quat(glm::radians(rotation))) *
						glm::scale(glm::mat4(1.0f), scale);
				}
			});

		drawComponent<MeshComponent>("Mesh", entity, [](MeshComponent& mc)
			{
				ImGui::Columns(3);
				ImGui::SetColumnWidth(0, 100);
				ImGui::SetColumnWidth(1, 300);
				ImGui::SetColumnWidth(2, 40);
				ImGui::Text("File Path");
				ImGui::NextColumn();
				ImGui::PushItemWidth(-1);
				if (mc.Mesh)
					ImGui::InputText("##meshfilepath", (char*)mc.Mesh->getFilePath().c_str(), 256, ImGuiInputTextFlags_ReadOnly);
				else
					ImGui::InputText("##meshfilepath", (char*)"NO MESH", 256, ImGuiInputTextFlags_ReadOnly);
				ImGui::PopItemWidth();
				//ImGui::NextColumn();
				if (ImGui::Button("Open Mesh##openmesh"))
				{
					std::string file = Game::get().openFile();
					if (!file.empty())
						mc.Mesh = Ref<Mesh>::create(file);
				}
				ImGui::Columns(1);
			});

		drawComponent<CameraComponent>("Camera", entity, [](CameraComponent& cc)
			{
				// Projection Type
				const char* projTypeStrings[] = { "Perspective", "Orthographic" };
				const char* currentProj = projTypeStrings[(int)cc.Camera.getProjectionType()];
				if (ImGui::BeginCombo("Projection", currentProj))
				{
					for (int type = 0; type < 2; type++)
					{
						const bool isSelected = (currentProj == projTypeStrings[type]);
						if (ImGui::Selectable(projTypeStrings[type], isSelected))
						{
							currentProj = projTypeStrings[type];
							cc.Camera.setProjectionType(SceneCamera::ProjectionType(type));
						}
						if (isSelected)
							ImGui::SetItemDefaultFocus();
					}
					ImGui::EndCombo();
				}

				ui::BeginPropertyGrid();
				// Perspective parameters
				if (cc.Camera.getProjectionType() == SceneCamera::ProjectionType::PERSPECTIVE)
				{
					float verticalFOV = cc.Camera.getPerspectiveVerticalFOV();
					if (ui::Property("Vertical FOV", verticalFOV))
						cc.Camera.setPerspectiveVerticalFOV(verticalFOV);

					float nearClip = cc.Camera.getPerspectiveNearClip();
					if (ui::Property("Near Clip", nearClip))
						cc.Camera.setPerspectiveNearClip(nearClip);
					ImGui::SameLine();
					float farClip = cc.Camera.getPerspectiveFarClip();
					if (ui::Property("Far Clip", farClip))
						cc.Camera.setPerspectiveFarClip(farClip);
				}

				// Orthographic parameters
				else if (cc.Camera.getProjectionType() == SceneCamera::ProjectionType::ORTHOGRAPHIC)
				{
					float orthoSize = cc.Camera.getOrthographicSize();
					if (ui::Property("Size", orthoSize))
						cc.Camera.setOrthographicSize(orthoSize);

					float nearClip = cc.Camera.getOrthographicNearClip();
					if (ui::Property("Near Clip", nearClip))
						cc.Camera.setOrthographicNearClip(nearClip);
					ImGui::SameLine();
					float farClip = cc.Camera.getOrthographicFarClip();
					if (ui::Property("Far Clip", farClip))
						cc.Camera.setOrthographicFarClip(farClip);
				}

				ui::EndPropertyGrid();
			});


		drawComponent<DirLightComponent>("Directional Light", entity, [](DirLightComponent& dlc)
			{
				ui::BeginPropertyGrid();
				ui::PropertyColor("Radiance", dlc.Radiance);
				ui::Property("Intensity", dlc.Intensity);
				// ui::Property("Cast Shadows", dlc.CastsShadows);
				ui::Property("Soft Shadows", dlc.SoftShadows);
				ui::Property("Source Size", dlc.LightSize);
				ui::EndPropertyGrid();
			});

		drawComponent<PointLightComponent>("Point Light", entity, [](PointLightComponent& dlc)
			{
				ui::BeginPropertyGrid();
				ui::PropertyColor("Radiance", dlc.Radiance);
				ui::Property("Intensity", dlc.Intensity);
				ui::Property("Cast Shadows", dlc.CastsShadows);
				ui::Property("Soft Shadows", dlc.SoftShadows);
				ui::Property("Source Size", dlc.LightSize);
				ui::Property("FarPlane", dlc.FarPlane);
				ui::EndPropertyGrid();
			});

		drawComponent<SkyLightComponent>("Sky Light", entity, [](SkyLightComponent& slc)
			{
				ImGui::Columns(3);
				ImGui::SetColumnWidth(0, 100);
				ImGui::SetColumnWidth(1, 300);
				ImGui::SetColumnWidth(2, 40);
				ImGui::Text("File Path");
				ImGui::NextColumn();
				ImGui::PushItemWidth(-1);
				if (!slc.SceneEnvironment.FilePath.empty())
					ImGui::InputText("##envfilepath", const_cast<char*>(slc.SceneEnvironment.FilePath.c_str()), 256, ImGuiInputTextFlags_ReadOnly);
				else
					ImGui::InputText("##envfilepath", const_cast<char*>("Empty"), 256, ImGuiInputTextFlags_ReadOnly);
				ImGui::PopItemWidth();
				/*ImGui::NextColumn();*/
				if (ImGui::Button("Open##openenv"))
				{
					const std::string file = Game::get().openFile("(Equirectangular HDR)\0*.hdr\0");
					if (!file.empty())
						slc.SceneEnvironment = Environment::load(file);
				}
				ImGui::Columns(1);

				ui::BeginPropertyGrid();
				ui::Property("Skybox LOD", slc.SkyboxLod, 0.01f, 0.f, 11.f);
				ui::Property("Intensity", slc.Intensity, 0.01f, 0.0f, 5.0f);
				ui::Property("Angle", slc.Angle, 0.1f, 0.0f, 360.0f);
				ui::EndPropertyGrid();
			});
	}

}