#include "pchs.h"
#include "TempCamera.h"

/*
constexpr auto PI_ = 3.14159f;

namespace bey {

	TempCamera::TempCamera(const glm::mat4& projectionMatrix) noexcept
		: Camera(projectionMatrix)
	{
		mRotation = glm::vec3(90.0f, 0.0f, 0.0f);
		mFocalPoint = glm::vec3(0.0f);

		glm::vec3 position = { -5, 5, 5};
		mDistance = glm::distance(position, mFocalPoint);

		mYaw = 3.0f * float(PI_) / 4.0f;
		mPitch = PI_ / 4.0f;

		updateCameraView();
	}

	void TempCamera::updateCameraView() noexcept
	{
		mPosition = calculatePosition();

		const glm::quat orientation = getOrientation();
		mRotation = glm::eulerAngles(orientation) * (180.0f / PI_);
		mViewMatrix = glm::translate(glm::mat4(1.0f), mPosition) * glm::toMat4(orientation);
		mViewMatrix = glm::inverse(mViewMatrix);
	}

	void TempCamera::focus()
	{
	}

	std::pair<float, float> TempCamera::panSpeed() const noexcept
	{
		const float x = std::min(mViewportWidth / 1000.0f, 2.4f); // max = 2.4f
		float xFactor = 0.0366f * (x * x) - 0.1778f * x + 0.3021f;

		const float y = std::min(mViewportHeight / 1000.0f, 2.4f); // max = 2.4f
		float yFactor = 0.0366f * (y * y) - 0.1778f * y + 0.3021f;

		return { xFactor, yFactor };
	}

	constexpr float TempCamera::rotationSpeed() noexcept
	{
		return 0.8f;
	}

	float TempCamera::zoomSpeed() const noexcept
	{
		float distance = mDistance * 0.2f;
		distance = std::max(distance, 0.0f);
		float speed = distance * distance;
		speed = std::min(speed, 100.0f); // max speed = 100
		return speed;
	}

	void TempCamera::onUpdate(TimeStep ts)
	{
		if (Input::isKeyPressed(KeyCode::LeftAlt))
		{
			const glm::vec2& mouse{ Input::getMouseX(), Input::getMouseY() };
			const glm::vec2 delta = (mouse - mInitialMousePosition) * 0.003f;
			mInitialMousePosition = mouse;

			if (Input::isMouseButtonPressed(GLFW_MOUSE_BUTTON_MIDDLE))
				mousePan(delta);
			else if (Input::isMouseButtonPressed(GLFW_MOUSE_BUTTON_LEFT))
			{
				mouseRotate(delta);
			}
			else if (Input::isMouseButtonPressed(GLFW_MOUSE_BUTTON_RIGHT))
			{
				mouseZoom(delta.y);

			}

		}

		updateCameraView();
	}

	void TempCamera::onEvent(Event& e)
	{
		EventDispatcher dispatcher(e);
		dispatcher.dispatch<MouseScrolledEvent>(BEY_BIND_EVENT_FN(TempCamera::onMouseScroll));
	}

	bool TempCamera::onMouseScroll(MouseScrolledEvent& e) noexcept
	{
		const float delta = e.getYOffset() * 0.1f;
		mouseZoom(delta);
		updateCameraView();
		return false;
	}

	void TempCamera::mousePan(const glm::vec2& delta) noexcept
	{
		auto [xSpeed, ySpeed] = panSpeed();
		mFocalPoint += -getRightDirection() * delta.x * xSpeed * mDistance;
		mFocalPoint += getUpDirection() * delta.y * ySpeed * mDistance;
	}

	void TempCamera::mouseRotate(const glm::vec2& delta) noexcept
	{
		const float yawSign = getUpDirection().y < 0 ? -1.0f : 1.0f;
		mYaw += yawSign * delta.x * rotationSpeed();
		mPitch += delta.y * rotationSpeed();
	}

	void TempCamera::mouseZoom(const float delta) noexcept
	{
		mDistance -= delta * zoomSpeed();
		if (mDistance < 1.0f)
		{
			mFocalPoint += getForwardDirection();
			mDistance = 1.0f;
		}
	}

	glm::vec3 TempCamera::getUpDirection() const noexcept
	{
		return glm::rotate(getOrientation(), glm::vec3(0.0f, 1.0f, 0.0f));
	}

	glm::vec3 TempCamera::getRightDirection() const noexcept
	{
		return glm::rotate(getOrientation(), glm::vec3(1.0f, 0.0f, 0.0f));
	}

	glm::vec3 TempCamera::getForwardDirection() const noexcept
	{
		return glm::rotate(getOrientation(), glm::vec3(0.0f, 0.0f, -1.0f));
	}

	glm::vec3 TempCamera::calculatePosition() const noexcept
	{
		return mFocalPoint - getForwardDirection() * mDistance;
	}

	glm::quat TempCamera::getOrientation() const noexcept
	{
		return glm::quat(glm::vec3(-mPitch, -mYaw, 0.0f));
	}
}
*/