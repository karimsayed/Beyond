#pragma once
#include "Beyond/Core/TimeStep.h"
#include "Beyond/Core/Events/KeyEvent.h"
#include "Beyond/Core/Events/MouseEvent.h"
#include "Beyond/Renderer/Camera.h"


namespace bey
{

	
	class FlyCamera final : public Camera
	{
	public:
		FlyCamera() = default;
		explicit FlyCamera(float fov, float width, float height, float nearClip, float farClip) noexcept;

		static void focus() noexcept;
		void onUpdate(TimeStep ts, const glm::vec2& delta) noexcept;
		void onEvent(Event& e) noexcept;

		[[nodiscard]] float getDistance() const noexcept { return mDistance; }
		void setDistance(const float distance)  noexcept { mDistance = distance; }
		void reset();

		[[nodiscard]] static constexpr glm::vec3 getUpDirection() noexcept;
		[[nodiscard]] glm::vec3 getStrafeDirection() const noexcept;
		[[nodiscard]] glm::vec3 getViewDirection() const noexcept override;
		[[nodiscard]] const glm::vec3& getPosition() const noexcept { return mPosition; }
		[[nodiscard]] glm::mat4 getOrientation() const noexcept;

		[[nodiscard]] float getCameraSpeed() const { return mCameraSpeed; }
		[[nodiscard]] constexpr float& getCameraSpeed() { return mCameraSpeed; }
		[[nodiscard]] const glm::mat4& getViewMatrix() const noexcept { return mViewMatrix; }
		[[nodiscard]] const glm::mat4& getViewMatrixInverse() const noexcept { return glm::inverse(mViewMatrix); }
		[[nodiscard]] glm::mat4 getViewProjection() const noexcept { return mProjectionMatrix * mViewMatrix; }
	private:
		void updateCameraView() noexcept;
		void changePitch(float degrees);
		void changeHeading(float degrees);

		bool onMouseScroll(MouseScrolledEvent& e) noexcept;
		bool onMousePressed(MouseButtonPressedEvent& e) const noexcept;
		bool onMouseReleased(MouseButtonReleasedEvent& e) const noexcept;
		bool onKeyReleased(KeyReleasedEvent& e) noexcept;
		bool onKeyPressed(KeyPressedEvent& e) noexcept;

		constexpr static float rotationSpeed() noexcept;
		[[nodiscard]] float zoomSpeed() const noexcept;
	private:
		glm::mat4 mViewMatrix{};
		glm::vec3 mPosition{50.f, 20.f, 50.f};

		//glm::vec2 mInitialMousePosition{};
		//glm::vec3 mInitialFocalPoint{}, mInitialRotation{};

		float mDistance;
		glm::vec3 mViewDirection{ -0.657824814f, -0.155215383f, -0.735670209f};
		glm::vec3 mStrafeDirection{ 1.f, 0.f, 0.f };



		
		//ImGui exposed
		float mCameraSpeed = 0.015f;

		/////////////////////////////////////

		float mCameraHeading{};
		float mCameraPitch{};

		float mMaxPitchRate {.12f};
		float mMaxHeadingRate {.12f};

		glm::vec3 mCameraPositionDelta{};
		glm::vec3 mCameraLookAt{};
	};
}
