#include "pchs.h"
#include "FlyCamera.hpp"


#include "Beyond/Core/Game.h"
#include "Beyond/Core/Input.h"
#include "Beyond/Core/Events/KeyEvent.h"

namespace bey
{
	FlyCamera::FlyCamera(const float fov, const float width, const float height, const float nearClip, const float farClip) noexcept
		: Camera(fov, width, height, nearClip, farClip)
	{
		BEY_PROFILE_FUNCTION();
		updateCameraView();

	}

	void FlyCamera::focus() noexcept
	{
	}


	constexpr glm::vec3 FlyCamera::getUpDirection() noexcept
	{
		return { 0.0f, 1.f, 0.0f };
	}

	void FlyCamera::reset()
	{
		mPosition = { 50.f, 20.f, 50.f };
		mViewDirection = { -0.657824814f, -0.155215383f, -0.735670209f };
		mViewMatrix = glm::lookAt(mPosition, mPosition + mViewDirection, getUpDirection());
	}

	glm::vec3 FlyCamera::getStrafeDirection() const noexcept
	{
		return mStrafeDirection;
	}

	glm::vec3 FlyCamera::getViewDirection() const noexcept
	{
		return mViewDirection;
	}

	glm::mat4 FlyCamera::getOrientation() const noexcept
	{
		return mViewMatrix;
	}

	void FlyCamera::updateCameraView() noexcept
	{
		BEY_PROFILE_FUNCTION();


		//mCameraLookAt = glm::cross(mStrafeDirection, getUpDirection()); // if you want to look at something
		
		mViewDirection = glm::normalize(mCameraLookAt - mPosition);
		mStrafeDirection = glm::cross(mViewDirection, getUpDirection());
		
		mViewDirection = glm::rotate(glm::normalize(glm::cross(glm::angleAxis(mCameraPitch, mStrafeDirection),
			glm::angleAxis(mCameraHeading, getUpDirection()))), mViewDirection);

		mViewDirection.y = glm::clamp(mViewDirection.y, -0.91f, 0.91f);

		mPosition += mCameraPositionDelta;
		//damping for smooth camera
		mCameraHeading *= 0.5f;
		mCameraPitch *= 0.5f;
		mCameraPositionDelta *= 0.8f;

		mCameraLookAt = mPosition + mViewDirection;
		
		
		mViewMatrix = glm::lookAt(mPosition, mPosition + mViewDirection, getUpDirection());
	}

	void FlyCamera::changePitch(float degrees) {
		//Check bounds with the max pitch rate so that we aren't moving too fast
		if (degrees < -mMaxPitchRate)
			degrees = -mMaxPitchRate;
		else if (degrees > mMaxPitchRate)
			degrees = mMaxPitchRate;
		mCameraPitch += degrees;

		//Check bounds for the camera pitch
		if (mCameraPitch > 6.283185f)
			mCameraPitch -= 6.283185f;
		else if (mCameraPitch < -6.283185f)
			mCameraPitch += 6.283185f;


	}
	void FlyCamera::changeHeading(float degrees) {
		//Check bounds with the max heading rate so that we aren't moving too fast
		if (degrees < -mMaxHeadingRate)
			degrees = -mMaxHeadingRate;
		else if (degrees > mMaxHeadingRate)
			degrees = mMaxHeadingRate;
		//This controls how the heading is changed if the camera is pointed straight up or down
		//The heading delta direction changes
		if (mCameraPitch > 1.570796f && mCameraPitch < 4.712389f || (mCameraPitch < -1.570796f && mCameraPitch > -4.712389f))
			mCameraHeading -= degrees;
		else
			mCameraHeading += degrees;


		//Check bounds for the camera heading
		if (mCameraHeading > 6.283185f)
			mCameraHeading -= 6.283185f;
		
		else if (mCameraHeading < -6.283185f)
			mCameraHeading += 6.283185f;

		
	}

	constexpr float FlyCamera::rotationSpeed() noexcept
	{
		return 0.8f;
	}

	float FlyCamera::zoomSpeed() const noexcept
	{
		BEY_PROFILE_FUNCTION();
		float distance = mDistance * 0.2f;
		distance = std::max(distance, 0.0f);
		float speed = distance * distance;
		speed = std::min(speed, 100.0f); // max speed = 100
		return speed;
	}

	void FlyCamera::onUpdate(const TimeStep ts, const glm::vec2& delta) noexcept
	{
		BEY_PROFILE_FUNCTION();



		if (Input::isKeyPressed(KeyCode::Q))
			mCameraPositionDelta -= ts.getMilliseconds() * mCameraSpeed * getUpDirection();
		if (Input::isKeyPressed(KeyCode::E))
			mCameraPositionDelta += ts.getMilliseconds() * mCameraSpeed * getUpDirection();

		if (Input::isKeyPressed(KeyCode::S))
			mCameraPositionDelta -= ts.getMilliseconds() * mCameraSpeed * mViewDirection;
		if (Input::isKeyPressed(KeyCode::W))
			mCameraPositionDelta += ts.getMilliseconds() * mCameraSpeed * mViewDirection;

		if (Input::isKeyPressed(KeyCode::A))
			mCameraPositionDelta -= ts.getMilliseconds() * mCameraSpeed * mStrafeDirection;
		if (Input::isKeyPressed(KeyCode::D))
			mCameraPositionDelta += ts.getMilliseconds() * mCameraSpeed * mStrafeDirection;

		changeHeading(delta.x);
		changePitch(delta.y);

		updateCameraView();

	}

	void FlyCamera::onEvent(Event& e) noexcept
	{
		BEY_PROFILE_FUNCTION();
		EventDispatcher dispatcher(e);
		dispatcher.dispatch<MouseScrolledEvent>([this](auto event) { return FlyCamera::onMouseScroll(event); });
		dispatcher.dispatch<MouseButtonPressedEvent>([this](auto event) { return FlyCamera::onMousePressed(event); });
		dispatcher.dispatch<MouseButtonReleasedEvent>([this](auto event) { return FlyCamera::onMouseReleased(event); });
		dispatcher.dispatch<KeyPressedEvent>([this](auto event) { return FlyCamera::onKeyPressed(event); });
		dispatcher.dispatch<KeyReleasedEvent>([this](auto event) { return FlyCamera::onKeyReleased(event); });
	}

	bool FlyCamera::onMouseScroll(MouseScrolledEvent& e) noexcept
	{
		BEY_PROFILE_FUNCTION();
		if (Input::isMouseButtonPressed(BEY_MOUSE_BUTTON_RIGHT))
		{
			e.getYOffset() > 0 ? mCameraSpeed += 0.3f * mCameraSpeed : mCameraSpeed -= 0.3f * mCameraSpeed;
			mCameraSpeed = std::clamp(mCameraSpeed, 0.0005f, 2.f);
		}
		return true;
	}

	bool FlyCamera::onMousePressed(MouseButtonPressedEvent& e) const noexcept
	{
		return true;
	}

	bool FlyCamera::onMouseReleased(MouseButtonReleasedEvent& e) const noexcept
	{
		return true;
	}

	float speedBefore = 0.f;


	bool FlyCamera::onKeyPressed(KeyPressedEvent& e) noexcept
	{
		if (e.getKeyCode() == BEY_KEY_LEFT_SHIFT || e.getKeyCode() == BEY_KEY_LEFT_CONTROL)
		{

			if (e.getKeyCode() == BEY_KEY_LEFT_SHIFT && speedBefore == 0.f)
			{
				speedBefore = mCameraSpeed;
				mCameraSpeed *= 2 - log(mCameraSpeed);
			}
			if (e.getKeyCode() == BEY_KEY_LEFT_CONTROL && speedBefore == 0.f)
			{
				speedBefore = mCameraSpeed;
				mCameraSpeed /= 2 - log(mCameraSpeed);
			}

			mCameraSpeed = std::clamp(mCameraSpeed, 0.0005f, 2.f);
		}
		return true;
	}

	bool FlyCamera::onKeyReleased(KeyReleasedEvent& e) noexcept
	{
		if (e.getKeyCode() == BEY_KEY_LEFT_SHIFT || e.getKeyCode() == BEY_KEY_LEFT_CONTROL)
		{

			if (speedBefore != 0.f)
			{
				mCameraSpeed = speedBefore;

				speedBefore = 0.f;
			}

			mCameraSpeed = std::clamp(mCameraSpeed, 0.0005f, 2.f);
		}
		return true;
	}



}
