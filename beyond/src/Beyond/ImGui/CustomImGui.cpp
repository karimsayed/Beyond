#include "pchs.h"


#include "CustomImGui.h"

#include <imgui_internal.h>


// We don't use BeginDragDropTargetCustom() and duplicate its code because:
// 1) we use LastItemRectHoveredRect which handles items that pushes a temporarily clip rectangle in their code. Calling BeginDragDropTargetCustom(LastItemRect) would not handle them.
// 2) and it's faster. as this code may be very frequently called, we want to early out as fast as we can.
// Also note how the HoveredWindow test is positioned differently in both functions (in both functions we optimize for the cheapest early out case)


bool ImGui::BeginDragDropTargetCustom1(const ImRect& bb, const ImGuiID id)
{
	BEY_PROFILE_FUNCTION();
	ImGuiContext& g = *GImGui;
	if (!g.DragDropActive)
		return false;

	ImGuiWindow* window = g.CurrentWindow;
	ImGuiWindow* hoveredWindow = g.HoveredWindowUnderMovingWindow;
	if (hoveredWindow == nullptr || window->RootWindow != hoveredWindow->RootWindow)
		return false;

	if (window->SkipItems)
		return false;

	g.DragDropTargetRect = bb;
	g.DragDropTargetId = id;
	g.DragDropWithinTarget = true;
	return true;
}


namespace bey::ui
{



	


}