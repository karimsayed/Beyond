#pragma once
#include <imgui.h>
#include <imgui_internal.h>
namespace ImGui
{


	bool BeginDragDropTargetCustom1(const ImRect& bb, ImGuiID id);

}
namespace bey::ui
{
	enum class PayloadKind
	{
		None, Mesh, Scene
	};

	static std::string getPayloadString(const char* kind)
	{
		std::string str;
		if (ImGui::BeginDragDropTargetCustom1({ ImGui::GetItemRectMin(), ImGui::GetItemRectMax() }, ImGui::GetHoveredID()))
		{
			if (const ImGuiPayload* payload = ImGui::AcceptDragDropPayload(kind))
				str = std::string(static_cast<char*>(payload->Data), payload->DataSize);
			ImGui::EndDragDropTarget();
		}
		return str;
	}


	static bool isImage(const std::string& ext)
	{
		static std::array<std::string, 10> IMAGE_EXTENSIONS{
	".png", ".jpg", ".tga", ".tga", ".bmp", ".psd", ".gif", ".hdr", ".pic", ".exr"
		};

		return std::ranges::any_of(IMAGE_EXTENSIONS, [ext](const std::string& inRange) { return ext == inRange; });
	}

	static bool isMesh(const std::string& ext)
	{
		static std::array<std::string, 7> MESH_EXTENSIONS{
		".fbx", ".ass", ".blend", ".obj", ".dae", ".gltf", ".gltf2"
		};
		return std::ranges::any_of(MESH_EXTENSIONS, [ext](const std::string& inRange) { return ext == inRange; });
	}

	static void startPayload(const std::string& fullpath)
	{

		if (ImGui::BeginDragDropSource())
		{


			// Set payload to carry the index of our item (could be anything)
			const std::filesystem::path path(fullpath);
			if (!path.has_extension())
			{
				ImGui::EndDragDropSource();
				return;
			}
			const auto format = path.extension().string();

			const char* kind;
			if (isImage(format))
				kind = "Image";
			else if (isMesh(format))
				kind = "Mesh";
			else if (format == ".hsc")
				kind = "Scene";
			else {
				ImGui::EndDragDropSource();
				return;
			}
			ImGui::SetDragDropPayload(kind, fullpath.c_str(), fullpath.length());
			ImGui::Text("Load %s \n format: %s", fullpath.c_str(), format.c_str());
			ImGui::EndDragDropSource();
		}
	}



	static int s_UIContextID;
	static uint32_t s_Counter;
	static char s_IDBuffer[16];


	static void PushID()
	{
		ImGui::PushID(s_UIContextID++);
		s_Counter = 0;
	}

	static void PopID()
	{
		ImGui::PopID();
		s_UIContextID--;
	}

	static void BeginPropertyGrid()
	{
		PushID();
		ImGui::Columns(2);
	}

	static bool PropertySlider(const char* label, int& value, const int min, const int max)
	{
		bool modified = false;

		ImGui::Text(label);
		ImGui::NextColumn();
		ImGui::PushItemWidth(-1);

		s_IDBuffer[0] = '#';
		s_IDBuffer[1] = '#';
		memset(s_IDBuffer + 2, 0, 14);
		//itoa(s_Counter++, s_IDBuffer + 2, 16);
		sprintf_s(s_IDBuffer + 2, 14, "%o", s_Counter++);

		if (ImGui::SliderInt(s_IDBuffer, &value, min, max))
			modified = true;

		ImGui::PopItemWidth();
		ImGui::NextColumn();

		return modified;
	}

	static bool Property(const char* label, int* flag, int flagValue)
	{
		bool modified = false;

		ImGui::Text(label);
		ImGui::NextColumn();
		ImGui::PushItemWidth(-1);

		s_IDBuffer[0] = '#';
		s_IDBuffer[1] = '#';
		memset(s_IDBuffer + 2, 0, 14);
		//itoa(s_Counter++, s_IDBuffer + 2, 16);
		sprintf_s(s_IDBuffer + 2, 14, "%o", s_Counter++);

		ImGui::CheckboxFlagsT(s_IDBuffer, flag, flagValue);

		ImGui::PopItemWidth();
		ImGui::NextColumn();

		return modified;
	}

	static bool Property(const char* label, bool& value)
	{
		bool modified = false;

		ImGui::Text(label);
		ImGui::NextColumn();
		ImGui::PushItemWidth(-1);

		s_IDBuffer[0] = '#';
		s_IDBuffer[1] = '#';
		memset(s_IDBuffer + 2, 0, 14);
		//itoa(s_Counter++, s_IDBuffer + 2, 16);
		sprintf_s(s_IDBuffer + 2, 14, "%o", s_Counter++);
		if (ImGui::Checkbox(s_IDBuffer, &value))
			modified = true;

		ImGui::PopItemWidth();
		ImGui::NextColumn();

		return modified;
	}

	static bool PropertyColor(const char* label, glm::vec3& value)
	{
		bool modified = false;

		ImGui::Text(label);
		ImGui::NextColumn();
		ImGui::PushItemWidth(-1);

		s_IDBuffer[0] = '#';
		s_IDBuffer[1] = '#';
		memset(s_IDBuffer + 2, 0, 14);
		sprintf_s(s_IDBuffer + 2, 14, "%o", s_Counter++);

		//itoa(s_Counter++, s_IDBuffer + 2, 16);
		if (ImGui::ColorEdit3(s_IDBuffer, glm::value_ptr(value)))
			modified = true;

		ImGui::PopItemWidth();
		ImGui::NextColumn();

		return modified;
	}

	static bool Property(const char* label, std::string& value, bool error = false)
	{
		bool modified = false;

		ImGui::Text("%s", label);
		ImGui::NextColumn();
		ImGui::PushItemWidth(-1);

		char buffer[256];
		strcpy_s(buffer, value.c_str());

		s_IDBuffer[0] = '#';
		s_IDBuffer[1] = '#';
		memset(s_IDBuffer + 2, 0, 14);
		//itoa(s_Counter++, s_IDBuffer + 2, 16);
		sprintf_s(s_IDBuffer + 2, 14, "%o", s_Counter++);

		if (error)
			ImGui::PushStyleColor(ImGuiCol_Text, ImVec4(0.9f, 0.2f, 0.2f, 1.0f));
		if (ImGui::InputText(s_IDBuffer, buffer, 256))
		{
			value = buffer;
			modified = true;
		}
		if (error)
			ImGui::PopStyleColor();
		ImGui::PopItemWidth();
		ImGui::NextColumn();

		return modified;
	}

	static void Property(const char* label, const char* value)
	{
		ImGui::Text("%s", label);
		ImGui::NextColumn();
		ImGui::PushItemWidth(-1);

		s_IDBuffer[0] = '#';
		s_IDBuffer[1] = '#';
		memset(s_IDBuffer + 2, 0, 14);
		//_itoa(s_Counter++, s_IDBuffer + 2, 16);
		sprintf_s(s_IDBuffer + 2, 14, "%o", s_Counter++);

		ImGui::InputText(s_IDBuffer, const_cast<char*>(value), 256, ImGuiInputTextFlags_ReadOnly);

		ImGui::PopItemWidth();
		ImGui::NextColumn();
	}

	static bool Property(const char* label, int& value, float speed = 1, int min = 0, int max = 0)
	{
		bool modified = false;

		ImGui::Text("%s", label);
		ImGui::NextColumn();
		ImGui::PushItemWidth(-1);

		s_IDBuffer[0] = '#';
		s_IDBuffer[1] = '#';
		memset(s_IDBuffer + 2, 0, 14);
		//_itoa(s_Counter++, s_IDBuffer + 2, 16);
		sprintf_s(s_IDBuffer + 2, 14, "%o", s_Counter++);
		if (ImGui::DragInt(s_IDBuffer, &value, speed, min, max))
			modified = true;

		ImGui::PopItemWidth();
		ImGui::NextColumn();

		return modified;
	}

	static bool Property(const char* label, float& value, const float delta = 0.1f, const float min = 0.f, const float max = 0.f)
	{
		bool modified = false;

		ImGui::Text(label);
		ImGui::NextColumn();
		ImGui::PushItemWidth(-1);

		s_IDBuffer[0] = '#';
		s_IDBuffer[1] = '#';
		memset(s_IDBuffer + 2, 0, 14);
		//itoa(s_Counter++, s_IDBuffer + 2, 16);
		sprintf_s(s_IDBuffer + 2, 14, "%o", s_Counter++);
		if (ImGui::DragFloat(s_IDBuffer, &value, delta, min, max))
			modified = true;

		ImGui::PopItemWidth();
		ImGui::NextColumn();

		return modified;
	}

	static bool Property(const char* label, glm::vec2& value, const float delta = 0.1f)
	{
		bool modified = false;

		ImGui::Text("%s", label);
		ImGui::NextColumn();
		ImGui::PushItemWidth(-1);

		s_IDBuffer[0] = '#';
		s_IDBuffer[1] = '#';
		memset(s_IDBuffer + 2, 0, 14);
		//_itoa(s_Counter++, s_IDBuffer + 2, 16);
		sprintf_s(s_IDBuffer + 2, 14, "%o", s_Counter++);
		if (ImGui::DragFloat2(s_IDBuffer, glm::value_ptr(value), delta))
			modified = true;

		ImGui::PopItemWidth();
		ImGui::NextColumn();

		return modified;
	}

	static bool Property(const char* label, glm::vec3& value, const float delta = 0.1f)
	{
		bool modified = false;

		ImGui::Text("%s", label);
		ImGui::NextColumn();
		ImGui::PushItemWidth(-1);

		s_IDBuffer[0] = '#';
		s_IDBuffer[1] = '#';
		memset(s_IDBuffer + 2, 0, 14);
		//_itoa(s_Counter++, s_IDBuffer + 2, 16);
		sprintf_s(s_IDBuffer + 2, 14, "%o", s_Counter++);
		if (ImGui::DragFloat3(s_IDBuffer, glm::value_ptr(value), delta))
			modified = true;

		ImGui::PopItemWidth();
		ImGui::NextColumn();

		return modified;
	}

	static bool Property(const char* label, glm::vec4& value, float delta = 0.1f)
	{
		bool modified = false;

		ImGui::Text("%s", label);
		ImGui::NextColumn();
		ImGui::PushItemWidth(-1);

		s_IDBuffer[0] = '#';
		s_IDBuffer[1] = '#';
		memset(s_IDBuffer + 2, 0, 14);
		//_itoa(s_Counter++, s_IDBuffer + 2, 16);
		sprintf_s(s_IDBuffer + 2, 14, "%o", s_Counter++);
		if (ImGui::DragFloat4(s_IDBuffer, glm::value_ptr(value), delta))
			modified = true;

		ImGui::PopItemWidth();
		ImGui::NextColumn();

		return modified;
	}

	static void EndPropertyGrid()
	{
		ImGui::Columns(1);
		PopID();
	}

	static bool BeginTreeNode(const char* name, bool defaultOpen = true)
	{
		ImGuiTreeNodeFlags treeNodeFlags = ImGuiTreeNodeFlags_Framed | ImGuiTreeNodeFlags_SpanAvailWidth | ImGuiTreeNodeFlags_FramePadding;
		if (defaultOpen)
			treeNodeFlags |= ImGuiTreeNodeFlags_DefaultOpen;

		return ImGui::TreeNodeEx(name, treeNodeFlags);
	}

	static void EndTreeNode()
	{
		ImGui::TreePop();
	}
}
