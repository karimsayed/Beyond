#pragma once

#include "Beyond/Core/Layer.h"

namespace bey {

	class ImGuiLayer final : public Layer
	{
	public:
		ImGuiLayer();
		explicit ImGuiLayer(const std::string& name);
		~ImGuiLayer();

		static void begin();
		static void end();

		void onAttach() override;
		void onDetach() override;
		void onImGuiRender() override;
	private:
		float mTime = 0.0f;
	};

}
