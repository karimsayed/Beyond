#include "pchs.h" 
#include "Mesh.h"

#include <assimp/scene.h>
#include <assimp/postprocess.h>
#include <assimp/Importer.hpp>
#include <assimp/Exporter.hpp>
#include <assimp/SceneCombiner.h>
#include <assimp/DefaultLogger.hpp>
#include <assimp/LogStream.hpp>

#include "SceneRenderer.h"
#include "Beyond/Renderer/Renderer.h"
#include "Beyond/Platform/OpenGL/Texture.h"
#include "Beyond/Scene/MaterialAttrFinder.hpp"
#include <Beyond/Core/Log.h>



namespace bey {

#define MESH_DEBUG_LOG 1
#if MESH_DEBUG_LOG
#define BEY_MESH_LOG(...) BEY_CORE_TRACE(__VA_ARGS__)
#else
#define BEY_MESH_LOG(...)
#endif

	static glm::mat4 mat4FromAssimpMat4(const aiMatrix4x4& matrix)
	{
		glm::mat4 result;
		//the a,b,c,d in assimp is the row ; the 1,2,3,4 is the column
		result[0][0] = matrix.a1; result[1][0] = matrix.a2; result[2][0] = matrix.a3; result[3][0] = matrix.a4;
		result[0][1] = matrix.b1; result[1][1] = matrix.b2; result[2][1] = matrix.b3; result[3][1] = matrix.b4;
		result[0][2] = matrix.c1; result[1][2] = matrix.c2; result[2][2] = matrix.c3; result[3][2] = matrix.c4;
		result[0][3] = matrix.d1; result[1][3] = matrix.d2; result[2][3] = matrix.d3; result[3][3] = matrix.d4;
		return result;
	}

	static aiMatrix4x4 glmMat4ToAi(glm::mat4 mat)
	{
		return aiMatrix4x4(mat[0][0], mat[1][0], mat[2][0], mat[3][0],
				     	   mat[0][1], mat[1][1], mat[2][1], mat[3][1],
						   mat[0][2], mat[1][2], mat[2][2], mat[3][2],
						   mat[0][3], mat[1][3], mat[2][3], mat[3][3]);
	}



#if MESH_DEBUG_LOG
	struct LogStream final : Assimp::LogStream
	{
		static void initialize()
		{
			if (Assimp::DefaultLogger::isNullLogger())
			{
				Assimp::DefaultLogger::create("", Assimp::Logger::VERBOSE);
				Assimp::DefaultLogger::get()->attachStream(new LogStream, Assimp::Logger::Err | Assimp::Logger::Warn);
			}
		}

		void write(const char* message) override
		{
			BEY_CORE_WARN("Assimp warning: {0}", message);
		}
	};
#endif

	//void Mesh::updateTransforms(const glm::mat4& rootTransform)
	//{

	//	mScene->mRootNode->mTransformation = glmMat4ToAi(rootTransform);
	//	for (unsigned i = 0; i < mScene->mRootNode->mNumChildren; i++)
	//		mScene->mRootNode->mChildren[i]->mTransformation = glmMat4ToAi(mSubMeshes[i].Transform);
	//}




	Mesh::Mesh(const std::string& filename)
		: mFilePath(filename)
	{
		BEY_PROFILE_FUNCTION();

#if MESH_DEBUG_LOG
		LogStream::initialize();
#endif
		BEY_CORE_INFO("Loading mesh: {0}", filename.c_str());

		mImporter = std::make_unique<Assimp::Importer>();


		const aiScene* scene = mImporter->ReadFile(filename, S_MESH_IMPORT_FLAGS);
		if (!scene || !scene->HasMeshes())
			BEY_CORE_ERROR("Failed to load mesh file: {0}", filename);


		aiCopyScene(scene, &mScene);

		mIsAnimated = mScene->mAnimations;
		mMeshShader = mIsAnimated ? Renderer::getShaderLibrary()->get("BeyondPBR_Anim") : Renderer::getShaderLibrary()->get("LightAccumulation");
		mBaseMaterial = Ref<Material>::create(mMeshShader);
		mInverseTransform = inverse(mat4FromAssimpMat4(scene->mRootNode->mTransformation));

		uint32_t vertexCount = 0;
		uint32_t indexCount = 0;
		{
			BEY_PROFILE_SCOPE("loading submeshes");

			mSubMeshes.reserve(scene->mNumMeshes);
			for (size_t m = 0; m < scene->mNumMeshes; m++)
			{
				aiMesh* mesh = scene->mMeshes[m];

				Submesh& submesh = mSubMeshes.emplace_back();
				submesh.BaseVertex = vertexCount;
				submesh.BaseIndex = indexCount;
				submesh.MaterialIndex = mesh->mMaterialIndex;
				submesh.IndexCount = mesh->mNumFaces * 3;
				submesh.MeshName = mesh->mName.C_Str();

				vertexCount += mesh->mNumVertices;
				indexCount += submesh.IndexCount;

				BEY_CORE_ASSERT(mesh->HasPositions(), "Meshes require positions.");
				BEY_CORE_ASSERT(mesh->HasNormals(), "Meshes require normals.");

				// Vertices
				if (mIsAnimated)
				{
					for (size_t i = 0; i < mesh->mNumVertices; i++)
					{
						AnimatedVertex vertex;
						vertex.Position = { mesh->mVertices[i].x, mesh->mVertices[i].y, mesh->mVertices[i].z };
						vertex.Normal = { mesh->mNormals[i].x, mesh->mNormals[i].y, mesh->mNormals[i].z };

						if (mesh->HasTangentsAndBitangents())
						{
							vertex.Tangent = { mesh->mTangents[i].x, mesh->mTangents[i].y, mesh->mTangents[i].z };
							vertex.Binormal = { mesh->mBitangents[i].x, mesh->mBitangents[i].y, mesh->mBitangents[i].z };
						}

						if (mesh->HasTextureCoords(0))
							vertex.TexCoord = { mesh->mTextureCoords[0][i].x, mesh->mTextureCoords[0][i].y };

						mAnimatedVertices.push_back(vertex);
					}
				}
				else
				{
					auto& aabb = submesh.BoundingBox;
					aabb.Min = { FLT_MAX, FLT_MAX, FLT_MAX };
					aabb.Max = { -FLT_MAX, -FLT_MAX, -FLT_MAX };
					for (size_t i = 0; i < mesh->mNumVertices; i++)
					{
						Vertex vertex{};
						vertex.Position = { mesh->mVertices[i].x, mesh->mVertices[i].y, mesh->mVertices[i].z };
						vertex.Normal = { mesh->mNormals[i].x, mesh->mNormals[i].y, mesh->mNormals[i].z };
						aabb.Min.x = glm::min(vertex.Position.x, aabb.Min.x);
						aabb.Min.y = glm::min(vertex.Position.y, aabb.Min.y);
						aabb.Min.z = glm::min(vertex.Position.z, aabb.Min.z);
						aabb.Max.x = glm::max(vertex.Position.x, aabb.Max.x);
						aabb.Max.y = glm::max(vertex.Position.y, aabb.Max.y);
						aabb.Max.z = glm::max(vertex.Position.z, aabb.Max.z);

						if (mesh->HasTangentsAndBitangents())
						{
							vertex.Tangent = { mesh->mTangents[i].x, mesh->mTangents[i].y, mesh->mTangents[i].z };
							vertex.Binormal = { mesh->mBitangents[i].x, mesh->mBitangents[i].y, mesh->mBitangents[i].z };
						}

						if (mesh->HasTextureCoords(0))
							vertex.TexCoord = { mesh->mTextureCoords[0][i].x, mesh->mTextureCoords[0][i].y };

						mStaticVertices.push_back(vertex);
					}
				}

				// Indices
				for (size_t i = 0; i < mesh->mNumFaces; i++)
				{
					BEY_CORE_ASSERT(mesh->mFaces[i].mNumIndices == 3, "Must have 3 indices.");
					Index index = { mesh->mFaces[i].mIndices[0], mesh->mFaces[i].mIndices[1], mesh->mFaces[i].mIndices[2] };
					mIndices.push_back(index);

					if (!mIsAnimated)
						mTriangleCache[unsigned(m)].emplace_back(mStaticVertices[size_t(index.V1 + submesh.BaseVertex)], mStaticVertices[size_t(index.V2 + submesh.BaseVertex)],
							mStaticVertices[size_t(index.V3 + submesh.BaseVertex)]);
				}


			}
		}

		traverseNodes(scene->mRootNode);

		// Bones
		if (mIsAnimated)
		{
			for (size_t m = 0; m < scene->mNumMeshes; m++)
			{
				aiMesh* mesh = scene->mMeshes[m];
				Submesh& submesh = mSubMeshes[m];

				for (size_t i = 0; i < mesh->mNumBones; i++)
				{
					aiBone* bone = mesh->mBones[i];
					std::string boneName(bone->mName.data);
					int boneIndex = 0;

					if (mBoneMapping.find(boneName) == mBoneMapping.end())
					{
						// Allocate an index for a new bone
						boneIndex = mBoneCount;
						mBoneCount++;
						BoneInfo bi{};
						mBoneInfo.push_back(bi);
						mBoneInfo[boneIndex].BoneOffset = mat4FromAssimpMat4(bone->mOffsetMatrix);
						mBoneMapping[boneName] = boneIndex;
					}
					else
					{
						BEY_MESH_LOG("Found existing bone in map");
						boneIndex = mBoneMapping[boneName];
					}

					for (size_t j = 0; j < bone->mNumWeights; j++)
					{
						unsigned vertexID = submesh.BaseVertex + bone->mWeights[j].mVertexId;
						float weight = bone->mWeights[j].mWeight;
						mAnimatedVertices[vertexID].addBoneData(boneIndex, weight);
					}
				}
			}
		}


		if (mScene->HasMaterials())
			setTextures();



		mVertexArray = VertexArray::create();
		if (mIsAnimated)
		{
			auto vb = VertexBuffer::create(mAnimatedVertices.data(), uint32_t(mAnimatedVertices.size() * sizeof(AnimatedVertex)));
			vb->setLayout({
				{ ShaderDataType::Float3, "a_Position" },
				{ ShaderDataType::Float3, "a_Normal" },
				{ ShaderDataType::Float3, "a_Tangent" },
				{ ShaderDataType::Float3, "a_Binormal" },
				{ ShaderDataType::Float2, "a_TexCoord" },
				{ ShaderDataType::Int4, "a_BoneIDs" },
				{ ShaderDataType::Float4, "a_BoneWeights" },
				});
			mVertexArray->addVertexBuffer(vb);
		}
		else
		{
			auto vb = VertexBuffer::create(mStaticVertices.data(), uint32_t(mStaticVertices.size() * sizeof(Vertex)));
			vb->setLayout({
				{ ShaderDataType::Float3, "a_Position" },
				{ ShaderDataType::Float3, "a_Normal" },
				{ ShaderDataType::Float3, "a_Tangent" },
				{ ShaderDataType::Float3, "a_Binormal" },
				{ ShaderDataType::Float2, "a_TexCoord" },
				});
			mVertexArray->addVertexBuffer(vb);
		}

		auto ib = IndexBuffer::create(mIndices.data(), uint32_t(mIndices.size() * sizeof(Index)));
		mVertexArray->setIndexBuffer(ib);
	}




	void Mesh::setTextures()
	{

		BEY_PROFILE_FUNCTION();
		// Materials
		BEY_MESH_LOG("---- Materials - {0} ----", mFilePath);

		mTextures.resize(mScene->mNumMaterials);
		mMaterials.resize(mScene->mNumMaterials);
		for (uint32_t i = 0; i < mScene->mNumMaterials; i++)
		{

			BEY_PROFILE_SCOPE("Loading Material");
			auto* aiMaterial = mScene->mMaterials[i];
			auto aiMaterialName = aiMaterial->GetName();

			auto mi = Ref<MaterialInstance>::create(mBaseMaterial, aiMaterialName.data);
			mMaterials[i] = mi;
			BEY_MESH_LOG("  {0} (Index = {1})", aiMaterialName.data, i);

			aiString aiTexPath;
			uint32_t textureCount = aiMaterial->GetTextureCount(aiTextureType_DIFFUSE);
			BEY_MESH_LOG("    TextureCount = {0}", textureCount);


			float  height, displacement;

			if (aiMaterial->Get(AI_MATKEY_BUMPSCALING, height) != aiReturn_SUCCESS)
				height = 0.0f;

			if (aiMaterial->Get(AI_MATKEY_BUMPSCALING, displacement) != aiReturn_SUCCESS)
				displacement = 0.0f;

			{
				auto [colorTexture, colorVec] = MaterialAttrFinder::findColor(mFilePath, aiMaterial);
				BEY_MESH_LOG("    COLOR = {0}, {1}, {2}", colorVec.r, colorVec.g, colorVec.b);
				if (colorTexture != nullptr)
				{
					BEY_MESH_LOG("    Albedo map path = {0}", colorTexture->getPath());
					if (colorTexture->loaded())
					{
						mTextures[i] = colorTexture;
						mi->set("u_AlbedoTexture", mTextures[i]);
						mi->set("u_AlbedoTexToggle", 1.0f, ShaderDomain::Pixel);
						mi->set<glm::vec4>("u_AlbedoColor", colorVec, ShaderDomain::Pixel);

					}
					else
					{
						BEY_CORE_ERROR("Could not load texture: {0}", colorTexture->getPath());
						// Fallback to albedo color
						mi->set<glm::vec4>("u_AlbedoColor", colorVec, ShaderDomain::Pixel);
					}
				}
				else
				{
					mi->set<glm::vec4>("u_AlbedoColor", colorVec, ShaderDomain::Pixel);
					BEY_MESH_LOG("    No albedo map");
				}
			}
			{

				auto normalTexture = MaterialAttrFinder::findNormalMap(mFilePath, aiMaterial);

				if (normalTexture != nullptr)
				{
					BEY_MESH_LOG("    Normal map path = {0}", normalTexture->getPath());
					if (normalTexture->loaded())
					{
						mi->set("u_NormalTexture", normalTexture);
						mi->set("u_NormalTexToggle", 1.0f, ShaderDomain::Pixel);
					}
					else
					{
						BEY_CORE_ERROR("    Could not load texture: {0}", normalTexture->getPath());
					}
				}
				else
				{
					BEY_MESH_LOG("    No normal map");
				}
			}
			{
				if (aiMaterial->GetTexture(aiTextureType_HEIGHT, 0, &aiTexPath) == AI_SUCCESS)
				{
					// TODO: Temp - this should be handled by bey's filesystem
					std::string texturePath = MaterialAttrFinder::getFullPath(mFilePath, aiTexPath.C_Str());
					BEY_MESH_LOG("    Height map path = {0}", texturePath);
					auto texture = Texture2D::create(texturePath, DataType::UNSIGNED_BYTE, false, false, aiTextureType_HEIGHT);
					if (texture->loaded())
					{
						mi->set("u_HeightTexture", texture);
						//mi->set("u_HeightScale", 0.04f, ShaderDomain::Pixel);
						mi->set("u_HeightTexToggle", 1.0f, ShaderDomain::Pixel);
					}
					else
						BEY_CORE_ERROR("    Could not load texture: {0}", texturePath);
				}
				else
				{
					BEY_MESH_LOG("    No Height map");
					mi->set("u_HeightScale", height, ShaderDomain::Pixel);
					mi->set("u_HeightTexToggle", 0.0f, ShaderDomain::Pixel);

				}
			}
			{
				auto [metalnessTex, metalnessValue] = MaterialAttrFinder::findMetalness(mFilePath, aiMaterial);

				if (metalnessTex != nullptr)
				{
					BEY_MESH_LOG("    Metalness map path = {0}", metalnessTex->getPath());

					if (metalnessTex->loaded())
					{
						mi->set("u_MetalnessTexture", metalnessTex);
						mi->set("u_MetalnessTexToggle", 1.0f, ShaderDomain::Pixel);
					}
					else
					{
						BEY_CORE_ERROR("    Could not load texture: {0}", metalnessTex->getPath());
					}
				}
				else
				{
					BEY_MESH_LOG("    No Metalness map");
					mi->set("u_Metalness", metalnessValue, ShaderDomain::Pixel);
				}
				BEY_MESH_LOG("    METALNESS = {0}", metalnessValue);

			}

			{

				auto [roughnessTexture, roughnessValue] = MaterialAttrFinder::findRoughness(mFilePath, aiMaterial);

				if (roughnessTexture != nullptr)
				{
					BEY_MESH_LOG("    Roughness map path = {0}", roughnessTexture->getPath());
					if (roughnessTexture->loaded())
					{
						mi->set("u_RoughnessTexture", roughnessTexture);
						mi->set("u_RoughnessTexToggle", 1.0f, ShaderDomain::Pixel);
					}
					else
					{
						BEY_CORE_ERROR("    Could not load texture: {0}", roughnessTexture->getPath());
					}
				}
				else
				{
					BEY_MESH_LOG("    No roughness map");
					mi->set("u_Roughness", roughnessValue, ShaderDomain::Pixel);

				}
				BEY_MESH_LOG("    ROUGHNESS = {0}", roughnessValue);

			}


			if (aiMaterial->GetTexture(aiTextureType_DISPLACEMENT, 0, &aiTexPath) == AI_SUCCESS)
			{
				// TODO: Temp - this should be handled by bey's filesystem
				std::string texturePath = MaterialAttrFinder::getFullPath(mFilePath, aiTexPath.C_Str());
				BEY_MESH_LOG("    Displacement map path = {0}", texturePath);
				auto texture = Texture2D::create(texturePath, DataType::UNSIGNED_BYTE, false, false, aiTextureType_DISPLACEMENT);
				if (texture->loaded())
				{
					mi->set("u_DisplacementTexture", texture);
					mi->set("u_DisplacementScale", 1.0f, ShaderDomain::TessES);
					mi->set("u_DisplacementTexToggle", 1.0f, ShaderDomain::TessES);
					//mi->set("u_Tesselate", true, ShaderDomain::TessCS);
				}
				else
					BEY_CORE_ERROR("    Could not load texture: {0}", texturePath);
			}
			else
			{
				BEY_MESH_LOG("    No Displacement map");
				mi->set("u_DisplacementScale", displacement, ShaderDomain::TessES);
				mi->set("u_DisplacementTexToggle", 0.0f, ShaderDomain::TessES);
				//mi->set("u_Tesselate", false, ShaderDomain::TessCS);

			}



#if 0
			// Metalness map (or is it??)
			if (aiMaterial->Get("$raw.ReflectionFactor|file", aiPTI_String, 0, aiTexPath) == AI_SUCCESS)
			{
				// TODO: Temp - this should be handled by bey's filesystem
				std::filesystem::path path = mFilePath;
				auto parentPath = path.parent_path();
				parentPath /= std::string(aiTexPath.data);
				std::string texturePath = parentPath.string();

				auto texture = Texture2D::create(texturePath, DataType::UNSIGNED_BYTE);
				if (texture->loaded())
				{
					BEY_MESH_LOG("    Metalness map path = {0}", texturePath);
					mi->set("u_MetalnessTexture", texture);
					mi->set("u_MetalnessTexToggle", 1.0f);
				}
				else
				{
					BEY_CORE_ERROR("Could not load texture: {0}", texturePath);
				}
			}
			else
			{
				BEY_MESH_LOG("    No metalness texture");
				mi->set("u_Metalness", metalness);
			}


			bool metalnessTextureFound = false;
			for (uint32_t j = 0; j < aiMaterial->mNumProperties; j++)
			{
				auto prop = aiMaterial->mProperties[j];

				BEY_MESH_LOG("Material Property:");
				BEY_MESH_LOG("  Name = {0}", prop->mKey.data);
				BEY_MESH_LOG("  Type = {0}", prop->mType);
				BEY_MESH_LOG("  Size = {0}", prop->mDataLength);
				float data = *(float*)prop->mData;
				BEY_MESH_LOG("  Value = {0}", data);

				switch (prop->mSemantic)
				{
				case aiTextureType_NONE:
					BEY_MESH_LOG("  Semantic = aiTextureType_NONE");
					break;
				case aiTextureType_DIFFUSE:
					BEY_MESH_LOG("  Semantic = aiTextureType_DIFFUSE");
					break;
				case aiTextureType_SPECULAR:
					BEY_MESH_LOG("  Semantic = aiTextureType_SPECULAR");
					break;
				case aiTextureType_AMBIENT:
					BEY_MESH_LOG("  Semantic = aiTextureType_AMBIENT");
					break;
				case aiTextureType_EMISSIVE:
					BEY_MESH_LOG("  Semantic = aiTextureType_EMISSIVE");
					break;
				case aiTextureType_HEIGHT:
					BEY_MESH_LOG("  Semantic = aiTextureType_HEIGHT");
					break;
				case aiTextureType_NORMALS:
					BEY_MESH_LOG("  Semantic = aiTextureType_NORMALS");
					break;
				case aiTextureType_SHININESS:
					BEY_MESH_LOG("  Semantic = aiTextureType_SHININESS");
					break;
				case aiTextureType_OPACITY:
					BEY_MESH_LOG("  Semantic = aiTextureType_OPACITY");
					break;
				case aiTextureType_DISPLACEMENT:
					BEY_MESH_LOG("  Semantic = aiTextureType_DISPLACEMENT");
					break;
				case aiTextureType_LIGHTMAP:
					BEY_MESH_LOG("  Semantic = aiTextureType_LIGHTMAP");
					break;
				case aiTextureType_REFLECTION:
					BEY_MESH_LOG("  Semantic = aiTextureType_REFLECTION");
					break;
				case aiTextureType_UNKNOWN:
					BEY_MESH_LOG("  Semantic = aiTextureType_UNKNOWN");
					break;
				}

				if (prop->mType == aiPTI_String)
				{
					uint32_t strLength = *reinterpret_cast<uint32_t*>(prop->mData);
					std::string str(prop->mData + 4, strLength);

					std::string key = prop->mKey.data;
					if (key == "$raw.ReflectionFactor|file")
					{
						metalnessTextureFound = true;

						// TODO: Temp - this should be handled by bey's filesystem
						std::filesystem::path path = mFilePath;
						auto parentPath = path.parent_path();
						parentPath /= str;
						std::string texturePath = parentPath.string();
						BEY_MESH_LOG("    Metalness map path = {0}", texturePath);
						auto texture = Texture2D::create(texturePath, DataType::UNSIGNED_BYTE, false, aiTextureType_METALNESS);
						if (texture->loaded())
						{
							mi->set("u_MetalnessTexture", texture);
							mi->set("u_MetalnessTexToggle", 1.0f, ShaderDomain::Pixel);
						}
						else
						{
							BEY_CORE_ERROR("    Could not load texture: {0}", texturePath);
							mi->set("u_Metalness", metalness, ShaderDomain::Pixel);
							mi->set("u_MetalnessTexToggle", 0.0f, ShaderDomain::Pixel);
						}
						break;
					}

				}

			}

			//if (!metalnessTextureFound)
			//{
			//	BEY_MESH_LOG("    No metalness map");
			//
			//	mi->set("u_Metalness", metalness, ShaderDomain::Pixel);
			//	mi->set("u_MetalnessTexToggle", 0.0f, ShaderDomain::Pixel);
			//}

#endif
		}
		BEY_MESH_LOG("-----------------------------------------------------------");

	}

	Mesh::~Mesh() = default;

	void Mesh::onUpdate(TimeStep ts, const glm::vec4& cameraPos)
	{
		BEY_PROFILE_FUNCTION();
		for (auto& subMesh : mSubMeshes)
			subMesh.Distance = glm::distance(subMesh.Transform[3], cameraPos);

		//std::ranges::sort(mSubMeshes, [](auto& a, auto& b) { return a.Distance < b.Distance; });
		if (mIsAnimated)
		{
			if (mAnimationPlaying)
			{
				mWorldTime += ts;

				const float ticksPerSecond = float(mScene->mAnimations[0]->mTicksPerSecond != 0.0 ? mScene->mAnimations[0]->mTicksPerSecond : 25.0) * mTimeMultiplier;
				mAnimationTime += ts * ticksPerSecond;
				mAnimationTime = std::fmod(mAnimationTime, float(mScene->mAnimations[0]->mDuration));
			}

			// TODO: We only need to recalculate bones if rendering has been requested at the current animation frame
			boneTransform(mAnimationTime);
		}

	}

	static std::string levelToSpaces(const uint32_t level)
	{
		std::string result;
		for (uint32_t i = 0; i < level; i++)
			result += "--";
		return result;
	}

	void Mesh::updateNodes(aiNode* node, const aiMatrix4x4& parentTransform, const uint32_t level)
	{
		BEY_PROFILE_FUNCTION();
		const auto transform = glm::inverse(mat4FromAssimpMat4(parentTransform));
		BEY_MESH_LOG("parent transform: {}", glm::to_string(transform));
		for (uint32_t i = 0; i < node->mNumMeshes; i++)
		{
			const uint32_t mesh = node->mMeshes[i];
			auto& submesh = mSubMeshes[mesh];
			node->mTransformation = glmMat4ToAi(transform * submesh.Transform);
			BEY_MESH_LOG("updated transoform: {}", node->mTransformation);
		}

		BEY_MESH_LOG("Updated: {0} {1}", levelToSpaces(level), node->mName.C_Str());

		for (uint32_t i = 0; i < node->mNumChildren; i++)
			updateNodes(node->mChildren[i], glmMat4ToAi(transform), level + 1);
	}

	void Mesh::traverseNodes(aiNode* node, const glm::mat4& parentTransform, const uint32_t level)
	{
		BEY_PROFILE_FUNCTION();
		const glm::mat4 transform = parentTransform * mat4FromAssimpMat4(node->mTransformation);
		for (uint32_t i = 0; i < node->mNumMeshes; i++)
		{
			const uint32_t mesh = node->mMeshes[i];
			auto& submesh = mSubMeshes[mesh];
			submesh.NodeName = node->mName.C_Str();
			submesh.Transform = transform;
		}

		BEY_MESH_LOG("{0} {1}", levelToSpaces(level), node->mName.C_Str());

		for (uint32_t i = 0; i < node->mNumChildren; i++)
			traverseNodes(node->mChildren[i], transform, level + 1);
	}

	uint32_t Mesh::findPosition(const float animationTime, const aiNodeAnim* pNodeAnim)
	{
		BEY_PROFILE_FUNCTION();
		for (uint32_t i = 0; i < pNodeAnim->mNumPositionKeys - 1; i++)
		{
			if (animationTime < float(pNodeAnim->mPositionKeys[i + 1].mTime))
				return i;
		}

		return 0;
	}


	uint32_t Mesh::findRotation(const float animationTime, const aiNodeAnim* pNodeAnim)
	{
		BEY_PROFILE_FUNCTION();
		BEY_CORE_ASSERT(pNodeAnim->mNumRotationKeys > 0);

		for (uint32_t i = 0; i < pNodeAnim->mNumRotationKeys - 1; i++)
		{
			if (animationTime < float(pNodeAnim->mRotationKeys[i + 1].mTime))
				return i;
		}

		return 0;
	}


	uint32_t Mesh::findScaling(const float animationTime, const aiNodeAnim* pNodeAnim)
	{
		BEY_PROFILE_FUNCTION();
		BEY_CORE_ASSERT(pNodeAnim->mNumScalingKeys > 0);

		for (uint32_t i = 0; i < pNodeAnim->mNumScalingKeys - 1; i++)
		{
			if (animationTime < float(pNodeAnim->mScalingKeys[i + 1].mTime))
				return i;
		}

		return 0;
	}


	glm::vec3 Mesh::interpolateTranslation(const float animationTime, const aiNodeAnim* nodeAnim)
	{
		BEY_PROFILE_FUNCTION();
		if (nodeAnim->mNumPositionKeys == 1)
		{
			// No interpolation necessary for single value
			const auto v = nodeAnim->mPositionKeys[0].mValue;
			return { v.x, v.y, v.z };
		}

		const uint32_t positionIndex = findPosition(animationTime, nodeAnim);
		const uint32_t nextPositionIndex = (positionIndex + 1);
		BEY_CORE_ASSERT(nextPositionIndex < nodeAnim->mNumPositionKeys);
		const auto deltaTime = float(nodeAnim->mPositionKeys[nextPositionIndex].mTime - nodeAnim->mPositionKeys[positionIndex].mTime);
		float factor = (animationTime - float(nodeAnim->mPositionKeys[positionIndex].mTime)) / deltaTime;
		if (factor < 0.0f)
			factor = 0.0f;
		BEY_CORE_ASSERT(factor <= 1.0f, "Factor must be below 1.0f");
		const aiVector3D& start = nodeAnim->mPositionKeys[positionIndex].mValue;
		const aiVector3D& end = nodeAnim->mPositionKeys[nextPositionIndex].mValue;
		const aiVector3D delta = end - start;
		const auto aiVec = start + factor * delta;
		return { aiVec.x, aiVec.y, aiVec.z };
	}


	glm::quat Mesh::interpolateRotation(const float animationTime, const aiNodeAnim* nodeAnim)
	{
		BEY_PROFILE_FUNCTION();
		if (nodeAnim->mNumRotationKeys == 1)
		{
			// No interpolation necessary for single value
			const auto v = nodeAnim->mRotationKeys[0].mValue;
			return glm::quat(v.w, v.x, v.y, v.z);
		}

		const uint32_t rotationIndex = findRotation(animationTime, nodeAnim);
		const uint32_t nextRotationIndex = (rotationIndex + 1);
		BEY_CORE_ASSERT(nextRotationIndex < nodeAnim->mNumRotationKeys);
		const auto deltaTime = float(nodeAnim->mRotationKeys[nextRotationIndex].mTime - nodeAnim->mRotationKeys[rotationIndex].mTime);
		float factor = (animationTime - float(nodeAnim->mRotationKeys[rotationIndex].mTime)) / deltaTime;
		if (factor < 0.0f)
			factor = 0.0f;
		BEY_CORE_ASSERT(factor <= 1.0f, "Factor must be below 1.0f");
		const aiQuaternion& startRotationQ = nodeAnim->mRotationKeys[rotationIndex].mValue;
		const aiQuaternion& endRotationQ = nodeAnim->mRotationKeys[nextRotationIndex].mValue;
		auto q = aiQuaternion();
		aiQuaternion::Interpolate(q, startRotationQ, endRotationQ, factor);
		q = q.Normalize();
		return glm::quat(q.w, q.x, q.y, q.z);
	}


	glm::vec3 Mesh::interpolateScale(const float animationTime, const aiNodeAnim* nodeAnim)
	{
		BEY_PROFILE_FUNCTION();
		if (nodeAnim->mNumScalingKeys == 1)
		{
			// No interpolation necessary for single value
			const auto v = nodeAnim->mScalingKeys[0].mValue;
			return { v.x, v.y, v.z };
		}

		const uint32_t index = findScaling(animationTime, nodeAnim);
		const uint32_t nextIndex = (index + 1);
		BEY_CORE_ASSERT(nextIndex < nodeAnim->mNumScalingKeys);
		const auto deltaTime = float(nodeAnim->mScalingKeys[nextIndex].mTime - nodeAnim->mScalingKeys[index].mTime);
		float factor = (animationTime - float(nodeAnim->mScalingKeys[index].mTime)) / deltaTime;
		if (factor < 0.0f)
			factor = 0.0f;
		BEY_CORE_ASSERT(factor <= 1.0f, "Factor must be below 1.0f");
		const auto& start = nodeAnim->mScalingKeys[index].mValue;
		const auto& end = nodeAnim->mScalingKeys[nextIndex].mValue;
		const auto delta = end - start;
		const auto aiVec = start + factor * delta;
		return { aiVec.x, aiVec.y, aiVec.z };
	}

	void Mesh::readNodeHierarchy(const float animationTime, const aiNode* pNode, const glm::mat4& parentTransform)
	{
		BEY_PROFILE_FUNCTION();
		const std::string name(pNode->mName.data);
		const aiAnimation* animation = mScene->mAnimations[0];
		glm::mat4 nodeTransform(mat4FromAssimpMat4(pNode->mTransformation));
		const aiNodeAnim* nodeAnim = findNodeAnim(animation, name);

		if (nodeAnim)
		{
			const glm::vec3 translation = interpolateTranslation(animationTime, nodeAnim);
			const glm::mat4 translationMatrix = glm::translate(glm::mat4(1.0f), glm::vec3(translation.x, translation.y, translation.z));

			const glm::quat rotation = interpolateRotation(animationTime, nodeAnim);
			const glm::mat4 rotationMatrix = glm::toMat4(rotation);

			const glm::vec3 scale = interpolateScale(animationTime, nodeAnim);
			const glm::mat4 scaleMatrix = glm::scale(glm::mat4(1.0f), glm::vec3(scale.x, scale.y, scale.z));

			nodeTransform = translationMatrix * rotationMatrix * scaleMatrix;
		}

		const glm::mat4 transform = parentTransform * nodeTransform;

		if (mBoneMapping.find(name) != mBoneMapping.end())
		{
			const uint32_t boneIndex = mBoneMapping[name];
			mBoneInfo[boneIndex].FinalTransformation = mInverseTransform * transform * mBoneInfo[boneIndex].BoneOffset;
		}

		for (uint32_t i = 0; i < pNode->mNumChildren; i++)
			readNodeHierarchy(animationTime, pNode->mChildren[i], transform);
	}

	const aiNodeAnim* Mesh::findNodeAnim(const aiAnimation* animation, const std::string& nodeName)
	{
		BEY_PROFILE_FUNCTION();
		for (uint32_t i = 0; i < animation->mNumChannels; i++)
		{
			const aiNodeAnim* nodeAnim = animation->mChannels[i];
			if (std::string(nodeAnim->mNodeName.data) == nodeName)
				return nodeAnim;
		}
		return nullptr;
	}

	void Mesh::boneTransform(const float time)
	{
		BEY_PROFILE_FUNCTION();
		readNodeHierarchy(time, mScene->mRootNode, glm::mat4(1.0f));
		mBoneTransforms.resize(mBoneCount);
		for (size_t i = 0; i < mBoneCount; i++)
			mBoneTransforms[i] = mBoneInfo[i].FinalTransformation;
	}

	void Mesh::dumpVertexBuffer()
	{
		// TODO: Convert to ImGui
		BEY_MESH_LOG("------------------------------------------------------");
		BEY_MESH_LOG("Vertex Buffer Dump");
		BEY_MESH_LOG("Mesh: {0}", mFilePath);
		if (mIsAnimated)
		{
			for (auto& vertex : mAnimatedVertices)
			{
				//BEY_MESH_LOG("Vertex: {0}", i);
				BEY_MESH_LOG("Position: {0}, {1}, {2}", vertex.Position.x, vertex.Position.y, vertex.Position.z);
				BEY_MESH_LOG("Normal: {0}, {1}, {2}", vertex.Normal.x, vertex.Normal.y, vertex.Normal.z);
				BEY_MESH_LOG("Binormal: {0}, {1}, {2}", vertex.Binormal.x, vertex.Binormal.y, vertex.Binormal.z);
				BEY_MESH_LOG("Tangent: {0}, {1}, {2}", vertex.Tangent.x, vertex.Tangent.y, vertex.Tangent.z);
				BEY_MESH_LOG("TexCoord: {0}, {1}", vertex.TexCoord.x, vertex.TexCoord.y);
				BEY_MESH_LOG("--");
			}
		}
		else
		{
			for (auto& vertex : mStaticVertices)
			{
				//BEY_MESH_LOG("Vertex: {0}", i);
				BEY_MESH_LOG("Position: {0}, {1}, {2}", vertex.Position.x, vertex.Position.y, vertex.Position.z);
				BEY_MESH_LOG("Normal: {0}, {1}, {2}", vertex.Normal.x, vertex.Normal.y, vertex.Normal.z);
				BEY_MESH_LOG("Binormal: {0}, {1}, {2}", vertex.Binormal.x, vertex.Binormal.y, vertex.Binormal.z);
				BEY_MESH_LOG("Tangent: {0}, {1}, {2}", vertex.Tangent.x, vertex.Tangent.y, vertex.Tangent.z);
				BEY_MESH_LOG("TexCoord: {0}, {1}", vertex.TexCoord.x, vertex.TexCoord.y);
				BEY_MESH_LOG("--");
			}
		}
		BEY_MESH_LOG("------------------------------------------------------");
	}

}