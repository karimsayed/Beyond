#include "pchs.h"
#include "SceneRenderer.h"

#include "Renderer.h"
#include "Renderer2D.h"
#include "KernelGenerator.hpp"
#include "Beyond/ImGui/CustomImGui.h"
#include "Beyond/Scene/Components.h"


namespace bey {

	enum RenderModeEn
	{
		None = 0,
		Position = BIT(1),
		Albedo = BIT(2),
		Normal = BIT(3),
		SSR = BIT(4),
		RoughnessMetalness = BIT(5),
		SSAO = BIT(6),
		Bloom = BIT(7),
		Lighting = BIT(8),
	};

	struct SceneRendererData
	{

		const Scene* ActiveScene = nullptr;
		struct SceneInfo
		{
			SceneRendererCamera SceneCamera;
			// Resources
			Ref<MaterialInstance> SkyBoxMaterial;
			LightEnvironment SceneLightEnvironment;
			Environment SceneEnvironment;
		} SceneData;

		Ref<Texture2D> BRDFLUT;
		Ref<Texture2D> SSAONoise;
		//Ref<Texture2D> PointLightIcon;
		//Ref<Texture2D> DirLightIcon;

		Ref<Shader> HZBShader;

		Ref<Shader> CompositeShader;
		Ref<Shader> DirShadowShader;
		Ref<Shader> LightCullingShader;
		//Ref<Shader> PointShadowShader;
		//Ref<Shader> BloomBlurShader;
		Ref<Shader> SSAOShader;
		Ref<Shader> SSAOBlurShader;
		Ref<Shader> ReInterleavingShader;
		Ref<Shader> DeInterleavingShader;
		Ref<Shader> ColorAccumulationShader;
		Ref<Shader> SSRShader;
		Ref<Shader> HBAOShader;
		Ref<Shader> HBAOBlurShader;
		Ref<Shader> HBAOBlur2Shader;
		Ref<Shader> ViewNormalsShader;
		//Ref<Shader> IconShader;


		Ref<RenderPass> DeinterleavedPass;
		Ref<RenderPass> HZBRenderPass;
		Ref<RenderPass> DirShadowPass;
		//Ref<RenderPass> PointShadowPass;
		Ref<RenderPass> ReInterleavingPass;
		Ref<RenderPass> GeometryPass;
		Ref<RenderPass> HBAOPass;
		Ref<RenderPass> ViewNormalPass;
		//Ref<RenderPass> LightingPass;
		//Ref<RenderPass> PingPass;
		//Ref<RenderPass> PongPass;
		Ref<RenderPass> SSRPass;
		Ref<RenderPass> ColorAccumulationPass;
		Ref<RenderPass> CompositePass;
		glm::vec2 ViewPortSize{};
		float SSRFadeIn = 0.7f;
		float SSRDepthTolerance = 0.1f;
		float SSRFadeDistance = 0.95f;
		int SSRMaxSteps = 100;
		float SSRSkipFactor = 1.f;
		glm::vec4 DispatchThreadIdToBufferUV{ 0.0017f, 0.0035f, 0.0f, 0.0f };
		glm::vec2 InputViewportMaxBound{ 0.9988f, 0.9991f };
		float BlurSharpness;


		struct DrawCommand
		{
			Ref<Mesh> Mesh;
			glm::mat4 Transform;
		};

		std::vector<DrawCommand> DrawList;
		std::vector<DrawCommand> SelectedMeshDrawList;

		//Materials
		Ref<MaterialInstance> GridMaterial;
		Ref<MaterialInstance> OutlineMaterial;
		//Ref<MaterialInstance> PBRMaterial;
		Ref<RenderPass> HBAOBlurPass;
		Ref<MaterialInstance> HBAOBlurMaterial;

		Ref<MaterialInstance> HBAOMaterial;
		Ref<MaterialInstance> ViewNormalsMaterial;
		Ref<MaterialInstance> DeInterleavingMaterial;
		Ref<MaterialInstance> ReInterleavingMaterial;
		Ref<MaterialInstance> HBAOBlur2Material;
		//Ref<MaterialInstance> BloomBlurMaterial;
		Ref<MaterialInstance> CompositeMaterial;
		Ref<MaterialInstance> LightCullingMaterial;
		Ref<MaterialInstance> SSAOMaterial;
		//Ref<MaterialInstance> SSAOBlurMaterial;
		Ref<MaterialInstance> SSRMaterial;
		Ref<MaterialInstance> ColorAccumulationMaterial;
		//Ref<MaterialInstance> IconMaterial;

		Ref<UniformBuffer> GLLightUniformBuffer;
		Ref<StorageUniformBuffer> GLVisibleLightsBuffer;
		//Ref<UniformBuffer> GLLightMatricesUniformBuffer;

		SceneRendererOptions Options;

		ShadowData Shadows;

		glm::ivec2 WorkGroup;

		struct UniformStruct
		{
			float BloomThreshold = 3.5f;
			float LightSize = 0.5f;
			float MaxShadowDistance = 200.0f;
			float ShadowFade = 25.0f;

			float CascadeTransitionFade = 1.0f;
			float SceneEnvironmentIntensity = 0.65f;
			glm::vec2 PixelSize{};
			glm::vec4 CameraPosition{};

			glm::vec4 CascadeSplits{};
			glm::mat4 LightViewMatrix{};
			glm::mat4 ProjectionMatrix{};
			glm::mat4 ProjectionMatrixInv{};
			glm::mat4 ViewMatrix{};
			glm::mat4 ViewMatrixInv{};
			glm::vec4 ProjectionParams{}; //x = near, y = far
			glm::vec2 ViewportSize{};
			glm::vec2 Padding{};
			glm::mat4 ViewProjectionMatrix{};
		} UniformStruct;

		Buffer UniformBuffer;
		Buffer PointLightMatricesUniformBuffer;

		using RenderMode = int;
		RenderMode RenderModeEn = 0;

		float SkyboxLod = 1.f;
		float SceneEnvironmentAngle{ 0.f };

		int SSGISamples = 20;
		float IndirectAmount = 2.f;
		float NoiseAmount = 2.5;
		bool EnableNoise = true;

		glm::vec4 View_ScreenPositionScaleBias = { 0.4996f, -0.5000f, 0.5000f, 0.4996f };
		int View_StateFrameIndexMod8{ 5 };
		glm::vec3 View_IndirectLightingColorScale{ 3 };

		float StateFrameIndexMod8 = 2.f;

		glm::vec4 HZBUvFactorAndInvFactor{ 0.5894f, 0.5586f, 1.6968f, 1.7902f };
		glm::vec4 ColorBufferScaleBias{ 0.9934, 0.9931, 0.9000, 0.9000 };
		glm::vec2 ReducedColorUVMax{ 0.9918f, 0.9913f };
		float PixelPositionToFullResPixel = 1.f;
		glm::vec2 FullResPixelOffset{ 0.5000f, 0.5000f };
		glm::vec4 ScreenPositionScaleBias{ 0.5f, -0.5f, 0.5f, 0.5f };


		
		std::vector<glm::vec4> Float2Offsets{ 16 };

	} static s_Data;





	void SceneRenderer::init()
	{
		BEY_PROFILE_FUNCTION();


#if 0
		FramebufferSpecification hzbfFramebufferSpecification; 
		hzbfFramebufferSpecification.Width = 1024;
		hzbfFramebufferSpecification.Height = 512;
		hzbfFramebufferSpecification.Samples = 1;
		hzbfFramebufferSpecification.ClearColor = { 0.f, 0.f, 0.f, 2.0f };
		hzbfFramebufferSpecification.DepthCount = 0;
		hzbfFramebufferSpecification.TextureFormats = { GL_RED, GL_RED, GL_RED, GL_RED, };
		hzbfFramebufferSpecification.TextureIFormats = { GL_R16F, GL_R16F, GL_R16F, GL_R16F, };
		hzbfFramebufferSpecification.TextureTargets = { GL_TEXTURE_2D, GL_TEXTURE_2D, GL_TEXTURE_2D, GL_TEXTURE_2D, };
		hzbfFramebufferSpecification.TextureDataType = { GL_HALF_FLOAT, GL_HALF_FLOAT, GL_HALF_FLOAT, GL_HALF_FLOAT, };
		hzbfFramebufferSpecification.TextureAttachment = { GL_COLOR_ATTACHMENT0, GL_COLOR_ATTACHMENT1, GL_COLOR_ATTACHMENT2, GL_COLOR_ATTACHMENT3, };
		hzbfFramebufferSpecification.ColorCount = 4;
		hzbfFramebufferSpecification.DrawBuffer = false;
		hzbfFramebufferSpecification.ArrayTextures = false;
		hzbfFramebufferSpecification.IsResizable = false;

		RenderPassSpecification hzbfRenderPassSpec;
		hzbfRenderPassSpec.TargetFramebuffer = FrameBuffer::create(hzbfFramebufferSpecification);
		s_Data.HZBRenderPass = RenderPass::create(hzbfRenderPassSpec);
#endif
		FramebufferSpecification dirShadowFramebufferSpec;
		dirShadowFramebufferSpec.Width = 4096;
		dirShadowFramebufferSpec.Height = 4096;
		dirShadowFramebufferSpec.Samples = 1;
		dirShadowFramebufferSpec.ClearColor = { 0.f, 0.f, 0.f, 2.0f };
		dirShadowFramebufferSpec.DepthCount = 1;
		dirShadowFramebufferSpec.TextureFormats = { GL_DEPTH_COMPONENT, };
		dirShadowFramebufferSpec.TextureIFormats = { GL_DEPTH_COMPONENT16, };
		dirShadowFramebufferSpec.TextureTargets = { GL_TEXTURE_2D , };
		dirShadowFramebufferSpec.TextureDataType = { GL_HALF_FLOAT, };
		dirShadowFramebufferSpec.TextureAttachment = { GL_DEPTH_ATTACHMENT, };
		dirShadowFramebufferSpec.DrawBuffer = false;
		dirShadowFramebufferSpec.ArrayTextures = false;
		dirShadowFramebufferSpec.IsResizable = false;


		RenderPassSpecification dirShadowRenderPassSpec;
		dirShadowRenderPassSpec.TargetFramebuffer = FrameBuffer::create(dirShadowFramebufferSpec);
		s_Data.DirShadowPass = RenderPass::create(dirShadowRenderPassSpec);
		//
		//
		//FramebufferSpecification pointShadowFramebufferSpec;
		//pointShadowFramebufferSpec.Width = 1024;
		//pointShadowFramebufferSpec.Height = 1024;
		//pointShadowFramebufferSpec.Samples = 1;
		//pointShadowFramebufferSpec.ClearColor = { 0.f, 0.f, 0.f, 3.0f };
		//pointShadowFramebufferSpec.DepthCount = 1;
		//pointShadowFramebufferSpec.ColorCount = 1;
		//pointShadowFramebufferSpec.CubeMapCount = 10;
		//pointShadowFramebufferSpec.TextureFormats = { GL_DEPTH_COMPONENT };
		//pointShadowFramebufferSpec.TextureIFormats = { GL_DEPTH_COMPONENT16 };
		//pointShadowFramebufferSpec.TextureTargets = { GL_TEXTURE_CUBE_MAP, GL_TEXTURE_CUBE_MAP, GL_TEXTURE_CUBE_MAP, GL_TEXTURE_CUBE_MAP, GL_TEXTURE_CUBE_MAP, GL_TEXTURE_CUBE_MAP, GL_TEXTURE_CUBE_MAP, GL_TEXTURE_CUBE_MAP,
		//GL_TEXTURE_CUBE_MAP, GL_TEXTURE_CUBE_MAP, GL_TEXTURE_CUBE_MAP, GL_TEXTURE_CUBE_MAP, GL_TEXTURE_CUBE_MAP };
		//pointShadowFramebufferSpec.TextureDataType = { GL_HALF_FLOAT };
		//pointShadowFramebufferSpec.TextureAttachment = { GL_DEPTH_ATTACHMENT, GL_DEPTH_ATTACHMENT, GL_DEPTH_ATTACHMENT, GL_DEPTH_ATTACHMENT, GL_DEPTH_ATTACHMENT, GL_DEPTH_ATTACHMENT
		//, GL_DEPTH_ATTACHMENT , GL_DEPTH_ATTACHMENT , GL_DEPTH_ATTACHMENT , GL_DEPTH_ATTACHMENT , GL_DEPTH_ATTACHMENT , GL_DEPTH_ATTACHMENT , GL_DEPTH_ATTACHMENT , GL_DEPTH_ATTACHMENT };
		//pointShadowFramebufferSpec.DrawBuffer = false;
		//pointShadowFramebufferSpec.ArrayTextures = true;
		//pointShadowFramebufferSpec.IsResizable = false;
		//
		//
		//RenderPassSpecification pointShadowRenderPassSpec;
		//pointShadowRenderPassSpec.TargetFramebuffer = FrameBuffer::create(pointShadowFramebufferSpec);
		//s_Data.PointShadowPass = RenderPass::create(pointShadowRenderPassSpec);


		FramebufferSpecification geoFramebufferSpec;
		geoFramebufferSpec.Width = 1280;
		geoFramebufferSpec.Height = 720;
		geoFramebufferSpec.Samples = 1;
		geoFramebufferSpec.ClearColor = { 0.f, 0.f, 0.f, 4.0f };
		geoFramebufferSpec.ColorCount = 6;
		geoFramebufferSpec.DepthCount = 1;
		geoFramebufferSpec.Stenciled = true;
		geoFramebufferSpec.TextureTargets = { GL_TEXTURE_2D,GL_TEXTURE_2D,GL_TEXTURE_2D,GL_TEXTURE_2D,GL_TEXTURE_2D,GL_TEXTURE_2D,GL_TEXTURE_2D, GL_TEXTURE_2D, };
		geoFramebufferSpec.TextureFormats = { GL_RGBA,GL_RGBA,GL_RGBA,GL_RGBA,GL_RGBA, GL_RED, GL_DEPTH_STENCIL };
		geoFramebufferSpec.TextureIFormats = { GL_RGBA16F,GL_RGBA16F,GL_RGBA16F,GL_RGBA16F,GL_RGBA16F, GL_R32F, GL_DEPTH24_STENCIL8 };
		geoFramebufferSpec.TextureDataType = { GL_HALF_FLOAT,GL_HALF_FLOAT,GL_HALF_FLOAT,GL_HALF_FLOAT, GL_HALF_FLOAT, GL_UNSIGNED_INT, GL_UNSIGNED_INT_24_8 };
		geoFramebufferSpec.TextureAttachment = { GL_COLOR_ATTACHMENT0,GL_COLOR_ATTACHMENT1,GL_COLOR_ATTACHMENT2,GL_COLOR_ATTACHMENT3,GL_COLOR_ATTACHMENT4, GL_COLOR_ATTACHMENT5, GL_DEPTH_STENCIL_ATTACHMENT, };
		geoFramebufferSpec.DrawBuffer = true;
		geoFramebufferSpec.ArrayTextures = false;
		geoFramebufferSpec.GenerateLODs = true;


		RenderPassSpecification geoRenderPassSpec;
		geoRenderPassSpec.TargetFramebuffer = FrameBuffer::create(geoFramebufferSpec);
		s_Data.GeometryPass = RenderPass::create(geoRenderPassSpec);

		FramebufferSpecification deinterleavedFramebufferSpec;
		deinterleavedFramebufferSpec.Width = 1280;
		deinterleavedFramebufferSpec.Height = 720;
		deinterleavedFramebufferSpec.Samples = 1;
		deinterleavedFramebufferSpec.ClearColor = { 0.0f, 0.0f, 0.0f, 5.0f };
		deinterleavedFramebufferSpec.ColorCount = 16;
		deinterleavedFramebufferSpec.DrawBuffer = true;
		deinterleavedFramebufferSpec.TextureTargets = { GL_TEXTURE_2D, GL_TEXTURE_2D, GL_TEXTURE_2D, GL_TEXTURE_2D, GL_TEXTURE_2D, GL_TEXTURE_2D, GL_TEXTURE_2D, GL_TEXTURE_2D,
		GL_TEXTURE_2D, GL_TEXTURE_2D, GL_TEXTURE_2D, GL_TEXTURE_2D, GL_TEXTURE_2D, GL_TEXTURE_2D, GL_TEXTURE_2D, GL_TEXTURE_2D, };
		deinterleavedFramebufferSpec.TextureFormats = { GL_RED, GL_RED, GL_RED, GL_RED, GL_RED, GL_RED, GL_RED, GL_RED,
		GL_RED, GL_RED, GL_RED, GL_RED, GL_RED, GL_RED, GL_RED, GL_RED, };
		deinterleavedFramebufferSpec.TextureIFormats = { GL_R32F, GL_R32F, GL_R32F, GL_R32F, GL_R32F, GL_R32F, GL_R32F, GL_R32F,
		GL_R32F, GL_R32F, GL_R32F, GL_R32F, GL_R32F, GL_R32F, GL_R32F, GL_R32F, };
		deinterleavedFramebufferSpec.TextureDataType = { GL_FLOAT, GL_FLOAT, GL_FLOAT, GL_FLOAT, GL_FLOAT, GL_FLOAT, GL_FLOAT, GL_FLOAT,
		GL_FLOAT, GL_FLOAT, GL_FLOAT, GL_FLOAT, GL_FLOAT, GL_FLOAT, GL_FLOAT, GL_FLOAT, };
		deinterleavedFramebufferSpec.TextureAttachment = { GL_COLOR_ATTACHMENT0, GL_COLOR_ATTACHMENT1, GL_COLOR_ATTACHMENT2, GL_COLOR_ATTACHMENT3, GL_COLOR_ATTACHMENT4, GL_COLOR_ATTACHMENT5, GL_COLOR_ATTACHMENT6,GL_COLOR_ATTACHMENT7,
		GL_COLOR_ATTACHMENT8, GL_COLOR_ATTACHMENT9, GL_COLOR_ATTACHMENT10, GL_COLOR_ATTACHMENT11, GL_COLOR_ATTACHMENT12, GL_COLOR_ATTACHMENT13, GL_COLOR_ATTACHMENT14,GL_COLOR_ATTACHMENT15, };
		deinterleavedFramebufferSpec.ArrayTextures = true;

		RenderPassSpecification deinteleavedRenderPassSpec;
		deinteleavedRenderPassSpec.TargetFramebuffer = FrameBuffer::create(deinterleavedFramebufferSpec);
		s_Data.DeinterleavedPass = RenderPass::create(deinteleavedRenderPassSpec);
		//
		// FramebufferSpecification hbaoFramebufferSpec;
		// hbaoFramebufferSpec.Width = 1280;
		// hbaoFramebufferSpec.Height = 720;
		// hbaoFramebufferSpec.Samples = 1;
		// hbaoFramebufferSpec.ClearColor = { 0.0f, 0.0f, 0.0f, 5.0f };
		// hbaoFramebufferSpec.ColorCount = 8;
		// hbaoFramebufferSpec.DrawBuffer = true;
		// hbaoFramebufferSpec.TextureTargets = { GL_TEXTURE_2D, GL_TEXTURE_2D, GL_TEXTURE_2D, GL_TEXTURE_2D, GL_TEXTURE_2D, GL_TEXTURE_2D, GL_TEXTURE_2D, GL_TEXTURE_2D, };
		// hbaoFramebufferSpec.TextureFormats = { GL_RED, GL_RED, GL_RED, GL_RED, GL_RED, GL_RED, GL_RED, GL_RED, };
		// hbaoFramebufferSpec.TextureIFormats = { GL_R32F, GL_R32F, GL_R32F, GL_R32F, GL_R32F, GL_R32F, GL_R32F, GL_R32F, };
		// hbaoFramebufferSpec.TextureDataType = { GL_UNSIGNED_BYTE };
		// hbaoFramebufferSpec.TextureAttachment = { GL_COLOR_ATTACHMENT0 };
		// hbaoFramebufferSpec.ArrayTextures = true;
		// hbaoFramebufferSpec.HBAO = true;
		//
		// RenderPassSpecification hbaoRenderPassSpec;
		// hbaoRenderPassSpec.TargetFramebuffer = FrameBuffer::create(hbaoFramebufferSpec);
		// s_Data.SSAOPass = RenderPass::create(hbaoRenderPassSpec);

		// FramebufferSpecification ssaoFramebufferSpec;
		// ssaoFramebufferSpec.Width = 1280;
		// ssaoFramebufferSpec.Height = 720;
		// ssaoFramebufferSpec.Samples = 1;
		// ssaoFramebufferSpec.ClearColor = { 0.0f, 0.0f, 0.0f, 5.0f };
		// ssaoFramebufferSpec.ColorCount = 1;
		// ssaoFramebufferSpec.DrawBuffer = true;
		// ssaoFramebufferSpec.TextureTargets = { GL_TEXTURE_2D };
		// ssaoFramebufferSpec.TextureFormats = { GL_RGBA };
		// ssaoFramebufferSpec.TextureIFormats = { GL_RGBA8 };
		// ssaoFramebufferSpec.TextureDataType = { GL_UNSIGNED_BYTE };
		// ssaoFramebufferSpec.TextureAttachment = { GL_COLOR_ATTACHMENT0 };
		// ssaoFramebufferSpec.ArrayTextures = false;
		//
		// RenderPassSpecification ssaoRenderPassSpec;
		// ssaoRenderPassSpec.TargetFramebuffer = FrameBuffer::create(ssaoFramebufferSpec);
		// s_Data.SSAOPass = RenderPass::create(ssaoRenderPassSpec);
		FramebufferSpecification ViewNormalsFramebufferSpec;
		ViewNormalsFramebufferSpec.Width = 1280;
		ViewNormalsFramebufferSpec.Height = 720;
		ViewNormalsFramebufferSpec.Samples = 1;
		ViewNormalsFramebufferSpec.ClearColor = { 0.0f, 0.0f, 0.0f, 5.0f };
		ViewNormalsFramebufferSpec.ColorCount = 1;
		ViewNormalsFramebufferSpec.DrawBuffer = true;
		ViewNormalsFramebufferSpec.TextureTargets = { GL_TEXTURE_2D };
		ViewNormalsFramebufferSpec.TextureFormats = { GL_RGBA };
		ViewNormalsFramebufferSpec.TextureIFormats = { GL_RGBA16F, };
		ViewNormalsFramebufferSpec.TextureDataType = { GL_HALF_FLOAT, };
		ViewNormalsFramebufferSpec.TextureAttachment = { GL_COLOR_ATTACHMENT0 };

		RenderPassSpecification viewNormalRenderPassSpec;
		viewNormalRenderPassSpec.TargetFramebuffer = FrameBuffer::create(ViewNormalsFramebufferSpec);
		s_Data.ViewNormalPass = RenderPass::create(viewNormalRenderPassSpec);

		FramebufferSpecification hbaoFramebufferSpec;
		hbaoFramebufferSpec.Width = 1280;
		hbaoFramebufferSpec.Height = 720;
		hbaoFramebufferSpec.Samples = 1;
		hbaoFramebufferSpec.ClearColor = { 0.0f, 0.0f, 0.0f, 5.0f };
		hbaoFramebufferSpec.ColorCount = 16;
		hbaoFramebufferSpec.DrawBuffer = true;
		hbaoFramebufferSpec.TextureTargets = { GL_TEXTURE_2D_ARRAY };
		hbaoFramebufferSpec.TextureFormats = { GL_RGBA };
		hbaoFramebufferSpec.TextureIFormats = { GL_RGBA16F, };
		hbaoFramebufferSpec.TextureDataType = { GL_HALF_FLOAT, };
		hbaoFramebufferSpec.TextureAttachment = { GL_COLOR_ATTACHMENT0 };
		hbaoFramebufferSpec.ArrayTextures = true;
		hbaoFramebufferSpec.HBAO = true;

		RenderPassSpecification hbaoRenderPassSpec;
		hbaoRenderPassSpec.TargetFramebuffer = FrameBuffer::create(hbaoFramebufferSpec);
		s_Data.HBAOPass = RenderPass::create(hbaoRenderPassSpec);


		FramebufferSpecification reInterleavedFramebufferSpec;
		reInterleavedFramebufferSpec.Width = 1280;
		reInterleavedFramebufferSpec.Height = 720;
		reInterleavedFramebufferSpec.Samples = 1;
		reInterleavedFramebufferSpec.ClearColor = { 0.0f, 0.0f, 0.0f, 5.0f };
		reInterleavedFramebufferSpec.ColorCount = 2;
		reInterleavedFramebufferSpec.DrawBuffer = true;
		reInterleavedFramebufferSpec.TextureTargets = { GL_TEXTURE_2D, GL_TEXTURE_2D, };
		reInterleavedFramebufferSpec.TextureFormats = { GL_RGBA, GL_RGBA };
		reInterleavedFramebufferSpec.TextureIFormats = { GL_RGBA16F, GL_RGBA16F, };
		reInterleavedFramebufferSpec.TextureDataType = { GL_HALF_FLOAT, GL_HALF_FLOAT, };
		reInterleavedFramebufferSpec.TextureAttachment = { GL_COLOR_ATTACHMENT0, GL_COLOR_ATTACHMENT1 };

		RenderPassSpecification reInterleavedRenderPassSpec;
		reInterleavedRenderPassSpec.TargetFramebuffer = FrameBuffer::create(reInterleavedFramebufferSpec);
		s_Data.ReInterleavingPass = RenderPass::create(reInterleavedRenderPassSpec);

		FramebufferSpecification HBAOBlurFramebufferSpec;
		HBAOBlurFramebufferSpec.Width = 1280;
		HBAOBlurFramebufferSpec.Height = 720;
		HBAOBlurFramebufferSpec.Samples = 1;
		HBAOBlurFramebufferSpec.ClearColor = { 0.0f, 0.0f, 0.0f, 5.0f };
		HBAOBlurFramebufferSpec.ColorCount = 1;
		HBAOBlurFramebufferSpec.DrawBuffer = true;
		HBAOBlurFramebufferSpec.TextureTargets = { GL_TEXTURE_2D, };
		HBAOBlurFramebufferSpec.TextureFormats = { GL_RGBA, };
		HBAOBlurFramebufferSpec.TextureIFormats = { GL_RGBA16F, };
		HBAOBlurFramebufferSpec.TextureDataType = { GL_FLOAT, };
		HBAOBlurFramebufferSpec.TextureAttachment = { GL_COLOR_ATTACHMENT0, };

		RenderPassSpecification hbaoBlurRenderPassSpec;
		hbaoBlurRenderPassSpec.TargetFramebuffer = FrameBuffer::create(HBAOBlurFramebufferSpec);
		s_Data.HBAOBlurPass = RenderPass::create(hbaoBlurRenderPassSpec);



		//FramebufferSpecification lightingFramebufferSpec;
		//lightingFramebufferSpec.Width = 1280;
		//lightingFramebufferSpec.Height = 720;
		//lightingFramebufferSpec.Samples = 1;
		//lightingFramebufferSpec.ClearColor = { 0.0f, 0.0f, 0.0f, 0.0f };
		//lightingFramebufferSpec.ColorCount = 2;
		//lightingFramebufferSpec.DrawBuffer = true;
		//lightingFramebufferSpec.TextureTargets = { GL_TEXTURE_2D,  };
		//lightingFramebufferSpec.TextureFormats = { GL_RGBA, };
		//lightingFramebufferSpec.TextureIFormats = { GL_RGBA16F, };
		//lightingFramebufferSpec.TextureDataType = { GL_HALF_FLOAT, };
		//lightingFramebufferSpec.TextureAttachment = { GL_COLOR_ATTACHMENT0, };
		//lightingFramebufferSpec.ArrayTextures = false;

		//RenderPassSpecification lightingRenderPassSpec;
		//lightingRenderPassSpec.TargetFramebuffer = FrameBuffer::create(lightingFramebufferSpec);
		//s_Data.LightingPass = RenderPass::create(lightingRenderPassSpec);

		//FramebufferSpecification indirectLightingFramebufferSpec;
		//indirectLightingFramebufferSpec.Width = 1280;
		//indirectLightingFramebufferSpec.Height = 720;
		//indirectLightingFramebufferSpec.Samples = 1;
		//indirectLightingFramebufferSpec.ClearColor = { 0.0f, 0.0f, 0.0f, 6.f };
		//indirectLightingFramebufferSpec.ColorCount = 2;
		//indirectLightingFramebufferSpec.DrawBuffer = true;
		//indirectLightingFramebufferSpec.TextureTargets = { GL_TEXTURE_2D, GL_TEXTURE_2D };
		//indirectLightingFramebufferSpec.TextureFormats = { GL_RGBA, GL_RGBA };
		//indirectLightingFramebufferSpec.TextureIFormats = { GL_RGBA16F, GL_RGBA16F };
		//indirectLightingFramebufferSpec.TextureDataType = { GL_HALF_FLOAT, GL_HALF_FLOAT };
		//indirectLightingFramebufferSpec.TextureAttachment = { GL_COLOR_ATTACHMENT0, GL_COLOR_ATTACHMENT1 };
		//indirectLightingFramebufferSpec.ArrayTextures = false;
		//
		//RenderPassSpecification colorAccumulationRenderPassSpec;
		//colorAccumulationRenderPassSpec.TargetFramebuffer = FrameBuffer::create(indirectLightingFramebufferSpec);
		//s_Data.ColorAccumulationPass = RenderPass::create(colorAccumulationRenderPassSpec);

		FramebufferSpecification ssrFramebufferSpec;
		ssrFramebufferSpec.Width = 1280;
		ssrFramebufferSpec.Height = 720;
		ssrFramebufferSpec.Samples = 1;
		ssrFramebufferSpec.ClearColor = { 0.0f, 0.0f, 0.0f, 6.f };
		ssrFramebufferSpec.ColorCount = 1;
		ssrFramebufferSpec.DrawBuffer = true;
		ssrFramebufferSpec.TextureTargets = { GL_TEXTURE_2D };
		ssrFramebufferSpec.TextureFormats = { GL_RGBA };
		ssrFramebufferSpec.TextureIFormats = { GL_RGBA16F };
		ssrFramebufferSpec.TextureDataType = { GL_HALF_FLOAT };
		ssrFramebufferSpec.TextureAttachment = { GL_COLOR_ATTACHMENT0 };
		ssrFramebufferSpec.ArrayTextures = false;

		RenderPassSpecification ssrRenderPassSpec;
		ssrRenderPassSpec.TargetFramebuffer = FrameBuffer::create(ssrFramebufferSpec);
		s_Data.SSRPass = RenderPass::create(ssrRenderPassSpec);

		//
		//FramebufferSpecification blurFramebufferSpec;
		//blurFramebufferSpec.Width = 1280;
		//blurFramebufferSpec.Height = 720;
		//blurFramebufferSpec.Samples = 1;
		//blurFramebufferSpec.ClearColor = { 0.0f, 0.0f, 0.0f, 7.f };
		//blurFramebufferSpec.ColorCount = 1;
		//blurFramebufferSpec.DrawBuffer = true;
		//blurFramebufferSpec.TextureTargets = { GL_TEXTURE_2D };
		//blurFramebufferSpec.TextureFormats = { GL_RGBA };
		//blurFramebufferSpec.TextureIFormats = { GL_RGBA16F };
		//blurFramebufferSpec.TextureDataType = { GL_HALF_FLOAT };
		//blurFramebufferSpec.TextureAttachment = { GL_COLOR_ATTACHMENT0 };
		//blurFramebufferSpec.ArrayTextures = false;
		//
		//RenderPassSpecification blurPingRenderPassSpec;
		//blurPingRenderPassSpec.TargetFramebuffer = FrameBuffer::create(blurFramebufferSpec);
		//s_Data.PingPass = RenderPass::create(blurPingRenderPassSpec);
		//
		//RenderPassSpecification blurPongRenderPassSpec;
		//blurPongRenderPassSpec.TargetFramebuffer = FrameBuffer::create(blurFramebufferSpec);
		//s_Data.PongPass = RenderPass::create(blurPongRenderPassSpec);

		FramebufferSpecification compFramebufferSpec;
		compFramebufferSpec.Width = 1280;
		compFramebufferSpec.Height = 720;
		compFramebufferSpec.Samples = 8;
		compFramebufferSpec.ClearColor = { 0.0f, 0.0f, 0.0f, 8.f };
		compFramebufferSpec.ColorCount = 1;
		compFramebufferSpec.DepthCount = 1;
		compFramebufferSpec.Stenciled = true;
		compFramebufferSpec.DrawBuffer = true;
		compFramebufferSpec.TextureTargets = { GL_TEXTURE_2D, GL_TEXTURE_2D };
		compFramebufferSpec.TextureFormats = { GL_RGBA, GL_DEPTH_STENCIL };
		compFramebufferSpec.TextureIFormats = { GL_RGBA8, GL_DEPTH24_STENCIL8 };
		compFramebufferSpec.TextureDataType = { GL_UNSIGNED_BYTE, GL_UNSIGNED_INT_24_8 };
		compFramebufferSpec.TextureAttachment = { GL_COLOR_ATTACHMENT0, GL_DEPTH_STENCIL_ATTACHMENT };
		compFramebufferSpec.ArrayTextures = false;

		RenderPassSpecification compRenderPassSpec;
		compRenderPassSpec.TargetFramebuffer = FrameBuffer::create(compFramebufferSpec);
		s_Data.CompositePass = RenderPass::create(compRenderPassSpec);


		s_Data.CompositeShader = Shader::create("assets/shaders/SceneComposite");
		s_Data.CompositeMaterial = MaterialInstance::create(Material::create(s_Data.CompositeShader));


		s_Data.DeInterleavingShader = Shader::create("assets/shaders/Deinterleave");
		s_Data.ReInterleavingShader = Shader::create("assets/shaders/ReInterleave");
		s_Data.ViewNormalsShader = Shader::create("assets/shaders/ViewNormals");
		s_Data.HBAOBlurShader = Shader::create("assets/shaders/HBAOBlur");
		s_Data.HBAOBlur2Shader = Shader::create("assets/shaders/HBAOBlur2");
		s_Data.SSAOShader = Shader::create("assets/shaders/SSAO");
		s_Data.HBAOShader = Shader::create("assets/shaders/HBAO");
		s_Data.HZBShader = Shader::create("assets/shaders/HZB");

		s_Data.HBAOMaterial = MaterialInstance::create(Material::create(s_Data.HBAOShader));
		s_Data.ViewNormalsMaterial = MaterialInstance::create(Material::create(s_Data.ViewNormalsShader));
		s_Data.SSAOMaterial = MaterialInstance::create(Material::create(s_Data.SSAOShader));
		s_Data.ReInterleavingMaterial = MaterialInstance::create(Material::create(s_Data.ReInterleavingShader));
		s_Data.DeInterleavingMaterial = MaterialInstance::create(Material::create(s_Data.DeInterleavingShader));
		s_Data.HBAOBlurMaterial = MaterialInstance::create(Material::create(s_Data.HBAOBlurShader));
		s_Data.HBAOBlur2Material = MaterialInstance::create(Material::create(s_Data.HBAOBlur2Shader));

		s_Data.BRDFLUT = Texture2D::create("assets/textures/BRDF_LUT.tga", DataType::UNSIGNED_BYTE, true, false, aiTextureType_BASE_COLOR);
		//s_Data.PointLightIcon = Texture2D::create("assets/textures/pointLight.png", DataType::UNSIGNED_BYTE, false, true, aiTextureType_BASE_COLOR);
		//s_Data.DirLightIcon = Texture2D::create("assets/textures/dirLight.png", DataType::UNSIGNED_BYTE, false, true, aiTextureType_BASE_COLOR);


		//Shadow depth shader
		s_Data.DirShadowShader = Shader::create("assets/shaders/DirShadowShader");
		s_Data.LightCullingShader = Shader::create("assets/shaders/LightCullingShader");
		//s_Data.PointShadowShader = Shader::create("assets/shaders/PointShadowShader");
		//s_Data.BloomBlurShader = Shader::create("assets/shaders/BloomBlur");
		//s_Data.SSAOBlurShader = Shader::create("assets/shaders/SSAOBlur");
		//s_Data.IconShader = Shader::create("assets/shaders/Icons");
		s_Data.ColorAccumulationShader = Shader::create("assets/shaders/ColorAccumulation");
		s_Data.SSRShader = Shader::create("assets/shaders/SSR");



		// Grid
		const auto gridShader = Shader::create("assets/shaders/Grid");
		s_Data.GridMaterial = MaterialInstance::create(Material::create(gridShader));
		s_Data.GridMaterial->setFlag(MaterialFlag::DepthTest, false);


		const float gridScale = 16.025f, gridSize = 0.025f;
		s_Data.GridMaterial->set("u_Scale", gridScale, ShaderDomain::Pixel);
		s_Data.GridMaterial->set("u_Res", gridSize, ShaderDomain::Pixel);

		// Outline
		const auto outlineShader = Shader::create("assets/shaders/Outline");
		s_Data.OutlineMaterial = MaterialInstance::create(Material::create(outlineShader));
		s_Data.OutlineMaterial->setFlag(MaterialFlag::DepthTest, true);

		//LightCulling
		s_Data.LightCullingMaterial = MaterialInstance::create(Material::create(s_Data.LightCullingShader));

		// Icons
		//s_Data.IconMaterial = MaterialInstance::create(Material::create(s_Data.IconShader));


		//s_Data.PBRMaterial = MaterialInstance::create(Material::create(Renderer::getShaderLibrary()->get("BeyondPBR_Static")));
		//s_Data.PBRMaterial->setFlag(MaterialFlag::DepthTest, false);
		//s_Data.BloomBlurMaterial = MaterialInstance::create(Material::create(s_Data.BloomBlurShader));
		//s_Data.BloomBlurMaterial->setFlag(MaterialFlag::DepthTest, false);
		//s_Data.SSAOBlurMaterial = MaterialInstance::create(Material::create(s_Data.SSAOBlurShader));
		//s_Data.SSAOBlurMaterial->setFlag(MaterialFlag::DepthTest, false);
		s_Data.SSRMaterial = MaterialInstance::create(Material::create(s_Data.SSRShader));
		s_Data.SSRMaterial->setFlag(MaterialFlag::DepthTest, false);

		s_Data.ColorAccumulationMaterial = MaterialInstance::create(Material::create(s_Data.ColorAccumulationShader));
		s_Data.ColorAccumulationMaterial->setFlag(MaterialFlag::DepthTest, false);


		RandomGenerator::generateKernel();
		RandomGenerator::generateNoise();
		RandomGenerator::generateHBAORandom();
		s_Data.SSAONoise = Texture2D::create(TextureFormat::RGB, TextureInternalFormat::RGBA32F, 4, 4,
			DataType::FLOAT, aiTextureType_NONE, TextureWrap::REPEAT);
		s_Data.SSAONoise->lock();
		s_Data.SSAONoise->getWritableBuffer().write(RandomGenerator::getNoise().data(), 256);
		s_Data.SSAONoise->unlock();

		for (int i = 0; i < 16; i++)
		{
			s_Data.Float2Offsets[i] = glm::vec4(float(i % 4) + 0.5f, float(i / 4) + 0.5f, 0.0f, 1.f);
		}

		s_Data.GLLightUniformBuffer = UniformBuffer::create(200000, 0);
		s_Data.UniformBuffer.allocate(200000);
		//s_Data.GLLightMatricesUniformBuffer = UniformBuffer::create(64560, 1);
		s_Data.PointLightMatricesUniformBuffer.allocate(64560);




		s_Data.LightCullingMaterial->getShader()->resolveGLUniformBuffer("light_data", s_Data.GLLightUniformBuffer);
		s_Data.GLVisibleLightsBuffer = StorageUniformBuffer::create(0u, 2);






	}


	void updateHBAOData()
	{
		auto& opts = s_Data.Options;
		// radius
		float meters2viewspace = 1.0f;
		float R = opts.SSAORadius * meters2viewspace;
		opts.R2 = R * R;
		opts.NegInvR2 = -1.0f / opts.R2;
		opts.RadiusToScreen = R * 0.5f * float(s_Data.ViewPortSize.y) / (tanf(glm::radians(65.f) * 0.5f) * 2.0f); //FOV is hard coded

		const float* P = glm::value_ptr(s_Data.SceneData.SceneCamera.Camera->getProjectionMatrix());
		glm::vec4 projInfoPerspective = {

			//	1.47276f, 0.828427f, -0.73638f, -0.414214f
				 2.0f / (P[4 * 0 + 0]),                  // (x) * (R - L)/N
				 2.0f / (P[4 * 1 + 1]),                  // (y) * (T - B)/N
				-(1.0f - P[4 * 2 + 0]) / P[4 * 0 + 0],  // L/N
				-(1.0f + P[4 * 2 + 1]) / P[4 * 1 + 1],  // B/N
		};
		opts.ProjInfo = projInfoPerspective;
		// ao
		opts.PowExponent = std::max(opts.SSAOIntensity, 0.0f);
		opts.NDotVBias = std::min(std::max(0.0f, opts.SSAOBias), 1.0f);
		opts.AOMultiplier = 1.0f / (1.0f - opts.NDotVBias);

		opts.ProjScale = float(s_Data.ViewPortSize.y) / (tanf(glm::radians(65.f) * 0.5f) * 2.0f);


	}




	void CascadeData::extractPlanes(glm::vec4& left, glm::vec4& right, glm::vec4& top, glm::vec4& bottom, glm::vec4& nearP, glm::vec4& farP)
	{
		BEY_PROFILE_FUNCTION();

		//for (int i = 4; i--; ) left[i] = ViewProjection[i][3] + ViewProjection[i][0];
		//for (int i = 4; i--; ) right[i] = ViewProjection[i][3] - ViewProjection[i][0];
		//for (int i = 4; i--; ) bottom[i] = ViewProjection[i][3] + ViewProjection[i][1];
		//for (int i = 4; i--; ) top[i] = ViewProjection[i][3] - ViewProjection[i][1];
		//for (int i = 4; i--; ) nearP[i] = ViewProjection[i][3] + ViewProjection[i][2];
		//for (int i = 4; i--; ) farP[i] = ViewProjection[i][3] - ViewProjection[i][2];
	}

	void calculateCascades(CascadeData* cascades, const glm::vec3& lightDirection, const SceneRendererCamera& sceneCamera, const ShadowData& shadowData)
	{
		BEY_PROFILE_FUNCTION();

		//const auto viewProjection = sceneCamera.Camera->getProjectionMatrix() * sceneCamera.ViewMatrix;
		//CascadeData::ViewProjection = viewProjection;
		//
		//const int SHADOW_MAP_CASCADE_COUNT = 4;
		//float cascadeSplits[SHADOW_MAP_CASCADE_COUNT];
		//
		//// TODO: less hard-coding!
		//float nearClip = 0.1f;
		//float farClip = 10000.0f;
		//float clipRange = farClip - nearClip;
		//
		//float minZ = nearClip;
		//float maxZ = nearClip + clipRange;
		//
		//float range = maxZ - minZ;
		//float ratio = maxZ / minZ;
		//
		//// Calculate split depths based on view camera frustum
		//// Based on method presented in https://developer.nvidia.com/gpugems/GPUGems3/gpugems3_ch10.html
		//for (uint32_t i = 0; i < SHADOW_MAP_CASCADE_COUNT; i++)
		//{
		//	float p = float(i + 1u) / static_cast<float>(SHADOW_MAP_CASCADE_COUNT);
		//	float log = minZ * std::pow(ratio, p);
		//	float uniform = minZ + range * p;
		//	float d = shadowData.CascadeSplitLambda * (log - uniform) + uniform;
		//	cascadeSplits[i] = (d - nearClip) / clipRange;
		//}
		//
		//cascadeSplits[3] = 0.3f;
		//
		//
		//// Calculate orthographic projection matrix for each cascade
		//float lastSplitDist = 0.0;
		//for (uint32_t i = 0; i < SHADOW_MAP_CASCADE_COUNT; i++)
		//{
		//	float splitDist = cascadeSplits[i];
		//
		//	glm::vec3 frustumCorners[8] =
		//	{
		//		glm::vec3(-1.0f,  1.0f, -1.0f),
		//		glm::vec3(1.0f,  1.0f, -1.0f),
		//		glm::vec3(1.0f, -1.0f, -1.0f),
		//		glm::vec3(-1.0f, -1.0f, -1.0f),
		//		glm::vec3(-1.0f,  1.0f,  1.0f),
		//		glm::vec3(1.0f,  1.0f,  1.0f),
		//		glm::vec3(1.0f, -1.0f,  1.0f),
		//		glm::vec3(-1.0f, -1.0f,  1.0f),
		//	};
		//
		//	// Project frustum corners into world space
		//	glm::mat4 invCam = glm::inverse(viewProjection);
		//	for (auto& frustumCorner : frustumCorners)
		//	{
		//		glm::vec4 invCorner = invCam * glm::vec4(frustumCorner, 1.0f);
		//		frustumCorner = invCorner / invCorner.w;
		//	}
		//
		//	for (uint32_t i = 0; i < 4; i++)
		//	{
		//		glm::vec3 dist = frustumCorners[i + 4] - frustumCorners[i];
		//		frustumCorners[i + 4] = frustumCorners[i] + (dist * splitDist);
		//		frustumCorners[i] = frustumCorners[i] + (dist * lastSplitDist);
		//	}
		//
		//	// Get frustum center
		//	auto frustumCenter = glm::vec3(0.0f);
		//	for (auto& frustumCorner : frustumCorners)
		//		frustumCenter += frustumCorner;
		//	frustumCenter /= 8.0f;
		//
		//	//frustumCenter *= 0.01f;
		//
		//	float radius = 0.0f;
		//	for (auto& frustumCorner : frustumCorners)
		//	{
		//		float distance = glm::length(frustumCorner - frustumCenter);
		//		radius = glm::max(radius, distance);
		//	}
		//	radius = glm::ceil(radius * 16.0f) / 16.0f;
		//
		//	auto maxExtents = glm::vec3(radius);
		//	glm::vec3 minExtents = -maxExtents;
		//
		//	glm::vec3 lightDir = -lightDirection;
		//	glm::mat4 lightViewMatrix = glm::lookAt(frustumCenter - lightDir * -minExtents.z, frustumCenter, glm::vec3(0.0f, 0.0f, 1.0f));
		//	glm::mat4 lightOrthoMatrix = glm::ortho(minExtents.x, maxExtents.x, minExtents.y, maxExtents.y, 0.0f +
		//		shadowData.CascadeNearPlaneOffset, maxExtents.z - minExtents.z + shadowData.CascadeFarPlaneOffset);
		//
		//	// Store split distance and matrix in cascade
		//	cascades[i].SplitDepth = (nearClip + splitDist * clipRange) * -1.0f;
		//	cascades[i].ViewProj = lightOrthoMatrix * lightViewMatrix;
		//	cascades[i].View = lightViewMatrix;
		//	std::memcpy(&cascades[i].FrustumCorners, &frustumCorners, sizeof frustumCorners);


			//lastSplitDist = cascadeSplits[i];
		//}
	}

	void SceneRenderer::setViewportSize(const uint32_t width, const uint32_t height)
	{
		BEY_PROFILE_FUNCTION();

		s_Data.DirShadowPass->getSpecification().TargetFramebuffer->resize(width, height);
		s_Data.GeometryPass->getSpecification().TargetFramebuffer->resize(width, height);
		//s_Data.SSAOPass->getSpecification().TargetFramebuffer->resize(width, height);
		s_Data.SSRPass->getSpecification().TargetFramebuffer->resize(width, height);
		//s_Data.LightingPass->getSpecification().TargetFramebuffer->resize(width, height);
		s_Data.CompositePass->getSpecification().TargetFramebuffer->resize(width, height);
		//s_Data.ColorAccumulationPass->getSpecification().TargetFramebuffer->resize(width, height);
		s_Data.DeinterleavedPass->getSpecification().TargetFramebuffer->resize(width / 4, height / 4);
		s_Data.ReInterleavingPass->getSpecification().TargetFramebuffer->resize(width, height);
		s_Data.HBAOBlurPass->getSpecification().TargetFramebuffer->resize(width, height);
		s_Data.ViewNormalPass->getSpecification().TargetFramebuffer->resize(width, height);
		s_Data.HBAOPass->getSpecification().TargetFramebuffer->resize(width / 4, height / 4);
		//s_Data.PingPass->getSpecification().TargetFramebuffer->resize(width, height);
		//s_Data.PongPass->getSpecification().TargetFramebuffer->resize(width, height);
		if (s_Data.ViewPortSize != glm::vec2{ width, height })
		{
			s_Data.WorkGroup.x = (width + width % 16) / 16;
			s_Data.WorkGroup.y = (height + height % 16) / 16;
			s_Data.GLVisibleLightsBuffer = StorageUniformBuffer::create(s_Data.WorkGroup.x * s_Data.WorkGroup.y * 4 * 1024, 2);
			s_Data.LightCullingMaterial->getShader()->resolveGLUniformBuffer("VisibleLightIndicesBuffer", s_Data.GLVisibleLightsBuffer);

		}

		s_Data.ViewPortSize = { width, height };





	}

	struct CameraDirection
	{
		GLenum CubemapFace;
		glm::vec3 Target;
		glm::vec3 Up;
	};

	constexpr static CameraDirection s_camera_directions[6] =
	{
		{ GL_TEXTURE_CUBE_MAP_POSITIVE_X, glm::vec3(1.0f, 0.0f, 0.0f),  glm::vec3(0.0f, -1.0f, 0.0f) },
		{ GL_TEXTURE_CUBE_MAP_NEGATIVE_X, glm::vec3(-1.0f, 0.0f, 0.0f), glm::vec3(0.0f, -1.0f, 0.0f) },
		{ GL_TEXTURE_CUBE_MAP_POSITIVE_Y, glm::vec3(0.0f, 1.0f, 0.0f),  glm::vec3(0.0f, 0.0f, -1.0f) },
		{ GL_TEXTURE_CUBE_MAP_NEGATIVE_Y, glm::vec3(0.0f, -1.0f, 0.0f), glm::vec3(0.0f, 0.0f, 1.0f) },
		{ GL_TEXTURE_CUBE_MAP_POSITIVE_Z, glm::vec3(0.0f, 0.0f, 1.0f),  glm::vec3(0.0f, -1.0f, 0.0f) },
		{ GL_TEXTURE_CUBE_MAP_NEGATIVE_Z, glm::vec3(0.0f, 0.0f, -1.0f), glm::vec3(0.0f, -1.0f, 0.0f) }
	};


	void SceneRenderer::depthPass()
	{
		BEY_PROFILE_FUNCTION();
		Renderer::submit([]
			{
				glEnable(GL_CULL_FACE);
				glCullFace(GL_BACK);
			});





		Renderer::beginRenderPass(s_Data.DirShadowPass, true);


		s_Data.DirShadowShader->bind();
		BEY_PROFILE_SCOPE("Drawing shadow Cascades");

		s_Data.DirShadowPass->getSpecification().TargetFramebuffer->bindForWritingDepth(0);

		const glm::mat4 viewProjection = s_Data.SceneData.SceneCamera.getViewProj();

		for (auto& dc : s_Data.SelectedMeshDrawList)
			Renderer::submitMeshDepth(dc.Mesh, viewProjection * dc.Transform, s_Data.DirShadowShader);
		for (auto& dc : s_Data.DrawList)
			Renderer::submitMeshDepth(dc.Mesh, viewProjection * dc.Transform, s_Data.DirShadowShader);

		Renderer::endRenderPass();

		s_Data.LightCullingMaterial->set("depthMap", s_Data.DirShadowPass->getSpecification().TargetFramebuffer->getTexture(0));
		const auto pointLights = s_Data.SceneData.SceneLightEnvironment.PointLights.size();
		s_Data.LightCullingMaterial->set("lightCount", int(pointLights), ShaderDomain::Comp);
		const uint32_t width = s_Data.DirShadowPass->getSpecification().TargetFramebuffer->getWidth();
		const uint32_t height = s_Data.DirShadowPass->getSpecification().TargetFramebuffer->getHeight();


		s_Data.LightCullingMaterial->set("screenSize", glm::ivec2(width, height), ShaderDomain::Comp);

		s_Data.GLVisibleLightsBuffer->bind();
		s_Data.GLLightUniformBuffer->bind();
		s_Data.LightCullingMaterial->bind();
		s_Data.LightCullingShader->bind();
		Renderer::submit([]
			{
				glDispatchCompute(s_Data.WorkGroup.x, s_Data.WorkGroup.y, 1);
			});
	}


	bool outline;
	void SceneRenderer::geometryPass()
	{
		BEY_PROFILE_FUNCTION();

		Renderer::beginRenderPass(s_Data.GeometryPass);


		outline = !s_Data.SelectedMeshDrawList.empty();
		if (!s_Data.DrawList.empty() || !s_Data.SelectedMeshDrawList.empty())
		{


			Renderer::submit([]
				{
					glStencilOp(GL_KEEP, GL_KEEP, GL_REPLACE);
					glStencilFunc(GL_ALWAYS, 0, 0xff);
					glStencilMask(0xff);
					if (s_Data.Options.DrawWireFrame)
						glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
					else
						glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
				});

			glm::vec4 projectionParams{ 0.1f * 1000.f, 0.1f - 1000.f, 1000.f, 1.f }; // z_n * z_f,  z_n - z_f,  z_f, perspective = 1 : 0

			for (auto& [Mesh, Transform] : s_Data.DrawList)
			{
				auto baseMaterial = Mesh->getMaterial();
				baseMaterial->set("u_BRDFLUTTexture", s_Data.BRDFLUT);
				const auto directionalLight = s_Data.SceneData.SceneLightEnvironment.DirectionalLights[0];
				baseMaterial->set("u_DirectionalLight", directionalLight, ShaderDomain::Pixel);
				baseMaterial->set("numberOfTilesX", s_Data.WorkGroup.x, ShaderDomain::Pixel);

				//baseMaterial->set("u_ClipInfo", projectionParams, ShaderDomain::Pixel);
				//baseMaterial->set("u_PreDepth", s_Data.DirShadowPass->getSpecification().TargetFramebuffer->getTexture(0));
				baseMaterial->set("u_EnvMapRotation", s_Data.SceneEnvironmentAngle, ShaderDomain::Pixel);
				baseMaterial->set("u_EnvRadianceTex", s_Data.SceneData.SceneEnvironment.RadianceMap);
				baseMaterial->set("u_EnvIrradianceTex", s_Data.SceneData.SceneEnvironment.IrradianceMap);
				baseMaterial->set("u_BRDFLUTTexture", s_Data.BRDFLUT);
				Renderer::submitGeometry(Mesh, Transform);
			}

			if (outline)
			{
				Renderer::submit([]() { glStencilFunc(GL_ALWAYS, 1, 0xff); });

				for (auto& dc : s_Data.SelectedMeshDrawList)
				{
					auto baseMaterial = dc.Mesh->getMaterial();
					baseMaterial->set("u_BRDFLUTTexture", s_Data.BRDFLUT);
					const auto directionalLight = s_Data.SceneData.SceneLightEnvironment.DirectionalLights[0];
					baseMaterial->set("u_DirectionalLight", directionalLight, ShaderDomain::Pixel);
					baseMaterial->set("numberOfTilesX", s_Data.WorkGroup.x, ShaderDomain::Pixel);

					//baseMaterial->set("u_ClipInfo", projectionParams, ShaderDomain::Pixel);
					//baseMaterial->set("u_PreDepth", s_Data.DirShadowPass->getSpecification().TargetFramebuffer->getTexture(0));
					baseMaterial->set("u_EnvMapRotation", s_Data.SceneEnvironmentAngle, ShaderDomain::Pixel);
					baseMaterial->set("u_EnvRadianceTex", s_Data.SceneData.SceneEnvironment.RadianceMap);
					baseMaterial->set("u_EnvIrradianceTex", s_Data.SceneData.SceneEnvironment.IrradianceMap);
					baseMaterial->set("u_BRDFLUTTexture", s_Data.BRDFLUT);
					Renderer::submitGeometry(dc.Mesh, dc.Transform);
				}
			}




			Renderer::submit([]
				{
					glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
				});
			Renderer::generateMipMaps();
			Renderer::endRenderPass();
		}

		{
			Renderer::beginRenderPass(s_Data.ViewNormalPass);

			auto baseMaterial = s_Data.ViewNormalsMaterial;

			baseMaterial->set("projInfo", s_Data.Options.ProjInfo, ShaderDomain::Pixel);
			baseMaterial->set("InvFullResolution", glm::vec2{ 1 / s_Data.ViewPortSize.x, 1 / s_Data.ViewPortSize.y }, ShaderDomain::Pixel);
			baseMaterial->set("texLinearDepth", s_Data.GeometryPass->getSpecification().TargetFramebuffer->getTexture(5));
			Renderer::submitFullscreenQuad(baseMaterial);
			Renderer::endRenderPass();
		}


		Renderer::beginRenderPass(s_Data.DeinterleavedPass);

		s_Data.DeInterleavingMaterial->set("u_LinearDepth", s_Data.GeometryPass->getSpecification().TargetFramebuffer->getTexture(5));

		s_Data.DeInterleavingShader->bind();

		glm::vec2 fullScreenSizeInv{ 1.f / s_Data.ViewPortSize.x, 1.f / s_Data.ViewPortSize.y };

		for (int i = 0; i < 16; i += 8)
		{
			s_Data.DeInterleavingMaterial->set("u_Info", glm::vec4{ float(i % 4) + 0.5f, float(i / 4) + 0.5f, fullScreenSizeInv.x, fullScreenSizeInv.y }, ShaderDomain::Pixel);

			s_Data.DeInterleavingMaterial->bind();
			for (int layer = 0; layer < 8; layer++)
			{
				Renderer::submit([layer, i]
					{
						glFramebufferTexture(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0 + layer, s_Data.DeinterleavedPass->getSpecification().TargetFramebuffer->getTextureID(i + layer), 0);
					});
			}
			Renderer::submitFullscreenQuad(nullptr);
		}
		Renderer::endRenderPass();

#if 0
		Renderer::beginRenderPass(s_Data.HZBRenderPass);

		s_Data.HZBShader->bind();

		s_Data.HZBShader->setFloat4("DispatchThreadIdToBufferUV", s_Data.DispatchThreadIdToBufferUV);
		s_Data.HZBShader->setFloat2("InvSize", glm::vec2(1.f) / s_Data.ViewPortSize);
		s_Data.HZBShader->setFloat2("InputViewportMaxBound", s_Data.InputViewportMaxBound);

		RendererID FurthestHZB0 = s_Data.HZBRenderPass->getSpecification().TargetFramebuffer->getTextureID(0);
		RendererID FurthestHZB1 = s_Data.HZBRenderPass->getSpecification().TargetFramebuffer->getTextureID(1);
		RendererID FurthestHZB2 = s_Data.HZBRenderPass->getSpecification().TargetFramebuffer->getTextureID(2);
		RendererID FurthestHZB3 = s_Data.HZBRenderPass->getSpecification().TargetFramebuffer->getTextureID(3);



		Renderer::submit([=]
			{

				glBindTextureUnit(0, s_Data.GeometryPass->getSpecification().TargetFramebuffer->getTextureID(5));
				glBindImageTexture(1, FurthestHZB0, 0, GL_FALSE, 0, GL_WRITE_ONLY, GL_R16F);
				glBindImageTexture(2, FurthestHZB1, 0, GL_FALSE, 0, GL_WRITE_ONLY, GL_R16F);
				glBindImageTexture(3, FurthestHZB2, 0, GL_FALSE, 0, GL_WRITE_ONLY, GL_R16F);
				glBindImageTexture(4, FurthestHZB3, 0, GL_FALSE, 0, GL_WRITE_ONLY, GL_R16F);

				glDispatchCompute(128, 64, 1);
			});
		Renderer::endRenderPass();
#elif 0

		Renderer::submit([=]
			{

				glBindTextureUnit(0, s_Data.GeometryPass->getSpecification().TargetFramebuffer->getTextureID(5));
				glBindImageTexture(1, FurthestHZB0, 0, GL_FALSE, 0, GL_WRITE_ONLY, GL_R16F);
				glBindImageTexture(2, FurthestHZB1, 0, GL_FALSE, 0, GL_WRITE_ONLY, GL_R16F);
				glBindImageTexture(3, FurthestHZB2, 0, GL_FALSE, 0, GL_WRITE_ONLY, GL_R16F);
				glBindImageTexture(4, FurthestHZB3, 0, GL_FALSE, 0, GL_WRITE_ONLY, GL_R16F);

			});

		Renderer::submitFullscreenQuad(nullptr);
		Renderer::endRenderPass();

#endif

	}

	void SceneRenderer::ssaoPass()
	{
		//BEY_PROFILE_FUNCTION();
		//if ((s_Data.DrawList.empty() && s_Data.SelectedMeshDrawList.empty()) || !s_Data.Options.EnableSSAO)
		//{
		//	Renderer::beginRenderPass(s_Data.HBAOPass);
		//	Renderer::endRenderPass();
		//}
		//else
		//{
		//	Renderer::beginRenderPass(s_Data.HBAOPass);
		//	auto& baseMaterial = s_Data.SSAOMaterial;
		//	
		//
		//	baseMaterial->set("u_Position", s_Data.GeometryPass->getSpecification().TargetFramebuffer->getTexture(1));
		//	baseMaterial->set("u_Normal", s_Data.GeometryPass->getSpecification().TargetFramebuffer->getTexture(3));
		//	baseMaterial->set("u_texNoise", s_Data.SSAONoise);
		//
		//	baseMaterial->set("u_NoiseScale", s_Data.ViewPortSize / 4.f, ShaderDomain::Pixel);
		//	baseMaterial->set("u_kernelSize", 8, ShaderDomain::Pixel);
		//	baseMaterial->set("u_radius", s_Data.Options.SSAORadius, ShaderDomain::Pixel);
		//	baseMaterial->set("u_bias", s_Data.Options.SSAOBias, ShaderDomain::Pixel);
		//	baseMaterial->bind();
		//	auto shader = s_Data.SSAOShader;
		//
		//	shader->setVec3Array("u_samples", RandomGenerator::getKernel(), int32_t(RandomGenerator::getKernel().size()));
		//
		//
		//	Renderer::submitFullscreenQuad(nullptr);
		//	Renderer::endRenderPass();
		//
		//}
	}

	void SceneRenderer::blurSSAO()
	{
		BEY_PROFILE_FUNCTION();
		//if ((s_Data.DrawList.empty() && s_Data.SelectedMeshDrawList.empty()) || !s_Data.Options.EnableSSAO)
		//{
		//	Renderer::beginRenderPass(s_Data.SSAOPass);
		//	Renderer::endRenderPass();
		//}
		//else
		//{
		//	auto& baseMaterial = s_Data.SSAOBlurMaterial;
		//	Renderer::beginRenderPass(s_Data.PongPass);
		//	baseMaterial->set("u_SSAOInput", s_Data.SSAOPass->getSpecification().TargetFramebuffer->getTexture(0));
		//	Renderer::submitFullscreenQuad(baseMaterial);
		//	Renderer::endRenderPass();
		//}
	}

	void SceneRenderer::copyDepthAndStencil()
	{
		BEY_PROFILE_FUNCTION();
		//if (!s_Data.DrawList.empty() || !s_Data.SelectedMeshDrawList.empty())
		//{
		//	auto sender = s_Data.GeometryPass->getSpecification().TargetFramebuffer;
		//	auto receiver = s_Data.CompositePass->getSpecification().TargetFramebuffer;
		//	auto width = sender->getSpecification().Width;
		//	auto height = sender->getSpecification().Height;
		//	Renderer::submit([width, height, sender, receiver]() mutable
		//		{
		//			glBlitNamedFramebuffer(sender->getRendererID(), receiver->getRendererID(),
		//				0, 0, width, height,
		//				0, 0, width, height,
		//				GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT, GL_NEAREST);
		//		});
		//}
	}

	void SceneRenderer::ssrPass()
	{
		BEY_PROFILE_FUNCTION();

		Renderer::beginRenderPass(s_Data.SSRPass);
		if (s_Data.SceneData.SceneEnvironment.RadianceMap)
		{
			const auto viewProjection = s_Data.SceneData.SceneCamera.getViewProj();
			// Skybox
			s_Data.SceneData.SkyBoxMaterial->set("u_InverseVP", inverse(viewProjection), ShaderDomain::Vertex);
			s_Data.SceneData.SkyBoxMaterial->set("u_SkyIntensity", s_Data.UniformStruct.SceneEnvironmentIntensity, ShaderDomain::Pixel);
			s_Data.SceneData.SkyBoxMaterial->set("u_TextureLod", s_Data.SkyboxLod, ShaderDomain::Pixel);
			s_Data.SceneData.SkyBoxMaterial->set("u_Angle", s_Data.SceneEnvironmentAngle, ShaderDomain::Pixel);
			Renderer::submitFullscreenQuad(s_Data.SceneData.SkyBoxMaterial);
		}

		if ((!s_Data.DrawList.empty() || !s_Data.SelectedMeshDrawList.empty()) && s_Data.Options.EnableSSR)
		{


			Renderer::submit([]
				{
					glDisable(GL_DEPTH_TEST);
					glDisable(GL_STENCIL_TEST);
					glDisable(GL_BLEND);
				});
			auto baseMaterial = s_Data.SSRMaterial;



			baseMaterial->set("u_NormalScene", s_Data.GeometryPass->getSpecification().TargetFramebuffer->getTexture(0));
			baseMaterial->set("G_Depth", s_Data.GeometryPass->getSpecification().TargetFramebuffer->getTexture(6));

			baseMaterial->set("G_Normal", s_Data.GeometryPass->getSpecification().TargetFramebuffer->getTexture(3));
			baseMaterial->set("G_MetalnessRoughness", s_Data.GeometryPass->getSpecification().TargetFramebuffer->getTexture(4));
			baseMaterial->set("u_FadeIn", s_Data.SSRFadeIn, ShaderDomain::Pixel);
			baseMaterial->set("u_DistanceFade", s_Data.SSRFadeDistance, ShaderDomain::Pixel);
			baseMaterial->set("u_DepthTolerance", s_Data.SSRDepthTolerance, ShaderDomain::Pixel);
			baseMaterial->set("u_MaxSteps", s_Data.SSRMaxSteps, ShaderDomain::Pixel);
			//baseMaterial->set("u_SkipFactor", s_Data.SSRSkipFactor, ShaderDomain::Pixel);

			Renderer::submitFullscreenQuad(baseMaterial);

			Renderer::submit([]
				{
					glEnable(GL_DEPTH_TEST);
					glEnable(GL_STENCIL_TEST);
				});

		}



		Renderer::endRenderPass();

	}




	void SceneRenderer::lightingPass()
	{
		BEY_PROFILE_FUNCTION();
		//Renderer::beginRenderPass(s_Data.LightingPass);
		//if (s_Data.SceneData.SceneEnvironment.RadianceMap)
		//{
		//	const auto viewProjection = s_Data.SceneData.SceneCamera.getViewProj();
		//	// Skybox
		//	s_Data.SceneData.SkyBoxMaterial->set("u_InverseVP", inverse(viewProjection), ShaderDomain::Vertex);
		//	s_Data.SceneData.SkyBoxMaterial->set("u_SkyIntensity", s_Data.UniformStruct.SceneEnvironmentIntensity, ShaderDomain::Pixel);
		//	s_Data.SceneData.SkyBoxMaterial->set("u_TextureLod", s_Data.UniformStruct.SkyboxLod, ShaderDomain::Pixel);
		//	s_Data.SceneData.SkyBoxMaterial->set("u_Angle", s_Data.UniformStruct.SceneEnvironmentAngle, ShaderDomain::Pixel);
		//	Renderer::submitFullscreenQuad(s_Data.SceneData.SkyBoxMaterial);
		//}

	//	if (!s_Data.DrawList.empty() || !s_Data.SelectedMeshDrawList.empty())
	//	{
	//		Renderer::submit([]
	//			{
	//				glDisable(GL_DEPTH_TEST);
	//				glDisable(GL_STENCIL_TEST);
	//			});
	//		auto& baseMaterial = s_Data.PBRMaterial;

	//		//baseMaterial->set("lightSpaceMatrix0", s_Data.Shadows.LightMatrices[0], ShaderDomain::Pixel);
	//		//baseMaterial->set("lightSpaceMatrix1", s_Data.Shadows.LightMatrices[1], ShaderDomain::Pixel);
	//		//baseMaterial->set("lightSpaceMatrix2", s_Data.Shadows.LightMatrices[2], ShaderDomain::Pixel);
	//		//baseMaterial->set("lightSpaceMatrix3", s_Data.Shadows.LightMatrices[3], ShaderDomain::Pixel);

	//		/*baseMaterial->set("u_EnableSSR", s_Data.Options.EnableSSR, ShaderDomain::Pixel);
	//		baseMaterial->set("u_EnableBloom", s_Data.Options.EnableBloom, ShaderDomain::Pixel);
	//		baseMaterial->set("u_ShowCascades", s_Data.Shadows.ShowCascades, ShaderDomain::Pixel);
	//		baseMaterial->set("u_SoftShadows", s_Data.Shadows.SoftShadows, ShaderDomain::Pixel);
	//		baseMaterial->set("u_CascadeFading", s_Data.Shadows.CascadeFading, ShaderDomain::Pixel);
	//		baseMaterial->set("u_SSAOToggle", s_Data.Options.EnableSSAO, ShaderDomain::Pixel);
	//		baseMaterial->set("u_UseIrradianceReflections", s_Data.Options.EnableIrradianceReflections, ShaderDomain::Pixel);
	//		baseMaterial->set("u_EnableWireframe", s_Data.Options.DrawWireFrame, ShaderDomain::Pixel);*/

	//		//baseMaterial->set("u_shadowMap0", s_Data.DirShadowPass->getSpecification().TargetFramebuffer->getTexture(0));
	//		//baseMaterial->set("u_shadowMap1", s_Data.DirShadowPass->getSpecification().TargetFramebuffer->getTexture(1));
	//		//baseMaterial->set("u_shadowMap2", s_Data.DirShadowPass->getSpecification().TargetFramebuffer->getTexture(2));
	//		//baseMaterial->set("u_shadowMap3", s_Data.DirShadowPass->getSpecification().TargetFramebuffer->getTexture(3));

	//		/*
	//		const auto directionalLight = s_Data.SceneData.SceneLightEnvironment.DirectionalLights[0];
	//		baseMaterial->set("u_DirectionalLight", directionalLight, ShaderDomain::Pixel);*/

	//		baseMaterial->set("u_EnvMapRotation", s_Data.UniformStruct.SceneEnvironmentAngle, ShaderDomain::Pixel);
	//		baseMaterial->set("u_EnvRadianceTex", s_Data.SceneData.SceneEnvironment.RadianceMap);
	//		baseMaterial->set("u_EnvIrradianceTex", s_Data.SceneData.SceneEnvironment.IrradianceMap);
	//		baseMaterial->set("u_BRDFLUTTexture", s_Data.BRDFLUT);
	//		//baseMaterial->set("G_SSRReflections", s_Data.SSRPass->getSpecification().TargetFramebuffer->getTexture(0));
	//		baseMaterial->set("G_Position", s_Data.GeometryPass->getSpecification().TargetFramebuffer->getTexture(0));
	//		baseMaterial->set("G_Albedo", s_Data.GeometryPass->getSpecification().TargetFramebuffer->getTexture(1));
	//		baseMaterial->set("G_Normal", s_Data.GeometryPass->getSpecification().TargetFramebuffer->getTexture(2));
	//		baseMaterial->set("G_MetalnessRoughness", s_Data.GeometryPass->getSpecification().TargetFramebuffer->getTexture(3));
	//		//baseMaterial->set("G_SSAO", s_Data.PongPass->getSpecification().TargetFramebuffer->getTexture(0));
	//		//baseMaterial->set("u_PointShadowMap", s_Data.PointShadowPass->getSpecification().TargetFramebuffer->getTexture(1));

	//		Renderer::submitFullscreenQuad(baseMaterial);
	//	}
	//	Renderer::submit([]
	//		{
	//			glEnable(GL_DEPTH_TEST);
	//			glEnable(GL_STENCIL_TEST);
	//		});
	}

	void SceneRenderer::bloomBlurPass()
	{
		BEY_PROFILE_FUNCTION();

		/*if (!s_Data.Options.EnableBloom || (s_Data.DrawList.empty() && s_Data.SelectedMeshDrawList.empty()))
		{
			Renderer::beginRenderPass(s_Data.PingPass);
			Renderer::endRenderPass();
			Renderer::beginRenderPass(s_Data.PongPass);
			Renderer::endRenderPass();

		}
		else
		{
			bool horizontal = true, firstIteration = true;
			const unsigned int amount = 10;

			auto& baseMaterial = s_Data.BloomBlurMaterial;
			for (unsigned int i = 0; i < amount; i++)
			{
				Renderer::beginRenderPass(horizontal ? s_Data.PingPass : s_Data.PongPass, false);
				if (firstIteration)
					baseMaterial->set("tex", s_Data.LightingPass->getSpecification().TargetFramebuffer->getTexture(1));
				else
					if (horizontal)
						baseMaterial->set("tex", s_Data.PongPass->getSpecification().TargetFramebuffer->getTexture(0));
					else
						baseMaterial->set("tex", s_Data.PingPass->getSpecification().TargetFramebuffer->getTexture(0));

				baseMaterial->bind();

				baseMaterial->getShader()->setInt("horizontal", horizontal);
				Renderer::submitFullscreenQuad(nullptr);
				horizontal = !horizontal;
				if (firstIteration)
					firstIteration = false;
				Renderer::endRenderPass();
			}
		}*/
	}

	void SceneRenderer::SSGIPass()
	{
		//Renderer::beginRenderPass(s_Data.ColorAccumulationPass);
		//
		//
		//
		//
		//
		//
		//;
		//s_Data.ColorAccumulationShader->bind();
		//
		//GLuint colorMapID = s_Data.GeometryPass->getSpecification().TargetFramebuffer->getTextureID(0);
		//GLuint depthMapID = s_Data.GeometryPass->getSpecification().TargetFramebuffer->getTextureID(5);
		////GLuint FurthestHZBID = s_Data.HZBRenderPass->getSpecification().TargetFramebuffer->getTextureID(0);
		//GLuint normalMapID = s_Data.GeometryPass->getSpecification().TargetFramebuffer->getTextureID(3);
		//GLuint ssgiOutput = s_Data.ColorAccumulationPass->getSpecification().TargetFramebuffer->getTextureID(0);
		//GLuint ambientOcclusionOutput = s_Data.ColorAccumulationPass->getSpecification().TargetFramebuffer->getTextureID(1);
		//
		//s_Data.ColorAccumulationShader->setFloat4("View_ScreenPositionScaleBias", s_Data.View_ScreenPositionScaleBias);
		//s_Data.ColorAccumulationShader->setUInt("View_StateFrameIndexMod8", s_Data.View_StateFrameIndexMod8);
		//s_Data.ColorAccumulationShader->setFloat3("View_IndirectLightingColorScale", s_Data.View_IndirectLightingColorScale);
		//s_Data.ColorAccumulationShader->setFloat("StateFrameIndexMod8", s_Data.StateFrameIndexMod8);
		//s_Data.ColorAccumulationShader->setFloat4("ScreenPositionScaleBias", s_Data.ScreenPositionScaleBias);
		//
		//s_Data.ColorAccumulationShader->setFloat4("HZBUvFactorAndInvFactor", s_Data.HZBUvFactorAndInvFactor);
		//s_Data.ColorAccumulationShader->setFloat4("ColorBufferScaleBias", s_Data.ColorBufferScaleBias);
		//s_Data.ColorAccumulationShader->setFloat2("ReducedColorUVMax", s_Data.ReducedColorUVMax);
		//s_Data.ColorAccumulationShader->setFloat("PixelPositionToFullResPixel", s_Data.PixelPositionToFullResPixel);
		//s_Data.ColorAccumulationShader->setFloat2("FullResPixelOffset", s_Data.FullResPixelOffset);
		//
		//
		//
		//Renderer::submit([colorMapID, depthMapID, normalMapID, ssgiOutput, ambientOcclusionOutput, FurthestHZBID]()
		//	{
		//		/*
		//		 * uniform vec4 View_ScreenPositionScaleBias;
		//	uniform uint View_StateFrameIndexMod8;
		//	uniform vec3 View_IndirectLightingColorScale;
		//
		//
		//	uniform vec4 HZBUvFactorAndInvFactor;
		//	uniform vec4 ColorBufferScaleBias;
		//	uniform vec2 ReducedColorUVMax;
		//	uniform float PixelPositionToFullResPixel;
		//	uniform vec2 FullResPixelOffset;
		//		 */
		//
		//
		//		glBindTextureUnit(0, colorMapID);
		//		glBindTextureUnit(1, depthMapID);
		//		glBindTextureUnit(2, normalMapID);
		//		glBindTextureUnit(5, FurthestHZBID);
		//		glBindImageTexture(3, ssgiOutput, 0, GL_FALSE, 0, GL_WRITE_ONLY, GL_RGBA16F);
		//		glBindImageTexture(4, ambientOcclusionOutput, 0, GL_FALSE, 0, GL_WRITE_ONLY, GL_RGBA16F);
		//		glDispatchCompute((s_Data.ViewPortSize.x) / 4, (s_Data.ViewPortSize.y) / 4, 1);
		//	});

		//Renderer::submit([]
		//	{
		//		glDisable(GL_DEPTH_TEST);
		//		glDisable(GL_STENCIL_TEST);
		//	});
		//auto baseMaterial = s_Data.ColorAccumulationMaterial;
		//
		//baseMaterial->set("u_NormalScene", s_Data.GeometryPass->getSpecification().TargetFramebuffer->getTexture(0));
		//baseMaterial->set("G_Depth", s_Data.GeometryPass->getSpecification().TargetFramebuffer->getTexture(5));
		////baseMaterial->set("G_Normal", s_Data.GeometryPass->getSpecification().TargetFramebuffer->getTexture(3));
		//
		//baseMaterial->set("u_Samples", s_Data.SSGISamples, ShaderDomain::Pixel);
		//baseMaterial->set("u_IndirectAmount", s_Data.IndirectAmount, ShaderDomain::Pixel);
		//baseMaterial->set("u_NoiseAmount", s_Data.NoiseAmount, ShaderDomain::Pixel);
		//baseMaterial->set("u_Noise", s_Data.EnableNoise, ShaderDomain::Pixel);
		//
		//Renderer::submitFullscreenQuad(baseMaterial);
		//
		//Renderer::submit([]
		//	{
		//		glEnable(GL_DEPTH_TEST);
		//		glEnable(GL_STENCIL_TEST);
		//	});


	//Renderer::endRenderPass();
	}

	void SceneRenderer::hbaoPass()
	{
		BEY_PROFILE_FUNCTION();
		if ((s_Data.DrawList.empty() && s_Data.SelectedMeshDrawList.empty()) || !s_Data.Options.EnableHBAO)
		{
			Renderer::beginRenderPass(s_Data.HBAOPass);
			Renderer::endRenderPass();
		}
		else
		{
			updateHBAOData();
			Renderer::beginRenderPass(s_Data.HBAOPass);
			auto& baseMaterial = s_Data.HBAOMaterial;

			baseMaterial->set("u_TexLinearDepth", s_Data.DeinterleavedPass->getSpecification().TargetFramebuffer->getTextureArray());
			baseMaterial->set("u_Normal", s_Data.ViewNormalPass->getSpecification().TargetFramebuffer->getTexture(0));
			baseMaterial->set("u_ViewPosition", s_Data.GeometryPass->getSpecification().TargetFramebuffer->getTexture(1));

			baseMaterial->set("u_RadiusToScreen", s_Data.Options.RadiusToScreen, ShaderDomain::Pixel);
			//baseMaterial->set("u_R2", s_Data.Options.R2, ShaderDomain::Pixel);
			baseMaterial->set("u_NegInvR2", s_Data.Options.NegInvR2, ShaderDomain::Pixel);
			baseMaterial->set("u_NDotVBias", s_Data.Options.NDotVBias, ShaderDomain::Pixel);
			//baseMaterial->set("u_InvFullResolution", glm::vec2{ 1 / s_Data.ViewPortSize.x, 1 / s_Data.ViewPortSize.y }, ShaderDomain::Pixel);

			const int quarterWidth = (s_Data.ViewPortSize.x + 3) / 4;
			const int quarterHeight = (s_Data.ViewPortSize.y + 3) / 4;
			baseMaterial->set("u_InvQuarterResolution", glm::vec2{ 1.f / float(quarterWidth), 1.f / float(quarterHeight) }, ShaderDomain::Pixel);
			baseMaterial->set("u_AOMultiplier", s_Data.Options.AOMultiplier, ShaderDomain::Pixel);
			baseMaterial->set("u_PowExponent", s_Data.Options.PowExponent, ShaderDomain::Pixel);
			//baseMaterial->set("u_projScale", s_Data.Options.ProjScale, ShaderDomain::Pixel);

			baseMaterial->set("u_projInfo", s_Data.Options.ProjInfo, ShaderDomain::Pixel);



			baseMaterial->bind();
			auto shader = s_Data.HBAOShader;

			shader->setVec4Array("u_Float2Offsets", s_Data.Float2Offsets, int32_t(16));
			shader->setVec4Array("u_Jitters", RandomGenerator::getHBAORandom(), int32_t(16));

			Renderer::submit([]
				{
					glDrawArrays(GL_TRIANGLES, 0, 3 * 16);
				});


			Renderer::endRenderPass();

		}


		{
			if ((s_Data.DrawList.empty() && s_Data.SelectedMeshDrawList.empty()) || !s_Data.Options.EnableHBAO)
			{
				Renderer::beginRenderPass(s_Data.ReInterleavingPass);
				Renderer::endRenderPass();
			}
			else
			{
				Renderer::beginRenderPass(s_Data.ReInterleavingPass);
				Renderer::submit([]
					{
						glDrawBuffer(GL_COLOR_ATTACHMENT0);
					});

				{
					auto& baseMaterial = s_Data.ReInterleavingMaterial;

					baseMaterial->set("u_TexResultsArray", s_Data.HBAOPass->getSpecification().TargetFramebuffer->getTextureArray());

					Renderer::submitFullscreenQuad(baseMaterial);
				}




				Renderer::submit([]
					{
						glDrawBuffer(GL_COLOR_ATTACHMENT1);
					});
				{
					auto& baseMaterial = s_Data.HBAOBlurMaterial;

					baseMaterial->set("u_TexSource", s_Data.ReInterleavingPass->getSpecification().TargetFramebuffer->getTexture(0));
					baseMaterial->set("u_Sharpness", s_Data.BlurSharpness, ShaderDomain::Pixel);
					baseMaterial->set("u_InvResolutionDirection", glm::vec2{ 1.f / (s_Data.ViewPortSize.x), 0 }, ShaderDomain::Pixel);

					Renderer::submitFullscreenQuad(baseMaterial);

					Renderer::endRenderPass();
				}
				{
					Renderer::beginRenderPass(s_Data.GeometryPass, false);
					Renderer::submit([]
						{
							// final output to main fbo
							glDisable(GL_DEPTH_TEST);
							glEnable(GL_BLEND);
							glBlendFunc(GL_ZERO, GL_SRC_COLOR);
						});
					auto baseMaterial = s_Data.HBAOBlur2Material;
					baseMaterial->set("u_TexSource", s_Data.ReInterleavingPass->getSpecification().TargetFramebuffer->getTexture(0));
					baseMaterial->set("u_Sharpness", s_Data.BlurSharpness, ShaderDomain::Pixel);
					baseMaterial->set("u_InvResolutionDirection", glm::vec2{ 0, 1.f / (s_Data.ViewPortSize.y) }, ShaderDomain::Pixel);
					Renderer::submitFullscreenQuad(baseMaterial);
					Renderer::submit([]
						{
							// final output to main fbo
							glEnable(GL_DEPTH_TEST);
							glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

							glDisable(GL_BLEND);

						});

					Renderer::endRenderPass();
				}
			}
		}
	}






	void SceneRenderer::compositePass()
	{
		BEY_PROFILE_FUNCTION();



		Renderer::beginRenderPass(s_Data.CompositePass);

		auto& baseMaterial = s_Data.CompositeMaterial;

		Renderer::submit([]()
			{
				glStencilFunc(GL_ALWAYS, 0, 0xff);
				glStencilMask(0xff);
			});
		baseMaterial->bind();
		baseMaterial->set("u_Exposure", s_Data.SceneData.SceneCamera.Camera->getExposure(), ShaderDomain::Pixel);
		// baseMaterial->set("u_EnableBloom", s_Data.Options.EnableBloom, ShaderDomain::Pixel);
		//baseMaterial->set("u_TextureSamples", s_Data.LightingPass->getSpecification().TargetFramebuffer->getSpecification().Samples);
		baseMaterial->set("u_NormalScene", s_Data.SSRPass->getSpecification().TargetFramebuffer->getTexture(0));
		//baseMaterial->set("u_Skybox", s_Data.GeometryPass->getSpecification().TargetFramebuffer->getTexture(0));
		//baseMaterial->set("u_BlurredScene", s_Data.PongPass->getSpecification().TargetFramebuffer->getTexture(0));
		Renderer::submitFullscreenQuad(baseMaterial);

		if (!s_Data.DrawList.empty() || !s_Data.SelectedMeshDrawList.empty())
		{


			//////////////////////////////////////
			///
			copyDepthAndStencil();
			if (outline)
			{

				Renderer::submit([]()
					{
						glStencilFunc(GL_NOTEQUAL, 1, 0xff);
						glStencilMask(0);

						glLineWidth(5);
						//glEnable(GL_LINE_SMOOTH);
						glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
						//glDisable(GL_DEPTH_TEST);
					});
				// Draw outline here
				s_Data.OutlineMaterial->getShader()->bind();
				for (auto& dc : s_Data.SelectedMeshDrawList)
					Renderer::submitMesh(dc.Mesh, dc.Transform, s_Data.OutlineMaterial);



				Renderer::submit([]
					{
						glPointSize(5);
						glPolygonMode(GL_FRONT_AND_BACK, GL_POINT);
					});


				for (auto& dc : s_Data.SelectedMeshDrawList)
					Renderer::submitMesh(dc.Mesh, dc.Transform, s_Data.OutlineMaterial);
			}

			Renderer::submit([]
				{
					glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
					glStencilFunc(GL_ALWAYS, 1, 0xff); // DO NOT REMOVE, Stencil buffer won't be cleared
					glEnable(GL_DEPTH_TEST);
					glStencilMask(0xff);
				});
		}

		/*if (s_Data.DrawList.empty() && s_Data.SelectedMeshDrawList.empty())
			Renderer::clearDepth();*/

			//// Corners
			//if (getOptions().ShowCameraCorners)
			//{
			//	Renderer2D::beginScene(s_Data.SceneData.SceneCamera.getViewProj());
			//	//for (size_t i = 0; i < 4; ++i)
			//	//	Renderer::drawCorners(shadows::pointer[i].FrustumCorners, i);
			//	Renderer::drawCorners(CascadeData::WholeFrustumCorners, 0);
			//	Renderer2D::endScene();
			//}

		Renderer::submit([]
			{
				glEnable(GL_BLEND);
				glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
			});
		// Grid
		if (getOptions().ShowGrid)
		{
			Renderer::submit([] { glDisable(GL_CULL_FACE); });
			s_Data.GridMaterial->setFlag(MaterialFlag::DepthTest, !(s_Data.SelectedMeshDrawList.empty() && s_Data.DrawList.empty()));
			static const auto GRID_TRANSFORM = glm::rotate(glm::mat4(1.0f), glm::radians(90.0f),
				glm::vec3(1.0f, 0.0f, 0.0f)) * glm::scale(glm::mat4(1.0f), glm::vec3(100.0f));
			Renderer::submitQuad(s_Data.GridMaterial, GRID_TRANSFORM);
			Renderer::submit([] { glEnable(GL_CULL_FACE); });
		}
		Renderer::submit([]
			{
				glDisable(GL_BLEND);
			});






		//{
		//	const auto viewDir = s_Data.SceneData.SceneCamera.Camera->getViewDirection();
		//	s_Data.IconMaterial->set("u_IconTexture", s_Data.PointLightIcon);
		//	for (auto& light : s_Data.SceneData.SceneLightEnvironment.PointLights)
		//		Renderer::submitQuad(s_Data.IconMaterial, light.UUID, glm::inverse(glm::lookAt(light.Position, light.Position + viewDir, { 0.f, -1.f, 0.f })));
		//
		//	s_Data.IconMaterial->set("u_IconTexture", s_Data.DirLightIcon);
		//	const auto* const lights = s_Data.SceneData.SceneLightEnvironment.DirectionalLights;
		//	for (size_t i = 0; auto & position : s_Data.SceneData.SceneLightEnvironment.DirLightPositions)
		//		Renderer::submitQuad(s_Data.IconMaterial, lights[i++].UUID, glm::inverse(glm::lookAt(position, position + viewDir, { 0.f, -1.f, 0.f })));
		//	//Renderer::submit([] { glDisable(GL_BLEND); });
		//}




		//if (getOptions().ShowBoundingBoxes)
		//{
		//	Renderer2D::beginScene(viewProjection);
		//	for (auto& dc : s_Data.DrawList)
		//		Renderer::drawAABB(dc.Mesh, dc.Transform);
		//	Renderer2D::endScene();
		//}



		Renderer::endRenderPass();

	}

	void SceneRenderer::flushDrawList()
	{
		BEY_PROFILE_FUNCTION();

		BEY_CORE_ASSERT(!s_Data.ActiveScene, "Did you forget to set the active scene?");

		if (!getOptions().ShowCameraCorners)
			depthPass();
		geometryPass();
		SSGIPass();
		ssaoPass();
		hbaoPass();
		blurSSAO();
		ssrPass();
		lightingPass();
		bloomBlurPass();
		compositePass();

		s_Data.DrawList.clear();
		s_Data.SelectedMeshDrawList.clear();
		s_Data.SceneData = {};
	}


	Ref<Texture2D> SceneRenderer::getFinalColorBuffer()
	{
		// return s_Data.CompositePass->GetSpecification().TargetFramebuffer;
		BEY_CORE_ASSERT(false, "Not implemented");
		return nullptr;
	}

	Ref<RenderPass> SceneRenderer::getFinalRenderPass()
	{
		return s_Data.CompositePass;
	}

	size_t SceneRenderer::getTargetRendererID(const int flag)
	{
		switch (s_Data.RenderModeEn & flag)
		{
		case Position: return s_Data.GeometryPass->getSpecification().TargetFramebuffer->getTextureID(0);
		case Albedo: return s_Data.GeometryPass->getSpecification().TargetFramebuffer->getTextureID(1);
		case Normal: return s_Data.GeometryPass->getSpecification().TargetFramebuffer->getTextureID(2);
		case RoughnessMetalness: return s_Data.GeometryPass->getSpecification().TargetFramebuffer->getTextureID(3);
			//case SSR: return s_Data.SSRPass->getSpecification().TargetFramebuffer->getTextureID(0);
			//case SSAO: return s_Data.SSAOPass->getSpecification().TargetFramebuffer->getTextureID(0);
			//case Lighting: return s_Data.LightingPass->getSpecification().TargetFramebuffer->getTextureID(0);
			//case Bloom: return s_Data.PongPass->getSpecification().TargetFramebuffer->getTextureID(0);
		case None:;
		default: BEY_CORE_ASSERT(false, "Wrong Enum Value"); return 0;
		}
	}

	uint64_t SceneRenderer::getFinalColorBufferRendererID()
	{
		BEY_PROFILE_FUNCTION();

		return s_Data.CompositePass->getSpecification().TargetFramebuffer->getTextureID(0);
	}

	static Ref<Shader> equirectangularConversionShader, envFilteringShader, envIrradianceShader;

	void SceneRenderer::terminate()
	{
		BEY_PROFILE_FUNCTION();

		delete s_Data.ActiveScene;
		s_Data.ActiveScene = nullptr;
		s_Data.BRDFLUT.reset();
		//s_Data.IconShader.reset();
		//s_Data.IconMaterial.reset();
		//s_Data.PointLightIcon.reset();
		//s_Data.DirLightIcon.reset();
		s_Data.SSAONoise.reset();
		s_Data.SSAOMaterial.reset();
		s_Data.SSAOShader.reset();
		s_Data.HBAOPass.reset();
		s_Data.SSRPass.reset();
		s_Data.CompositeShader.reset();
		//s_Data.BloomBlurShader.reset();
		//s_Data.DirShadowShader.reset();
		//s_Data.SSRShader.reset();
		//s_Data.LightingPass.reset();
		s_Data.CompositePass.reset();
		//s_Data.PingPass.reset();
		//s_Data.PongPass.reset();
		//s_Data.DirShadowPass.reset();
		//s_Data.SSRPass.reset();
		s_Data.DrawList.clear();
		s_Data.SelectedMeshDrawList.clear();
		s_Data.GridMaterial.reset();
		s_Data.OutlineMaterial.reset();
		//s_Data.SSRMaterial.reset();
		//s_Data.PBRMaterial.reset();
		s_Data.GLLightUniformBuffer.reset();
		s_Data.GLVisibleLightsBuffer.reset();
		//s_Data.GLLightMatricesUniformBuffer.reset();
		equirectangularConversionShader.reset();
		envFilteringShader.reset();
		envIrradianceShader.reset();
	}

	SceneRendererOptions& SceneRenderer::getOptions()
	{
		return s_Data.Options;
	}


	void SceneRenderer::beginScene(Scene* const scene, const SceneRendererCamera& camera)
	{
		BEY_PROFILE_FUNCTION();

		BEY_CORE_ASSERT(!s_Data.ActiveScene, "Did you call beginScene() twice?");
		getOptions().ShowCameraCorners = camera.Camera->getIsDebug();

		s_Data.ActiveScene = scene;
		s_Data.SceneData.SceneCamera = camera;
		s_Data.SceneData.SkyBoxMaterial = scene->mSkyboxMaterial;
		s_Data.SceneData.SceneEnvironment = scene->mEnvironment;
		s_Data.SceneData.SceneLightEnvironment = scene->mLightEnvironment;

		//s_Data.UniformStruct.SkyboxLod = scene->mSkyboxLod;
		//s_Data.UniformStruct.SceneEnvironmentAngle = scene->mEnvironmentAngle;
		s_Data.UniformStruct.SceneEnvironmentIntensity = scene->mEnvironmentIntensity;
		s_Data.UniformStruct.ViewMatrix = s_Data.SceneData.SceneCamera.ViewMatrix;
		s_Data.UniformStruct.CameraPosition = glm::inverse(s_Data.UniformStruct.ViewMatrix)[3];
		s_Data.UniformStruct.ViewProjectionMatrix = s_Data.SceneData.SceneCamera.getViewProj();
		s_Data.UniformStruct.ProjectionMatrix = s_Data.SceneData.SceneCamera.Camera->getProjectionMatrix();
		s_Data.UniformStruct.ViewMatrixInv = glm::inverse(s_Data.UniformStruct.ViewMatrix);
		s_Data.UniformStruct.PixelSize = glm::vec4(2.f / s_Data.ViewPortSize.x, 2.f / s_Data.ViewPortSize.y, 0.f, 0.f);
		s_Data.UniformStruct.ViewportSize = s_Data.ViewPortSize;
		s_Data.UniformStruct.ProjectionMatrixInv = glm::inverse(s_Data.SceneData.SceneCamera.Camera->getProjectionMatrix());
		s_Data.UniformStruct.ProjectionParams = glm::vec4(0.1f, 1000.f, 0.f, 0.f);

		std::vector<PointLight>& pointLights = scene->mLightEnvironment.PointLights;
		//std::vector<glm::mat4>& pointLightsMatrices = scene->mLightEnvironment.PointLightMatrices;

		//for (size_t index = 0; auto & pointLight : pointLights)
		//{
		//	const glm::vec3& lightPos = pointLight.Position;
		//	const glm::mat4 shadowProj = glm::perspective(glm::radians(90.0f), (float)1024 / (float)1024, pointLight.NearPlane, pointLight.FarPlane);
		//	pointLightsMatrices[6ul * index + 0ul] = (shadowProj * glm::lookAt(lightPos, lightPos + glm::vec3(1.0f, 0.0f, 0.0f), glm::vec3(0.0f, -1.0f, 0.0f)));
		//	pointLightsMatrices[6ul * index + 1ul] = (shadowProj * glm::lookAt(lightPos, lightPos + glm::vec3(-1.0f, 0.0f, 0.0f), glm::vec3(0.0f, -1.0f, 0.0f)));
		//	pointLightsMatrices[6ul * index + 2ul] = (shadowProj * glm::lookAt(lightPos, lightPos + glm::vec3(0.0f, 1.0f, 0.0f), glm::vec3(0.0f, 0.0f, 1.0f)));
		//	pointLightsMatrices[6ul * index + 3ul] = (shadowProj * glm::lookAt(lightPos, lightPos + glm::vec3(0.0f, -1.0f, 0.0f), glm::vec3(0.0f, 0.0f, -1.0f)));
		//	pointLightsMatrices[6ul * index + 4ul] = (shadowProj * glm::lookAt(lightPos, lightPos + glm::vec3(0.0f, 0.0f, 1.0f), glm::vec3(0.0f, -1.0f, 0.0f)));
		//	pointLightsMatrices[6ul * index + 5ul] = (shadowProj * glm::lookAt(lightPos, lightPos + glm::vec3(0.0f, 0.0f, -1.0f), glm::vec3(0.0f, -1.0f, 0.0f)));
		//	index++;
		//}

		uint32_t offset = 0;

		s_Data.UniformBuffer.write(&s_Data.UniformStruct, sizeof s_Data.UniformStruct, offset); offset += sizeof s_Data.UniformStruct;

		size_t size = pointLights.size();
		s_Data.UniformBuffer.write(&size, 16, offset); offset += 16;
		if (!pointLights.empty())
		{
			s_Data.UniformBuffer.write((void*)pointLights.data(), sizeof PointLight * pointLights.size(), offset); offset += uint32_t(sizeof PointLight * pointLights.size());
			//s_Data.PointLightMatricesUniformBuffer.write((void*)pointLightsMatrices.data(), sizeof glm::mat4 * pointLights.size() * 6, 0);
			//s_Data.GLLightMatricesUniformBuffer->setData(s_Data.PointLightMatricesUniformBuffer.Data, uint32_t(sizeof glm::mat4 * pointLights.size() * 6));

		}
		s_Data.GLLightUniformBuffer->setData(s_Data.UniformBuffer.Data, uint32_t(offset));

	}

	void SceneRenderer::endScene()
	{
		BEY_PROFILE_FUNCTION();
		BEY_CORE_ASSERT(s_Data.ActiveScene, "Active Scene cannot be null.");
		s_Data.ActiveScene = nullptr;
		flushDrawList();
	}

	void SceneRenderer::submitMesh(const Ref<Mesh>& mesh, const glm::mat4& transform)
	{
		BEY_PROFILE_FUNCTION();
		// TODO: Culling, sorting, etc.
		s_Data.DrawList.push_back({ mesh /*,overrideMaterial*/, transform });
	}

	void SceneRenderer::submitSelectedMesh(const Ref<Mesh>& mesh, const glm::mat4& transform)
	{
		BEY_PROFILE_FUNCTION();
		s_Data.SelectedMeshDrawList.push_back({ mesh/*, nullptr*/, transform });
	}


	uint32_t SceneRenderer::getLightPixelIDAt(const glm::ivec2& mouse)
	{
		//glBindFramebuffer(GL_READ_FRAMEBUFFER, s_Data.CompositePass->getSpecification().TargetFramebuffer->getRendererID());
		//glReadBuffer(GL_COLOR_ATTACHMENT1);
		//uint32_t pixelData;
		//glReadPixels(mouse.x, mouse.y, 1, 1, GL_RED_INTEGER, GL_UNSIGNED_INT, &pixelData);
		return 0;
	}


	std::pair<Ref<TextureCube>, Ref<TextureCube>> SceneRenderer::createEnvironmentMap(const std::string& filepath)
	{
		BEY_PROFILE_FUNCTION();
		const uint32_t cubemapSize = 2048;
		const uint32_t irradianceMapSize = 32;

		Ref<TextureCube> envUnfiltered = TextureCube::create(TextureFormat::RGBA, TextureInternalFormat::RGBA16F, cubemapSize, cubemapSize);
		if (!equirectangularConversionShader)
			equirectangularConversionShader = Shader::create("assets/shaders/EquirectangularToCubeMap");
		Ref<Texture2D> envEquirect = Texture2D::create(filepath, DataType::FLOAT, false, true, aiTextureType_BASE_COLOR);
		BEY_CORE_ASSERT(envEquirect->getIntFormat() >= TextureInternalFormat::RGB16F, "Texture is not HDR!");
		equirectangularConversionShader->bind();
		envEquirect->bind();
		Renderer::submit([envUnfiltered, cubemapSize]()
			{
				glBindImageTexture(0, envUnfiltered->getRendererID(), 0, GL_TRUE, 0, GL_WRITE_ONLY, GL_RGBA16F);
				glDispatchCompute(cubemapSize / 32, cubemapSize / 32, 6);
				glGenerateTextureMipmap(envUnfiltered->getRendererID());
			});


		if (!envFilteringShader)
			envFilteringShader = Shader::create("assets/shaders/EnvironmentMipFilter");

		Ref<TextureCube> envFiltered = TextureCube::create(TextureFormat::RGBA, TextureInternalFormat::RGBA16F, cubemapSize, cubemapSize);

		Renderer::submit([envUnfiltered, envFiltered]
			{
				glCopyImageSubData(envUnfiltered->getRendererID(), GL_TEXTURE_CUBE_MAP, 0, 0, 0, 0,
					envFiltered->getRendererID(), GL_TEXTURE_CUBE_MAP, 0, 0, 0, 0,
					envFiltered->getWidth(), envFiltered->getHeight(), 6);
			});

		envFilteringShader->bind();
		envUnfiltered->bind();

		Renderer::submit([envFiltered, cubemapSize]
			{
				const float deltaRoughness = 1.0f / glm::max(float(envFiltered->getMipLevelCount()) - 1.0f, 1.0f);
				for (unsigned level = 1, size = cubemapSize / 2; level < envFiltered->getMipLevelCount(); level++, size /= 2)
				{
					const GLuint numGroups = glm::max(1, int(size) / 32);
					glBindImageTexture(0, envFiltered->getRendererID(), level, GL_TRUE, 0, GL_WRITE_ONLY, GL_RGBA16F);
					glProgramUniform1f(envFilteringShader->getRendererID(), 0, deltaRoughness * float(level));
					glDispatchCompute(numGroups, numGroups, 6);
				}
			});

		if (!envIrradianceShader)
			envIrradianceShader = Shader::create("assets/shaders/EnvironmentIrradiance");

		Ref<TextureCube> irradianceMap = TextureCube::create(TextureFormat::RGBA, TextureInternalFormat::RGBA16F, irradianceMapSize, irradianceMapSize);
		envIrradianceShader->bind();
		envFiltered->bind();
		Renderer::submit([irradianceMap]()
			{
				glBindImageTexture(0, irradianceMap->getRendererID(), 0, GL_TRUE, 0, GL_WRITE_ONLY, GL_RGBA16F);
				glDispatchCompute(irradianceMap->getWidth() / 32, irradianceMap->getHeight() / 32, 6);
				glGenerateTextureMipmap(irradianceMap->getRendererID());
			});

		return { envFiltered, irradianceMap };
	}




	void SceneRenderer::onImGuiRender()
	{
		BEY_PROFILE_FUNCTION();
		if (s_Data.RenderModeEn != 0)
		{
			ImGui::Begin("Render Targets", nullptr, ImGuiWindowFlags_NoFocusOnAppearing | ImGuiWindowFlags_NoScrollbar);
			auto fb = s_Data.GeometryPass->getSpecification().TargetFramebuffer;
			const float size = ImGui::GetContentRegionAvailWidth();
			const float w = size;
			const float h = w / (float(fb->getWidth()) / float(fb->getHeight()));
			for (uint64_t i = 1; i <= 8; i++)
				if (s_Data.RenderModeEn & BIT(i))
					ImGui::Image(reinterpret_cast<void*>(getTargetRendererID(BIT(i))), { w, h }, { 0, 1 }, { 1, 0 });
			ImGui::End();
		}

		ImGui::Begin("Scene Renderer");
		// if (ui::BeginTreeNode("Render Targets"))
		// {
		// 	ui::BeginPropertyGrid();
		//
		// 	ui::Property("min Y", Scene::min);
		// 	ui::Property("max Y", Scene::max);
		// 	ui::Property("multiplyer", Scene::multiplyer);
		// 	ui::Property("Enable Position", &s_Data.RenderModeEn, RenderModeEn::Position);
		// 	ui::Property("Enable Albedo", &s_Data.RenderModeEn, RenderModeEn::Albedo);
		// 	ui::Property("Enable Normal", &s_Data.RenderModeEn, RenderModeEn::Normal);
		// 	ui::Property("Enable Roughness Metalness", &s_Data.RenderModeEn, RenderModeEn::RoughnessMetalness);
		// 	ui::Property("Enable Bloom", &s_Data.RenderModeEn, RenderModeEn::Bloom);
		// 	ui::Property("Enable SSR", &s_Data.RenderModeEn, RenderModeEn::SSR);
		// 	ui::Property("Enable SSAO", &s_Data.RenderModeEn, RenderModeEn::SSAO);
		// 	ui::Property("Enable Lighting", &s_Data.RenderModeEn, RenderModeEn::Lighting);
		//
		// 	ui::EndPropertyGrid();
		//
		// 	ui::EndTreeNode();
		// }


		if (ui::BeginTreeNode("Screen Space Ambient Occlusion"))
		{
			ui::BeginPropertyGrid();
			ui::Property("SSAO", s_Data.Options.EnableSSAO);
			ui::Property("SSAO Radius", s_Data.Options.SSAORadius);
			ui::Property("SSAO Bias", s_Data.Options.SSAOBias);

			//ImGui::Text("Some these attributes are shared between SSAO and HBAO");
			ui::Property("HBAO", s_Data.Options.EnableHBAO);
			ui::Property("Intensity", s_Data.Options.SSAOIntensity);

			//ui::Property("Blur", s_Data.Options.SSAOIntensity);
			ui::Property("Blur Sharpness", s_Data.BlurSharpness);

			ui::EndPropertyGrid();

			ui::EndTreeNode();
		}
		if (ui::BeginTreeNode("SSR"))
		{
			ui::BeginPropertyGrid();
			ui::Property("DepthTolerance", s_Data.SSRDepthTolerance, 0.001f);
			ui::Property("Fade in", s_Data.SSRFadeIn, 0.001f);
			ui::Property("Fade Distance", s_Data.SSRFadeDistance, 0.001f);
			ui::Property("Max Marching Steps", s_Data.SSRMaxSteps, 1, 1, 512);
			//ui::Property("SkipFactor", s_Data.SSRSkipFactor, 0.05f, 0.01f, 1.f);
			ui::EndPropertyGrid();
			ui::EndTreeNode();
		}


		if (ui::BeginTreeNode("Shadows"))
		{
			ui::BeginPropertyGrid();
			ui::Property("Soft Shadows", s_Data.Shadows.SoftShadows);
			ui::Property("Light Size", s_Data.UniformStruct.LightSize, 0.01f);
			ui::Property("Max Shadow Distance", s_Data.UniformStruct.MaxShadowDistance, 1.0f);
			ui::Property("Shadow Fade", s_Data.UniformStruct.ShadowFade, 5.0f);
			ui::EndPropertyGrid();
			if (ui::BeginTreeNode("Cascade Settings"))
			{
				ui::BeginPropertyGrid();
				ui::Property("Show Cascades", s_Data.Shadows.ShowCascades);
				ui::Property("Cascade Fading", s_Data.Shadows.CascadeFading);
				ui::Property("Cascade Transition Fade", s_Data.UniformStruct.CascadeTransitionFade, 0.05f, 0.0f, FLT_MAX);
				ui::Property("Cascade Split", s_Data.Shadows.CascadeSplitLambda, 0.01f);
				ui::Property("Cascade Near Plane Offset", s_Data.Shadows.CascadeNearPlaneOffset, 0.1f, -FLT_MAX, 0.0f);
				ui::Property("Cascade Far Plane Offset", s_Data.Shadows.CascadeFarPlaneOffset, 0.1f, 0.0f, FLT_MAX);
				ui::EndPropertyGrid();
				ui::EndTreeNode();
			}
			if (ui::BeginTreeNode("Shadow Map", false))
			{
				static int cascadeIndex = 0;
				const auto id = s_Data.ViewNormalPass->getSpecification().TargetFramebuffer->getTextureID(cascadeIndex);

				const float size = ImGui::GetContentRegionAvailWidth(); // (float)fb->GetWidth() * 0.5f, (float)fb->GetHeight() * 0.5f
				ui::BeginPropertyGrid();
				ui::PropertySlider("Cascade Index", cascadeIndex, 0, 3);
				ui::EndPropertyGrid();
				ImGui::Image(ImTextureID(uint64_t(id)), { size, size }, { 0, 1 }, { 1, 0 });
				ui::EndTreeNode();
			}

			ui::EndTreeNode();
		}

		if (ui::BeginTreeNode("Bloom"))
		{
			ui::BeginPropertyGrid();
			ui::Property("Bloom", s_Data.Options.EnableBloom);
			ui::Property("Bloom Threshold", s_Data.UniformStruct.BloomThreshold, 0.05f);
			ui::EndPropertyGrid();

			/*auto fb = s_Data.LightingPass->getSpecification().TargetFramebuffer;
			const auto id = fb->getTextureID(0);
			const float size = ImGui::GetContentRegionAvailWidth();
			const float w = size;
			const float h = w / (float(fb->getWidth()) / float(fb->getHeight()));
			ImGui::Image(ImTextureID(uint64_t(id)), { w, h }, { 0, 1 }, { 1, 0 });*/
			ui::EndTreeNode();
		}

		if (ui::BeginTreeNode("Reflections"))
		{
			ui::BeginPropertyGrid();
			ui::Property("Screen-Space Reflections", s_Data.Options.EnableSSR);
			ui::Property("Irradiance Reflections", s_Data.Options.EnableIrradianceReflections);
			ui::EndPropertyGrid();

			ui::EndTreeNode();
		}

		if (ui::BeginTreeNode("Mesh Rendering"))
		{
			ui::BeginPropertyGrid();
			ui::Property("Draw Wireframe", s_Data.Options.DrawWireFrame);

			ui::EndPropertyGrid();
			ui::EndTreeNode();
		}

		// 		if (ui::BeginTreeNode("SSGI"))
		// 		{
		// 			/*
		// 			 *
		// 			 * 		glm::vec4 View_ScreenPositionScaleBias = {0.4996f, -0.5000f, 0.5000f, 0.4996f};
		// 		glm::uint View_StateFrameIndexMod8 { 3 };
		// 		glm::vec3 View_IndirectLightingColorScale {3};
		//
		// 		glm::vec4 ColorBufferScaleBias { 0.9934, 0.9931, -0.0000, -0.0000};
		// 		glm::vec2 ReducedColorUVMax { 0.9918f, 0.9913f};
		// 		float PixelPositionToFullResPixel = 1.f;
		// 		glm::vec2 FullResPixelOffset{ 0.5000f, 0.5000f };
		// 			 */
		// 			ui::BeginPropertyGrid();
		//
		// 			ui::Property("View_ScreenPositionScaleBias", s_Data.View_ScreenPositionScaleBias);
		// 			ui::Property("View_StateFrameIndexMod8", s_Data.View_StateFrameIndexMod8, 1, 0, 20);
		// 			ui::Property("View_IndirectLightingColorScale", s_Data.View_IndirectLightingColorScale);
		//
		// 			ui::Property("HZBUvFactorAndInvFactor", s_Data.HZBUvFactorAndInvFactor);
		// 			ui::Property("ColorBufferScaleBias", s_Data.ColorBufferScaleBias);
		// 			ui::Property("ReducedColorUVMax", s_Data.ReducedColorUVMax);
		// 			ui::Property("PixelPositionToFullResPixel", s_Data.PixelPositionToFullResPixel);
		// 			ui::Property("FullResPixelOffset", s_Data.FullResPixelOffset);
		// 			ui::Property("ScreenPositionScaleBias", s_Data.ScreenPositionScaleBias);
		// 			ui::Property("StateFrameIndexMod8", s_Data.StateFrameIndexMod8);
		// 			ui::EndPropertyGrid();
		// 			ui::EndTreeNode();
		// 		}
		ImGui::End();
	}
}

