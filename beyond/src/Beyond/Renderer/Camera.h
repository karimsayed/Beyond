#pragma once


namespace bey {

	class Camera : public RefCounted
	{
	public:
		Camera() = default;
		explicit Camera(float fov, float width, float height, float nearClip, float farClip);
		virtual ~Camera() = default;

		void setDebug(const bool enable) noexcept { mDebug = enable; }
		bool getIsDebug() const { return mDebug; }

		[[nodiscard]] virtual glm::vec3 getViewDirection() const = 0;


		[[nodiscard]] const glm::mat4& getProjectionMatrix() const { return mProjectionMatrix; }
		void setProjectionMatrix(const glm::mat4& projectionMatrix, const float nearClip, const float farClip)
		{
			mProjectionMatrix = projectionMatrix;
			mNear = nearClip;
			mFar = farClip;
		}

		void setViewportSize(const uint32_t width, const uint32_t height) noexcept
		{
			mViewportWidth = width;
			mViewportHeight = height;
		}
		[[nodiscard]] std::pair<float, float> getViewportSize() const
		{
			return std::pair(float(mViewportWidth), float(mViewportHeight));
		}
		void setFarClip(const float farClip) noexcept { mFar = farClip; }
		void setNearClip(const float nearClip) noexcept { mNear = nearClip; }
		[[nodiscard]] float getFarClip() const { return mFar; }
		[[nodiscard]] float getNearClip() const { return mNear; }

		[[nodiscard]] float getExposure() const { return mExposure; }
		[[nodiscard]] float& getExposure() { return mExposure; }
		[[nodiscard]] float getFOV() const { return mFOV; }
		[[nodiscard]] float getAspectRatio() const { return float(mViewportWidth) / mViewportHeight; }

	protected:
		glm::mat4 mProjectionMatrix{ glm::mat4(1.0f) };
		float mExposure{ 0.8f };
		uint32_t mViewportWidth{ 1280 }, mViewportHeight{ 720 };
		float mFOV  {};
		float mNear{};
		float mFar{};
		bool mDebug{ false };
	};

}
