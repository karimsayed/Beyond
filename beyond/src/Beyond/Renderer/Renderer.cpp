#include "pchs.h"
#include "Renderer.h"
#include <ranges>


#include "SceneRenderer.h"
#include "Renderer2D.h"

namespace bey {


	struct RendererData
	{
		Ref<RenderPass> ActiveRenderPass;
		RenderCommandQueue CommandQueue;
		Ref<ShaderLibrary> ShaderLibrary;
		Ref<VertexArray> FullscreenQuadVertexArray;
	} static s_RendererData;

	void Renderer::init()
	{
		BEY_PROFILE_FUNCTION();
		s_RendererData.ShaderLibrary = Ref<ShaderLibrary>::create();
		submit([]() { RendererAPI::init(); });

		// getShaderLibrary()->load("assets/shaders/BeyondPBR_Static");
		getShaderLibrary()->load("assets/shaders/LightAccumulation");
		getShaderLibrary()->load("assets/shaders/BeyondPBR_Anim");

		SceneRenderer::init();

		// Create fullscreen quad
		const float x = -1;
		const float y = -1;
		const float width = 1, height = 1;
		struct QuadVertex
		{
			glm::vec3 Position;
			glm::vec2 TexCoord;
		};

		auto* data = new QuadVertex[4];

		data[0].Position = glm::vec3(x, width, 0.1f);
		data[0].TexCoord = glm::vec2(0, 1);

		data[1].Position = glm::vec3(x, y, 0.1f);
		data[1].TexCoord = glm::vec2(0, 0);

		data[2].Position = glm::vec3(width, height, 0.1f);
		data[2].TexCoord = glm::vec2(1, 1);

		data[3].Position = glm::vec3(width, y, 0.1f);
		data[3].TexCoord = glm::vec2(1, 0);

		s_RendererData.FullscreenQuadVertexArray = VertexArray::create();
		auto quadVB = VertexBuffer::create(data, 4 * sizeof(QuadVertex));
		quadVB->setLayout({
			{ ShaderDataType::Float3, "a_Position" },
			{ ShaderDataType::Float2, "a_TexCoord" }
			});


		s_RendererData.FullscreenQuadVertexArray->addVertexBuffer(quadVB);

		Renderer2D::init();
	}

	void Renderer::terminate()
	{
		BEY_PROFILE_FUNCTION();
		SceneRenderer::terminate();
	}

	Ref<ShaderLibrary> Renderer::getShaderLibrary()
	{
		return s_RendererData.ShaderLibrary;
	}

	void Renderer::clear()
	{
		BEY_PROFILE_FUNCTION();
		submit([] {
			BEY_PROFILE_SCOPE("RendererAPI::clear(0.0f, 0.0f, 0.0f, 1.0f)");

			RendererAPI::clear(0.0f, 0.0f, 0.0f, 1.0f);
			});
	}

	void Renderer::clearDepth()
	{
		BEY_PROFILE_FUNCTION();
		submit([] {
			BEY_PROFILE_SCOPE("RendererAPI::clearDepth()");
			RendererAPI::clearDepth();
			});
	}

	void Renderer::clear(const float r, const float g, const float b, const float a)
	{
		BEY_PROFILE_FUNCTION();
		submit([r, g, b, a] {
			BEY_PROFILE_SCOPE("RendererAPI::clear(r, g, b, a);");

			RendererAPI::clear(r, g, b, a);
			});
	}

	void Renderer::clearMagenta()
	{
		BEY_PROFILE_FUNCTION();
		submit([]
			{
				BEY_PROFILE_SCOPE("RendererAPI::clear(1, 0, 1);");
				clear(1, 0, 1);
			});
	}

	void Renderer::setClearColor(const float r, const float g, const float b, const float a)
	{
		BEY_PROFILE_FUNCTION();
		submit([=]
			{
				BEY_PROFILE_SCOPE("RendererAPI::clear(r,g,b,a);");
				RendererAPI::setClearColor(r, g, b, a);
			});
	}

	void Renderer::drawArrays(const uint32_t first, const uint32_t count, const PrimitiveType type, const bool depthTest)
	{
		BEY_PROFILE_FUNCTION();
		Renderer::submit([=]() {
			BEY_PROFILE_SCOPE("RendererAPI::drawArrays(first, count, type, depthTest);");

			RendererAPI::drawArrays(first, count, type, depthTest);
			});
	}

	void Renderer::drawIndexed(const uint32_t count, const PrimitiveType type, const bool depthTest)
	{
		BEY_PROFILE_FUNCTION();
		Renderer::submit([=]() {
			BEY_PROFILE_SCOPE("endererAPI::drawIndexed(count, type, depthTest);");
			RendererAPI::drawIndexed(count, type, depthTest);
			});
	}

	void Renderer::setLineThickness(const float thickness)
	{
		BEY_PROFILE_FUNCTION();
		Renderer::submit([=]() {
			BEY_PROFILE_SCOPE("RendererAPI::setLineThickness(thickness)");
			RendererAPI::setLineThickness(thickness);
			});
	}

	void Renderer::waitAndRender()
	{
		s_RendererData.CommandQueue.execute();
	}

	void Renderer::beginRenderPass(Ref<RenderPass> renderPass, const bool clear)
	{
		BEY_PROFILE_FUNCTION();
		BEY_CORE_ASSERT(renderPass, "Render pass cannot be null!");

		// TODO: Convert all of this into a render command buffer
		s_RendererData.ActiveRenderPass = renderPass;

		renderPass->getSpecification().TargetFramebuffer->bind();

		if (clear)
		{
			const glm::vec4& clearColor = renderPass->getSpecification().TargetFramebuffer->getSpecification().ClearColor;
			submit([clearColor]() { RendererAPI::clear(clearColor.r, clearColor.g, clearColor.b, clearColor.a); });
		}
	}

	void Renderer::generateMipMaps()
	{
		s_RendererData.ActiveRenderPass->getSpecification().TargetFramebuffer->generateMipMaps();
	}

	void Renderer::endRenderPass()
	{
		BEY_PROFILE_FUNCTION();
		BEY_CORE_ASSERT(s_RendererData.ActiveRenderPass, "No active render pass! Have you called Renderer::EndRenderPass twice?");
		s_RendererData.ActiveRenderPass->getSpecification().TargetFramebuffer->unBind();
		s_RendererData.ActiveRenderPass = nullptr;
	}

	void Renderer::submitQuad(Ref<MaterialInstance> material, const uint32_t objectID, const glm::mat4& transform)
	{
		BEY_PROFILE_FUNCTION();
		bool depthTest = true;
		if (material)
		{
			material->bind();
			depthTest = material->getFlag(MaterialFlag::DepthTest);

			auto shader = material->getShader();
			shader->setMat4("u_Transform", transform);
			shader->setUInt("u_IconID", objectID);
		}

		s_RendererData.FullscreenQuadVertexArray->bind();
		drawArrays(0, 4, PrimitiveType::TriangleStrip, depthTest);
	}

	void Renderer::submitQuad(Ref<MaterialInstance> material, const glm::mat4& transform)
	{
		BEY_PROFILE_FUNCTION();
		bool depthTest = true;
		if (material)
		{
			material->bind();
			depthTest = material->getFlag(MaterialFlag::DepthTest);

			material->getShader()->setMat4("u_Transform", transform);
		}

		s_RendererData.FullscreenQuadVertexArray->bind();
		drawArrays(0, 4, PrimitiveType::TriangleStrip, depthTest);
	}

	void Renderer::submitFullscreenQuad(Ref<MaterialInstance> material)
	{
		BEY_PROFILE_FUNCTION();
		bool depthTest = true;
		if (material)
		{
			material->bind();
			depthTest = material->getFlag(MaterialFlag::DepthTest);
		}

		s_RendererData.FullscreenQuadVertexArray->bind();
		drawArrays(0, 4, PrimitiveType::TriangleStrip, depthTest);
	}

	void Renderer::hbaoFullScreenQuad()
	{
		BEY_PROFILE_FUNCTION();
		bool depthTest = true;

		s_RendererData.FullscreenQuadVertexArray->bind();
		drawArrays(0, 3 * 16, PrimitiveType::Triangles, depthTest);
	}

	void Renderer::submitMesh(Ref<Mesh> mesh, const glm::mat4& transform, const Ref<MaterialInstance>& overrideMaterial)
	{
		BEY_PROFILE_FUNCTION();
		// TODO: Sort this out
		mesh->mVertexArray->bind();
		overrideMaterial->getShader()->bind();
		for (Submesh& submesh : mesh->mSubMeshes)
		{
			BEY_PROFILE_SCOPE("submeshes drawing");
			auto shader = overrideMaterial->getShader();
			//overrideMaterial->bind();
			shader->setMat4("u_Transform", transform * submesh.Transform);

			submit([submesh]() {
				glDrawElementsBaseVertex(GL_TRIANGLES, submesh.IndexCount, GL_UNSIGNED_INT, reinterpret_cast<void*>(sizeof(uint32_t) * submesh.BaseIndex), submesh.BaseVertex);
				});
		}
	}

	void Renderer::submitMeshDepth(Ref<Mesh> mesh, const glm::mat4& transform, Ref<Shader> shader)
	{
		BEY_PROFILE_FUNCTION();
		//std::ranges::sort(mesh->mSubMeshes, [](auto& a, auto& b) { return a.Distance > b.Distance; });
		mesh->mVertexArray->bind();
		for (Submesh& submesh : mesh->mSubMeshes)
		{
			BEY_PROFILE_SCOPE("submeshes depth drawing");

			shader->setMat4("u_Transform", transform * submesh.Transform);
			submit([submesh] {
				glDrawElementsBaseVertex(GL_TRIANGLES, submesh.IndexCount, GL_UNSIGNED_INT, reinterpret_cast<void*>(sizeof(uint32_t) * submesh.BaseIndex), submesh.BaseVertex);
				});
		}
	}

	void Renderer::submitMeshDepthInstanced(Ref<Mesh> mesh, const glm::mat4& transform, Ref<Shader> shader, const int lightCount)
	{
		BEY_PROFILE_FUNCTION();
		// TODO: Sort this out
		mesh->mVertexArray->bind();
		for (Submesh& submesh : mesh->mSubMeshes)
		{
			BEY_PROFILE_SCOPE("submeshes depth drawing");

			shader->setMat4("u_Transform", transform * submesh.Transform);
			submit([submesh, lightCount] {
				glDrawElementsInstancedBaseVertex(GL_TRIANGLES, submesh.IndexCount, GL_UNSIGNED_INT, reinterpret_cast<void*>(sizeof(uint32_t) * submesh.BaseIndex), lightCount, submesh.BaseVertex);
				});
		}
	}
	void Renderer::SubmitMesh(Ref<Mesh> mesh, const glm::mat4& transform, Ref<MaterialInstance> overrideMaterial)
	{
		// auto material = overrideMaterial ? overrideMaterial : mesh->GetMaterialInstance();
		// auto shader = material->GetShader();
		// TODO: Sort this out
		mesh->mVertexArray->bind();

		auto& materials = mesh->getMaterials();
		for (Submesh& submesh : mesh->mSubMeshes)
		{
			// Material
			auto material = overrideMaterial ? overrideMaterial : materials[submesh.MaterialIndex];
			auto shader = material->getShader();
			material->bind();

			if (mesh->mIsAnimated)
			{
				for (size_t i = 0; i < mesh->mBoneTransforms.size(); i++)
				{
					std::string uniformName = std::string("u_BoneTransforms[") + std::to_string(i) + std::string("]");
					shader->setMat4(uniformName, mesh->mBoneTransforms[i]);
				}
			}
			shader->setMat4("u_Transform", transform * submesh.Transform);

			Renderer::submit([submesh, material]() {
				if (material->getFlag(MaterialFlag::DepthTest))
					glEnable(GL_DEPTH_TEST);
				else
					glDisable(GL_DEPTH_TEST);

					Renderer::submit([]() { glDisable(GL_CULL_FACE); });

				glDrawElementsBaseVertex(GL_TRIANGLES, submesh.IndexCount, GL_UNSIGNED_INT, (void*)(sizeof(uint32_t) * submesh.BaseIndex), submesh.BaseVertex);
				});
		}
	}



	void Renderer::submitGeometry(Ref<Mesh> mesh, const glm::mat4& transform)
	{
		BEY_PROFILE_FUNCTION();
		mesh->mVertexArray->bind();
		auto& materials = mesh->getMaterials();


		for (Submesh& submesh : mesh->mSubMeshes | std::views::filter([](auto& a) { return a.BoundingBox.intersectsCameraFrustum(); }))
		{
			BEY_PROFILE_SCOPE("submeshes geometry drawing");

			auto& material = materials[submesh.MaterialIndex];
			auto shader = material->getShader();
			material->bind();
			if (mesh->mIsAnimated)
				for (size_t i = 0; i < mesh->mBoneTransforms.size(); i++)
					mesh->mMeshShader->setMat4(fmt::format("u_BoneTransforms[{}]", i), mesh->mBoneTransforms[i]);

			shader->setMat4("u_Transform", transform * submesh.Transform);
			submit([submesh, material] {
				BEY_PROFILE_SCOPE("submesh geometry drawing");

				material->getFlag(MaterialFlag::DepthTest) ? glEnable(GL_DEPTH_TEST) : glDisable(GL_DEPTH_TEST);

				glDrawElementsBaseVertex(GL_PATCHES, submesh.IndexCount, GL_UNSIGNED_INT, 
					reinterpret_cast<void*>(sizeof(uint32_t) * submesh.BaseIndex), submesh.BaseVertex);
				});
		}
	}

	void Renderer::drawAABB(Ref<Mesh> mesh, const glm::mat4& transform, const glm::vec4& color)
	{
		BEY_PROFILE_FUNCTION();
		for (Submesh& submesh : mesh->mSubMeshes)
			drawAABB(submesh.BoundingBox, transform * submesh.Transform, color);
	}

	void Renderer::drawAABB(const AABB& aabb, const glm::mat4& transform, const glm::vec4& color)
	{
		BEY_PROFILE_FUNCTION();
		glm::vec4 corners[8] =
		{
			transform * glm::vec4 { aabb.Min.x, aabb.Min.y, aabb.Max.z, 1.0f },
			transform * glm::vec4 { aabb.Min.x, aabb.Max.y, aabb.Max.z, 1.0f },
			transform * glm::vec4 { aabb.Max.x, aabb.Max.y, aabb.Max.z, 1.0f },
			transform * glm::vec4 { aabb.Max.x, aabb.Min.y, aabb.Max.z, 1.0f },

			transform * glm::vec4 { aabb.Min.x, aabb.Min.y, aabb.Min.z, 1.0f },
			transform * glm::vec4 { aabb.Min.x, aabb.Max.y, aabb.Min.z, 1.0f },
			transform * glm::vec4 { aabb.Max.x, aabb.Max.y, aabb.Min.z, 1.0f },
			transform * glm::vec4 { aabb.Max.x, aabb.Min.y, aabb.Min.z, 1.0f }
		};

		for (uint32_t i = 0; i < 4; i++)
			Renderer2D::drawLine(corners[i], corners[(i + 1) % 4], color);

		for (uint32_t i = 0; i < 4; i++)
			Renderer2D::drawLine(corners[i + 4], corners[(i + 1) % 4 + 4], color);

		for (uint32_t i = 0; i < 4; i++)
			Renderer2D::drawLine(corners[i], corners[i + 4], color);
	}


	void Renderer::drawCorners(const glm::vec3* corners, const size_t index)
	{
		BEY_PROFILE_FUNCTION();
		glm::vec4 color;

		switch (index)
		{
		case 0: color = { 1.f, 0.f, 0.f, 1.f }; break;
		case 1: color = { 0.f, 1.f, 0.f, 1.f }; break;
		case 2: color = { 0.f, 0.f, 1.f, 1.f }; break;
		case 3: color = { 1.f, 1.f, 0.f, 1.f }; break;
		default: BEY_CORE_ERROR("Wrong index .. out of bounds");
		}
		//Near Plane
		Renderer2D::drawLine(corners[0], corners[1], color);
		Renderer2D::drawLine(corners[1], corners[2], color);
		Renderer2D::drawLine(corners[2], corners[3], color);
		Renderer2D::drawLine(corners[3], corners[0], color);

		//Far Plane
		Renderer2D::drawLine(corners[7], corners[4], color);
		Renderer2D::drawLine(corners[4], corners[5], color);
		Renderer2D::drawLine(corners[5], corners[6], color);
		Renderer2D::drawLine(corners[6], corners[7], color);

		//Connecting Planes
		Renderer2D::drawLine(corners[0], corners[4], color);
		Renderer2D::drawLine(corners[1], corners[5], color);
		Renderer2D::drawLine(corners[2], corners[6], color);
		Renderer2D::drawLine(corners[3], corners[7], color);
	}

	RenderCommandQueue& Renderer::getRenderCommandQueue()
	{
		return s_RendererData.CommandQueue;
	}


}