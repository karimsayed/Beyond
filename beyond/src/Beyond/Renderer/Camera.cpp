#include "pchs.h"
#include "Camera.h"

namespace bey {

	Camera::Camera(const float fov, const float width, const float height, const float nearClip, const float farClip)
		: mProjectionMatrix(glm::perspectiveFov(glm::radians(fov), width, height, nearClip, farClip)), mFOV(fov), mNear(nearClip), mFar(farClip)
	{
	}

}