#pragma once

#include "RenderCommandQueue.h"

#include "Mesh.h"
#include "Shadows.hpp"
#include "Beyond/Platform/OpenGL/RenderPass.h"

namespace bey {

	class ShaderLibrary;



	class Renderer
	{
	public:
		typedef void(*RenderCommandFn)(void*);


		// Commands
		static void clear();
		static void clearDepth();
		static void clear(float r, float g, float b, float a = 1.0f);
		static void setClearColor(float r, float g, float b, float a);

		static void drawArrays(const uint32_t first, const uint32_t count, const PrimitiveType type, const bool depthTest = true);
		static void drawIndexed(uint32_t count, PrimitiveType type, bool depthTest = true);

		// For OpenGL
		static void setLineThickness(float thickness);

		static void clearMagenta();

		static void init();
		static void terminate();

		static Ref<ShaderLibrary> getShaderLibrary();

		template<typename FuncT>
		static void submit(FuncT&& func) requires std::invocable<FuncT>
		{
			
			auto renderCmd = [](void* ptr) {
				auto* pFunc = static_cast<FuncT*>(ptr);

				(*pFunc)();

				// NOTE: Instead of destroying we could try and enforce all items to be trivially destructible
				// however some items like uniforms which contain std::strings still exist for now
				// static_assert(std::is_trivially_destructible_v<FuncT>, "FuncT must be trivially destructible");
				pFunc->~FuncT();
			};
			auto storageBuffer = getRenderCommandQueue().allocate(renderCmd, sizeof func);
			new (storageBuffer) FuncT(std::forward<FuncT>(func));
		}

		static void waitAndRender();

		// ~Actual~ Renderer here... TODO: remove confusion later
		static void beginRenderPass(Ref<RenderPass> renderPass, bool clear = true);
		static void generateMipMaps();
		static void endRenderPass();

		static void submitQuad(Ref<MaterialInstance> material, uint32_t objectID, const glm::mat4& transform = glm::mat4(1.0f));
		static void submitQuad(Ref<MaterialInstance> material, const glm::mat4& transform = glm::mat4(1.0f));
		static void submitFullscreenQuad(Ref<MaterialInstance> material);
		static void hbaoFullScreenQuad();
		static void submitMesh(Ref<Mesh> mesh, const glm::mat4& transform, const Ref<MaterialInstance>& overrideMaterial = nullptr);
		static void submitMeshDepth(Ref<Mesh> mesh, const glm::mat4& transform, Ref<Shader> shader);
		static void submitMeshDepthInstanced(Ref<Mesh> mesh, const glm::mat4& transform, Ref<Shader> shader, const int lightCount);

		static void submitGeometry(Ref<Mesh> mesh, const glm::mat4& transform);
		static void SubmitMesh(Ref<Mesh> mesh, const glm::mat4& transform, Ref<MaterialInstance> overrideMaterial);


		static void drawAABB(const AABB& aabb, const glm::mat4& transform, const glm::vec4& color = glm::vec4(1.0f));
		static void drawAABB(Ref<Mesh> mesh, const glm::mat4& transform, const glm::vec4& color = glm::vec4(1.0f));
		static void drawCorners(const glm::vec3* corners, size_t index);

	private:
		static RenderCommandQueue& getRenderCommandQueue();

	};

}
