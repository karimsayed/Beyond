#include "pchs.h"
#include "RenderCommandQueue.h"

#include "Renderer.h"

#define BEY_RENDER_TRACE(...) BEY_CORE_TRACE(__VA_ARGS__)

#define RENDER_COMMAND_BUFFER_SIZE (10 * 1024 * 1024)

namespace bey {

	RenderCommandQueue::RenderCommandQueue()
	{
		BEY_PROFILE_FUNCTION();
		mCommandBuffer = new uint8_t[RENDER_COMMAND_BUFFER_SIZE]; // 10 MiB buffer
		mCommandBufferPtr = mCommandBuffer;
		memset(mCommandBuffer, 0, RENDER_COMMAND_BUFFER_SIZE);
	}

	RenderCommandQueue::~RenderCommandQueue()
	{
		BEY_PROFILE_FUNCTION();
		delete[] mCommandBuffer;
	}

	void* RenderCommandQueue::allocate(const RenderCommandFn func, const uint32_t size)
	{
		BEY_PROFILE_FUNCTION();

		// TODO: alignment
		if (mCommandBufferPtr - mCommandBuffer > RENDER_COMMAND_BUFFER_SIZE)
			BEY_FATAL("Buffer overflow, size = {}", mCommandBufferPtr - mCommandBuffer);
		
		*reinterpret_cast<RenderCommandFn*>(mCommandBufferPtr) = func;
		mCommandBufferPtr += sizeof(RenderCommandFn);

		*reinterpret_cast<uint32_t*>(mCommandBufferPtr) = size;
		mCommandBufferPtr += sizeof(uint32_t);

		void* memory = mCommandBufferPtr;
		mCommandBufferPtr += size;

		mCommandCount++;
		return memory;
	}

	void RenderCommandQueue::execute()
	{
		BEY_PROFILE_FUNCTION();

		//BEY_RENDER_TRACE("RenderCommandQueue::execute -- {0} commands, {1} bytes", mCommandCount, (mCommandBufferPtr - mCommandBuffer));
		byte* buffer = mCommandBuffer;

		for (uint32_t i = 0; i < mCommandCount; i++)
		{
			const RenderCommandFn function = *reinterpret_cast<RenderCommandFn*>(buffer);
			buffer += sizeof(RenderCommandFn);

			const uint32_t size = *reinterpret_cast<uint32_t*>(buffer);
			buffer += sizeof(uint32_t);
			{
				BEY_PROFILE_SCOPE("function(buffer);");
				function(buffer);
			}
			buffer += size;
		}

		mCommandBufferPtr = mCommandBuffer;
		mCommandCount = 0;
	}


}
