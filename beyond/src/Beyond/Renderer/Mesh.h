#pragma once

#include <assimp/Exporter.hpp>
#include <assimp/postprocess.h>
#include <assimp/scene.h>



#include "Beyond/Core/TimeStep.h"

#include "Beyond/Renderer/Material.h"

#include "Beyond/Core/Math/AABB.h"
#include "Beyond/Platform/OpenGL/Shader.h"
#include "Beyond/Platform/OpenGL/VertexArray.h"

struct aiNode;
struct aiAnimation;
struct aiNodeAnim;
struct aiScene;

// ReSharper disable once CppInconsistentNaming
namespace Assimp {
	class Importer;
}

namespace bey {

	struct Vertex
	{
		glm::vec3 Position;
		glm::vec3 Normal;
		glm::vec3 Tangent;
		glm::vec3 Binormal;
		glm::vec2 TexCoord;
	};

	struct AnimatedVertex
	{
		glm::vec3 Position;
		glm::vec3 Normal;
		glm::vec3 Tangent;
		glm::vec3 Binormal;
		glm::vec2 TexCoord;

		uint32_t IDs[4] = { 0, 0, 0, 0 };
		double Weights[4]{ 0.0, 0.0, 0.0, 0.0 };

		void addBoneData(const uint32_t boneID, const float weight) noexcept
		{
			for (size_t i = 0; i < 4; i++)
			{
				if (fabs(Weights[i] - 0.0) < DBL_EPSILON)
				{
					IDs[i] = boneID;
					Weights[i] = weight;
					return;
				}
			}

			// TODO: Keep top weights
			BEY_CORE_WARN("Vertex has more than four bones/weights affecting it, extra data will be discarded (BoneID={0}, Weight={1})", boneID, weight);
		}
	};

	static const int NUM_ATTRIBUTES = 5;

	struct Index
	{
		uint32_t V1, V2, V3;
	};

	static_assert(sizeof(Index) == 3 * sizeof(uint32_t));

	struct BoneInfo
	{
		glm::mat4 BoneOffset;
		glm::mat4 FinalTransformation;
	};

	struct VertexBoneData
	{
		uint32_t IDs[4];
		float Weights[4];

		VertexBoneData()
		{
			memset(IDs, 0, sizeof(IDs));
			memset(Weights, 0, sizeof(Weights));
		}

		void addBoneData(const uint32_t boneID, const float weight) noexcept
		{
			for (size_t i = 0; i < 4; i++)
			{
				if (fabs(Weights[i] - 0.0) < DBL_EPSILON)
				{
					IDs[i] = boneID;
					Weights[i] = weight;
					return;
				}
			}
			
			// should never get here - more bones than we have space for
			BEY_CORE_ASSERT(false, "Too many bones!");
		}
	};

	struct Triangle
	{
		Vertex V0, V1, V2;

		Triangle(const Vertex& v0, const Vertex& v1, const Vertex& v2) noexcept
			: V0(v0), V1(v1), V2(v2) {}
	};

	class Submesh
	{
	public:
		uint32_t BaseVertex{};
		uint32_t BaseIndex{};
		uint32_t MaterialIndex{};
		uint32_t IndexCount{};

		glm::mat4 Transform{};
		AABB BoundingBox{};
		float Distance{};

		std::string NodeName, MeshName;
	};

	static constexpr uint32_t S_MESH_IMPORT_FLAGS =
		aiProcess_CalcTangentSpace |        // create binormals/tangents just in case
		aiProcess_Triangulate |             // Make sure we're triangles
		aiProcess_SortByPType |             // Split meshes by primitive type
		aiProcess_GenNormals |              // Make sure we have legit normals
		aiProcess_GenUVCoords |             // Convert UVs if required 
		aiProcess_OptimizeMeshes |          // Batch draws where possible
		aiProcess_JoinIdenticalVertices |
		aiProcess_ValidateDataStructure;    // Validation
	
	class Mesh final : public RefCounted
	{
	public:
		explicit Mesh(const std::string& filename);
		void setTextures();
		~Mesh();
		void onUpdate(TimeStep ts, const glm::vec4& cameraPos);

		void dumpVertexBuffer();

		aiMatrix4x4& getAiRootMatrix() const { return mScene->mRootNode->mTransformation; }
		aiMatrix4x4& getAiRootMatrix() { return mScene->mRootNode->mTransformation; }
		aiMatrix4x4& getSubmeshMatrix(const uint32_t index) const { return mScene->mRootNode->mChildren[index]->mTransformation; }
		aiMatrix4x4& getSubmeshMatrix(const uint32_t index) { return mScene->mRootNode->mChildren[index]->mTransformation; }

		std::vector<Submesh>& getSubMeshes() { return mSubMeshes; }
		const std::vector<Submesh>& getSubMeshes() const { return mSubMeshes; }

		Ref<Shader> getMeshShader() const { return mMeshShader; }
		Ref<Material> getMaterial() const { return mBaseMaterial; }
		Ref<Material> getMaterial() { return mBaseMaterial; }
		void setMaterial(const Ref<Material>& material)
		{
			BEY_ASSERT(material, "setting material that is empty");
			mBaseMaterial = material;
		}
		const std::vector<Ref<MaterialInstance>>& getMaterials() const { return mMaterials; }
		std::vector<Ref<MaterialInstance>>& getMaterials() { return mMaterials; }
		const std::vector<Ref<Texture2D>>& getTextures() const { return mTextures; }
		std::vector<Ref<Texture2D>>& getTextures() { return mTextures; }
		
		Ref<Texture2D> getTexture(const aiTextureType aiTextureType) 
		{
			for (auto& X : mTextures)
				if (X && X->getType() == aiTextureType)
					return X;
			return {};
			
		}

		void setTexture(const Ref<Texture2D>& ref)
		{
			for (auto& X : mTextures)
			{
				if (X && X->getType() == ref->getType())
					X = ref;
				return;
			}
			mTextures.emplace_back(ref);
		}
		const std::string& getFilePath() const { return mFilePath; }

		std::vector<Triangle> getTriangleCache(const uint32_t index) const { return mTriangleCache.at(index); }
	private:
		void boneTransform(float time);
		void readNodeHierarchy(float animationTime, const aiNode* pNode, const glm::mat4& parentTransform);
		void traverseNodes(aiNode* node, const glm::mat4& parentTransform = glm::mat4(1.0f), uint32_t level = 0);
		void updateNodes(aiNode* node, const aiMatrix4x4& parentTransform, const uint32_t level);

		static const aiNodeAnim* findNodeAnim(const aiAnimation* animation, const std::string& nodeName);
		static uint32_t findPosition(float animationTime, const aiNodeAnim* pNodeAnim);
		static uint32_t findRotation(float animationTime, const aiNodeAnim* pNodeAnim);
		static uint32_t findScaling(float animationTime, const aiNodeAnim* pNodeAnim);
		static glm::vec3 interpolateTranslation(float animationTime, const aiNodeAnim* nodeAnim);
		static glm::quat interpolateRotation(float animationTime, const aiNodeAnim* nodeAnim);
		static glm::vec3 interpolateScale(float animationTime, const aiNodeAnim* nodeAnim);
	private:
		std::vector<Submesh> mSubMeshes;
		
		std::unique_ptr<Assimp::Importer> mImporter;

		glm::mat4 mInverseTransform{};

		uint32_t mBoneCount = 0;
		std::vector<BoneInfo> mBoneInfo;

		Ref<VertexArray> mVertexArray;

		std::vector<Vertex> mStaticVertices;
		std::vector<AnimatedVertex> mAnimatedVertices;
		std::vector<Index> mIndices;
		std::unordered_map<std::string, uint32_t> mBoneMapping;
		std::vector<glm::mat4> mBoneTransforms;
		aiScene* mScene;

		// Materials
		Ref<Shader> mMeshShader;
		Ref<Material> mBaseMaterial;
		std::vector<Ref<Texture2D>> mTextures;
		std::vector<Ref<MaterialInstance>> mMaterials;

		std::unordered_map<uint32_t, std::vector<Triangle>> mTriangleCache;

		// Animation
		bool mIsAnimated = false;
		float mAnimationTime = 0.0f;
		float mWorldTime = 0.0f;
		float mTimeMultiplier = 1.0f;
		bool mAnimationPlaying = true;

		std::string mFilePath;

		friend class Renderer;
		friend class SceneHierarchyPanel;
		friend class MeshExporter;
	};

	
}
