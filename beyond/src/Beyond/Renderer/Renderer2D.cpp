#include "pchs.h"
#include "Beyond/Renderer/Renderer2D.h"

#include "Beyond/Platform/OpenGL/Shader.h"
#include "Beyond/Renderer/Renderer.h"


namespace bey {

	struct QuadVertex
	{
		glm::vec3 Position;
		glm::vec4 Color;
		glm::vec2 TexCoord;
		float TexIndex;
		float TilingFactor;
	};

	struct LineVertex
	{
		glm::vec3 Position;
		glm::vec4 Color;
	};

	struct Renderer2DData
	{
		static constexpr uint32_t MAX_QUADS = 20000;
		static constexpr uint32_t MAX_VERTICES = MAX_QUADS * 4;
		static constexpr uint32_t MAX_INDICES = MAX_QUADS * 6;
		static constexpr uint32_t MAX_TEXTURE_SLOTS = 32; // TODO: RenderCaps

		static constexpr uint32_t MAX_LINES = 10000;
		static constexpr uint32_t MAX_LINE_VERTICES = MAX_LINES * 2;
		static constexpr uint32_t MAX_LINE_INDICES = MAX_LINES * 6;

		Ref<VertexArray> QuadVertexArray;
		Ref<VertexBuffer> QuadVertexBuffer;
		Ref<Shader> TextureShader;
		Ref<Texture2D> WhiteTexture;

		uint32_t QuadIndexCount = 0;
		QuadVertex* QuadVertexBufferBase = nullptr;
		QuadVertex* QuadVertexBufferPtr = nullptr;

		std::array<Ref<Texture2D>, MAX_TEXTURE_SLOTS> TextureSlots;
		uint32_t TextureSlotIndex = 1; // 0 = white texture

		glm::vec4 QuadVertexPositions[4]{};

		// Lines
		Ref<VertexArray> LineVertexArray;
		Ref<VertexBuffer> LineVertexBuffer;
		Ref<Shader> LineShader;

		uint32_t LineIndexCount = 0;
		LineVertex* LineVertexBufferBase = nullptr;
		LineVertex* LineVertexBufferPtr = nullptr;

		glm::mat4 CameraViewProj{};
		bool DepthTest = true;

		Renderer2D::Statistics Stats;
	}static s_Renderer2DData;


	void Renderer2D::init()
	{
		BEY_PROFILE_FUNCTION();
		s_Renderer2DData.QuadVertexArray = VertexArray::create();

		s_Renderer2DData.QuadVertexBuffer = VertexBuffer::create(Renderer2DData::MAX_VERTICES * sizeof(QuadVertex));
		s_Renderer2DData.QuadVertexBuffer->setLayout({
			{ ShaderDataType::Float3, "a_Position" },
			{ ShaderDataType::Float4, "a_Color" },
			{ ShaderDataType::Float2, "a_TexCoord" },
			{ ShaderDataType::Float, "a_TexIndex" },
			{ ShaderDataType::Float, "a_TilingFactor" }
			});
		s_Renderer2DData.QuadVertexArray->addVertexBuffer(s_Renderer2DData.QuadVertexBuffer);

		s_Renderer2DData.QuadVertexBufferBase = new QuadVertex[Renderer2DData::MAX_VERTICES];

		auto* quadIndices = new uint32_t[Renderer2DData::MAX_INDICES];

		uint32_t offset = 0;
		for (uint32_t i = 0; i < Renderer2DData::MAX_INDICES; i += 6)
		{
			quadIndices[i + 0] = offset + 0;
			quadIndices[i + 1] = offset + 1;
			quadIndices[i + 2] = offset + 2;

			quadIndices[i + 3] = offset + 2;
			quadIndices[i + 4] = offset + 3;
			quadIndices[i + 5] = offset + 0;

			offset += 4;
		}

		const Ref<IndexBuffer> quadIB = IndexBuffer::create(quadIndices, s_Renderer2DData.MAX_INDICES);
		s_Renderer2DData.QuadVertexArray->setIndexBuffer(quadIB);
		delete[] quadIndices;

		s_Renderer2DData.WhiteTexture = Texture2D::create(TextureFormat::RGBA, TextureInternalFormat::RGBA8, 1, 1, DataType::UNSIGNED_BYTE);
		uint32_t whiteTextureData = 0xffffffff;
		s_Renderer2DData.WhiteTexture->lock();
		s_Renderer2DData.WhiteTexture->getWritableBuffer().write(&whiteTextureData, sizeof(uint32_t));
		s_Renderer2DData.WhiteTexture->unlock();

		s_Renderer2DData.TextureShader = Shader::create("assets/shaders/Renderer2D");

		// Set all texture slots to 0
		s_Renderer2DData.TextureSlots[0] = s_Renderer2DData.WhiteTexture;

		s_Renderer2DData.QuadVertexPositions[0] = { -0.5f, -0.5f, 0.0f, 1.0f };
		s_Renderer2DData.QuadVertexPositions[1] = { 0.5f, -0.5f, 0.0f, 1.0f };
		s_Renderer2DData.QuadVertexPositions[2] = { 0.5f,  0.5f, 0.0f, 1.0f };
		s_Renderer2DData.QuadVertexPositions[3] = { -0.5f,  0.5f, 0.0f, 1.0f };

		// Lines
		s_Renderer2DData.LineShader = Shader::create("assets/shaders/Renderer2D_Line");
		s_Renderer2DData.LineVertexArray = VertexArray::create();

		s_Renderer2DData.LineVertexBuffer = VertexBuffer::create(Renderer2DData::MAX_LINE_VERTICES * sizeof(LineVertex));
		s_Renderer2DData.LineVertexBuffer->setLayout({
			{ ShaderDataType::Float3, "a_Position" },
			{ ShaderDataType::Float4, "a_Color" }
			});
		s_Renderer2DData.LineVertexArray->addVertexBuffer(s_Renderer2DData.LineVertexBuffer);

		s_Renderer2DData.LineVertexBufferBase = new LineVertex[bey::Renderer2DData::MAX_LINE_VERTICES];

		uint32_t* lineIndices = new uint32_t[bey::Renderer2DData::MAX_LINE_INDICES];
		for (uint32_t i = 0; i < bey::Renderer2DData::MAX_LINE_INDICES; i++)
			lineIndices[i] = i;

		const Ref<IndexBuffer> lineIB = IndexBuffer::create(lineIndices, s_Renderer2DData.MAX_LINE_INDICES);
		s_Renderer2DData.LineVertexArray->setIndexBuffer(lineIB);
		delete[] lineIndices;
	}

	void Renderer2D::shutdown()
	{
	}

	void Renderer2D::beginScene(const glm::mat4& viewProj, const bool depthTest)
	{
		BEY_PROFILE_FUNCTION();

		s_Renderer2DData.CameraViewProj = viewProj;
		s_Renderer2DData.DepthTest = depthTest;

		s_Renderer2DData.TextureShader->bind();
		s_Renderer2DData.TextureShader->setMat4("u_ViewProjection", viewProj);

		s_Renderer2DData.QuadIndexCount = 0;
		s_Renderer2DData.QuadVertexBufferPtr = s_Renderer2DData.QuadVertexBufferBase;

		s_Renderer2DData.LineIndexCount = 0;
		s_Renderer2DData.LineVertexBufferPtr = s_Renderer2DData.LineVertexBufferBase;

		s_Renderer2DData.TextureSlotIndex = 1;
	}

	void Renderer2D::endScene()
	{
		BEY_PROFILE_FUNCTION();

		auto dataSize = uint32_t(reinterpret_cast<uint8_t*>(s_Renderer2DData.QuadVertexBufferPtr) - reinterpret_cast<uint8_t*>(s_Renderer2DData.QuadVertexBufferBase));
		if (dataSize)
		{
			s_Renderer2DData.QuadVertexBuffer->setData(s_Renderer2DData.QuadVertexBufferBase, dataSize);

			s_Renderer2DData.TextureShader->bind();
			s_Renderer2DData.TextureShader->setMat4("u_ViewProjection", s_Renderer2DData.CameraViewProj);

			for (uint32_t i = 0; i < s_Renderer2DData.TextureSlotIndex; i++)
				s_Renderer2DData.TextureSlots[i]->bind(i);

			s_Renderer2DData.QuadVertexArray->bind();
			Renderer::drawIndexed(s_Renderer2DData.QuadIndexCount, PrimitiveType::Triangles, s_Renderer2DData.DepthTest);
			s_Renderer2DData.Stats.DrawCalls++;
		}

		dataSize = uint32_t(reinterpret_cast<uint8_t*>(s_Renderer2DData.LineVertexBufferPtr) - reinterpret_cast<uint8_t*>(s_Renderer2DData.LineVertexBufferBase));
		if (dataSize)
		{
			s_Renderer2DData.LineVertexBuffer->setData(s_Renderer2DData.LineVertexBufferBase, dataSize);

			s_Renderer2DData.LineShader->bind();
			s_Renderer2DData.LineShader->setMat4("u_ViewProjection", s_Renderer2DData.CameraViewProj);

			s_Renderer2DData.LineVertexArray->bind();
			Renderer::setLineThickness(2.0f);
			Renderer::drawIndexed(s_Renderer2DData.LineIndexCount, PrimitiveType::Lines, s_Renderer2DData.DepthTest);
			s_Renderer2DData.Stats.DrawCalls++;
		}

		flush();
	}

	void Renderer2D::flush()
	{

	}

	void Renderer2D::flushAndReset()
	{
		BEY_PROFILE_FUNCTION();

		endScene();

		s_Renderer2DData.QuadIndexCount = 0;
		s_Renderer2DData.QuadVertexBufferPtr = s_Renderer2DData.QuadVertexBufferBase;

		s_Renderer2DData.TextureSlotIndex = 1;
	}

	void Renderer2D::flushAndResetLines()
	{

	}

	void Renderer2D::drawQuad(const glm::mat4& transform, const glm::vec4& color)
	{
		BEY_PROFILE_FUNCTION();

		constexpr size_t quadVertexCount = 4;
		const float textureIndex = 0.0f; // White Texture
		constexpr glm::vec2 textureCoords[] = { { 0.0f, 0.0f }, { 1.0f, 0.0f }, { 1.0f, 1.0f }, { 0.0f, 1.0f } };
		const float tilingFactor = 1.0f;

		if (s_Renderer2DData.QuadIndexCount >= Renderer2DData::MAX_INDICES)
			flushAndReset();

		for (size_t i = 0; i < quadVertexCount; i++)
		{
			s_Renderer2DData.QuadVertexBufferPtr->Position = transform * s_Renderer2DData.QuadVertexPositions[i];
			s_Renderer2DData.QuadVertexBufferPtr->Color = color;
			s_Renderer2DData.QuadVertexBufferPtr->TexCoord = textureCoords[i];
			s_Renderer2DData.QuadVertexBufferPtr->TexIndex = textureIndex;
			s_Renderer2DData.QuadVertexBufferPtr->TilingFactor = tilingFactor;
			s_Renderer2DData.QuadVertexBufferPtr++;
		}

		s_Renderer2DData.QuadIndexCount += 6;

		s_Renderer2DData.Stats.QuadCount++;
	}

	void Renderer2D::drawQuad(const glm::mat4& transform, const Ref<Texture2D>& texture, const float tilingFactor, const glm::vec4&)
	{
		BEY_PROFILE_FUNCTION();

		constexpr size_t quadVertexCount = 4;
		constexpr glm::vec4 color = { 1.0f, 1.0f, 1.0f, 1.0f };
		constexpr glm::vec2 textureCoords[] = { { 0.0f, 0.0f }, { 1.0f, 0.0f }, { 1.0f, 1.0f }, { 0.0f, 1.0f } };

		if (s_Renderer2DData.QuadIndexCount >= Renderer2DData::MAX_INDICES)
			flushAndReset();

		float textureIndex = 0.0f;
		for (uint32_t i = 1; i < s_Renderer2DData.TextureSlotIndex; i++)
		{
			if (*s_Renderer2DData.TextureSlots[i].raw() == *texture.raw())
			{
				textureIndex = float(i);
				break;
			}
		}

		if (fabs(textureIndex) < FLT_EPSILON)
		{
			if (s_Renderer2DData.TextureSlotIndex >= Renderer2DData::MAX_TEXTURE_SLOTS)
				flushAndReset();

			textureIndex = float(s_Renderer2DData.TextureSlotIndex);
			s_Renderer2DData.TextureSlots[size_t(textureIndex)] = texture;
			s_Renderer2DData.TextureSlotIndex++;
		}

		for (size_t i = 0; i < quadVertexCount; i++)
		{
			s_Renderer2DData.QuadVertexBufferPtr->Position = transform * s_Renderer2DData.QuadVertexPositions[i];
			s_Renderer2DData.QuadVertexBufferPtr->Color = color;
			s_Renderer2DData.QuadVertexBufferPtr->TexCoord = textureCoords[i];
			s_Renderer2DData.QuadVertexBufferPtr->TexIndex = textureIndex;
			s_Renderer2DData.QuadVertexBufferPtr->TilingFactor = tilingFactor;
			s_Renderer2DData.QuadVertexBufferPtr++;
		}

		s_Renderer2DData.QuadIndexCount += 6;
		++s_Renderer2DData.Stats.QuadCount;
	}

	void Renderer2D::drawQuad(const glm::vec2& position, const glm::vec2& size, const glm::vec4& color)
	{
		BEY_PROFILE_FUNCTION();

		drawQuad({ position.x, position.y, 0.0f }, size, color);
	}

	void Renderer2D::drawQuad(const glm::vec3& position, const glm::vec2& size, const glm::vec4& color)
	{
		BEY_PROFILE_FUNCTION();

		if (s_Renderer2DData.QuadIndexCount >= Renderer2DData::MAX_INDICES)
			flushAndReset();

		const float textureIndex = 0.0f; // White Texture
		const float tilingFactor = 1.0f;

		const glm::mat4 transform = glm::translate(glm::mat4(1.0f), position)
			* glm::scale(glm::mat4(1.0f), { size.x, size.y, 1.0f });

		s_Renderer2DData.QuadVertexBufferPtr->Position = transform * s_Renderer2DData.QuadVertexPositions[0];
		s_Renderer2DData.QuadVertexBufferPtr->Color = color;
		s_Renderer2DData.QuadVertexBufferPtr->TexCoord = { 0.0f, 0.0f };
		s_Renderer2DData.QuadVertexBufferPtr->TexIndex = textureIndex;
		s_Renderer2DData.QuadVertexBufferPtr->TilingFactor = tilingFactor;
		s_Renderer2DData.QuadVertexBufferPtr++;

		s_Renderer2DData.QuadVertexBufferPtr->Position = transform * s_Renderer2DData.QuadVertexPositions[1];
		s_Renderer2DData.QuadVertexBufferPtr->Color = color;
		s_Renderer2DData.QuadVertexBufferPtr->TexCoord = { 1.0f, 0.0f };
		s_Renderer2DData.QuadVertexBufferPtr->TexIndex = textureIndex;
		s_Renderer2DData.QuadVertexBufferPtr->TilingFactor = tilingFactor;
		s_Renderer2DData.QuadVertexBufferPtr++;

		s_Renderer2DData.QuadVertexBufferPtr->Position = transform * s_Renderer2DData.QuadVertexPositions[2];
		s_Renderer2DData.QuadVertexBufferPtr->Color = color;
		s_Renderer2DData.QuadVertexBufferPtr->TexCoord = { 1.0f, 1.0f };
		s_Renderer2DData.QuadVertexBufferPtr->TexIndex = textureIndex;
		s_Renderer2DData.QuadVertexBufferPtr->TilingFactor = tilingFactor;
		s_Renderer2DData.QuadVertexBufferPtr++;

		s_Renderer2DData.QuadVertexBufferPtr->Position = transform * s_Renderer2DData.QuadVertexPositions[3];
		s_Renderer2DData.QuadVertexBufferPtr->Color = color;
		s_Renderer2DData.QuadVertexBufferPtr->TexCoord = { 0.0f, 1.0f };
		s_Renderer2DData.QuadVertexBufferPtr->TexIndex = textureIndex;
		s_Renderer2DData.QuadVertexBufferPtr->TilingFactor = tilingFactor;
		s_Renderer2DData.QuadVertexBufferPtr++;

		s_Renderer2DData.QuadIndexCount += 6;

		s_Renderer2DData.Stats.QuadCount++;
	}

	void Renderer2D::drawQuad(const glm::vec2& position, const glm::vec2& size, const Ref<Texture2D>& texture, const float tilingFactor, const glm::vec4& tintColor)
	{
		BEY_PROFILE_FUNCTION();
		drawQuad({ position.x, position.y, 0.0f }, size, texture, tilingFactor, tintColor);
	}

	void Renderer2D::drawQuad(const glm::vec3& position, const glm::vec2& size, const Ref<Texture2D>& texture, const float tilingFactor, const glm::vec4& tintColor)
	{
		BEY_PROFILE_FUNCTION();
		if (s_Renderer2DData.QuadIndexCount >= Renderer2DData::MAX_INDICES)
			flushAndReset();

		constexpr glm::vec4 color = { 1.0f, 1.0f, 1.0f, 1.0f };

		float textureIndex = 0.0f;
		for (uint32_t i = 1; i < s_Renderer2DData.TextureSlotIndex; i++)
		{
			if (*s_Renderer2DData.TextureSlots[i].raw() == *texture.raw())
			{
				textureIndex = float(i);
				break;
			}
		}

		if (fabs(textureIndex) < FLT_EPSILON)
		{
			textureIndex = float(s_Renderer2DData.TextureSlotIndex);
			s_Renderer2DData.TextureSlots[s_Renderer2DData.TextureSlotIndex] = texture;
			s_Renderer2DData.TextureSlotIndex++;
		}

		const glm::mat4 transform = glm::translate(glm::mat4(1.0f), position)
			* glm::scale(glm::mat4(1.0f), { size.x, size.y, 1.0f });

		s_Renderer2DData.QuadVertexBufferPtr->Position = transform * s_Renderer2DData.QuadVertexPositions[0];
		s_Renderer2DData.QuadVertexBufferPtr->Color = color;
		s_Renderer2DData.QuadVertexBufferPtr->TexCoord = { 0.0f, 0.0f };
		s_Renderer2DData.QuadVertexBufferPtr->TexIndex = textureIndex;
		s_Renderer2DData.QuadVertexBufferPtr->TilingFactor = tilingFactor;
		s_Renderer2DData.QuadVertexBufferPtr++;

		s_Renderer2DData.QuadVertexBufferPtr->Position = transform * s_Renderer2DData.QuadVertexPositions[1];
		s_Renderer2DData.QuadVertexBufferPtr->Color = color;
		s_Renderer2DData.QuadVertexBufferPtr->TexCoord = { 1.0f, 0.0f };
		s_Renderer2DData.QuadVertexBufferPtr->TexIndex = textureIndex;
		s_Renderer2DData.QuadVertexBufferPtr->TilingFactor = tilingFactor;
		s_Renderer2DData.QuadVertexBufferPtr++;

		s_Renderer2DData.QuadVertexBufferPtr->Position = transform * s_Renderer2DData.QuadVertexPositions[2];
		s_Renderer2DData.QuadVertexBufferPtr->Color = color;
		s_Renderer2DData.QuadVertexBufferPtr->TexCoord = { 1.0f, 1.0f };
		s_Renderer2DData.QuadVertexBufferPtr->TexIndex = textureIndex;
		s_Renderer2DData.QuadVertexBufferPtr->TilingFactor = tilingFactor;
		s_Renderer2DData.QuadVertexBufferPtr++;

		s_Renderer2DData.QuadVertexBufferPtr->Position = transform * s_Renderer2DData.QuadVertexPositions[3];
		s_Renderer2DData.QuadVertexBufferPtr->Color = color;
		s_Renderer2DData.QuadVertexBufferPtr->TexCoord = { 0.0f, 1.0f };
		s_Renderer2DData.QuadVertexBufferPtr->TexIndex = textureIndex;
		s_Renderer2DData.QuadVertexBufferPtr->TilingFactor = tilingFactor;
		s_Renderer2DData.QuadVertexBufferPtr++;

		s_Renderer2DData.QuadIndexCount += 6;

		s_Renderer2DData.Stats.QuadCount++;
	}

	void Renderer2D::drawRotatedQuad(const glm::vec2& position, const glm::vec2& size, const float rotation, const glm::vec4& color)
	{
		BEY_PROFILE_FUNCTION();
		drawRotatedQuad({ position.x, position.y, 0.0f }, size, rotation, color);
	}

	void Renderer2D::drawRotatedQuad(const glm::vec3& position, const glm::vec2& size, const float rotation, const glm::vec4& color)
	{
		BEY_PROFILE_FUNCTION();
		if (s_Renderer2DData.QuadIndexCount >= Renderer2DData::MAX_INDICES)
			flushAndReset();

		const float textureIndex = 0.0f; // White Texture
		const float tilingFactor = 1.0f;

		const glm::mat4 transform = glm::translate(glm::mat4(1.0f), position)
			* glm::rotate(glm::mat4(1.0f), glm::radians(rotation), { 0.0f, 0.0f, 1.0f })
			* glm::scale(glm::mat4(1.0f), { size.x, size.y, 1.0f });

		s_Renderer2DData.QuadVertexBufferPtr->Position = transform * s_Renderer2DData.QuadVertexPositions[0];
		s_Renderer2DData.QuadVertexBufferPtr->Color = color;
		s_Renderer2DData.QuadVertexBufferPtr->TexCoord = { 0.0f, 0.0f };
		s_Renderer2DData.QuadVertexBufferPtr->TexIndex = textureIndex;
		s_Renderer2DData.QuadVertexBufferPtr->TilingFactor = tilingFactor;
		s_Renderer2DData.QuadVertexBufferPtr++;

		s_Renderer2DData.QuadVertexBufferPtr->Position = transform * s_Renderer2DData.QuadVertexPositions[1];
		s_Renderer2DData.QuadVertexBufferPtr->Color = color;
		s_Renderer2DData.QuadVertexBufferPtr->TexCoord = { 1.0f, 0.0f };
		s_Renderer2DData.QuadVertexBufferPtr->TexIndex = textureIndex;
		s_Renderer2DData.QuadVertexBufferPtr->TilingFactor = tilingFactor;
		s_Renderer2DData.QuadVertexBufferPtr++;

		s_Renderer2DData.QuadVertexBufferPtr->Position = transform * s_Renderer2DData.QuadVertexPositions[2];
		s_Renderer2DData.QuadVertexBufferPtr->Color = color;
		s_Renderer2DData.QuadVertexBufferPtr->TexCoord = { 1.0f, 1.0f };
		s_Renderer2DData.QuadVertexBufferPtr->TexIndex = textureIndex;
		s_Renderer2DData.QuadVertexBufferPtr->TilingFactor = tilingFactor;
		s_Renderer2DData.QuadVertexBufferPtr++;

		s_Renderer2DData.QuadVertexBufferPtr->Position = transform * s_Renderer2DData.QuadVertexPositions[3];
		s_Renderer2DData.QuadVertexBufferPtr->Color = color;
		s_Renderer2DData.QuadVertexBufferPtr->TexCoord = { 0.0f, 1.0f };
		s_Renderer2DData.QuadVertexBufferPtr->TexIndex = textureIndex;
		s_Renderer2DData.QuadVertexBufferPtr->TilingFactor = tilingFactor;
		s_Renderer2DData.QuadVertexBufferPtr++;

		s_Renderer2DData.QuadIndexCount += 6;

		s_Renderer2DData.Stats.QuadCount++;
	}

	void Renderer2D::drawRotatedQuad(const glm::vec2& position, const glm::vec2& size, const float rotation, const Ref<Texture2D>& texture, const float tilingFactor, const glm::vec4& tintColor)
	{
		BEY_PROFILE_FUNCTION();
		drawRotatedQuad({ position.x, position.y, 0.0f }, size, rotation, texture, tilingFactor, tintColor);
	}

	void Renderer2D::drawRotatedQuad(const glm::vec3& position, const glm::vec2& size, const float rotation, const Ref<Texture2D>& texture, const float tilingFactor, const glm::vec4& tintColor)
	{
		BEY_PROFILE_FUNCTION();
		if (s_Renderer2DData.QuadIndexCount >= Renderer2DData::MAX_INDICES)
			flushAndReset();

		constexpr glm::vec4 color = { 1.0f, 1.0f, 1.0f, 1.0f };

		float textureIndex = 0.0f;
		for (uint32_t i = 1; i < s_Renderer2DData.TextureSlotIndex; i++)
		{
			if (*s_Renderer2DData.TextureSlots[i].raw() == *texture.raw())
			{
				textureIndex = float(i);
				break;
			}
		}

		if (fabs(textureIndex) < FLT_EPSILON)
		{
			textureIndex = float(s_Renderer2DData.TextureSlotIndex);
			s_Renderer2DData.TextureSlots[s_Renderer2DData.TextureSlotIndex] = texture;
			s_Renderer2DData.TextureSlotIndex++;
		}

		const glm::mat4 transform = glm::translate(glm::mat4(1.0f), position)
			* glm::rotate(glm::mat4(1.0f), glm::radians(rotation), { 0.0f, 0.0f, 1.0f })
			* glm::scale(glm::mat4(1.0f), { size.x, size.y, 1.0f });

		s_Renderer2DData.QuadVertexBufferPtr->Position = transform * s_Renderer2DData.QuadVertexPositions[0];
		s_Renderer2DData.QuadVertexBufferPtr->Color = color;
		s_Renderer2DData.QuadVertexBufferPtr->TexCoord = { 0.0f, 0.0f };
		s_Renderer2DData.QuadVertexBufferPtr->TexIndex = textureIndex;
		s_Renderer2DData.QuadVertexBufferPtr->TilingFactor = tilingFactor;
		s_Renderer2DData.QuadVertexBufferPtr++;

		s_Renderer2DData.QuadVertexBufferPtr->Position = transform * s_Renderer2DData.QuadVertexPositions[1];
		s_Renderer2DData.QuadVertexBufferPtr->Color = color;
		s_Renderer2DData.QuadVertexBufferPtr->TexCoord = { 1.0f, 0.0f };
		s_Renderer2DData.QuadVertexBufferPtr->TexIndex = textureIndex;
		s_Renderer2DData.QuadVertexBufferPtr->TilingFactor = tilingFactor;
		s_Renderer2DData.QuadVertexBufferPtr++;

		s_Renderer2DData.QuadVertexBufferPtr->Position = transform * s_Renderer2DData.QuadVertexPositions[2];
		s_Renderer2DData.QuadVertexBufferPtr->Color = color;
		s_Renderer2DData.QuadVertexBufferPtr->TexCoord = { 1.0f, 1.0f };
		s_Renderer2DData.QuadVertexBufferPtr->TexIndex = textureIndex;
		s_Renderer2DData.QuadVertexBufferPtr->TilingFactor = tilingFactor;
		s_Renderer2DData.QuadVertexBufferPtr++;

		s_Renderer2DData.QuadVertexBufferPtr->Position = transform * s_Renderer2DData.QuadVertexPositions[3];
		s_Renderer2DData.QuadVertexBufferPtr->Color = color;
		s_Renderer2DData.QuadVertexBufferPtr->TexCoord = { 0.0f, 1.0f };
		s_Renderer2DData.QuadVertexBufferPtr->TexIndex = textureIndex;
		s_Renderer2DData.QuadVertexBufferPtr->TilingFactor = tilingFactor;
		s_Renderer2DData.QuadVertexBufferPtr++;

		s_Renderer2DData.QuadIndexCount += 6;

		s_Renderer2DData.Stats.QuadCount++;
	}

	void Renderer2D::drawLine(const glm::vec3& p0, const glm::vec3& p1, const glm::vec4& color)
	{
		BEY_PROFILE_FUNCTION();
		if (s_Renderer2DData.LineIndexCount >= Renderer2DData::MAX_LINE_INDICES)
			flushAndResetLines();

		s_Renderer2DData.LineVertexBufferPtr->Position = p0;
		s_Renderer2DData.LineVertexBufferPtr->Color = color;
		s_Renderer2DData.LineVertexBufferPtr++;

		s_Renderer2DData.LineVertexBufferPtr->Position = p1;
		s_Renderer2DData.LineVertexBufferPtr->Color = color;
		s_Renderer2DData.LineVertexBufferPtr++;

		s_Renderer2DData.LineIndexCount += 2;

		s_Renderer2DData.Stats.LineCount++;
	}

	void Renderer2D::resetStats()
	{
		BEY_PROFILE_FUNCTION();
		memset(&s_Renderer2DData.Stats, 0, sizeof(Statistics));
	}

	Renderer2D::Statistics Renderer2D::getStats()
	{
		return s_Renderer2DData.Stats;
	}

}