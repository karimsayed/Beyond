#include "pchs.h"
#include "Material.h"

#include <utility>

namespace bey {

	//////////////////////////////////////////////////////////////////////////////////
	// Material
	//////////////////////////////////////////////////////////////////////////////////

	Ref<Material> Material::create(const Ref<Shader>& shader)
	{
		BEY_PROFILE_FUNCTION();
		return Ref<Material>::create(shader);
	}

	Material::Material(const Ref<Shader>& shader)
		: mShader(shader)
	{
		BEY_PROFILE_FUNCTION();
		mShader->addShaderReloadedCallback([this] { onShaderReloaded(); });
		allocateStorage();

		mMaterialFlags |= (uint32_t)MaterialFlag::DepthTest;
		mMaterialFlags |= (uint32_t)MaterialFlag::Blend;
	}

	Material::~Material()
	{
		BEY_PROFILE_FUNCTION();
	}

	void Material::allocateStorage()
	{
		BEY_PROFILE_FUNCTION();
		if (mShader->hasVSMaterialUniformBuffer())
		{
			BEY_PROFILE_SCOPE("allocateStorage VS");
			const auto& vsBuffer = mShader->getVSMaterialUniformBuffer();
			mVsUniformStorageBuffer.allocate(vsBuffer.getSize());
			mVsUniformStorageBuffer.zeroInitialize();
		}

		if (mShader->hasPSMaterialUniformBuffer())
		{
			BEY_PROFILE_SCOPE("allocateStorage PS");
			const auto& psBuffer = mShader->getPSMaterialUniformBuffer();
			mPsUniformStorageBuffer.allocate(psBuffer.getSize());
			mPsUniformStorageBuffer.zeroInitialize();
		}

		if (mShader->hasTessEMaterialUniformBuffer())
		{
			BEY_PROFILE_SCOPE("allocateStorage PS");
			const auto& tessBuffer = mShader->getTessESMaterialUniformBuffer();
			mTessESUniformStorageBuffer.allocate(tessBuffer.getSize());
			mTessESUniformStorageBuffer.zeroInitialize();
		}

		if (mShader->hasTessCMaterialUniformBuffer())
		{
			BEY_PROFILE_SCOPE("allocateStorage PS");
			const auto& tessBuffer = mShader->getTessCSMaterialUniformBuffer();
			mTessCSUniformStorageBuffer.allocate(tessBuffer.getSize());
			mTessCSUniformStorageBuffer.zeroInitialize();
		}

		if (mShader->hasCompMaterialUniformBuffer())
		{
			BEY_PROFILE_SCOPE("allocateStorage Comp");
			const auto& tessBuffer = mShader->getCompMaterialUniformBuffer();
			mCompUniformStorageBuffer.allocate(tessBuffer.getSize());
			mCompUniformStorageBuffer.zeroInitialize();
		}
	}

	void Material::onShaderReloaded()
	{
		return;
		allocateStorage();

		for (auto mi : mMaterialInstances)
			mi->onShaderReloaded();
	}

	ShaderUniformDeclaration* Material::findUniformDeclaration(const std::string& name, const ShaderDomain domain)
	{
		BEY_PROFILE_FUNCTION();
		switch (domain)
		{
		case ShaderDomain::Vertex:
		{
			BEY_PROFILE_SCOPE("findUniformDeclaration VS");
			const auto& declarations = mShader->getVSMaterialUniformBuffer().getUniformDeclarations();
			for (ShaderUniformDeclaration* uniform : declarations)
			{
				if (uniform->getName() == name)
					return uniform;
			}
			break;
		}
		case ShaderDomain::Pixel:
		{
			BEY_PROFILE_SCOPE("findUniformDeclaration PS");
			const auto& declarations = mShader->getPSMaterialUniformBuffer().getUniformDeclarations();
			for (ShaderUniformDeclaration* uniform : declarations)
			{
				if (uniform->getName() == name)
					return uniform;
			}
			break;
		}

		case ShaderDomain::TessES:
		{
			BEY_PROFILE_SCOPE("findUniformDeclaration Tess ES");
			const auto& declarations = mShader->getTessESMaterialUniformBuffer().getUniformDeclarations();
			for (ShaderUniformDeclaration* uniform : declarations)
			{
				if (uniform->getName() == name)
					return uniform;
			}
			break;
		}
		case ShaderDomain::TessCS:
		{
			BEY_PROFILE_SCOPE("findUniformDeclaration Tess CS");
			const auto& declarations = mShader->getTessCSMaterialUniformBuffer().getUniformDeclarations();
			for (ShaderUniformDeclaration* uniform : declarations)
			{
				if (uniform->getName() == name)
					return uniform;
			}
			break;
		}
		case ShaderDomain::Comp:
		{
			BEY_PROFILE_SCOPE("findUniformDeclaration Comp");
			const auto& declarations = mShader->getCompMaterialUniformBuffer().getUniformDeclarations();
			for (ShaderUniformDeclaration* uniform : declarations)
			{
				if (uniform->getName() == name)
					return uniform;
			}
			break;
		}
		}
		return nullptr;
	}
	ShaderResourceDeclaration* Material::findResourceDeclaration(const std::string& name)
	{
		BEY_PROFILE_FUNCTION();
		const auto& resources = mShader->getResources();
		for (ShaderResourceDeclaration* resource : resources)
		{
			if (resource->getName() == name)
				return resource;
		}
		return nullptr;
	}

	Buffer& Material::getUniformBufferTarget(ShaderUniformDeclaration* uniformDeclaration)
	{
		BEY_PROFILE_FUNCTION();
		switch (uniformDeclaration->getDomain())
		{
		case ShaderDomain::Vertex:    return mVsUniformStorageBuffer;
		case ShaderDomain::Pixel:     return mPsUniformStorageBuffer;
		case ShaderDomain::TessES:     return mTessESUniformStorageBuffer;
		case ShaderDomain::TessCS:     return mTessCSUniformStorageBuffer;
		case ShaderDomain::Comp:     return mCompUniformStorageBuffer;
		}

		BEY_CORE_ASSERT(false, "Invalid uniform declaration domain! Material does not support this shader type.");
		return mVsUniformStorageBuffer;
	}

	void Material::bind()
	{
		BEY_PROFILE_FUNCTION();

		mShader->bind();

		if (mVsUniformStorageBuffer)
			mShader->setVSMaterialUniformBuffer(mVsUniformStorageBuffer);

		if (mPsUniformStorageBuffer)
			mShader->setPSMaterialUniformBuffer(mPsUniformStorageBuffer);

		if (mTessESUniformStorageBuffer)
			mShader->setTessESMaterialUniformBuffer(mTessESUniformStorageBuffer);

		if (mTessCSUniformStorageBuffer)
			mShader->setTessCSMaterialUniformBuffer(mTessCSUniformStorageBuffer);

		if (mCompUniformStorageBuffer)
			mShader->setCompMaterialUniformBuffer(mCompUniformStorageBuffer);
		
		bindTextures();
	}

	void Material::bindTextures() const
	{
		BEY_PROFILE_FUNCTION();

		for (size_t i = 0; i < mTextures.size(); i++)
		{
			const auto& texture = mTextures[i];
			if (texture)
				texture->bind(uint32_t(i));
		}
	}

	//////////////////////////////////////////////////////////////////////////////////
	// MaterialInstance
	//////////////////////////////////////////////////////////////////////////////////

	Ref<MaterialInstance> MaterialInstance::create(const Ref<Material>& material)
	{
		BEY_PROFILE_FUNCTION();
		return Ref<MaterialInstance>::create(material);
	}

	MaterialInstance::MaterialInstance(const Ref<Material>& material, std::string name)
		: mMaterial(material), mName(std::move(name))
	{
		BEY_PROFILE_FUNCTION();
		mMaterial->mMaterialInstances.insert(this);
		allocateStorage();
	}

	MaterialInstance::~MaterialInstance()
	{
		BEY_PROFILE_FUNCTION();
		mMaterial->mMaterialInstances.erase(this);
	}

	void MaterialInstance::onShaderReloaded()
	{
		BEY_PROFILE_FUNCTION();
		allocateStorage();
		mOverriddenValues.clear();
	}

	void MaterialInstance::allocateStorage()
	{
		BEY_PROFILE_FUNCTION();

		if (mMaterial->mShader->hasVSMaterialUniformBuffer())
		{
			BEY_PROFILE_SCOPE("allocateStorage VS");
			const auto& vsBuffer = mMaterial->mShader->getVSMaterialUniformBuffer();
			mVsUniformStorageBuffer.allocate(vsBuffer.getSize());
			memcpy(mVsUniformStorageBuffer.Data, mMaterial->mVsUniformStorageBuffer.Data, vsBuffer.getSize());
		}

		if (mMaterial->mShader->hasPSMaterialUniformBuffer())
		{
			BEY_PROFILE_SCOPE("allocateStorage PS");
			const auto& psBuffer = mMaterial->mShader->getPSMaterialUniformBuffer();
			mPsUniformStorageBuffer.allocate(psBuffer.getSize());
			memcpy(mPsUniformStorageBuffer.Data, mMaterial->mPsUniformStorageBuffer.Data, psBuffer.getSize());
		}

		if (mMaterial->mShader->hasTessEMaterialUniformBuffer())
		{
			BEY_PROFILE_SCOPE("allocateStorage Tess ES");
			const auto& tessESBuffer = mMaterial->mShader->getTessESMaterialUniformBuffer();
			mTessESUniformStorageBuffer.allocate(tessESBuffer.getSize());
			memcpy(mTessESUniformStorageBuffer.Data, mMaterial->mTessESUniformStorageBuffer.Data, tessESBuffer.getSize());
		}

		if (mMaterial->mShader->hasTessCMaterialUniformBuffer())
		{
			BEY_PROFILE_SCOPE("allocateStorage Tess CS");
			const auto& tessCSBuffer = mMaterial->mShader->getTessCSMaterialUniformBuffer();
			mTessCSUniformStorageBuffer.allocate(tessCSBuffer.getSize());
			memcpy(mTessCSUniformStorageBuffer.Data, mMaterial->mTessCSUniformStorageBuffer.Data, tessCSBuffer.getSize());
		}

		if (mMaterial->mShader->hasCompMaterialUniformBuffer())
		{
			BEY_PROFILE_SCOPE("allocateStorage Tess CS");
			const auto& compBuffer = mMaterial->mShader->getCompMaterialUniformBuffer();
			mCompUniformStorageBuffer.allocate(compBuffer.getSize());
			memcpy(mCompUniformStorageBuffer.Data, mMaterial->mCompUniformStorageBuffer.Data, compBuffer.getSize());
		}
	}

	void MaterialInstance::setFlag(MaterialFlag flag, const bool value)
	{
		BEY_PROFILE_FUNCTION();
		value ? mMaterial->mMaterialFlags |= uint32_t(flag) : mMaterial->mMaterialFlags &= ~uint32_t(flag);
	}

	void MaterialInstance::onMaterialValueUpdated(ShaderUniformDeclaration* decl)
	{
		BEY_PROFILE_FUNCTION();
		if (mOverriddenValues.find(decl->getName()) == mOverriddenValues.end())
		{
			auto& buffer = getUniformBufferTarget(decl);
			auto& materialBuffer = mMaterial->getUniformBufferTarget(decl);
			buffer.write(materialBuffer.Data + decl->getOffset(), decl->getSize(), decl->getOffset());
		}
	}

	Buffer& MaterialInstance::getUniformBufferTarget(const ShaderUniformDeclaration* uniformDeclaration)
	{
		BEY_PROFILE_FUNCTION();
		switch (uniformDeclaration->getDomain())
		{
		case ShaderDomain::Vertex:    return mVsUniformStorageBuffer;
		case ShaderDomain::Pixel:     return mPsUniformStorageBuffer;
		case ShaderDomain::TessES:     return mTessESUniformStorageBuffer;
		case ShaderDomain::TessCS:     return mTessCSUniformStorageBuffer;
		case ShaderDomain::Comp:     return mCompUniformStorageBuffer;
		}

		BEY_CORE_ASSERT(false, "Invalid uniform declaration domain! Material does not support this shader type.");
		return mVsUniformStorageBuffer;
	}

	void MaterialInstance::bind() const
	{
		BEY_PROFILE_FUNCTION();
		mMaterial->mShader->bind();

		if (mVsUniformStorageBuffer)
			mMaterial->mShader->setVSMaterialUniformBuffer(mVsUniformStorageBuffer);

		if (mPsUniformStorageBuffer)
			mMaterial->mShader->setPSMaterialUniformBuffer(mPsUniformStorageBuffer);

		if (mTessESUniformStorageBuffer)
			mMaterial->mShader->setTessESMaterialUniformBuffer(mTessESUniformStorageBuffer);
		
		if (mTessCSUniformStorageBuffer)
			mMaterial->mShader->setTessCSMaterialUniformBuffer(mTessCSUniformStorageBuffer);

		if (mCompUniformStorageBuffer)
			mMaterial->mShader->setCompMaterialUniformBuffer(mCompUniformStorageBuffer);

		mMaterial->bindTextures();
		for (size_t i = 0; i < mTextures.size(); i++)
		{
			const auto texture = mTextures[i];
			if (texture)
				texture->bind(uint32_t(i));
		}
	}

}
