#pragma once

#include "Beyond/Core/Base.h"

#include "Beyond/Platform/OpenGL/Shader.h"
#include "Beyond/Platform/OpenGL/Texture.h"

#include <unordered_set>

namespace bey {

	enum class MaterialFlag
	{
		None = BIT(0),
		DepthTest = BIT(1),
		Blend = BIT(2)
	};
	class MaterialInstance;
	class Material : public RefCounted
	{
		friend class MaterialInstance;
	public:
		explicit Material(const Ref<Shader>& shader);
		virtual ~Material();

		void bind();

		uint32_t getFlags() const { return mMaterialFlags; }
		void setFlag(MaterialFlag flag) { mMaterialFlags |= (uint32_t)flag; }

		template <typename T>
		void set(const std::string& name, const T& value, const ShaderDomain domain)
		{
			auto decl = findUniformDeclaration(name, domain);
			BEY_CORE_ASSERT(decl, "Could not find uniform with name 'x'");
			auto& buffer = getUniformBufferTarget(decl);
			buffer.write((std::byte*)&value, decl->getSize(), decl->getOffset());

			for (MaterialInstance* mi : mMaterialInstances)
				mi->onMaterialValueUpdated(decl);
		}

		void set(const std::string& name, const Ref<Texture>& texture)
		{
			auto* const decl = findResourceDeclaration(name);
			const uint32_t slot = decl->getRegister();
			if (mTextures.size() <= slot)
				mTextures.resize(size_t(slot) + 1);
			mTextures[slot] = texture;
		}

		void set(const std::string& name, const Ref<Texture2D>& texture)
		{
			set(name, static_cast<const Ref<Texture>&>(texture));
		}

		void set(const std::string& name, const Ref<TextureCube>& texture)
		{
			set(name, static_cast<const Ref<Texture>&>(texture));
		}

		template<typename T>
		T& get(const std::string& name)
		{
			const auto* decl = findUniformDeclaration(name);
			BEY_CORE_ASSERT(decl, "Could not find uniform with name 'x'");
			auto& buffer = getUniformBufferTarget(decl);
			return buffer.read<T>(decl->getOffset());
		}

		template<typename T>
		Ref<T> getResource(const std::string& name)
		{
			const auto decl = findResourceDeclaration(name);
			const uint32_t slot = decl->getRegister();
			BEY_CORE_ASSERT(slot < mTextures.size(), "Texture slot is invalid!");
			return mTextures[slot];
		}
	public:
		static Ref<Material> create(const Ref<Shader>& shader);
	private:
		void allocateStorage();
		void onShaderReloaded();
		void bindTextures() const;

		ShaderUniformDeclaration* findUniformDeclaration(const std::string& name, const ShaderDomain domain);
		ShaderResourceDeclaration* findResourceDeclaration(const std::string& name);
		Buffer& getUniformBufferTarget(ShaderUniformDeclaration* uniformDeclaration);
	private:
		Ref<Shader> mShader;
		std::unordered_set<MaterialInstance*> mMaterialInstances;

		Buffer mVsUniformStorageBuffer;
		Buffer mPsUniformStorageBuffer;
		Buffer mTessESUniformStorageBuffer;
		Buffer mTessCSUniformStorageBuffer;
		Buffer mCompUniformStorageBuffer;
		std::vector<Ref<Texture>> mTextures;

		uint32_t mMaterialFlags {};
	};

	class MaterialInstance : public RefCounted
	{
		friend class Material;
	public:
		explicit MaterialInstance(const Ref<Material>& material, std::string name = "");
		virtual ~MaterialInstance();

		template <typename T>
		void set(const std::string& name, const T& value, const ShaderDomain domain)
		{
			const auto* const decl = mMaterial->findUniformDeclaration(name, domain);
			if (!decl)
			{
				BEY_CORE_ERROR("Could not find uniform with name '{}'", name);
				return;
			}
			auto& buffer = getUniformBufferTarget(decl);
			buffer.write((std::byte*)&value, decl->getSize(), decl->getOffset());

			mOverriddenValues.insert(name);
		}

		void set(const std::string& name, const Ref<Texture>& texture)
		{
			const auto* const decl = mMaterial->findResourceDeclaration(name);
			if (!decl)
			{
				BEY_CORE_WARN("Cannot find material property: ", name);
				return;
			}
			const uint32_t slot = decl->getRegister();
			if (mTextures.size() <= slot)
				mTextures.resize(size_t(slot) + 1);
			mTextures[slot] = texture;
		}

		void set(const std::string& name, const Ref<Texture2D>& texture)
		{
			set(name, static_cast<const Ref<Texture>&>(texture));
		}

		void set(const std::string& name, const Ref<TextureCube>& texture)
		{
			set(name, static_cast<const Ref<Texture>&>(texture));
		}

		template<typename T>
		T& get(const std::string& name, const ShaderDomain domain)
		{
			const auto decl = mMaterial->findUniformDeclaration(name, domain);
			BEY_CORE_ASSERT(decl, "Could not find uniform with name 'x'");
			auto& buffer = getUniformBufferTarget(decl);
			return buffer.read<T>(decl->getOffset());
		}

		template<typename T>
		Ref<T> getResource(const std::string& name) requires std::derived_from<T, Texture>
		{
			const auto decl = mMaterial->findResourceDeclaration(name);
			BEY_CORE_ASSERT(decl, "Could not find uniform with name 'x'");
			const uint32_t slot = decl->getRegister();
			BEY_CORE_ASSERT(slot < mTextures.size(), "Texture slot is invalid!");
			return Ref<T>(mTextures[slot]);
		}

		template<typename T>
		Ref<T> tryGetResource(const std::string& name) requires std::derived_from<T, Texture> 
		{
			const auto decl = mMaterial->findResourceDeclaration(name);
			if (!decl)
			{
				BEY_CORE_WARN("Could not find resource declaration");
				return nullptr;
			}
			const uint32_t slot = decl->getRegister();
			if (slot >= mTextures.size())
				return nullptr;

			return Ref<T>(mTextures[slot]);
		}


		void bind() const;

		uint32_t getFlags() const { return mMaterial->mMaterialFlags; }
		bool getFlag(MaterialFlag flag) const { return uint32_t(flag) & mMaterial->mMaterialFlags; }
		void setFlag(MaterialFlag flag, bool value = true);

		Ref<Shader> getShader() const { return mMaterial->mShader; }

		const std::string& getName() const { return mName; }
	public:
		static Ref<MaterialInstance> create(const Ref<Material>& material);
	private:
		void allocateStorage();
		void onShaderReloaded();
		Buffer& getUniformBufferTarget(const ShaderUniformDeclaration* uniformDeclaration);
		void onMaterialValueUpdated(ShaderUniformDeclaration* decl);
	private:
		Ref<Material> mMaterial;
		std::string mName;

		Buffer mVsUniformStorageBuffer;
		Buffer mPsUniformStorageBuffer;
		Buffer mTessESUniformStorageBuffer;
		Buffer mTessCSUniformStorageBuffer;
		Buffer mCompUniformStorageBuffer;
		std::vector<Ref<Texture>> mTextures;

		// TODO: This is temporary; come up with a proper system to track overrides
		std::unordered_set<std::string> mOverriddenValues;
	};

}
