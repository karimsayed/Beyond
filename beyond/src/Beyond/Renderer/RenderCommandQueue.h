#pragma once


namespace bey {

	class RenderCommandQueue
	{
	public:
		using RenderCommandFn = void(*)(void*);

		RenderCommandQueue();
		~RenderCommandQueue();
		RenderCommandQueue(const RenderCommandQueue&) = delete;
		RenderCommandQueue(RenderCommandQueue&&) = delete;
		RenderCommandQueue& operator=(const RenderCommandQueue&) = delete;
		RenderCommandQueue& operator=(RenderCommandQueue&&) = delete;

		void* allocate(RenderCommandFn func, uint32_t size);

		void execute();

		[[nodiscard]] bool hasCommands() const { return bool(mCommandCount); }


	private:
		uint8_t* mCommandBuffer{};
		uint8_t* mCommandBufferPtr{};
		uint32_t mCommandCount = 0;
	};



}
