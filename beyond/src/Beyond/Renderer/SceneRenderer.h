#pragma once

#include "Beyond/Platform/OpenGL/RenderPass.h"
#include "Beyond/Scene/Scene.h"
#include "Beyond/Renderer/Mesh.h"



namespace bey {

	struct SceneRendererOptions
	{
		bool ShowGrid = true;
		bool ShowBoundingBoxes = false;
		bool ShowCameraCorners = false;
		bool EnableSSAO = true;
		float SSAORadius = 2.5f;
		float SSAOBias = 0.2f;
		bool EnableBloom = true;
		bool EnableSSR = true;
		bool EnableIrradianceReflections = true;
		bool DrawWireFrame = false;
		// ao
		bool EnableHBAO = true;
		float R2;
		float NegInvR2;
		float RadiusToScreen;

		float SSAOIntensity = 2;
		float PowExponent;
		float NDotVBias;
		float AOMultiplier;
		glm::vec4    Float2Offsets[16];
		glm::vec4    Jitters[16];
		glm::vec4 ProjInfo;
		float ProjScale;
	};

	struct SceneRendererCamera
	{
		Ref<Camera> Camera;
		glm::mat4 ViewMatrix{};
		[[nodiscard]] glm::mat4 getViewProj() const noexcept { return Camera->getProjectionMatrix() * ViewMatrix; }
	};

	struct FrustumBounds
	{
		float R, L, B, T, F, N;
	};
	struct CascadeData
	{
		glm::mat4 ViewProj{};
		glm::mat4 View{};
		float SplitDepth = 0;
		glm::vec3 FrustumCorners[8]{};
		inline static glm::vec3 WholeFrustumCorners[8]{};
		inline static glm::vec4 WholeFrustumPlanes[6]{};
		inline static glm::mat4 ViewProjection;

		static void extractPlanes(glm::vec4& left, glm::vec4& right, glm::vec4& top, glm::vec4& bottom, glm::vec4& nearP, glm::vec4& farP);
	};

	struct ShadowData
	{
		glm::mat4 LightMatrices[4]{};
		bool ShowCascades = false;
		bool SoftShadows = true;
		bool CascadeFading = true;
		float ShadowMapSize = 20.0f;
		float LightDistance = 0.1f;
		float CascadeSplitLambda = 0.965f;
		float CascadeFarPlaneOffset = 15.0f, CascadeNearPlaneOffset = -15.0f;

		CascadeData Cascades[4];

	};


	class SceneRenderer
	{
	public:
		static void init();

		static void setViewportSize(uint32_t width, uint32_t height);

		static void beginScene(Scene* scene, const SceneRendererCamera& camera);
		static void endScene();
		static void onImGuiRender();

		static void submitMesh(const Ref<Mesh>& mesh, const glm::mat4& transform = glm::mat4(1.0f));
		static void submitSelectedMesh(const Ref<Mesh>& mesh, const glm::mat4& transform = glm::mat4(1.0f));

		static std::pair<Ref<TextureCube>, Ref<TextureCube>> createEnvironmentMap(const std::string& filepath);

		static Ref<RenderPass> getFinalRenderPass();
		static size_t getTargetRendererID(const int flag);
		static Ref<Texture2D> getFinalColorBuffer();

		static uint64_t getFinalColorBufferRendererID();
		static uint32_t getLightPixelIDAt(const glm::ivec2& mouse);

		static void terminate();

		static SceneRendererOptions& getOptions();
	private:
		static void geometryPass();
		static void ssaoPass();
		static void blurSSAO();
		static void copyDepthAndStencil();
		static void ssrPass();
		static void SSGIPass();
		static void hbaoPass();
		static void flushDrawList();
		static void depthPass();
		static void lightingPass();
		static void bloomBlurPass();
		static void compositePass();
		friend class Mesh;

	};

}