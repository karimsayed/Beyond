#pragma once

#include <random>
#include <memory>
namespace bey
{

	class RandomGenerator
	{
	public:
		static void generateKernel();
		static void generateNoise();
		static void generateHBAORandom();

		static std::vector<glm::vec3> getKernel() { return mSSAOKernel; }
		static std::vector<glm::vec3> getNoise() { return mSSAONoise; }
		static std::vector<glm::vec4> getHBAORandom() { return mHBAORandom; }

	private:
		static std::uniform_real_distribution<float> mRandomFloats;
		static std::default_random_engine mGenerator;
		static std::vector<glm::vec3> mSSAOKernel;
		static std::vector<glm::vec3> mSSAONoise;
		static std::vector<glm::vec4> mHBAORandom;
		
		
	};

}
