#include "pchs.h"
#include "MeshExporter.h"

#include <ranges>


#include "Entity.h"
#include <assimp/scene.h>
#include <assimp/SceneCombiner.h>
#include <assimp/Exporter.hpp>


namespace bey
{

	static aiMatrix4x4 glmMat4ToAi(glm::mat4 mat)
	{
		return aiMatrix4x4(mat[0][0], mat[1][0], mat[2][0], mat[3][0],
			mat[0][1], mat[1][1], mat[2][1], mat[3][1],
			mat[0][2], mat[1][2], mat[2][2], mat[3][2],
			mat[0][3], mat[1][3], mat[2][3], mat[3][3]);
	}

	
	void MeshExporter::exportMesh(const std::string& fileName, Ref<Mesh> mesh, const glm::mat4& entityTransform)
	{
		Assimp::Exporter exporter;
		aiScene* newScene;
		mesh->updateNodes(mesh->mScene->mRootNode, glmMat4ToAi(entityTransform), 0);
		
		Assimp::SceneCombiner::CopyScene(&newScene, mesh->mScene);
		//Assimp::SceneCombiner::MergeScenes()
		exporter.Export(newScene, "fbx", fileName);
	}

	void MeshExporter::exportScene(const Ref<Scene>& scene, const std::string& exportPath)
	{
		auto* newScene = new aiScene;
		std::vector<aiScene*> scenes;
		for (const auto entity : scene->getEntityMap() | std::views::values)
		{
			if (entity.hasComponent<MeshComponent>())
			{
				scenes.push_back(entity.getComponent<MeshComponent>().Mesh->mScene);
			}
		}

		Assimp::Exporter exporter;
		exporter.Export(newScene, "fbx", exportPath);
		delete newScene;
	}




	static void dumpMaterialsToConsole(const aiScene& scene, const int index, const aiPropertyTypeInfo propertyType)
	{
		const auto dump = [propertyType](const aiMaterial& mat)
		{
			for (unsigned i = 0; i < mat.mNumProperties; i++)
			{
				const auto* const prop = mat.mProperties[i];
				if (prop->mType == propertyType)
				{
					if (propertyType == aiPTI_String)
						BEY_CORE_INFO("key: {}, semantic: {}, data: {}", prop->mKey.C_Str(), prop->mSemantic, prop->mData + 4);
					else if (propertyType == aiPTI_Float || propertyType == aiPTI_Integer || propertyType == aiPTI_Double)
						BEY_CORE_INFO("key: {}, semantic: {}, data: {}", prop->mKey.C_Str(), prop->mSemantic,
							*reinterpret_cast<float*>(prop->mData));
					else if (propertyType == aiPTI_Buffer)
					{
						//aiColor4D color = *reinterpret_cast<aiColor4D*>(prop->mData);
						//BEY_CORE_INFO("key: {}, semantic: {}, data: r: {}, g: {}, b: {}, a: {}", prop->mKey.C_Str(), prop->mSemantic, color.r, color.g, color.b, color.a);
					}

				}
			}

		};



		const aiMaterial& mat = *scene.mMaterials[index];
		dump(mat);
		//BEY_CORE_INFO("mat: {} has ...", i);
		/*dump(mat, aiTextureType_NONE);
		dump(mat, aiTextureType_DIFFUSE);
		dump(mat, aiTextureType_SPECULAR);
		dump(mat, aiTextureType_AMBIENT);
		dump(mat, aiTextureType_EMISSIVE);
		dump(mat, aiTextureType_HEIGHT);
		dump(mat, aiTextureType_NORMALS);
		dump(mat, aiTextureType_SHININESS);
		dump(mat, aiTextureType_OPACITY);
		dump(mat, aiTextureType_DISPLACEMENT);
		dump(mat, aiTextureType_LIGHTMAP);
		dump(mat, aiTextureType_REFLECTION);
		dump(mat, aiTextureType_BASE_COLOR);
		dump(mat, aiTextureType_NORMAL_CAMERA);
		dump(mat, aiTextureType_EMISSION_COLOR);
		dump(mat, aiTextureType_METALNESS);
		dump(mat, aiTextureType_DIFFUSE_ROUGHNESS);
		dump(mat, aiTextureType_AMBIENT_OCCLUSION);*/

	}





	void MeshExporter::updateFactor(const float value, Ref<Mesh> mesh, const uint32_t index, const char* matKey)
	{
		auto* aiMaterial = mesh->mScene->mMaterials[index];

		//	DumpMaterialsToConsole(*mesh->mScene);

		if (aiMaterial->RemoveProperty(matKey, aiTextureType_NONE) != 0)
		{
			BEY_CORE_ASSERT(false, "Failed to add property!");
			BEY_CORE_ERROR("couldnt remove property");
		}


		aiMaterial->AddBinaryProperty((void*)&value, 4, matKey, aiTextureType_NONE, 0, aiPTI_Float);

		//dumpMaterialsToConsole(*mesh->mScene, index, aiPTI_Float);

	}

	void MeshExporter::updateFactor(const aiColor4D value, Ref<Mesh> mesh, const uint32_t index, const char* matKey)
	{
		auto* aiMaterial = mesh->mScene->mMaterials[index];

		//	DumpMaterialsToConsole(*mesh->mScene);

		if (aiMaterial->RemoveProperty(matKey, 0) != 0)
		{
			BEY_CORE_ASSERT(false, "Failed to add property!");
			BEY_CORE_ERROR("couldnt remove property");
		}


		aiMaterial->AddBinaryProperty((void*)&value, 16, matKey, aiTextureType_NONE, 0, aiPTI_Buffer);

		//dumpMaterialsToConsole(*mesh->mScene, index, aiPTI_Buffer);

	}

	void MeshExporter::updateTexture(const std::string& fileName, Ref<Mesh> mesh, const uint32_t index, const char* matKey, const aiTextureType type)
	{
		auto* aiMaterial = mesh->mScene->mMaterials[index];

		//	DumpMaterialsToConsole(*mesh->mScene);

		if (aiMaterial->RemoveProperty(matKey, type) != 0)
		{
		//	BEY_CORE_ASSERT(false, "Failed to add property!");
		//	BEY_CORE_ERROR("couldnt remove property");
		}
		if (fileName.empty())
			return;



		char str[FILENAME_MAX];
		*reinterpret_cast<uint32_t*>(str) = uint32_t(fileName.size() + 1);
		fmt::format_to(str + 4, fileName);
		aiMaterial->AddBinaryProperty(str, unsigned(fileName.size() + 4), matKey, type, 0, aiPTI_String);

		//dumpMaterialsToConsole(*mesh->mScene, index, aiPTI_String);

	}


	void MeshExporter::updateMetalnessTexture(const std::string& fileName, Ref<Mesh> mesh, const uint32_t index)
	{
		auto* aiMaterial = mesh->mScene->mMaterials[index];

		//	DumpMaterialsToConsole(*mesh->mScene);
		constexpr static std::array<std::pair<const char*, aiTextureType>, 4> MAT_KEYS{
			std::make_pair(_AI_MATKEY_TEXTURE_BASE, aiTextureType_METALNESS),
			std::make_pair(AI_MATKEY_FBX_MAYA_STINGRAY_METALLIC_TEXTURE, aiTextureType_UNKNOWN),
			std::make_pair(AI_MATKEY_FBX_MAYA_METALNESS_TEXTURE, aiTextureType_UNKNOWN),
			std::make_pair("$raw.ReflectionFactor|file", aiTextureType_NONE) };

		for (const auto& [key, type] : MAT_KEYS)
		{
			if (aiMaterial->RemoveProperty(key, type) != 0)
			{
				//BEY_CORE_ASSERT(false, "Failed to add property!");
				//BEY_CORE_ERROR("Couldn't remove property: {}", key);
			}
			//else
			//	BEY_CORE_INFO("Removed property: {}", key);


		}
		if (fileName.empty())
			return;


		char str[FILENAME_MAX];
		*reinterpret_cast<uint32_t*>(str) = uint32_t(fileName.size() + 1);
		fmt::format_to(str + 4, fileName.data());
		
		aiMaterial->AddBinaryProperty(&str, unsigned(fileName.size() + 4), AI_MATKEY_FBX_MAYA_METALNESS_TEXTURE, aiTextureType_UNKNOWN, 0, aiPTI_String);
		aiMaterial->AddBinaryProperty(&str, unsigned(fileName.size() + 4), _AI_MATKEY_TEXTURE_BASE, aiTextureType_METALNESS, 0, aiPTI_String);
		aiMaterial->AddBinaryProperty(&str, unsigned(fileName.size() + 4), _AI_MATKEY_TEXTURE_BASE, aiTextureType_NONE, 0, aiPTI_String);
		aiMaterial->AddBinaryProperty(&str, unsigned(fileName.size() + 4), _AI_MATKEY_TEXTURE_BASE, aiTextureType_UNKNOWN, 0, aiPTI_String);
		aiMaterial->AddBinaryProperty(&str, unsigned(fileName.size() + 4), AI_MATKEY_FBX_MAYA_STINGRAY_METALLIC_TEXTURE, aiTextureType_UNKNOWN, 0, aiPTI_String);
		aiMaterial->AddBinaryProperty(&str, unsigned(fileName.size() + 4), "$raw.ReflectionFactor|file", aiTextureType_UNKNOWN, 0, aiPTI_String);
		aiMaterial->AddBinaryProperty(&str, unsigned(fileName.size() + 4), "$raw.ReflectionFactor|file", aiTextureType_NONE, 0, aiPTI_String);
		aiMaterial->AddBinaryProperty(&str, unsigned(fileName.size() + 4), "$raw.ReflectionFactor|file", aiTextureType_METALNESS, 0, aiPTI_String);

		//dumpMaterialsToConsole(*mesh->mScene, index, aiPTI_String);

	}
}
