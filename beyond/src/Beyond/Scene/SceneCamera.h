#pragma once

#include "Beyond/Renderer/Camera.h"

namespace bey {

	class SceneCamera final : public Camera 
	{
	public:
		enum class ProjectionType { PERSPECTIVE = 0, ORTHOGRAPHIC = 1 };
	public:
		SceneCamera();
		virtual ~SceneCamera();

		void setPerspective(float verticalFOV, float nearClip = 0.01f, float farClip = 10000.0f);
		void setOrthographic(float size, float nearClip = -1.0f, float farClip = 1.0f);
		//void setViewportSize(uint32_t width, uint32_t height);

		void setPerspectiveVerticalFOV(const float verticalFov) { mPerspectiveFOV = glm::radians(verticalFov); }
		float getPerspectiveVerticalFOV() const { return glm::degrees(mPerspectiveFOV); }
		void setPerspectiveNearClip(const float nearClip) { mPerspectiveNear = nearClip; }
		float getPerspectiveNearClip() const { return mPerspectiveNear; }
		void setPerspectiveFarClip(const float farClip) { mPerspectiveFar = farClip; }
		float getPerspectiveFarClip() const { return mPerspectiveFar; }

		void setOrthographicSize(const float size) { mOrthographicSize = size; }
		float getOrthographicSize() const { return mOrthographicSize; }
		void setOrthographicNearClip(const float nearClip) { mOrthographicNear = nearClip; }
		float getOrthographicNearClip() const { return mOrthographicNear; }
		void setOrthographicFarClip(const float farClip) { mOrthographicFar = farClip; }
		float getOrthographicFarClip() const { return mOrthographicFar; }

		[[nodiscard]] glm::vec3 getViewDirection() const override { return glm::vec3{ 1 }; }


		void setProjectionType(ProjectionType type) { m_ProjectionType = type; }
		ProjectionType getProjectionType() const { return m_ProjectionType; }
	private:
		ProjectionType m_ProjectionType = ProjectionType::PERSPECTIVE;

		float mPerspectiveFOV = glm::radians(45.0f);
		float mPerspectiveNear = 0.01f, mPerspectiveFar = 10000.0f;

		float mOrthographicSize = 10.0f;
		float mOrthographicNear = -1.0f, mOrthographicFar = 1.0f;
	};
	

}