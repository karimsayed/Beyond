#pragma once

#include "Scene.h"

namespace bey {

	class SceneSerializer
	{
	public:
		explicit SceneSerializer(const Ref<Scene>& scene);

		void serialize(const std::string& filepath);
		bool deserialize(const std::string& filepath);
	private:
		Ref<Scene> mScene;
	};


}
