#pragma once

#include <assimp/Exporter.hpp>
#include "Beyond/Core/AssimpUtils.h"

#include "Entity.h"
#include "Beyond/Renderer/Mesh.h"

namespace bey
{
	
class MeshExporter
{
public:
	static void exportMesh(const std::string& fileName, Ref<Mesh> mesh, const glm::mat4& entityTransform);
	void fbxImport(const std::string& fileName, const Entity& entity);
	static void exportScene(const Ref<Scene>& scene, const std::string& exportPath);
	static void updateFactor(float value, Ref<Mesh> mesh, uint32_t index, const char* matKey);
	static void updateFactor(aiColor4D value, Ref<Mesh> mesh, uint32_t index, const char* matKey);
	static void updateTexture(const std::string& fileName, Ref<Mesh> mesh, const uint32_t index, const char* matKey, const aiTextureType type);
	static void updateMetalnessTexture(const std::string& fileName, Ref<Mesh> mesh, uint32_t index);
	//static void updateRootTransformation(Ref<Mesh> mesh, const glm::mat4 mat);


private:

	//std::
	
};


}
