#include "pchs.h"
#include "SceneCamera.h"

#include <glm/gtc/matrix_transform.hpp>

namespace bey {
	
	SceneCamera::SceneCamera()
	{
	}

	SceneCamera::~SceneCamera()
	{
	}

	void SceneCamera::setPerspective(const float verticalFOV, const float nearClip, const float farClip)
	{
		BEY_PROFILE_FUNCTION();
		m_ProjectionType = ProjectionType::PERSPECTIVE;
		mPerspectiveFOV = verticalFOV;
		mPerspectiveNear = nearClip;
		mPerspectiveFar = farClip;
	}

	void SceneCamera::setOrthographic(const float size, const float nearClip, const float farClip)
	{
		BEY_PROFILE_FUNCTION();
		m_ProjectionType = ProjectionType::ORTHOGRAPHIC;
		mOrthographicSize = size;
		mOrthographicNear = nearClip;
		mOrthographicFar = farClip;
	}
	/*
	void SceneCamera::setViewportSize(const uint32_t width, const uint32_t height)
	{
		switch (m_ProjectionType)
		{
		case ProjectionType::PERSPECTIVE:
			mProjectionMatrix = glm::perspectiveFov(mPerspectiveFOV, float(width), float(height), mPerspectiveNear, mPerspectiveFar);
			break;
		case ProjectionType::ORTHOGRAPHIC:
			const float aspect = float(width) / float(height);
			const float width = mOrthographicSize * aspect;
			const float height = mOrthographicSize;
			mProjectionMatrix = glm::ortho(-width * 0.5f, width * 0.5f, -height * 0.5f, height * 0.5f);
			break;
		}
	}
	*/

}