#pragma once

#include "Beyond/Core/UUID.h"
#include "Beyond/Renderer/Mesh.h"
#include "Beyond/Scene/SceneCamera.h"

namespace bey {


	struct IDComponent
	{
		UUID ID = 0;
	};

	struct TagComponent
	{
		std::string Tag;

		TagComponent() = default;
		TagComponent(const TagComponent& other) = default;

		explicit TagComponent(std::string tag)
			: Tag(std::move(tag)) {}

		TagComponent& operator= (const TagComponent& other) = default;
		operator std::string& () { return Tag; }
		operator const std::string& () const { return Tag; }
	};

	struct TransformComponent
	{
		glm::mat4 Transform;

		TransformComponent() = default;
		TransformComponent(const TransformComponent& other) = default;
		explicit TransformComponent(const glm::mat4& transform)
			: Transform(transform) {}
		TransformComponent& operator= (const TransformComponent& other) = default;

		operator glm::mat4& () { return Transform; }
		operator const glm::mat4& () const { return Transform; }
	};

	struct MeshComponent
	{
		Ref<bey::Mesh> Mesh;

		MeshComponent() = default;
		MeshComponent(const MeshComponent& other) = default;
		explicit MeshComponent(const Ref<bey::Mesh> mesh)
			: Mesh(mesh) {}
		explicit MeshComponent(const std::string& path)
			: Mesh (Ref<bey::Mesh>::create(path)) {}
		MeshComponent& operator= (const MeshComponent& other) = default;

		operator Ref<bey::Mesh>() const { return Mesh; }
	};


	struct CameraComponent
	{
		SceneCamera Camera;
		bool Primary = true;

		CameraComponent() = default;
		CameraComponent(const CameraComponent& other) = default;
		CameraComponent& operator= (const CameraComponent& other) = default;

		operator SceneCamera& () { return Camera; }
		operator const SceneCamera& () const { return Camera; }
	};


	struct DirLightComponent
	{
		glm::vec3 Radiance = { 1.0f, 1.0f, 1.0f };
		float Intensity = 1.0f;
		// bool CastsShadows = true;
		bool SoftShadows = true;
		float LightSize = 0.5f; // For PCSS
		UUID UUID{};
	};

	struct PointLightComponent
	{
		glm::vec3 Radiance = { 1.0f, 1.0f, 1.0f };
		float Intensity = 1.0f;
		float LightSize = 0.5f; // For PCSS
		float NearPlane = 1.f;
		float FarPlane = 25.f;
		bool CastsShadows = true;
		bool SoftShadows = true;
		UUID UUID{};
	};

	struct SkyLightComponent
	{
		Environment SceneEnvironment;
		float SkyboxLod = 1.f;
		float Intensity = 1.0f;
		float Angle = 0.0f;
	};

	template <typename  T>
	concept is_component = requires
	{
		std::is_assignable_v<IDComponent, T> || std::is_assignable_v<MeshComponent, T> ||
			std::is_assignable_v<CameraComponent, T> || std::is_assignable_v<DirLightComponent, T> ||
			std::is_assignable_v<SkyLightComponent, T> || std::is_assignable_v<TransformComponent, T> ||
			std::is_assignable_v<TagComponent, T> ||
			std::is_assignable_v<PointLightComponent, T>;
	};


}
