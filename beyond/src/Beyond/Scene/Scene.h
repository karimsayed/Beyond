#pragma once

#include "Beyond/Core/UUID.h"
#include "Beyond/Core/TimeStep.h"
#include "Beyond/Renderer/Material.h"
#include "entt/entt.hpp"
#include "Beyond/Editor/FlyCamera.hpp"

namespace bey {

	struct Environment
	{
		std::string FilePath;
		Ref<TextureCube> RadianceMap;
		Ref<TextureCube> IrradianceMap;
		static Environment load(const std::string& filepath);
	};


	struct DirectionalLight
	{
		glm::vec3 Direction = { 0.0f, 0.0f, 0.0f };
		glm::vec3 Radiance = { 0.0f, 0.0f, 0.0f };
		float Multiplier = 0.0f;
		bool CastsShadows = true;
		UUID UUID;
		glm::vec3 Dummy;


	};

	struct PointLight
	{
		glm::vec3 Position = { 0.0f, 0.0f, 0.0f };
		float Multiplier = 0.0f;
		glm::vec3 Radiance = { 0.0f, 0.0f, 0.0f };
		float NearPlane = 0.001f;
		float FarPlane = 25.0f;
		bool CastsShadows = true;
		UUID UUID;
		float Dummy;
	};

	struct LightEnvironment 
	{
		DirectionalLight DirectionalLights[4];
		std::vector<PointLight> PointLights;
		std::vector<glm::mat4> PointLightMatrices{600};
		std::vector<glm::vec3> DirLightPositions;
	};

	class Entity;
	using EntityMap = std::unordered_map<UUID, Entity>;

	class Scene : public RefCounted
	{
	public:
		explicit Scene(std::string debugName = "Scene");
		~Scene();
		
		void init();

		void onUpdate(TimeStep ts);
		void onRenderEditor(TimeStep ts, const Ref<FlyCamera>& editorCamera);
		void onEvent(Event& e);

		void setViewportSize(uint32_t width, uint32_t height);

		void setEnvironment(const Environment& environment);
		const Environment& getEnvironment() const { return mEnvironment; }

		void setSkybox(const Ref<TextureCube>& skybox);

		Entity getMainCameraEntity();


		Entity createEntity(const std::string& name = "");
		Entity createEntityWithID(const UUID& uuid, const std::string& name = "");
		void destroyEntity(const Entity & entity);
		void duplicateEntity(Entity entity);

		template<typename T>
		auto getAllEntitiesWith() { return mRegistry.view<T>(); }




		
		const EntityMap& getEntityMap() const { return mEntityIDMap; }
		void copyTo(Ref<Scene>& target);

		UUID getUUID() const { return mSceneID; }

		static Ref<Scene> getScene(const UUID& uuid);

		// Editor-specific
		void setSelectedEntity(const entt::entity entity) { mSelectedEntity = entity; }

		static inline float min = 1.6f, max = 44.6f;
		static inline float multiplyer = .1f;

	private:
		UUID mSceneID;
		entt::entity mSceneEntity;
		entt::registry mRegistry;

		std::string mDebugName;
		uint32_t mViewportWidth = 0, mViewportHeight = 0;

		EntityMap mEntityIDMap;
		inline static float mOrthoCoords = 100.f;

		LightEnvironment mLightEnvironment;

		Environment mEnvironment;
		float mEnvironmentIntensity = 1.f;
		float mEnvironmentAngle = 0.f;
		float mSkyboxLod = 1.f;
		Ref<TextureCube> mSkyboxTexture;
		Ref<MaterialInstance> mSkyboxMaterial;

		entt::entity mSelectedEntity {};

		bool mIsPlaying = false;

		friend class Entity;
		friend class SceneRenderer;
		friend class SceneSerializer;
		friend class SceneHierarchyPanel;
	};

}