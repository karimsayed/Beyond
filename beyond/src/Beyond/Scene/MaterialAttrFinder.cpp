#include "pchs.h"
#include "MaterialAttrFinder.hpp"

#include "Beyond/Core/AssimpUtils.h"

namespace bey
{

	std::string MaterialAttrFinder::getFullPath(const std::string& meshPath, const std::string& resultPath)
	{
		const std::filesystem::path path = meshPath;
		auto parentPath = path.parent_path();
		parentPath /= resultPath;
		return parentPath.string();
	}

	std::pair<Ref<Texture2D>, float> MaterialAttrFinder::findMetalness(const std::string& meshPath, aiMaterial* aiMaterial)
	{
		std::string texturePath;

		aiString path;
		bool textureFound = false;
		if (aiMaterial->GetTexture(aiTextureType_METALNESS, 0, &path) == aiReturn_SUCCESS)
		{
			texturePath = path.C_Str();
			textureFound = true;

		}
		else
		{
			for (uint32_t j = 0; j < aiMaterial->mNumProperties; j++)
			{
				const auto* prop = aiMaterial->mProperties[j];
				if (prop->mType == aiPTI_String)
				{
					std::string key = prop->mKey.data;

					if (key == AI_MATKEY_FBX_MAYA_STINGRAY_METALLIC_TEXTURE ||
						key == AI_MATKEY_FBX_MAYA_METALNESS_TEXTURE ||
						key == "$mat.ReflectionFactor|file"||
						key == "$ReflectionFactor"||
						key == "ReflectionFactor"||
						key == "$mat.ReflectionFactor"||
						key == "$raw.ReflectionFactor"||
						key == "$mat.ReflectionFactor|file"||
						key == "$raw.ReflectionFactor|file")
					{
						const std::string str(prop->mData + 4, *reinterpret_cast<uint32_t*>(prop->mData));
						texturePath = str;
						textureFound = true;
						break;
					}
					else
					{
						// const std::string str(prop->mData + 4, *reinterpret_cast<uint32_t*>(prop->mData));

						// BEY_CORE_ERROR("What is this thing with semantic:{}, and data: {}", prop->mSemantic, str);
					}
				}
			}
		}


		float metalness = 0.f;
		if (aiMaterial->Get(AI_MATKEY_FBX_MAYA_METALNESS_FACTOR, 0, 0, metalness) != aiReturn_SUCCESS &&
			aiMaterial->Get(AI_MATKEY_REFLECTIVITY, metalness) != aiReturn_SUCCESS &&
			aiMaterial->Get("$clr.reflective", 0, 0, metalness) != aiReturn_SUCCESS &&
			aiMaterial->Get("$raw.Reflectivity", 0, 0, metalness) != aiReturn_SUCCESS &&
			aiMaterial->Get(AI_MATKEY_FBX_MAYA_STINGRAY_METALLIC_FACTOR, 0, 0, metalness) != aiReturn_SUCCESS)
		{
			metalness = 0.0f; // Default value
		}



		if (textureFound)
		{
			texturePath = getFullPath(meshPath, texturePath);

			auto found = TexturePool::getGlobal()->findAlreadyLoaded(getFullPath(meshPath, texturePath));
			if (found == nullptr)
				found = Texture2D::create(texturePath, DataType::UNSIGNED_BYTE, false, false, aiTextureType_METALNESS);
			return { found, metalness };
		}

		return { nullptr, metalness };

	}

	std::pair<Ref<Texture2D>, float> MaterialAttrFinder::findRoughness(const std::string& meshPath, aiMaterial* aiMaterial)
	{
		std::string texturePath;

		aiString path;
		bool textureFound = false;
		if (aiMaterial->GetTexture(aiTextureType_SHININESS, 0, &path) == aiReturn_SUCCESS)
		{
			texturePath = path.C_Str();
			textureFound = true;

		}
		else
		{
			for (unsigned i = 0; i < aiMaterial->mNumProperties; ++i)
			{
				auto* prop = aiMaterial->mProperties[i];
				std::string key = prop->mKey.data;
				if (prop->mType == aiPTI_String && key == AI_MATKEY_FBX_MAYA_STINGRAY_ROUGHNESS_TEXTURE)
				{
					const uint32_t strLength = *reinterpret_cast<uint32_t*>(prop->mData);
					texturePath = { prop->mData + 4, strLength };
					textureFound = true;
					break;
				}
			}

		}

		float roughness;
		float shininess;

		if (aiMaterial->Get(AI_MATKEY_SHININESS, shininess) == aiReturn_SUCCESS ||
			aiMaterial->Get(AI_MATKEY_SHININESS_STRENGTH, shininess) == aiReturn_SUCCESS)
		{
			roughness = 1.0f - glm::sqrt(shininess / 100.0f);
		}
		else if (aiMaterial->Get(AI_MATKEY_FBX_MAYA_DIFFUSE_ROUGHNESS_FACTOR, 0, 0, roughness) != aiReturn_SUCCESS &&
			aiMaterial->Get(AI_MATKEY_FBX_MAYA_STINGRAY_ROUGHNESS_FACTOR, 0, 0, roughness) != aiReturn_SUCCESS)
		{
			roughness = 1.0f; // Default value
		}

		if (textureFound)
		{

			texturePath = getFullPath(meshPath, texturePath);

			auto found = TexturePool::getGlobal()->findAlreadyLoaded(getFullPath(meshPath, texturePath));
			if (found == nullptr)
				found = Texture2D::create(texturePath, DataType::UNSIGNED_BYTE, false, false, aiTextureType_SHININESS);
			return { found, roughness };
		}

		return { nullptr, roughness };

	}


	Ref<Texture2D> MaterialAttrFinder::findNormalMap(const std::string& meshPath, aiMaterial* aiMaterial)
	{
		std::string texturePath;

		aiString path;
		bool textureFound = false;
		if (aiMaterial->GetTexture(aiTextureType_NORMALS, 0, &path) == aiReturn_SUCCESS || aiMaterial->GetTexture(aiTextureType_HEIGHT, 0, &path) == aiReturn_SUCCESS)
		{
			texturePath = path.C_Str();
			textureFound = true;
		}
		else
		{
			for (unsigned i = 0; i < aiMaterial->mNumProperties; ++i)
			{
				auto* prop = aiMaterial->mProperties[i];
				std::string key = prop->mKey.data;
				if (prop->mType == aiPTI_String && key == "$raw.Maya|TEX_normal_map|file")
				{
					const uint32_t strLength = *reinterpret_cast<uint32_t*>(prop->mData);
					texturePath = { prop->mData + 4, strLength };
					textureFound = true;
					break;
				}
			}
		}

		if (textureFound)
		{

			texturePath = getFullPath(meshPath, texturePath);

			auto found = TexturePool::getGlobal()->findAlreadyLoaded(getFullPath(meshPath, texturePath));
			if (found == nullptr)
				found = Texture2D::create(texturePath, DataType::UNSIGNED_BYTE, false, true, aiTextureType_BASE_COLOR);
			return found;  // NOLINT(clang-diagnostic-return-std-move-in-c++11)
		}

		return nullptr;

	}



	std::pair<Ref<Texture2D>, glm::vec4> MaterialAttrFinder::findColor(const std::string& meshPath, aiMaterial* aiMaterial)
	{
		std::string texturePath;
		glm::vec4 color;

		aiString path;
		bool textureFound = false;
		if (aiMaterial->GetTexture(aiTextureType_DIFFUSE, 0, &path) == aiReturn_SUCCESS ||
			aiMaterial->GetTexture(aiTextureType_BASE_COLOR, 0, &path) == aiReturn_SUCCESS)
		{
			texturePath = path.C_Str();
			textureFound = true;

		}
		else
		{
			for (unsigned i = 0; i < aiMaterial->mNumProperties; ++i)
			{
				auto* prop = aiMaterial->mProperties[i];
				std::string key = prop->mKey.data;
				if (prop->mType == aiPTI_String && (
					key == AI_MATKEY_FBX_MAYA_BASE_COLOR_TEXTURE ||
					key == AI_MATKEY_FBX_MAYA_STINGRAY_COLOR_TEXTURE))
				{
					const uint32_t strLength = *reinterpret_cast<uint32_t*>(prop->mData);
					texturePath = { prop->mData + 4, strLength };
					textureFound = true;
					break;
				}
			}
		}

		aiColor3D aiColor;
		if (aiMaterial->Get(AI_MATKEY_FBX_MAYA_BASE_COLOR_FACTOR, 0, 0, aiColor) == aiReturn_SUCCESS ||
			aiMaterial->Get(AI_MATKEY_COLOR_DIFFUSE, aiColor) == aiReturn_SUCCESS ||
			aiMaterial->Get(AI_MATKEY_FBX_MAYA_STINGRAY_BASE_COLOR_FACTOR, 0, 0, aiColor) == aiReturn_SUCCESS)
		{
			color = glm::vec4{ aiColor.r, aiColor.g,aiColor.b, 1.f };
		}
		else
			color = glm::vec4{ 1.f };


		if (textureFound)
		{

			texturePath = getFullPath(meshPath, texturePath);

			auto found = TexturePool::getGlobal()->findAlreadyLoaded(getFullPath(meshPath, texturePath));
			if (found == nullptr)
				found = Texture2D::create(texturePath, DataType::UNSIGNED_BYTE, true, true, aiTextureType_BASE_COLOR);
			return { found, color };
		}

		return { nullptr, color };

	}

}
