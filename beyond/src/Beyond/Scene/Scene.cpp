#include "pchs.h"
#include "Scene.h"

#include <utility>
#include "Entity.h"
#include "Components.h"
#include "Beyond/Renderer/Renderer.h"
#include "Beyond/Renderer/SceneRenderer.h"
#include "Beyond/Renderer/Renderer2D.h"

namespace bey {

	std::unordered_map<UUID, Scene*> s_ActiveScenes;

	struct SceneComponent
	{
		UUID SceneID;
	};


	Scene::Scene(std::string debugName)
		: mDebugName(std::move(debugName))
	{
		BEY_PROFILE_FUNCTION();

		mSceneEntity = mRegistry.create();
		mRegistry.emplace<SceneComponent>(mSceneEntity, mSceneID);

		s_ActiveScenes[mSceneID] = this;

		init();
	}

	Scene::~Scene()
	{
		BEY_PROFILE_FUNCTION();
		mRegistry.clear();
		s_ActiveScenes.erase(mSceneID);
	}

	void Scene::init()
	{
		BEY_PROFILE_FUNCTION();
		auto skyboxShader = Renderer::getShaderLibrary()->find("assets/shaders/SkyboxShader");
		if (!skyboxShader)
			skyboxShader = Renderer::getShaderLibrary()->add(Shader::create("assets/shaders/SkyboxShader"));
		mSkyboxMaterial = MaterialInstance::create(Material::create(skyboxShader));
		mSkyboxMaterial->setFlag(MaterialFlag::DepthTest, false);
	}

	// Merge OnUpdate/Render into one function?
	void Scene::onUpdate(TimeStep ts)
	{
	}


	void Scene::onRenderEditor(const TimeStep ts, const Ref<FlyCamera>& editorCamera)
	{
		/////////////////////////////////////////////////////////////////////
		// RENDER 3D SCENE
		/////////////////////////////////////////////////////////////////////
		BEY_PROFILE_FUNCTION();
		// Process lights
		{
			mLightEnvironment = LightEnvironment();
			auto dirLights = mRegistry.group<DirLightComponent>(entt::get<TransformComponent>);
			mLightEnvironment.DirLightPositions.resize(dirLights.size());
			for (uint32_t directionalLightIndex = 0; auto entity : dirLights)
			{
				auto [transformComponent, lightComponent] = dirLights.get<TransformComponent, DirLightComponent>(entity);
				const glm::vec3 direction = -glm::normalize(glm::mat3(transformComponent.Transform) * glm::vec3(1.0f));
				mLightEnvironment.DirLightPositions[directionalLightIndex] = transformComponent.Transform[3];
				// mLightEnvironment.DirectionalLights[directionalLightIndex].CastsShadows = lightComponent.CastsShadows;
				mLightEnvironment.DirectionalLights[directionalLightIndex].Direction = direction;
				mLightEnvironment.DirectionalLights[directionalLightIndex].Multiplier = lightComponent.Intensity;
				mLightEnvironment.DirectionalLights[directionalLightIndex].UUID = lightComponent.UUID;
				mLightEnvironment.DirectionalLights[directionalLightIndex++].Radiance = lightComponent.Radiance;
			}

			auto pointLights = mRegistry.group<PointLightComponent>(entt::get<TransformComponent>);
			mLightEnvironment.PointLights.resize(pointLights.size());
			for (uint32_t directionalLightIndex = 0; auto entity : pointLights)
			{
				auto [transformComponent, lightComponent] = pointLights.get<TransformComponent, PointLightComponent>(entity);
				glm::vec4& position = transformComponent.Transform[3];
				mLightEnvironment.PointLights[directionalLightIndex].Radiance = lightComponent.Radiance;
				mLightEnvironment.PointLights[directionalLightIndex].CastsShadows = lightComponent.CastsShadows;
				mLightEnvironment.PointLights[directionalLightIndex].Multiplier = lightComponent.Intensity;
				mLightEnvironment.PointLights[directionalLightIndex].FarPlane = lightComponent.FarPlane;
				mLightEnvironment.PointLights[directionalLightIndex].UUID = lightComponent.UUID;

//				position.y = fmod((position.y + (multiplyer * ts.getMilliseconds()) - min), max);
				mLightEnvironment.PointLights[directionalLightIndex++].Position = position;
			}
			
		
			std::ranges::partition(mLightEnvironment.PointLights, [](auto& light) { return light.CastsShadows; });
			
		}

		// TODO: only one sky light at the moment!
		{
			mEnvironment = Environment();
			auto lights = mRegistry.group<SkyLightComponent>(entt::get<TransformComponent>);
			for (auto entity : lights)
			{
				auto [transformComponent, skyLightComponent] = lights.get<TransformComponent, SkyLightComponent>(entity);
				mEnvironment = skyLightComponent.SceneEnvironment;
				mEnvironmentIntensity = skyLightComponent.Intensity;
				mEnvironmentAngle = skyLightComponent.Angle;
				mSkyboxLod = skyLightComponent.SkyboxLod;
				setSkybox(mEnvironment.RadianceMap);
			}
		}
		

		auto group = mRegistry.group<MeshComponent>(entt::get<TransformComponent>);
		SceneRenderer::beginScene(this, { editorCamera, editorCamera->getViewMatrix() });
		for (auto entity : group)
		{
			auto [transformComponent, meshComponent] = group.get<TransformComponent, MeshComponent>(entity);
			if (meshComponent.Mesh)
			{
				meshComponent.Mesh->onUpdate(ts, glm::inverse(editorCamera->getViewMatrix())[3]);
				if (mSelectedEntity == entity)
					SceneRenderer::submitSelectedMesh(meshComponent, transformComponent);
				else
					SceneRenderer::submitMesh(meshComponent, transformComponent);
			}
		}
		SceneRenderer::endScene();
	}

	void Scene::onEvent(Event& e)
	{
	}

	
	void Scene::setViewportSize(const uint32_t width, const uint32_t height)
	{
		BEY_PROFILE_FUNCTION();
		mViewportWidth = width;
		mViewportHeight = height;
	}

	void Scene::setEnvironment(const Environment& environment)
	{
		BEY_PROFILE_FUNCTION();
		mEnvironment = environment;
		setSkybox(environment.RadianceMap);
	}

	void Scene::setSkybox(const Ref<TextureCube>& skybox)
	{
		BEY_PROFILE_FUNCTION();
		mSkyboxTexture = skybox;
		mSkyboxMaterial->set("u_Texture", skybox);
	}

	Entity Scene::getMainCameraEntity()
	{
		BEY_PROFILE_FUNCTION();
		auto view = mRegistry.view<CameraComponent>();
		for (auto entity : view)
		{
			if (view.get<CameraComponent>(entity).Primary)
				return { entity, this };
		}
		return {};
	}

	Entity Scene::createEntity(const std::string& name)
	{
		BEY_PROFILE_FUNCTION();
		const auto entity = Entity{ mRegistry.create(), this };
		auto& idComponent = entity.addComponent<IDComponent>();
		idComponent.ID = {};

		entity.addComponent<TransformComponent>(glm::mat4(1.0f));
		if (!name.empty())
			entity.addComponent<TagComponent>(name);

		mEntityIDMap[idComponent.ID] = entity;
		return entity;
	}

	Entity Scene::createEntityWithID(const UUID& uuid, const std::string& name)
	{
		BEY_PROFILE_FUNCTION();
		const auto entity = Entity{ mRegistry.create(), this };
		auto& idComponent = entity.addComponent<IDComponent>();
		idComponent.ID = uuid;

		entity.addComponent<TransformComponent>(glm::mat4(1.0f));
		if (!name.empty())
			entity.addComponent<TagComponent>(name);

		BEY_CORE_ASSERT(mEntityIDMap.find(uuid) == mEntityIDMap.end());
		mEntityIDMap[uuid] = entity;
		return entity;
	}

	void Scene::destroyEntity(const Entity & entity)
	{
		BEY_PROFILE_FUNCTION();
		mRegistry.destroy(entity.mEntityHandle);
	}

	template<typename T>
	static void copyComponent(entt::registry& dstRegistry, entt::registry& srcRegistry, const std::unordered_map<UUID, entt::entity>& enttMap)
	{
		BEY_PROFILE_FUNCTION();
		auto components = srcRegistry.view<T>();
		for (auto srcEntity : components)
		{
			entt::entity destEntity = enttMap.at(srcRegistry.get<IDComponent>(srcEntity).ID);

			auto& srcComponent = srcRegistry.get<T>(srcEntity);
			auto& destComponent = dstRegistry.emplace_or_replace<T>(destEntity, srcComponent);
		}
	}

	template<typename T> requires is_component<T>
	static void copyComponentIfExists(entt::entity dst, const entt::entity src, entt::registry& registry)
	{
		BEY_PROFILE_FUNCTION();
		if (registry.has<T>(src))
		{
			auto& srcComponent = registry.get<T>(src);
			registry.emplace_or_replace<T>(dst, srcComponent);
		}
	}

	void Scene::duplicateEntity(Entity entity)
	{
		BEY_PROFILE_FUNCTION();
		Entity newEntity;
		if (entity.hasComponent<TagComponent>())
			newEntity = createEntity(entity.getComponent<TagComponent>().Tag);
		else
			newEntity = createEntity();

		copyComponentIfExists<TransformComponent>(newEntity.mEntityHandle, entity.mEntityHandle, mRegistry);
		copyComponentIfExists<DirLightComponent>(newEntity.mEntityHandle, entity.mEntityHandle, mRegistry);
		copyComponentIfExists<PointLightComponent>(newEntity.mEntityHandle, entity.mEntityHandle, mRegistry);
		copyComponentIfExists<SkyLightComponent>(newEntity.mEntityHandle, entity.mEntityHandle, mRegistry);
		copyComponentIfExists<MeshComponent>(newEntity.mEntityHandle, entity.mEntityHandle, mRegistry);
		copyComponentIfExists<CameraComponent>(newEntity.mEntityHandle, entity.mEntityHandle, mRegistry);
	}

	// Copy to runtime
	void Scene::copyTo(Ref<Scene>& target)
	{
		BEY_PROFILE_FUNCTION();
		// Environment

		target->mLightEnvironment = mLightEnvironment;
		target->mEnvironment = mEnvironment;
		target->mSkyboxTexture = mSkyboxTexture;
		target->mSkyboxMaterial = mSkyboxMaterial;
		target->mSkyboxLod = mSkyboxLod;

		std::unordered_map<UUID, entt::entity> enttMap;
		auto idComponents = mRegistry.view<IDComponent>();
		for (auto entity : idComponents)
		{
			auto uuid = mRegistry.get<IDComponent>(entity).ID;
			const Entity e = target->createEntityWithID(uuid, "");
			enttMap[uuid] = e.mEntityHandle;
		}

		copyComponent<TagComponent>(target->mRegistry, mRegistry, enttMap);
		copyComponent<TransformComponent>(target->mRegistry, mRegistry, enttMap);
		copyComponent<MeshComponent>(target->mRegistry, mRegistry, enttMap);
		copyComponent<CameraComponent>(target->mRegistry, mRegistry, enttMap);
		copyComponent<DirLightComponent>(target->mRegistry, mRegistry, enttMap);
		copyComponent<PointLightComponent>(target->mRegistry, mRegistry, enttMap);
		copyComponent<SkyLightComponent>(target->mRegistry, mRegistry, enttMap);

	}

	Ref<Scene> Scene::getScene(const UUID& uuid)
	{
		BEY_PROFILE_FUNCTION();
		if (s_ActiveScenes.find(uuid) != s_ActiveScenes.end())
			return s_ActiveScenes.at(uuid);

		return {};
	}

	Environment Environment::load(const std::string& filepath)
	{
		BEY_PROFILE_FUNCTION();
		auto [radiance, irradiance] = SceneRenderer::createEnvironmentMap(filepath);
		return { filepath, radiance, irradiance };
	}
}