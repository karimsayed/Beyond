#pragma once
#include "Beyond/Platform/OpenGL/Texture.h"

namespace bey
{

	class MaterialAttrFinder
	{
	public:
		static std::string getFullPath(const std::string& meshPath, const std::string& resultPath);
		static std::pair<Ref<Texture2D>, float> findMetalness(const std::string& meshPath, aiMaterial* aiMaterial);
		static std::pair<Ref<Texture2D>, float> findRoughness(const std::string& meshPath, aiMaterial* aiMaterial);
		static Ref<Texture2D> findNormalMap(const std::string& meshPath, aiMaterial* aiMaterial);
		static std::pair<Ref<Texture2D>, glm::vec4> findColor(const std::string& meshPath, aiMaterial* aiMaterial);
	};

}
