#pragma once


#include "Scene.h"
#include "Components.h"

namespace bey {

	class Entity
	{
	public:
		Entity() = default;
		Entity(entt::entity handle, Scene* scene);


		template<typename T, typename... Args>
		T& addComponent(Args&&... args) const { return mScene->mRegistry.emplace<T>(mEntityHandle, std::forward<Args>(args)...); }

		template<typename T>
		T& getComponent() { return mScene->mRegistry.get<T>(mEntityHandle); }

		template<typename T>
		[[nodiscard]] const T& getComponent() const { return mScene->mRegistry.get<T>(mEntityHandle); }
		template<typename T>
		[[nodiscard]] bool hasComponent() const { return mScene->mRegistry.has<T>(mEntityHandle); }

		template<typename T>
		void removeComponent() const
		{
			BEY_CORE_ASSERT(hasComponent<T>(), "Entity doesn't have component!");
			mScene->mRegistry.remove<T>(mEntityHandle);
		}

		[[nodiscard]] glm::mat4& transform() { return mScene->mRegistry.get<TransformComponent>(mEntityHandle); }
		[[nodiscard]] const glm::mat4& transform() const { return mScene->mRegistry.get<TransformComponent>(mEntityHandle); }

		explicit operator uint32_t () const { return uint32_t(mEntityHandle); }
		explicit operator uint64_t () const { return uint64_t(mEntityHandle); }
		explicit operator entt::entity () const { return mEntityHandle; }
		operator entt::entity () { return mEntityHandle; }
		explicit operator bool () const { return uint32_t(mEntityHandle) && mScene; }

		bool operator==(const Entity& other) const
		{
			return mEntityHandle == other.mEntityHandle && mScene == other.mScene;
		}

		bool operator!=(const Entity& other) const
		{
			return !(*this == other);
		}

		[[nodiscard]] UUID getUUID() const { return getComponent<IDComponent>().ID; }
		[[nodiscard]] UUID getSceneUUID() const { return mScene->getUUID(); }
	private:
		entt::entity mEntityHandle{ };
		Scene* mScene = nullptr;

		friend class Scene;
		friend class SceneSerializer;
	};

}