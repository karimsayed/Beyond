#pragma once
#include <utility>


namespace bey {
	class ShaderStruct;


	enum class ShaderDomain
	{
		None = 0, Vertex = 0, Pixel = 1,
		TessES, TessCS, Comp
	};


	class ShaderUniformDeclaration final : public RefCounted
	{
	private:
		friend class Shader;
		friend class ShaderUniformBufferDeclaration;
	public:
		enum class Type
		{
			NONE, FLOAT32, VEC2, IVEC2, VEC3, VEC4, MAT3, MAT4, UINT32, INT32, STRUCT, BOOL
		};
	private:
		std::string mName;
		uint32_t mSize;
		uint32_t mCount;
		uint32_t mOffset;
		ShaderDomain mDomain;

		Type mType;
		ShaderStruct* mStruct;
		mutable int32_t mLocation;
	public:
		ShaderUniformDeclaration(ShaderDomain domain, Type type, std::string name, uint32_t count = 1);
		ShaderUniformDeclaration(ShaderDomain domain, ShaderStruct* uniformStruct, std::string name, uint32_t count = 1);

		const std::string& getName() const { return mName; }
		uint32_t getSize() const { return mSize; }
		uint32_t getCount() const { return mCount; }
		uint32_t getOffset() const { return mOffset; }
		//uint32_t getAbsoluteOffset() const { return mStruct ? mStruct->getOffset() + mOffset : mOffset; }
		ShaderDomain getDomain() const { return mDomain; }

		int32_t getLocation() const { return mLocation; }
		Type getType() const { return mType; }
		bool isArray() const { return mCount > 1; }
		const ShaderStruct& getShaderUniformStruct() const { BEY_CORE_ASSERT(mStruct, ""); return *mStruct; }
		void setOffset(uint32_t offset);
		bool isStruct() const { return mType == Type::STRUCT; }
	public:
		static uint32_t sizeOfUniformType(Type type);
		static Type stringToType(const std::string& type);
		static std::string typeToString(Type type);
	};
	typedef std::vector<ShaderUniformDeclaration*> ShaderUniformBufferList;

	typedef std::vector<ShaderUniformDeclaration*> ShaderUniformList;




	class ShaderStruct
	{
	private:
		friend class Shader;
	private:
		std::string mName;
		std::vector<ShaderUniformDeclaration*> mFields;
		uint32_t mSize;
		uint32_t mOffset;
	public:
		explicit ShaderStruct(std::string name)
			: mName(std::move(name)), mSize(0), mOffset(0)
		{
		}

		void addField(ShaderUniformDeclaration* field)
		{
			mSize += field->getSize();
			uint32_t offset = 0;
			if (!mFields.empty())
			{
				ShaderUniformDeclaration* previous = mFields.back();
				offset = previous->getOffset() + previous->getSize();
			}
			field->setOffset(offset);
			mFields.push_back(field);
		}

		void setOffset(const uint32_t offset) { mOffset = offset; }

		[[nodiscard]] const std::string& getName() const { return mName; }
		[[nodiscard]] uint32_t getSize() const { return mSize; }
		[[nodiscard]] uint32_t getOffset() const { return mOffset; }
		[[nodiscard]] const std::vector<ShaderUniformDeclaration*>& getFields() const { return mFields; }
	};

	typedef std::vector<ShaderStruct*> ShaderStructList;




	
	class ShaderResourceDeclaration 
	{
	public:
		enum class Type
		{
			NONE, TEXTURE_2D, TEXTURECUBE
		};
	private:
		friend class Shader;
	private:
		std::string mName;
		uint32_t mRegister = 0;
		uint32_t mCount;
		Type mType;
	public:
		ShaderResourceDeclaration(Type type, const std::string& name, uint32_t count);

		[[nodiscard]] const std::string& getName() const { return mName; }
		[[nodiscard]] uint32_t getRegister() const { return mRegister; }
		[[nodiscard]] uint32_t getCount() const { return mCount; }

		[[nodiscard]] Type getType() const { return mType; }
	public:
		[[nodiscard]] static Type stringToType(const std::string& type);
		[[nodiscard]] static std::string typeToString(Type type);
	};

	typedef std::vector<ShaderResourceDeclaration*> ShaderResourceList;




	struct GLShaderUniformField
	{
		ShaderUniformDeclaration::Type Type;
		std::string Name;
		uint32_t Count;
		mutable uint32_t Size;
		mutable int32_t Location;
	};

	class ShaderUniformBufferDeclaration : public RefCounted
	{
	private:
		friend class Shader;
	private:
		std::string mName;
		ShaderUniformList mUniforms;
		uint32_t mRegister;
		uint32_t mSize;
		ShaderDomain mDomain;
	public:
		ShaderUniformBufferDeclaration(const std::string& name, ShaderDomain domain);

		void pushUniform(ShaderUniformDeclaration* uniform);

		[[nodiscard]] const std::string& getName() const { return mName; }
		[[nodiscard]] uint32_t getRegister() const { return mRegister; }
		[[nodiscard]] uint32_t getSize() const { return mSize; }
		[[nodiscard]] virtual ShaderDomain getDomain() const { return mDomain; }
		[[nodiscard]] const ShaderUniformList& getUniformDeclarations() const { return mUniforms; }

		ShaderUniformDeclaration* findUniform(const std::string& name);
	};

}
