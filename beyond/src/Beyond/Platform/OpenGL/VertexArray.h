#pragma once
#include "GLBuffer.h"


namespace bey {

	class VertexArray final : public RefCounted
	{
	public:
		VertexArray();
		virtual ~VertexArray();
		static Ref<VertexArray> create();


		void bind() const;
		void unbind() const;

		void addVertexBuffer(const Ref<VertexBuffer>& vertexBuffer);
		void setIndexBuffer(const Ref<IndexBuffer>& indexBuffer);

		const std::vector<Ref<VertexBuffer>>& getVertexBuffers() const { return mVertexBuffers; }
		const Ref<IndexBuffer>& getIndexBuffer() const { return mIndexBuffer; }

		RendererID getRendererID() const { return mRendererID; };
	private:
		RendererID mRendererID = 0;
		uint32_t mVertexBufferIndex = 0;
		std::vector<Ref<VertexBuffer>> mVertexBuffers;
		Ref<IndexBuffer> mIndexBuffer;
	};

}
