#include "pchs.h"
#include "VertexArray.h"


#include "Beyond/Renderer/Renderer.h"

namespace bey {

	
	Ref<VertexArray> VertexArray::create()
	{
		BEY_PROFILE_FUNCTION();
		return Ref<VertexArray>::create();
	}

	
	constexpr static GLenum shaderDataTypeToOpenGlBaseType(const ShaderDataType type)
	{
		BEY_PROFILE_FUNCTION();

		switch (type)
		{
			case bey::ShaderDataType::Float:    return GL_FLOAT;
			case bey::ShaderDataType::Float2:   return GL_FLOAT;
			case bey::ShaderDataType::Float3:   return GL_FLOAT;
			case bey::ShaderDataType::Float4:   return GL_FLOAT;
			case bey::ShaderDataType::Mat3:     return GL_FLOAT;
			case bey::ShaderDataType::Mat4:     return GL_FLOAT;
			case bey::ShaderDataType::UInt:     return GL_UNSIGNED_INT;
			case bey::ShaderDataType::Int:      return GL_INT;
			case bey::ShaderDataType::Int2:     return GL_INT;
			case bey::ShaderDataType::Int3:     return GL_INT;
			case bey::ShaderDataType::Int4:     return GL_INT;
			case bey::ShaderDataType::Bool:     return GL_BOOL;
		}

		BEY_CORE_ASSERT(false, "Unknown ShaderDataType!");
		return 0;
	}

	VertexArray::VertexArray()
	{
		BEY_PROFILE_FUNCTION();

		Renderer::submit([this]() {
			BEY_PROFILE_SCOPE("glCreateVertexArrays");
			glCreateVertexArrays(1, &mRendererID);
		});
	}

	VertexArray::~VertexArray()
	{
		BEY_PROFILE_FUNCTION();

		GLuint rendererID = mRendererID;
		Renderer::submit([rendererID]() {
			BEY_PROFILE_SCOPE("glDeleteVertexArrays");
			glDeleteVertexArrays(1, &rendererID);
		});
	}

	void VertexArray::bind() const
	{
		BEY_PROFILE_FUNCTION();

		Ref<const VertexArray> instance = this;
		Renderer::submit([instance]() {
			BEY_PROFILE_SCOPE("glBindVertexArray");
			glBindVertexArray(instance->mRendererID);
		});
	}

	void VertexArray::unbind() const
	{
		BEY_PROFILE_FUNCTION();

		Ref<const VertexArray> instance = this;
		Renderer::submit([this]() {
			BEY_PROFILE_SCOPE("glBindVertexArray(0)");
			glBindVertexArray(0);
		});
	}

	void VertexArray::addVertexBuffer(const Ref<VertexBuffer>& vertexBuffer)
	{
		BEY_PROFILE_FUNCTION();

		BEY_CORE_ASSERT(vertexBuffer->getLayout().getElements().size(), "Vertex Buffer has no layout!");

		bind();
		vertexBuffer->bind();

		Ref<VertexArray> instance = this;
		Renderer::submit([instance, vertexBuffer]() mutable {
			BEY_PROFILE_SCOPE("uploading VertArr");

			const auto& layout = vertexBuffer->getLayout();
			for (const auto& element : layout)
			{
				const auto glBaseType = shaderDataTypeToOpenGlBaseType(element.Type);
				glEnableVertexAttribArray(instance->mVertexBufferIndex);
				if (glBaseType == GL_INT)
				{
					glVertexAttribIPointer(instance->mVertexBufferIndex,
						element.getComponentCount(),
						glBaseType,
						layout.getStride(),
						reinterpret_cast<const void*>(intptr_t(element.Offset)));
				}
				else
				{
					glVertexAttribPointer(instance->mVertexBufferIndex,
						element.getComponentCount(),
						glBaseType,
						element.Normalized ? GL_TRUE : GL_FALSE,
						layout.getStride(),
						reinterpret_cast<const void*>(intptr_t(element.Offset)));
				}
				instance->mVertexBufferIndex++;
			}
		});
		mVertexBuffers.push_back(vertexBuffer);
	}

	void VertexArray::setIndexBuffer(const Ref<IndexBuffer>& indexBuffer)
	{
		BEY_PROFILE_FUNCTION();

		bind();
		indexBuffer->bind();

		mIndexBuffer = indexBuffer;
	}

}
