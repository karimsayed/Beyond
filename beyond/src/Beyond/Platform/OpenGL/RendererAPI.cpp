#include "pchs.h"
#include "RendererAPI.h"



namespace bey {

	static void openGlLogMessage(GLenum, const GLenum type, GLuint, const GLenum severity, GLsizei, const GLchar* message, const void*)
	{
		switch (severity)
		{
			case GL_DEBUG_SEVERITY_HIGH:
				BEY_CORE_ERROR("[OpenGL Debug HIGH][{0:X}] {1}", type, message);
				BEY_CORE_ASSERT(false, "GL_DEBUG_SEVERITY_HIGH");  // NOLINT(clang-diagnostic-extra-semi-stmt)
				break;
			case GL_DEBUG_SEVERITY_MEDIUM:
				BEY_CORE_WARN("[OpenGL Debug MEDIUM][{0:X}] {1}", type, message);
#ifdef BEY_DEBUG
				BEY_CORE_ASSERT(false, "GL_DEBUG_SEVERITY_MEDIUM");
#endif
				break;
			case GL_DEBUG_SEVERITY_LOW:
				//BEY_CORE_INFO("[OpenGL Debug LOW][{0}] {1}", type, message);
				break;
			case GL_DEBUG_SEVERITY_NOTIFICATION:
				// BEY_CORE_TRACE("[OpenGL Debug NOTIFICATION] {0}", message);
				[[fallthrough]];
			default: break;
		}
	}

	void RendererAPI::init()
	{
		BEY_PROFILE_FUNCTION();
		
#ifdef BEY_DEBUG
		glDebugMessageCallback(openGlLogMessage, nullptr);
		glEnable(GL_DEBUG_OUTPUT);
		glEnable(GL_DEBUG_OUTPUT_SYNCHRONOUS);
#else
		glDisable(GL_DEBUG_OUTPUT);
		glDisable(GL_DEBUG_OUTPUT_SYNCHRONOUS);
#endif


		unsigned int vao {0};
		glCreateVertexArrays(1, &vao);
		glBindVertexArray(vao);

		glEnable(GL_DEPTH_TEST);
		glEnable(GL_CULL_FACE);
		glEnable(GL_TEXTURE_CUBE_MAP_SEAMLESS);
		glFrontFace(GL_CCW);


		
		//glEnable(GL_MULTISAMPLE);
		glEnable(GL_STENCIL_TEST);

		auto& caps = getCapabilities();

		caps.Vendor = (const char*)glGetString(GL_VENDOR);
		caps.Renderer = (const char*)glGetString(GL_RENDERER);
		caps.Version = (const char*)glGetString(GL_VERSION);

		
		glGetIntegerv(GL_MAX_SAMPLES, &caps.MaxSamples);
		glGetIntegerv(GL_MAX_TEXTURE_MAX_ANISOTROPY, &caps.MaxAnisotropy);

		glGetIntegerv(GL_MAX_COMBINED_TEXTURE_IMAGE_UNITS, &caps.MaxTextureUnits);

		glGetIntegerv(GL_MAX_PATCH_VERTICES, &caps.MaxPatchVertices);
		//glPatchParameteri(GL_PATCH_VERTICES, 3);

		GLenum error = glGetError();
		while (error != GL_NO_ERROR)
		{
			BEY_CORE_ERROR("OpenGL Error {0}", error);
			error = glGetError();
		}

		loadRequiredAssets();
	}

	void RendererAPI::shutdown()
	{
		BEY_PROFILE_FUNCTION();
	}

	void RendererAPI::loadRequiredAssets()
	{
		BEY_PROFILE_FUNCTION();
	}

	void RendererAPI::clearDepth()
	{
		BEY_PROFILE_FUNCTION();
		glClear(GL_DEPTH_BUFFER_BIT);
	}

	void RendererAPI::clear(const float r, const float g, const float b, const float a)
	{
		BEY_PROFILE_FUNCTION();
		glClearColor(r, g, b, a);
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);
	}

	void RendererAPI::setClearColor(const float r, const float g, const float b, const float a)
	{
		BEY_PROFILE_FUNCTION();
		glClearColor(r, g, b, a);
	}

	void RendererAPI::drawArrays(const uint32_t first, const uint32_t count, const PrimitiveType type, const bool depthTest)
	{
		BEY_PROFILE_FUNCTION();
		if (!depthTest)
			glDisable(GL_DEPTH_TEST);
		GLenum glPrimitiveType = 0;
		switch (type)
		{
		case PrimitiveType::Triangles:
			glPrimitiveType = GL_TRIANGLES;
			break;
		case PrimitiveType::Lines:
			glPrimitiveType = GL_LINES;
			break;
		case PrimitiveType::TriangleStrip:
			glPrimitiveType = GL_TRIANGLE_STRIP;
			break;
		case PrimitiveType::None: break;
		default: break;
		}

		glDrawArrays(glPrimitiveType, first, count);

		if (!depthTest)
			glEnable(GL_DEPTH_TEST);
	}

	void RendererAPI::drawIndexed(const uint32_t count, const PrimitiveType type, const bool depthTest)
	{
		BEY_PROFILE_FUNCTION();
		if (!depthTest)
			glDisable(GL_DEPTH_TEST);

		GLenum glPrimitiveType = 0;
		switch (type)
		{
			case PrimitiveType::Triangles:
				glPrimitiveType = GL_TRIANGLES;
				break;
			case PrimitiveType::Lines:
				glPrimitiveType = GL_LINES;
				break;
			case PrimitiveType::TriangleStrip:
				glPrimitiveType = GL_TRIANGLE_STRIP;
			default: break;
		}

		glDrawElements(glPrimitiveType, count, GL_UNSIGNED_INT, nullptr);

		if (!depthTest)
			glEnable(GL_DEPTH_TEST);
	}

	void RendererAPI::setLineThickness(const float thickness)
	{
		BEY_PROFILE_FUNCTION();
		glLineWidth(thickness);
	}

}