#include "pchs.h"
#include "RenderPass.h"

namespace bey {

	
	Ref<RenderPass> RenderPass::create(const RenderPassSpecification& spec)
	{
		BEY_PROFILE_FUNCTION();
		return Ref<RenderPass>::create(spec);
	}

	
	RenderPass::RenderPass(const RenderPassSpecification& spec)
		: mSpecification(spec)
	{

	}

	RenderPass::~RenderPass()
	{

	}

}