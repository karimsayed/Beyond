#pragma once
#include "RendererAPI.h"
#include "Texture.h"


namespace bey {


	
	struct FramebufferSpecification
	{
		uint32_t Width = 1280;
		uint32_t Height = 720;
		uint32_t Samples = 1; // multisampling
		uint32_t ColorCount = 0;
		uint32_t DepthCount = 0;
		uint32_t CubeMapCount = 0;
		uint32_t TotalTextures = 0;
		bool Stenciled = false;
		bool DrawBuffer = false;
		bool ArrayTextures {false};
		bool IsResizable = true;
		bool GenerateLODs = false;
		std::vector<int> TextureTargets;
		std::vector<int> TextureFormats;
		std::vector<int> TextureIFormats;
		std::vector<int> TextureDataType;
		std::vector<int> TextureAttachment;
		glm::vec4 ClearColor = glm::vec4(1);
		bool HBAO = false;
	};

	

	class FrameBuffer final : public RefCounted
	{
	public:
		explicit FrameBuffer(const FramebufferSpecification& spec);
		static Ref<FrameBuffer> create(const FramebufferSpecification& spec);
		~FrameBuffer();
		void resize(uint32_t width, uint32_t height, bool forceRecreate = false);

		void bindForWritingDepth(size_t index);
		void bindArrayForWritingDepth(size_t index, GLenum cubeFace);
		void bindCubemapLayer(size_t index);
		void bindForWritingStencil(size_t index);
		void bindForWritingDepthStencil(size_t index);
		void bindForWriting(size_t index);
		void setNumberOfComponents(const size_t count) { mCascadedMaps.resize(count); }
		void bindTexture(size_t index, uint32_t slot = 0);
		RendererID getTextureID(const size_t index) { return mCascadedMaps[index]->getRendererID(); }

		RendererID bindAndGetTextureID(const uint32_t index)
		{
			mCascadedMaps[index]->bind();
			return mCascadedMaps[index]->getRendererID();
		}

		Ref<Texture2D> getTexture(const int i) { return mCascadedMaps[i]; }

		FramebufferSpecification getSpecification() const { return mSpecification; }
		uint32_t getWidth() const { return mSpecification.Width; }
		uint32_t getHeight() const { return mSpecification.Height; }
		void bind();
		void generateMipMaps();

		Ref<Texture> getTextureArray()
		{
			return mTextureArray;
		}

		static void unBind();
		GLuint& getRendererID() { return mRendererID; }

	private:

		
		std::vector<Ref<Texture>> mCascadedMaps;
		Ref<Texture> mTextureArray;
		FramebufferSpecification mSpecification;
		RendererID mRendererID = 0;
	
		
	};


	class FramebufferPool final
	{
	public:
		explicit FramebufferPool(uint32_t maxFBs = 32);

		static std::weak_ptr<FrameBuffer> allocateBuffer();
		void add(Ref<FrameBuffer> framebuffer);

		[[nodiscard]] std::vector<Ref<FrameBuffer>>& getAll() { return mPool; }
		[[nodiscard]] const std::vector<Ref<FrameBuffer>>& getAll() const { return mPool; }

		static FramebufferPool* getGlobal() { return sInstance; }
	private:
		std::vector<Ref<FrameBuffer>> mPool;

		static FramebufferPool* sInstance;
	};

}
