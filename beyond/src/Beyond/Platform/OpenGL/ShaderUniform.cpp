#include "pchs.h"
#include "ShaderUniform.h"

#include <utility>

namespace bey {

	ShaderUniformDeclaration::ShaderUniformDeclaration(const ShaderDomain domain, const Type type, std::string name, const uint32_t count)
		: mName(std::move(name)), mSize(sizeOfUniformType(type)* count), mCount(count), mOffset(0), mDomain(domain), mType(type), mStruct(nullptr), mLocation(-1)
	{
	}

	ShaderUniformDeclaration::ShaderUniformDeclaration(const ShaderDomain domain, ShaderStruct* uniformStruct, std::string name, const uint32_t count)
		: mName(std::move(name)), mCount(count), mOffset(0), mDomain(domain), mType(Type::STRUCT), mStruct(uniformStruct), mLocation(-1)
	{
		mSize = mStruct->getSize() * count;
	}

	void ShaderUniformDeclaration::setOffset(const uint32_t offset)
	{
		if (mType == Type::STRUCT)
			mStruct->setOffset(offset);

		mOffset = offset;
	}

	uint32_t ShaderUniformDeclaration::sizeOfUniformType(const Type type)
	{
		switch (type)
		{
		case Type::INT32: [[fallthrough]];
		case Type::UINT32: [[fallthrough]];
		case Type::BOOL: [[fallthrough]];
		case Type::FLOAT32:    return 4;
		case Type::VEC2:       return 4 * 2;
		case Type::IVEC2:       return 4 * 2;
		case Type::VEC3:       return 4 * 3;
		case Type::VEC4:       return 4 * 4;
		case Type::MAT3:       return 4 * 3 * 3;
		case Type::MAT4:       return 4 * 4 * 4;
		case Type::NONE: [[fallthrough]];
		case Type::STRUCT:	   return 0;
		default:			   BEY_CORE_ASSERT(false, "Unknown Type!");
		}
		return 0;
	}

	ShaderUniformDeclaration::Type ShaderUniformDeclaration::stringToType(const std::string& type)
	{
		if (type == "uint")      return Type::UINT32;
		if (type == "int")      return Type::INT32;
		if (type == "float")    return Type::FLOAT32;
		if (type == "bool")    return Type::BOOL;
		if (type == "vec2")     return Type::VEC2;
		if (type == "ivec2")     return Type::IVEC2;
		if (type == "vec3")     return Type::VEC3;
		if (type == "vec4")     return Type::VEC4;
		if (type == "mat3")     return Type::MAT3;
		if (type == "mat4")     return Type::MAT4;

		return Type::NONE;
	}

	std::string ShaderUniformDeclaration::typeToString(const Type type)
	{
		switch (type)
		{
		case Type::UINT32:		return "uint32";
		case Type::INT32:		return "int32";
		case Type::FLOAT32:		return "float";
		case Type::BOOL:		return "bool";
		case Type::VEC2:		return "vec2";
		case Type::IVEC2:		return "ivec2";
		case Type::VEC3:		return "vec3";
		case Type::VEC4:		return "vec4";
		case Type::MAT3:		return "mat3";
		case Type::MAT4:		return "mat4";
		default:				return "Invalid Type";
		}
	}

	ShaderUniformBufferDeclaration::ShaderUniformBufferDeclaration(const std::string& name, const ShaderDomain domain)
		: mName(name), mRegister(0), mSize(0), mDomain(domain)
	{
	}

	void ShaderUniformBufferDeclaration::pushUniform(ShaderUniformDeclaration* uniform)
	{
		BEY_PROFILE_FUNCTION();
		uint32_t offset = 0;
		if (!mUniforms.empty())
		{
			auto* previous = static_cast<ShaderUniformDeclaration*>(mUniforms.back());
			offset = previous->mOffset + previous->mSize;
		}
		uniform->setOffset(offset);
		mSize += uniform->getSize();
		mUniforms.push_back(uniform);
	}

	ShaderUniformDeclaration* ShaderUniformBufferDeclaration::findUniform(const std::string& name)
	{
		BEY_PROFILE_FUNCTION();
		for (ShaderUniformDeclaration* uniform : mUniforms)
		{
			if (uniform->getName() == name)
				return uniform;
		}
		return nullptr;
	}

	ShaderResourceDeclaration::ShaderResourceDeclaration(const Type type, const std::string& name, const uint32_t count)
		: mName(name), mCount(count), mType(type)
	{
	}

	ShaderResourceDeclaration::Type ShaderResourceDeclaration::stringToType(const std::string& type)
	{
		if (type == "sampler2D")    return Type::TEXTURE_2D;
		if (type == "sampler2DArray")    return Type::TEXTURE_2D;
		if (type == "sampler2DMS")  return Type::TEXTURE_2D;
		if (type == "samplerCube")  return Type::TEXTURECUBE;
		if (type == "image2D")  return Type::TEXTURE_2D;
		if (type == "image2DArray")  return Type::TEXTURE_2D;

		return Type::NONE;
	}

	std::string ShaderResourceDeclaration::typeToString(const Type type)
	{
		switch (type)
		{
		case Type::TEXTURE_2D:	return "sampler2D";
		case Type::TEXTURECUBE:	return "samplerCube";
		default:				return "Invalid Type";
		}
	}

}