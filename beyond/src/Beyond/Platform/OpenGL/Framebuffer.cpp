#include "pchs.h"
#include "Framebuffer.h"

#include "Beyond/Renderer/Renderer.h"

namespace bey {

	FramebufferPool* FramebufferPool::sInstance = new FramebufferPool;

	FramebufferPool::FramebufferPool(uint32_t maxFBs /* = 32 */)
	{

	}


	std::weak_ptr<FrameBuffer> FramebufferPool::allocateBuffer()
	{
		// m_Pool.push_back();
		return std::weak_ptr<FrameBuffer>();
	}

	void FramebufferPool::add(const Ref<FrameBuffer> framebuffer)
	{
		mPool.push_back(framebuffer);
	}



	FrameBuffer::FrameBuffer(const FramebufferSpecification& spec)
		: mSpecification(spec)
	{
		BEY_PROFILE_FUNCTION();
		mSpecification.TotalTextures = (mSpecification.ColorCount + mSpecification.DepthCount);

		resize(spec.Width, spec.Height, true);
	}

	Ref<FrameBuffer> FrameBuffer::create(const FramebufferSpecification& spec)
	{
		BEY_PROFILE_FUNCTION();
		Ref<FrameBuffer> result = Ref<FrameBuffer>::create(spec);
		FramebufferPool::getGlobal()->add(result);
		return result;
	}

	FrameBuffer::~FrameBuffer()
	{
		RendererID rendererID = mRendererID;
		Renderer::submit([rendererID]() {
			BEY_PROFILE_SCOPE("delete Framebuffer");
			glDeleteFramebuffers(1, &rendererID);
			});
	}

	void FrameBuffer::bind()
	{
		Ref<FrameBuffer> ptr = this;
		Renderer::submit([ptr]() {
			BEY_PROFILE_SCOPE("bind Framebuffer");
			glBindFramebuffer(GL_DRAW_FRAMEBUFFER, ptr->mRendererID);
			glViewport(0, 0, ptr->mSpecification.Width, ptr->mSpecification.Height);
			});
	}

	void FrameBuffer::generateMipMaps()
	{
		for (auto cascadedMap : mCascadedMaps)
		{
			Renderer::submit([cascadedMap]
				{
					glGenerateTextureMipmap(cascadedMap->getRendererID());
				});
		}
	}


	void FrameBuffer::unBind()
	{
		Renderer::submit([] {
			BEY_PROFILE_SCOPE("unbind Framebuffer");
			glBindFramebuffer(GL_FRAMEBUFFER, 0);
			});
	}

	void FrameBuffer::resize(const uint32_t width, const uint32_t height, const bool forceRecreate)
	{
		BEY_PROFILE_FUNCTION();
		if (!forceRecreate && (mSpecification.Width == width && mSpecification.Height == height))
			return;
		//BEY_CORE_INFO("X: {}, Y: {}", width, height);


		mSpecification.Width = width;
		mSpecification.Height = height;

		mCascadedMaps.resize(mSpecification.TotalTextures);
		for (auto& X : mCascadedMaps)
			X = Texture2D::create(mSpecification.Width, mSpecification.Height);
		if (mSpecification.ArrayTextures)
			mTextureArray = Texture2D::create(mSpecification.Width, mSpecification.Height);

		Ref<FrameBuffer> ptr = this;
		Renderer::submit([ptr]() mutable
			{
				BEY_PROFILE_SCOPE("resize submittion Framebuffer");

				if (ptr->mRendererID)
				{
					glDeleteFramebuffers(1, &ptr->mRendererID);
					std::vector<unsigned> vec(ptr->mCascadedMaps.size());
					for (size_t i = 0; i < ptr->mCascadedMaps.size(); ++i)
						vec[i] = ptr->mCascadedMaps[i]->getRendererID();
					glDeleteTextures(ptr->mSpecification.TotalTextures, vec.data());//when changing to renderbuffers this would ruin the second resizing(that is resizing the framebuffer to correct size.
				}
				glCreateFramebuffers(1, &ptr->mRendererID);
				glBindFramebuffer(GL_FRAMEBUFFER, ptr->mRendererID);

#if POINTLIGHTS
				if (ptr->getSpecification().ArrayTextures)
				{

					const auto internalFormat = ptr->mSpecification.TextureIFormats[0];
					const auto format = ptr->mSpecification.TextureFormats[0];
					const auto type = ptr->mSpecification.TextureDataType[0];

					// Create the depth buffer
					glGenTextures(1, &ptr->mCascadedMaps[0]->getRendererID());
					glBindTexture(GL_TEXTURE_2D, ptr->mCascadedMaps[0]->getRendererID());
					glTexImage2D(GL_TEXTURE_2D, 0, GL_DEPTH_COMPONENT32, width, width, 0, GL_DEPTH_COMPONENT, GL_FLOAT, nullptr);
					glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
					glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
					glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
					glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
					glBindTexture(GL_TEXTURE_2D, 0);


					// Create the cube map
					glGenTextures(1, &ptr->mCascadedMaps[1]->getRendererID());
					glBindTexture(GL_TEXTURE_CUBE_MAP, ptr->mCascadedMaps[1]->getRendererID());
					glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
					glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
					glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
					glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
					glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_R, GL_CLAMP_TO_EDGE);

					for (uint32_t i = 0; i < 6u; i++)
						glTexImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_X + i, 0, GL_R32F, width, width, 0, GL_RED, GL_FLOAT, nullptr);


				}
#elif 1

				if (ptr->getSpecification().HBAO)
				{
					glGenTextures(1, &ptr->mTextureArray->getRendererID());
					glBindTexture(GL_TEXTURE_2D_ARRAY, ptr->mTextureArray->getRendererID());
					glTexStorage3D(GL_TEXTURE_2D_ARRAY, 1, GL_RGBA16F, ptr->mSpecification.Width, ptr->mSpecification.Height, 16);
					glTexParameteri(GL_TEXTURE_2D_ARRAY, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
					glTexParameteri(GL_TEXTURE_2D_ARRAY, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
					glTexParameteri(GL_TEXTURE_2D_ARRAY, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
					glTexParameteri(GL_TEXTURE_2D_ARRAY, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
					glBindTexture(GL_TEXTURE_2D_ARRAY, 0);
				}
				else if (ptr->getSpecification().ArrayTextures)
				{

					const auto internalFormat = ptr->mSpecification.TextureIFormats[0];
					const auto format = ptr->mSpecification.TextureFormats[0];
					const auto type = ptr->mSpecification.TextureDataType[0];

					// Create the depth buffer
					glGenTextures(1, &ptr->mTextureArray->getRendererID());
					glBindTexture(GL_TEXTURE_2D_ARRAY, ptr->mTextureArray->getRendererID());
					glTexStorage3D(GL_TEXTURE_2D_ARRAY, 1, GL_R32F, ptr->mSpecification.Width, ptr->mSpecification.Height, 16);
					glTexParameteri(GL_TEXTURE_2D_ARRAY, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
					glTexParameteri(GL_TEXTURE_2D_ARRAY, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
					glTexParameteri(GL_TEXTURE_2D_ARRAY, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
					glTexParameteri(GL_TEXTURE_2D_ARRAY, GL_TEXTURE_MAG_FILTER, GL_NEAREST);


					for (int i = 0; i < 16; i++)
					{
						if (ptr->mCascadedMaps[i])
						{
							glDeleteTextures(1, &ptr->mCascadedMaps[i]->getRendererID());
						}
						glGenTextures(1, &ptr->mCascadedMaps[i]->getRendererID());
						glTextureView(ptr->mCascadedMaps[i]->getRendererID(), GL_TEXTURE_2D, ptr->mTextureArray->getRendererID(), GL_R32F, 0, 1, i, 1);
						glBindTexture(GL_TEXTURE_2D, ptr->mCascadedMaps[i]->getRendererID());
						glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
						glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
						glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
						glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
					}


				}
#endif
				else
					for (unsigned i = 0; i < ptr->mSpecification.TotalTextures; ++i)
					{
						const auto target = ptr->mSpecification.TextureTargets[i];
						const bool multiSampled = target == GL_TEXTURE_2D_MULTISAMPLE;
						const auto internalFormat = ptr->mSpecification.TextureIFormats[i];
						const auto format = ptr->mSpecification.TextureFormats[i];
						const auto type = ptr->mSpecification.TextureDataType[i];
						glCreateTextures(target, 1, &ptr->mCascadedMaps[i]->getRendererID());
						glBindTexture(target, ptr->mCascadedMaps[i]->getRendererID());
						if (multiSampled)
							glTexImage2DMultisample(target, ptr->mSpecification.Samples, internalFormat, ptr->mSpecification.Width, ptr->mSpecification.Height, false);
						else
						{
							if (ptr->mSpecification.GenerateLODs)
							{
								glTexStorage2D(target, 5, internalFormat, ptr->mSpecification.Width, ptr->mSpecification.Height);
								//glTexSubImage2D(target, 0, 0, 0, width, height, format, type, nullptr);
							}
							else
								glTexImage2D(target, 0, internalFormat, ptr->mSpecification.Width, ptr->mSpecification.Height, 0, format, type, nullptr);

							if (format == GL_RGBA || format == GL_RED)
							{
								glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
								glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
							}
							else
							{
								//float borderColor[] = { 1.0, 1.0, 1.0, 1.0 };
								//glTexParameterfv(GL_TEXTURE_2D, GL_TEXTURE_BORDER_COLOR, borderColor);
								glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
								glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
								glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
								glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
								glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_COMPARE_MODE, GL_NONE);
								/*			if (attachment == GL_DEPTH_ATTACHMENT)
												glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_COMPARE_MODE, GL_COMPARE_REF_TO_TEXTURE);
											else
												glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_COMPARE_MODE, GL_COMPARE_R_TO_TEXTURE);*/

							}
							if (ptr->mSpecification.GenerateLODs)
							{
								glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
								glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
								glTexParameteri(GL_TEXTURE_2D, GL_GENERATE_MIPMAP, GL_TRUE);
								//glGenerateMipmap(GL_TEXTURE_2D);
							}
						}

					}

				glBindFramebuffer(GL_FRAMEBUFFER, ptr->mRendererID);

				if (ptr->mSpecification.DrawBuffer)
				{
					constexpr static unsigned ATTACHMENTS[] = { GL_COLOR_ATTACHMENT0, GL_COLOR_ATTACHMENT1, GL_COLOR_ATTACHMENT2, GL_COLOR_ATTACHMENT3, GL_COLOR_ATTACHMENT4, GL_COLOR_ATTACHMENT5,
						GL_COLOR_ATTACHMENT6, GL_COLOR_ATTACHMENT7, GL_COLOR_ATTACHMENT8, GL_COLOR_ATTACHMENT9, GL_COLOR_ATTACHMENT10, GL_COLOR_ATTACHMENT11, GL_COLOR_ATTACHMENT12,
					GL_COLOR_ATTACHMENT13 , GL_COLOR_ATTACHMENT14 , GL_COLOR_ATTACHMENT15 ,GL_COLOR_ATTACHMENT16 ,GL_COLOR_ATTACHMENT17 ,GL_COLOR_ATTACHMENT18 ,GL_COLOR_ATTACHMENT19 };
					if (ptr->mSpecification.ArrayTextures)
						glDrawBuffers(8, ATTACHMENTS);
					else
						glDrawBuffers(ptr->mSpecification.ColorCount, ATTACHMENTS);
					glReadBuffer(GL_NONE);
				}
				else
				{
					// Disable writes to the color buffer
					glDrawBuffer(GL_NONE);
					glReadBuffer(GL_NONE);
				}

				glBindFramebuffer(GL_FRAMEBUFFER, ptr->mRendererID);

				if (ptr->getSpecification().ArrayTextures)
					glFramebufferTexture(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, ptr->mTextureArray->getRendererID(), 0);
				else
					for (unsigned i = 0; i < ptr->mSpecification.TotalTextures; ++i)
					{
						const auto attachment = ptr->mSpecification.TextureAttachment[i];
						const auto target = ptr->mSpecification.TextureTargets[i];
						if (target == GL_TEXTURE_2D_MULTISAMPLE)
							glFramebufferTexture2D(GL_FRAMEBUFFER, attachment, GL_TEXTURE_2D_MULTISAMPLE, ptr->mCascadedMaps[i]->getRendererID(), 0);
						else
							glFramebufferTexture(GL_FRAMEBUFFER, attachment, ptr->mCascadedMaps[i]->getRendererID(), 0);
					}

				glClear(GL_COLOR_BUFFER_BIT | GL_STENCIL_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

				const GLenum status = glCheckNamedFramebufferStatus(ptr->mRendererID, GL_FRAMEBUFFER);

				switch (status) {

				case GL_FRAMEBUFFER_COMPLETE: break;
				case GL_FRAMEBUFFER_UNSUPPORTED: BEY_CORE_ERROR("Unsupported framebuffer format!"); break;
				case GL_FRAMEBUFFER_INCOMPLETE_ATTACHMENT: BEY_CORE_ERROR("Incomplete framebuffer attachment!"); break;
				case GL_FRAMEBUFFER_INCOMPLETE_MISSING_ATTACHMENT: BEY_CORE_ERROR("Missing framebuffer attachment!"); break;
				case GL_FRAMEBUFFER_INCOMPLETE_LAYER_TARGETS: BEY_CORE_ERROR("Incomplete layer target!"); break;
				default: BEY_CORE_ERROR("Invalid framebuffer format!"); break;
				}
				glBindFramebuffer(GL_FRAMEBUFFER, 0);

			});


	}

	void FrameBuffer::bindForWritingDepth(const size_t index)
	{
		Ref<FrameBuffer> ptr = this;
		Renderer::submit([index, ptr]() {
			BEY_PROFILE_SCOPE("bindForWritingDepth Framebuffer");

			if (ptr->mSpecification.Samples > 1)
				glFramebufferTexture2D(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_TEXTURE_2D_MULTISAMPLE, ptr->mCascadedMaps[index]->getRendererID(), 0);
			else
				glFramebufferTexture(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, ptr->mCascadedMaps[index]->getRendererID(), 0);

			});
	}

	void FrameBuffer::bindArrayForWritingDepth(const size_t index, GLenum cubeFace)
	{
		Ref<FrameBuffer> ptr = this;
		Renderer::submit([index, ptr, cubeFace] {
			BEY_PROFILE_SCOPE("bindArrayForWritingDepth Framebuffer");
			glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, cubeFace, ptr->mCascadedMaps[1]->getRendererID(), 0);
			glDrawBuffer(GL_COLOR_ATTACHMENT0);
			});
	}


	void FrameBuffer::bindCubemapLayer(const size_t index)
	{
		Ref<FrameBuffer> ptr = this;
		Renderer::submit([index, ptr] {
			BEY_PROFILE_SCOPE("bindCubemapLayer Framebuffer");
			glFramebufferTextureLayer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, ptr->mCascadedMaps[0]->getRendererID(), 0, int32_t(index));
			});
	}

	void FrameBuffer::bindForWritingStencil(const size_t index)
	{
		Ref<FrameBuffer> ptr = this;
		Renderer::submit([index, ptr]() {
			BEY_PROFILE_SCOPE("bindForWritingStencil Framebuffer");

			if (ptr->mSpecification.Samples > 1)
				glFramebufferTexture2D(GL_FRAMEBUFFER, GL_STENCIL_ATTACHMENT, GL_TEXTURE_2D_MULTISAMPLE, ptr->mCascadedMaps[index]->getRendererID(), 0);
			else
				glFramebufferTexture(GL_FRAMEBUFFER, GL_STENCIL_ATTACHMENT, ptr->mCascadedMaps[index]->getRendererID(), 0);

			});
	}

	void FrameBuffer::bindForWritingDepthStencil(const size_t index)
	{
		Ref<FrameBuffer> ptr = this;
		Renderer::submit([index, ptr]() {
			BEY_PROFILE_SCOPE("bindForWritingStencil Framebuffer");

			if (ptr->mSpecification.Samples > 1)
				glFramebufferTexture2D(GL_FRAMEBUFFER, GL_DEPTH_STENCIL_ATTACHMENT, GL_TEXTURE_2D_MULTISAMPLE, ptr->mCascadedMaps[index]->getRendererID(), 0);
			else
				glFramebufferTexture(GL_FRAMEBUFFER, GL_DEPTH_STENCIL_ATTACHMENT, ptr->mCascadedMaps[index]->getRendererID(), 0);

			});
	}

	void FrameBuffer::bindForWriting(const size_t index)
	{

		Ref<FrameBuffer> ptr = this;
		Renderer::submit([index, ptr]() {
			BEY_PROFILE_SCOPE("bindForWriting Framebuffer");

			glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0 + static_cast<int>(index), ptr->mSpecification.Samples > 1 ?
				GL_TEXTURE_2D_MULTISAMPLE : GL_TEXTURE_2D, ptr->mCascadedMaps[index]->getRendererID(), 0);
			});
	}

	void FrameBuffer::bindTexture(const size_t index, const uint32_t slot)
	{
		Ref<FrameBuffer> ptr = this;
		Renderer::submit([ptr, slot, index]() {
			BEY_PROFILE_SCOPE("bindTexture Framebuffer");
			glBindTextureUnit(slot, ptr->mCascadedMaps[index]->getRendererID());
			});
	}

}
