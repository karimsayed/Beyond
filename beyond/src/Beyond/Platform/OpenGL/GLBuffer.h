#pragma once

#include <utility>


#include "RendererAPI.h"
#include "Beyond/Core/Buffer.h"

namespace bey {

	//////////////////////////////////////////////////////////////////////////////////
	// VertexBuffer
	//////////////////////////////////////////////////////////////////////////////////
	enum class ShaderDataType
	{
		None = 0, Float, Float2, Float3, Float4, Mat3, Mat4, UInt, Int, Int2, Int3, Int4, Bool
	};

	constexpr static uint32_t shaderDataTypeSize(const ShaderDataType type)
	{
		switch (type)
		{
		case ShaderDataType::Float:    return 4;
		case ShaderDataType::Float2:   return 4 * 2;
		case ShaderDataType::Float3:   return 4 * 3;
		case ShaderDataType::Float4:   return 4 * 4;
		case ShaderDataType::Mat3:     return 4 * 3 * 3;
		case ShaderDataType::Mat4:     return 4 * 4 * 4;
		case ShaderDataType::Int:      return 4;
		case ShaderDataType::UInt:     return 4;
		case ShaderDataType::Int2:     return 4 * 2;
		case ShaderDataType::Int3:     return 4 * 3;
		case ShaderDataType::Int4:     return 4 * 4;
		case ShaderDataType::Bool:     return 1;
		}
		

		BEY_CORE_ASSERT(false, "Unknown ShaderDataType!");
		return 0;
	}

	struct BufferElement
	{
		std::string Name;
		ShaderDataType Type;
		uint32_t Size;
		uint32_t Offset;
		bool Normalized;

		BufferElement() = default;

		BufferElement(const ShaderDataType type, std::string name, const bool normalized = false)
			: Name(std::move(name)), Type(type), Size(shaderDataTypeSize(type)), Offset(0), Normalized(normalized)
		{
		}

		[[nodiscard]] uint32_t getComponentCount() const
		{
			switch (Type)
			{
			case ShaderDataType::Float:   return 1;
			case ShaderDataType::Float2:  return 2;
			case ShaderDataType::Float3:  return 3;
			case ShaderDataType::Float4:  return 4;
			case ShaderDataType::Mat3:    return 3 * 3;
			case ShaderDataType::Mat4:    return 4 * 4;
			case ShaderDataType::UInt:    return 1;
			case ShaderDataType::Int:     return 1;
			case ShaderDataType::Int2:    return 2;
			case ShaderDataType::Int3:    return 3;
			case ShaderDataType::Int4:    return 4;
			case ShaderDataType::Bool:    return 1;
			default: BEY_CORE_ASSERT(false, "Unknown ShaderDataType!");
				return 0;
			}
		}
	};

	class BufferLayout
	{
	public:
		BufferLayout() = default;

		BufferLayout(const std::initializer_list<BufferElement>& elements)
			: mElements(elements)
		{
			calculateOffsetsAndStride();
		}

		[[nodiscard]] uint32_t getStride() const { return mStride; }
		[[nodiscard]] const std::vector<BufferElement>& getElements() const { return mElements; }

		[[nodiscard]] std::vector<BufferElement>::iterator begin() { return mElements.begin(); }
		[[nodiscard]] std::vector<BufferElement>::iterator end() { return mElements.end(); }
		[[nodiscard]] std::vector<BufferElement>::const_iterator begin() const { return mElements.begin(); }
		[[nodiscard]] std::vector<BufferElement>::const_iterator end() const { return mElements.end(); }
	private:
		void calculateOffsetsAndStride()
		{
			uint32_t offset = 0;
			mStride = 0;
			for (auto& element : mElements)
			{
				element.Offset = offset;
				offset += element.Size;
				mStride += element.Size;
			}
		}
	private:
		std::vector<BufferElement> mElements;
		uint32_t mStride = 0;
	};

	enum class VertexBufferUsage
	{
		None = 0, Static = 1, Dynamic = 2
	};
	class VertexBuffer final : public RefCounted
	{
	public:
		VertexBuffer(void* data, uint32_t size, VertexBufferUsage usage = VertexBufferUsage::Static) noexcept;
		explicit VertexBuffer(uint64_t size, VertexBufferUsage usage = VertexBufferUsage::Dynamic) noexcept;
		static Ref<VertexBuffer> create(void* data, uint32_t size, VertexBufferUsage usage = VertexBufferUsage::Static) noexcept;
		static Ref<VertexBuffer> create(uint32_t size, VertexBufferUsage usage = VertexBufferUsage::Dynamic) noexcept;
		~VertexBuffer() noexcept;

		void setData(void* data, uint32_t size, uint32_t offset = 0) noexcept;
		void bind() const noexcept;

		const BufferLayout& getLayout() const { return mLayout; }
		void setLayout(const BufferLayout& layout) { mLayout = layout; }

		uint64_t getSize() const { return mSize; }
		RendererID getRendererID() const { return mRendererID; }
	private:
		RendererID mRendererID = 0;
		uint64_t mSize;
		VertexBufferUsage mUsage;
		BufferLayout mLayout;

		Buffer mLocalData;
	};

	//////////////////////////////////////////////////////////////////////////////////
	// IndexBuffer
	//////////////////////////////////////////////////////////////////////////////////

	class IndexBuffer final : public RefCounted
	{
	public:
		explicit IndexBuffer(uint32_t size) noexcept;
		IndexBuffer(void* data, uint32_t size) noexcept;

		static Ref<IndexBuffer> create(uint32_t size) noexcept;
		static Ref<IndexBuffer> create(void* data, uint32_t size = 0) noexcept;

		~IndexBuffer() noexcept;

		void setData(void* data, uint32_t size, uint32_t offset = 0) noexcept;
		void bind() const noexcept;

		uint64_t getCount() const { return mSize / sizeof(uint32_t); }

		uint64_t getSize() const { return mSize; }
		RendererID getRendererID() const { return mRendererID; }
	private:
		RendererID mRendererID = 0;
		uint64_t mSize;

		Buffer mLocalData;
	};

	//////////////////////////////////////////////////////////////////////////////////
	// UniformBuffer
	//////////////////////////////////////////////////////////////////////////////////

	class UniformBuffer final : public RefCounted
	{
	public:
		explicit UniformBuffer(uint32_t size, const uint32_t binding) noexcept;
		UniformBuffer(void* data, const uint32_t binding, uint32_t size) noexcept;

		static Ref<UniformBuffer> create(uint32_t size, const uint32_t binding) noexcept;
		static Ref<UniformBuffer> create(void* data, const uint32_t binding, uint32_t size = 0) noexcept;

		~UniformBuffer() noexcept;

		void setData(void* data, uint32_t size, uint32_t offset = 0) noexcept;
		void setSubData(void* data, uint32_t size, uint32_t offset = 0) noexcept;
		void bind() const noexcept;

		uint64_t getCount() const noexcept { return mSize / sizeof(uint32_t); }

		[[nodiscard]] uint32_t& getIndex() { return mIndex; }
		
		[[nodiscard]] uint64_t getSize() const noexcept { return mSize; }
		[[nodiscard]] RendererID getRendererID() const noexcept { return mRendererID; }
		[[nodiscard]] GLuint getBinding() const { return mBinding; }
	private:
		RendererID mRendererID = 0;
		uint64_t mSize;
		const GLuint mBinding{};
		uint32_t mIndex{}; 

		Buffer mLocalData;
	};

	//////////////////////////////////////////////////////////////////////////////////
	// StorageUniformBuffer
	//////////////////////////////////////////////////////////////////////////////////

	class StorageUniformBuffer final : public RefCounted
	{
	public:
		explicit StorageUniformBuffer(uint32_t size, uint32_t binding) noexcept;
		StorageUniformBuffer(void* data, uint32_t binding, uint32_t size) noexcept;

		static Ref<StorageUniformBuffer> create(uint32_t size, uint32_t binding) noexcept;
		static Ref<StorageUniformBuffer> create(void* data, uint32_t binding, uint32_t size = 0) noexcept;

		~StorageUniformBuffer() noexcept;

		void setData(void* data, uint32_t size, uint32_t offset = 0) noexcept;
		void bind() const noexcept;

		uint64_t getCount() const noexcept { return mSize / sizeof(uint32_t); }

		uint64_t getSize() const noexcept { return mSize; }
		RendererID getRendererID() const noexcept { return mRendererID; }
		uint32_t getIndex() const { return mIndex; }
		uint32_t& getIndex() { return mIndex; }
		GLuint getBinding() const { return mBinding; }
	private:
		RendererID mRendererID = 0;
		uint64_t mSize;
		const GLuint mBinding{};
		uint32_t mIndex{};


		Buffer mLocalData;
	};

}
