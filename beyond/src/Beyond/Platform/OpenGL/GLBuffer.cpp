#include "pchs.h"
#include "GLBuffer.h"


#include "Beyond/Renderer/Renderer.h"

namespace bey {
	Ref<VertexBuffer> VertexBuffer::create(void* data, uint32_t size, VertexBufferUsage usage) noexcept
	{
		BEY_PROFILE_FUNCTION();
		return Ref<VertexBuffer>::create(data, size, usage);
	}

	Ref<VertexBuffer> VertexBuffer::create(uint32_t size, VertexBufferUsage usage) noexcept
	{
		BEY_PROFILE_FUNCTION();
		return Ref<VertexBuffer>::create(size, usage);
	}

	Ref<IndexBuffer> IndexBuffer::create(uint32_t size) noexcept
	{
		BEY_PROFILE_FUNCTION();
		return Ref<IndexBuffer>::create(size);
	}

	Ref<IndexBuffer> IndexBuffer::create(void* data, uint32_t size) noexcept
	{
		BEY_PROFILE_FUNCTION();
		return Ref<IndexBuffer>::create(data, size);
	}

	Ref<UniformBuffer> UniformBuffer::create(uint32_t size, const uint32_t binding) noexcept
	{
		BEY_PROFILE_FUNCTION();
		return Ref<UniformBuffer>::create(size, binding);
	}

	Ref<UniformBuffer> UniformBuffer::create(void* data, const uint32_t binding, uint32_t size) noexcept
	{
		BEY_PROFILE_FUNCTION();
		return Ref<UniformBuffer>::create(data, binding, size);
	}

	Ref<StorageUniformBuffer> StorageUniformBuffer::create(uint32_t size, const uint32_t binding) noexcept
	{
		BEY_PROFILE_FUNCTION();
		return Ref<StorageUniformBuffer>::create(size, binding);
	}
	
	Ref<StorageUniformBuffer> StorageUniformBuffer::create(void* data, const uint32_t binding, uint32_t size) noexcept
	{
		BEY_PROFILE_FUNCTION();
		return Ref<StorageUniformBuffer>::create(data, binding, size);
	}
	//////////////////////////////////////////////////////////////////////////////////
	// VertexBuffer
	//////////////////////////////////////////////////////////////////////////////////

	static GLenum openGlUsage(const VertexBufferUsage usage) noexcept
	{
		switch (usage)
		{
		case VertexBufferUsage::Static:    return GL_STATIC_DRAW;
		case VertexBufferUsage::Dynamic:   return GL_DYNAMIC_DRAW;
		}
		BEY_CORE_ASSERT(false, "Unknown vertex buffer usage");
		return 0;
	}

	VertexBuffer::VertexBuffer(void* data, const uint32_t size, const VertexBufferUsage usage) noexcept
		: mSize(size), mUsage(usage)
	{
		BEY_PROFILE_FUNCTION();
		mLocalData = Buffer::copy(data, size);

		Ref<VertexBuffer> instance = this;
		Renderer::submit([instance]() mutable
			{
				BEY_PROFILE_SCOPE("creating vertex buffer");

				glCreateBuffers(1, &instance->mRendererID);
				glNamedBufferData(instance->mRendererID, instance->mSize, instance->mLocalData.Data, openGlUsage(instance->mUsage));
			});
	}

	VertexBuffer::VertexBuffer(const uint64_t size, const VertexBufferUsage usage) noexcept
		: mSize(size), mUsage(usage)
	{
		Ref<VertexBuffer> instance = this;
		Renderer::submit([instance]() mutable
			{
				BEY_PROFILE_SCOPE("creating vertex buffer");

				glCreateBuffers(1, &instance->mRendererID);
				glNamedBufferData(instance->mRendererID, instance->mSize, nullptr, openGlUsage(instance->mUsage));
			});
	}

	VertexBuffer::~VertexBuffer() noexcept
	{
		GLuint rendererID = mRendererID;
		Renderer::submit([rendererID]() {
			BEY_PROFILE_SCOPE("deleting vertex buffer");

			glDeleteBuffers(1, &rendererID);
			});
	}

	void VertexBuffer::setData(void* data, const uint32_t size, uint32_t offset) noexcept
	{
		BEY_PROFILE_FUNCTION();
		mLocalData = Buffer::copy(data, size);
		mSize = size;
		Ref<VertexBuffer> instance = this;
		Renderer::submit([instance, offset]() {
			glNamedBufferSubData(instance->mRendererID, offset, instance->mSize, instance->mLocalData.Data);
			});
	}

	void VertexBuffer::bind() const noexcept
	{
		BEY_PROFILE_FUNCTION();
		Ref<const VertexBuffer> instance = this;
		Renderer::submit([instance]() {
			glBindBuffer(GL_ARRAY_BUFFER, instance->mRendererID);
			});
	}

	//////////////////////////////////////////////////////////////////////////////////
	// IndexBuffer
	//////////////////////////////////////////////////////////////////////////////////

	IndexBuffer::IndexBuffer(void* data, const uint32_t size) noexcept
		: mSize(size)
	{
		BEY_PROFILE_FUNCTION();
		mLocalData = Buffer::copy(data, size);

		Ref<IndexBuffer> instance = this;
		Renderer::submit([instance]() mutable {
			BEY_PROFILE_SCOPE("creating index buffer");

			glCreateBuffers(1, &instance->mRendererID);
			glNamedBufferData(instance->mRendererID, instance->mSize, instance->mLocalData.Data, GL_STATIC_DRAW);
			});
	}

	IndexBuffer::IndexBuffer(const uint32_t size) noexcept
		: mSize(size)
	{
		BEY_PROFILE_FUNCTION();

		Ref<IndexBuffer> instance = this;
		Renderer::submit([instance]() mutable {
			BEY_PROFILE_SCOPE("creating index buffer");

			glCreateBuffers(1, &instance->mRendererID);
			glNamedBufferData(instance->mRendererID, instance->mSize, nullptr, GL_DYNAMIC_DRAW);
			});
	}

	IndexBuffer::~IndexBuffer() noexcept
	{
		BEY_PROFILE_FUNCTION();
		GLuint rendererID = mRendererID;

		Renderer::submit([rendererID]() {
			BEY_PROFILE_SCOPE("deleting index buffer");
			glDeleteBuffers(1, &rendererID);
			});
	}

	void IndexBuffer::setData(void* data, const uint32_t size, uint32_t offset) noexcept
	{
		BEY_PROFILE_FUNCTION();
		mLocalData = Buffer::copy(data, size);
		mSize = size;
		Ref<IndexBuffer> instance = this;
		Renderer::submit([instance, offset]() {
			BEY_PROFILE_SCOPE("setting index buffer");
			glNamedBufferSubData(instance->mRendererID, offset, instance->mSize, instance->mLocalData.Data);
			});
	}

	void IndexBuffer::bind() const noexcept
	{
		BEY_PROFILE_FUNCTION();
		Renderer::submit([this]() {
			BEY_PROFILE_SCOPE("binding index buffer");
			glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, mRendererID);
			});
	}


	//////////////////////////////////////////////////////////////////////////////////
	// UniformBuffer
	//////////////////////////////////////////////////////////////////////////////////

	UniformBuffer::UniformBuffer(void* data, const uint32_t binding, const uint32_t size) noexcept
		: mSize(size), mBinding(binding)
	{
		BEY_PROFILE_FUNCTION();
		mLocalData = Buffer::copy(data, size);

		Ref<UniformBuffer> instance = this;
		Renderer::submit([instance]() mutable {
			BEY_PROFILE_SCOPE("creating uniform buffer");

			glCreateBuffers(1, &instance->mRendererID);
			glNamedBufferData(instance->mRendererID, instance->mSize, instance->mLocalData.Data, GL_STATIC_DRAW);
			});
	}

	UniformBuffer::UniformBuffer(const uint32_t size, const uint32_t binding) noexcept
		: mSize(size), mBinding(binding)
	{
		BEY_PROFILE_FUNCTION();
		mLocalData.Data = new uint8_t[size];
		
		
		Ref<UniformBuffer> instance = this;
		Renderer::submit([instance]() mutable {
			BEY_PROFILE_SCOPE("creating uniform buffer");

			glCreateBuffers(1, &instance->mRendererID);
			glNamedBufferData(instance->mRendererID, instance->mSize, nullptr, GL_DYNAMIC_DRAW);
			});
	}

	UniformBuffer::~UniformBuffer() noexcept
	{
		BEY_PROFILE_FUNCTION();
		auto rendererID = mRendererID;
		Renderer::submit([rendererID]() {
			BEY_PROFILE_SCOPE("deleting uniform buffer");
			glDeleteBuffers(1, &rendererID);
			});
	}

	void UniformBuffer::setData(void* data, const uint32_t size, uint32_t offset) noexcept
	{
		BEY_PROFILE_FUNCTION();
		mLocalData = Buffer::copy(data, size);
		mSize = size;
		Ref<UniformBuffer> instance = this;
		Renderer::submit([instance, offset, size]() {
			BEY_PROFILE_SCOPE("setting Uniform buffer");
			glNamedBufferSubData(instance->mRendererID, offset, size, instance->mLocalData.Data);
			glBindBufferRange(GL_UNIFORM_BUFFER, instance->mIndex, instance->mRendererID, 0, size);
			});
	}

	void UniformBuffer::setSubData(void* data, const uint32_t size, uint32_t offset) noexcept
	{
		BEY_PROFILE_FUNCTION();
		std::memcpy(mLocalData.Data + offset, data, size);
		
		mSize = (mLocalData.Data + offset + size) - mLocalData.Data;
		Ref<UniformBuffer> instance = this;
		Renderer::submit([instance, offset, size]() {
			BEY_PROFILE_SCOPE("setting Uniform buffer");
			glNamedBufferSubData(instance->mRendererID, offset, instance->mSize, instance->mLocalData.Data);
			glBindBufferRange(GL_UNIFORM_BUFFER, 0, instance->mRendererID, 0, size);
			});
	}

	void UniformBuffer::bind() const noexcept
	{
		BEY_PROFILE_FUNCTION();
		Renderer::submit([this]() {
			BEY_PROFILE_SCOPE("binding Uniform buffer");
			glBindBuffer(GL_UNIFORM_BUFFER, mRendererID);
			});
	}


	//////////////////////////////////////////////////////////////////////////////////
	// StorageUniformBuffer
	//////////////////////////////////////////////////////////////////////////////////

	StorageUniformBuffer::StorageUniformBuffer(void* data, const uint32_t binding, const uint32_t size) noexcept
		: mSize(size), mBinding(binding)
	{
		BEY_PROFILE_FUNCTION();
		mLocalData = Buffer::copy(data, size);

		Ref<StorageUniformBuffer> instance = this;
		Renderer::submit([instance]() mutable {
			BEY_PROFILE_SCOPE("creating StorageUniform buffer");

			glCreateBuffers(1, &instance->mRendererID);
			glNamedBufferData(instance->mRendererID, instance->mSize, instance->mLocalData.Data, GL_STATIC_DRAW);
			});
	}



	StorageUniformBuffer::StorageUniformBuffer(const uint32_t size, const uint32_t binding) noexcept
		: mSize(size), mBinding(binding)
	{
		BEY_PROFILE_FUNCTION();

		Ref<StorageUniformBuffer> instance = this;
		Renderer::submit([instance]() mutable {
			BEY_PROFILE_SCOPE("creating StorageUniform buffer");

			glCreateBuffers(1, &instance->mRendererID);
			glNamedBufferData(instance->mRendererID, instance->mSize, nullptr, GL_DYNAMIC_DRAW);
			});
	}

	StorageUniformBuffer::~StorageUniformBuffer() noexcept
	{
		BEY_PROFILE_FUNCTION();
		GLuint rendererID = mRendererID;
		Renderer::submit([rendererID]() {
			BEY_PROFILE_SCOPE("deleting uniform buffer");
			glDeleteBuffers(1, &rendererID);
			});
	}

	void StorageUniformBuffer::setData(void* data, const uint32_t size, uint32_t offset) noexcept
	{
		BEY_PROFILE_FUNCTION();
		mLocalData = Buffer::copy(data, size);
		mSize = size;
		Ref<StorageUniformBuffer> instance = this;
		Renderer::submit([instance, offset]() {
			BEY_PROFILE_SCOPE("setting StorageUniformBuffer");
			glNamedBufferSubData(instance->mRendererID, offset, instance->mSize, instance->mLocalData.Data);
			});
	}

	void StorageUniformBuffer::bind() const noexcept
	{
		BEY_PROFILE_FUNCTION();
		Ref<const StorageUniformBuffer> instance = this;

		Renderer::submit([instance]() {
			BEY_PROFILE_SCOPE("binding StorageUniformBuffer");
			glBindBufferBase(GL_SHADER_STORAGE_BUFFER, instance->mBinding, instance->mRendererID);
			});
	}
}