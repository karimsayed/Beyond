#pragma once


#include "GLBuffer.h"
#include "RendererAPI.h"
#include "ShaderUniform.h"


namespace bey {
	struct Buffer;

	struct ShaderUniform
	{

	};

	struct ShaderUniformCollection
	{

	};

	enum class UniformType
	{
		None = 0,
		Float, Float2, Float3, Float4,
		Matrix3x3, Matrix4x4,
		Int32, Uint32
	};

	struct UniformDecl
	{
		UniformType Type;
		std::ptrdiff_t Offset;
		std::string Name;
	};

	struct CPUUniformBuffer
	{
		// TODO: This currently represents a byte buffer that has been
		// packed with uniforms. This was primarily created for OpenGL,
		// and needs to be revisted for other rendering APIs. Furthermore,
		// this currently does not assume any alignment. This also has
		// nothing to do with GL uniform buffers, this is simply a CPU-side
		// buffer abstraction.
		std::byte* Buffer;
		std::vector<UniformDecl> Uniforms;
	};

	/*template <typename T>
	struct UniformBufferBase
	{
		[[nodiscard]] const std::byte* getBuffer() const { return static_cast<T*> (this)->getBuffer(); }
		[[nodiscard]] const UniformDecl* getUniforms() const { return static_cast<T*> (this)->getUniforms(); }
		[[nodiscard]] unsigned int getUniformCount() const { return static_cast<T*>(this)->getUniformCount(); }
	};*/

	template<unsigned int N, unsigned int U>
	struct UniformBufferDeclaration
	{
		std::byte Buffer[N];
		UniformDecl Uniforms[U];
		std::ptrdiff_t Cursor = 0;
		int Index = 0;

		[[nodiscard]] const std::byte* getBuffer() const { return Buffer; }
		[[nodiscard]] const UniformDecl* getUniforms() const { return Uniforms; }
		static unsigned int getUniformCount() { return U; }

		template<typename T>
		void push(const std::string& name, const T& data) { BEY_CORE_ASSERT(false); }

		template<>
		void push(const std::string& name, const float& data)
		{
			BEY_PROFILE_FUNCTION();
			Uniforms[Index++] = { UniformType::Float, Cursor, name };
			std::memcpy(Buffer + Cursor, &data, sizeof(float));
			Cursor += sizeof(float);
		}

		template<>
		void push(const std::string& name, const glm::vec3& data)
		{
			BEY_PROFILE_FUNCTION();
			Uniforms[Index++] = { UniformType::Float3, Cursor, name };
			std::memcpy(Buffer + Cursor, glm::value_ptr(data), sizeof(glm::vec3));
			Cursor += sizeof(glm::vec3);
		}

		template<>
		void push(const std::string& name, const glm::vec4& data)
		{
			BEY_PROFILE_FUNCTION();
			Uniforms[Index++] = { UniformType::Float4, Cursor, name };
			std::memcpy(Buffer + Cursor, glm::value_ptr(data), sizeof(glm::vec4));
			Cursor += sizeof(glm::vec4);
		}

		template<>
		void push(const std::string& name, const glm::mat4& data)
		{
			BEY_PROFILE_FUNCTION();
			Uniforms[Index++] = { UniformType::Matrix4x4, Cursor, name };
			std::memcpy(Buffer + Cursor, glm::value_ptr(data), sizeof(glm::mat4));
			Cursor += sizeof(glm::mat4);
		}

	};
	
	class Shader final : public RefCounted
	{
	public:
		static std::vector<Ref<Shader>> s_AllShaders;
		using ShaderReloadedCallback = std::function<void()>;
		Shader() = default;
		explicit Shader(const std::string& filepath);
  		static Ref<Shader> create(const std::string& filepath);

		void reload();
		void addShaderReloadedCallback(const ShaderReloadedCallback& callback);

		void bind() const;
		RendererID getRendererID() const { return mRendererID; }

		//void uploadUniformBuffer(const UniformBufferDeclaration<5,5>& uniformBuffer);

		void setVSMaterialUniformBuffer(Buffer buffer) const;
		void setPSMaterialUniformBuffer(Buffer buffer) const;
		void setTessESMaterialUniformBuffer(Buffer buffer) const;
		void setTessCSMaterialUniformBuffer(Buffer buffer) const;
		void setCompMaterialUniformBuffer(Buffer buffer) const;


		void setFloat(const std::string& name, float value);
		void setInt(const std::string& name, int value);
		void setInt2(const std::string& name, glm::ivec2 vec);
		void setUInt(const std::string& name, unsigned value);
		void setFloat2(const std::string& name, const glm::vec2& value);
		void setFloat3(const std::string& name, const glm::vec3& value);
		void setFloat4(const std::string& name, const glm::vec4& value);
		void setMat4(const std::string& name, const glm::mat4& value);
		void setMat4FromRenderThread(const std::string& name, const glm::mat4& value, bool bind = true);

		void setIntArray(const std::string& name, int* values, uint32_t size);
		void setVec3Array(const std::string& name, const std::vector<glm::vec3>& values, uint32_t size);
		void setVec4Array(const std::string& name, const std::vector<glm::vec4>& values, uint32_t size);
		void setMat4Vector(const std::string& name, const std::vector<glm::mat4>& values, uint32_t size);
		void setMat4Array(const std::string& name, const glm::mat4 values[], uint32_t size);

		const std::string& getName() const { return mName; }

		const ShaderUniformBufferList& getVSRendererUniforms() const { return mVSRendererUniformBuffers; }
		const ShaderUniformBufferList& getPSRendererUniforms() const { return mPSRendererUniformBuffers; }
		const ShaderUniformBufferList& getTessESRendererUniforms() const { return mTessESRendererUniformBuffers; }
		const ShaderUniformBufferList& getCompRendererUniforms() const { return mCompRendererUniformBuffers; }
		bool hasVSMaterialUniformBuffer() const { return bool(mVSMaterialUniformBuffer); }
		bool hasPSMaterialUniformBuffer() const { return bool(mPSMaterialUniformBuffer); }
		bool hasTessEMaterialUniformBuffer() const { return bool(mTessESMaterialUniformBuffer); }
		bool hasTessCMaterialUniformBuffer() const { return bool(mTessCSMaterialUniformBuffer); }
		bool hasCompMaterialUniformBuffer() const { return bool(mCompMaterialUniformBuffer); }
		const ShaderUniformBufferDeclaration& getVSMaterialUniformBuffer() const { return *mVSMaterialUniformBuffer; }
		const ShaderUniformBufferDeclaration& getPSMaterialUniformBuffer() const { return *mPSMaterialUniformBuffer; }
		const ShaderUniformBufferDeclaration& getTessESMaterialUniformBuffer() const { return *mTessESMaterialUniformBuffer; }
		const ShaderUniformBufferDeclaration& getTessCSMaterialUniformBuffer() const { return *mTessCSMaterialUniformBuffer; }
		const ShaderUniformBufferDeclaration& getCompMaterialUniformBuffer() const { return *mCompMaterialUniformBuffer; }
		const ShaderResourceList& getResources() const { return mResources; }
		void resolveGLUniformBuffer(const std::string& bufferName, Ref<UniformBuffer> uniformBuffer) const noexcept;
		void resolveGLUniformBuffer(const std::string& bufferName, Ref<StorageUniformBuffer> uniformBuffer) const noexcept;
	private:

		void load(const std::unordered_map<GLenum, std::string>& shaderSources);

		std::unordered_map<GLenum, std::string> readShaderFromFile(const std::string& filepath);
		void parse();
		void parseUniform(const std::string& statement, ShaderDomain domain);
		void parseUniformStruct(const std::string& block, ShaderDomain domain);
		ShaderStruct* findStruct(const std::string& name);

		int32_t getUniformLocation(const std::string& name);
		
		void resolveUniforms();
		void validateUniforms();
		void compileAndUploadShader();
		static GLenum shaderTypeFromString(const std::string& type);

		void resolveAndSetUniforms(const Ref<ShaderUniformBufferDeclaration>& decl, Buffer buffer) const;
		void resolveAndSetUniform(ShaderUniformDeclaration* uniform, Buffer buffer) const;
		void resolveAndSetUniformArray(ShaderUniformDeclaration* uniform, Buffer buffer) const;
		void resolveAndSetUniformField(const ShaderUniformDeclaration& field, std::uint8_t* data, int32_t offset) const;

		static void uploadUniformInt(uint32_t location, int32_t value);
		static void uploadUniformInt2(int32_t location, glm::ivec2 value);
		static void uploadUniformUInt(uint32_t location, uint32_t value);
		static void uploadUniformIntArray(uint32_t location, int32_t* values, int32_t count);
		static void uploadUniformFloat(uint32_t location, float value);
		static void uploadUniformFloat2(uint32_t location, const glm::vec2& value);
		static void uploadUniformFloat3(uint32_t location, const glm::vec3& value);
		static void uploadUniformFloat4(uint32_t location, const glm::vec4& value);
		static void uploadUniformMat3(uint32_t location, const glm::mat3& value);
		static void uploadUniformMat4(uint32_t location, const glm::mat4& value);


		void uploadUniformVec3Array(uint32_t location,const std::vector<glm::vec3>& values, uint32_t count) const;
		void uploadUniformVec4Array(const uint32_t location, const std::vector<glm::vec4>& values, const uint32_t count) const;
		void uploadUniformMat4Vector(uint32_t location, const std::vector<glm::mat4>& values, uint32_t count) const;
		void uploadUniformMat4Array(const uint32_t location, const glm::mat4 values[], const uint32_t count) const;
		void uploadUniformStruct(ShaderUniformDeclaration* uniform, std::uint8_t* buffer, uint32_t offset) const;
		void uploadUniformInt(const std::string& name, int32_t value);
		void uploadUniformUInt(const std::string& name, uint32_t value);
		void uploadUniformInt2(const std::string& name, const glm::ivec2& value);

		void uploadUniformIntArray(const std::string& name, int32_t* values, uint32_t count);
		void uploadUniformFloat(const std::string& name, float value);
		void uploadUniformFloat2(const std::string& name, const glm::vec2& value);
		void uploadUniformFloat3(const std::string& name, const glm::vec3& value);
		void uploadUniformFloat4(const std::string& name, const glm::vec4& value);
		void uploadUniformMat4(const std::string& name, const glm::mat4& value);


	private:
		RendererID mRendererID = 0;
		bool mLoaded = false;
		bool mIsCompute = false;

		std::string mName, mAssetPath;
		std::unordered_map<GLenum, std::string> mShaderSource;

		std::vector<ShaderReloadedCallback> mShaderReloadedCallbacks;

		std::unordered_map<std::string, int> mLocationCache {};

		ShaderUniformBufferList mVSRendererUniformBuffers;
		ShaderUniformBufferList mPSRendererUniformBuffers;
		ShaderUniformBufferList mTessESRendererUniformBuffers;
		ShaderUniformBufferList mTessCSRendererUniformBuffers;
		ShaderUniformBufferList mCompRendererUniformBuffers;
		Ref<ShaderUniformBufferDeclaration> mVSMaterialUniformBuffer;
		Ref<ShaderUniformBufferDeclaration> mPSMaterialUniformBuffer;
		Ref<ShaderUniformBufferDeclaration> mTessESMaterialUniformBuffer;
		Ref<ShaderUniformBufferDeclaration> mTessCSMaterialUniformBuffer;
		Ref<ShaderUniformBufferDeclaration> mCompMaterialUniformBuffer;
		ShaderResourceList mResources;
		ShaderStructList mStructs;
	};

	// This should be eventually handled by the Asset Manager
	class ShaderLibrary : public RefCounted
	{
	public:

		Ref<Shader> add(const Ref<Shader>& shader);
		void load(const std::string& path);
		void load(const std::string& name, const std::string& path);

		const Ref<Shader>& get(const std::string& name) const;
		Ref<Shader> find(const std::string& name);
	private:
		std::unordered_map<std::string, Ref<Shader>> mShaders;
	};

}
