#include "pchs.h"
#include "Texture.h"

#include <iostream>


#include "Beyond/Renderer/Renderer.h"



namespace bey {

	TexturePool* TexturePool::sInstance = new TexturePool;


	Ref<Texture2D> Texture2D::create(TextureFormat format, TextureInternalFormat intFormat, uint32_t width, uint32_t height,
		const DataType dataType, aiTextureType type, TextureWrap wrap)
	{
		BEY_PROFILE_FUNCTION();
		return Ref<Texture2D>::create(format, intFormat, width, height, dataType, wrap, type);
	}

	Ref<Texture2D> Texture2D::create(const std::string& path, const DataType dataType, bool srgb, const bool transparent, aiTextureType type)
	{
		BEY_PROFILE_FUNCTION();
		Ref<Texture> result = Ref<Texture2D>::create(path, srgb, type, dataType, transparent);
		TexturePool::getGlobal()->add(result);
		return result;
	}

	Ref<Texture2D> Texture2D::create(const uint32_t width, const uint32_t height)
	{
		BEY_PROFILE_FUNCTION();
		return Ref<Texture2D>::create(width, height);
	}

	Ref<TextureCube> TextureCube::create(TextureFormat format, TextureInternalFormat internalFormat, uint32_t width, uint32_t height, aiTextureType type)
	{
		BEY_PROFILE_FUNCTION();
		return Ref<TextureCube>::create(format, internalFormat, width, height, type);
	}

	Ref<TextureCube> TextureCube::create(const std::string& path, aiTextureType type)
	{
		BEY_PROFILE_FUNCTION();

		Ref<Texture> result = Ref<TextureCube>::create(path, type);
		TexturePool::getGlobal()->add(result);
		return result;
	}

	constexpr uint32_t Texture::getBPP(const TextureInternalFormat format)
	{
		//BEY_PROFILE_FUNCTION();
		switch (format)
		{
		case TextureInternalFormat::RED8:	 return 1;
		case TextureInternalFormat::RG8:	 return 2;
		case TextureInternalFormat::RGB8:    return 3;
		case TextureInternalFormat::RGBA8:	 return 4;
		case TextureInternalFormat::RED16F:	 return 2;
		case TextureInternalFormat::RG16F:	 return 4;
		case TextureInternalFormat::RGB16F:  return 6;
		case TextureInternalFormat::RGBA16F: return 8;
		case TextureInternalFormat::RED32F:	 return 4;
		case TextureInternalFormat::RG32F:   return 8;
		case TextureInternalFormat::RGB32F:	 return 12;
		case TextureInternalFormat::RGBA32F: return 16;
		case TextureInternalFormat::NONE:
		default:;
		}
		BEY_CORE_ASSERT(false, "Wrong Texture Format");
		return 0;
	}

	constexpr uint32_t Texture::calculateMipMapCount(const uint32_t width, const uint32_t height)
	{
		//BEY_PROFILE_FUNCTION();
		uint32_t levels = 0x1;
		while ((width | height) >> levels)
			levels++;

		return levels;
	}

	constexpr static GLenum toOpenGlTextureInternalFormat(const TextureInternalFormat format)
	{
		//BEY_PROFILE_FUNCTION();
		switch (format)
		{
		case TextureInternalFormat::RED8:	  return GL_R8;
		case TextureInternalFormat::RED16F:   return GL_R16;
		case TextureInternalFormat::RED32F:	  return GL_R32F;
		case TextureInternalFormat::RG8:	  return GL_RG8;
		case TextureInternalFormat::RG16F:	  return GL_RG16F;
		case TextureInternalFormat::RG32F:	  return GL_RG32F;
		case TextureInternalFormat::RGB8:     return GL_RGB8;
		case TextureInternalFormat::RGB16F:	  return GL_RGB16F;
		case TextureInternalFormat::RGB32F:	  return GL_RGB32F;
		case TextureInternalFormat::RGBA8:    return GL_RGBA8;
		case TextureInternalFormat::RGBA16F:  return GL_RGBA16F;
		case TextureInternalFormat::RGBA32F:  return GL_RGBA32F;
		case TextureInternalFormat::NONE:
		default:
			BEY_CORE_ASSERT(false, "Unknown texture format!");
			return 0;
		}

	}

	constexpr static GLenum toOpenGlTextureFormat(const TextureFormat format)
	{
		//BEY_PROFILE_FUNCTION();
		switch (format)
		{
		case TextureFormat::RED:	 return GL_RED;
		case TextureFormat::REDGREEN:return GL_RG;
		case TextureFormat::RGB:     return GL_RGB;
		case TextureFormat::RGBA:    return GL_RGBA;
		case TextureFormat::NONE:
		default:
			BEY_CORE_ASSERT(false, "Unknown texture format!");
		}
		return 0;
	}

	constexpr static GLenum toOpenGlTextureDataType(const DataType dataType)
	{
		//BEY_PROFILE_FUNCTION();
		switch (dataType)
		{
		case DataType::BYTE:			return GL_BYTE;
		case DataType::HALF_FLOAT:		return GL_HALF_FLOAT;
		case DataType::FLOAT:			return GL_FLOAT;
		case DataType::UNSIGNED_BYTE:	return GL_UNSIGNED_BYTE;
		case DataType::None:
		default: 
			BEY_CORE_ASSERT(false, "Unknown texture format!");
		}
		return 0;
	}

	//////////////////////////////////////////////////////////////////////////////////
	// Texture2D
	//////////////////////////////////////////////////////////////////////////////////


	Texture2D::Texture2D(const TextureFormat format, const TextureInternalFormat internalFormat, const uint32_t width, const uint32_t height, const DataType dataType, const TextureWrap wrap, const aiTextureType type)
		: mFormat(format), mInternalFormat(internalFormat), mDataType(dataType), mWrap(wrap), mWidth(width), mHeight(height), mType(type)
	{
		BEY_PROFILE_FUNCTION();
		Ref<Texture2D> ptr = this;
		Renderer::submit([ptr]() mutable
			{
				BEY_PROFILE_SCOPE("glGenTextures: NoData");
				glGenTextures(1, &ptr->mRendererID);
				glBindTexture(GL_TEXTURE_2D, ptr->mRendererID);

				glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
				glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
				const GLenum glWrap = ptr->mWrap == TextureWrap::CLAMP ? GL_CLAMP_TO_EDGE : GL_REPEAT;
				glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, glWrap);
				glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, glWrap);
				//glTextureParameteri(mRendererID, GL_TEXTURE_MAX_ANISOTROPY, RendererAPI::getCapabilities().MaxAnisotropy);

				glTexImage2D(GL_TEXTURE_2D, 0, toOpenGlTextureInternalFormat(ptr->mInternalFormat), ptr->mWidth, ptr->mHeight, 0,
					toOpenGlTextureFormat(ptr->mFormat), toOpenGlTextureDataType(ptr->mDataType), nullptr);

				glBindTexture(GL_TEXTURE_2D, 0);
			});

		{
			BEY_PROFILE_SCOPE("Texture Allocating buffer");
			mImageData.allocate(width * height * getBPP(mInternalFormat));
		}
	}

	Texture2D::Texture2D(const uint32_t width, const uint32_t height)
		: mFormat(TextureFormat::RGBA), mInternalFormat(TextureInternalFormat::RGBA16F), mDataType(DataType::HALF_FLOAT), mWidth(width), mHeight(height)
	{
	}

	TextureFormat determineFormat(const int channels)
	{
		switch (channels)
		{
		case 1: return TextureFormat::RED;
		case 2: return TextureFormat::REDGREEN;
		case 3: return TextureFormat::RGB;
		case 4: return TextureFormat::RGBA;
		default: BEY_CORE_ASSERT(false, "Cannot determine format");
			return TextureFormat::NONE;
		}
	}

	TextureInternalFormat determineInternalFormat(const int channels, const DataType dataType)
	{
		switch (channels)
		{
		case 1:
			if (dataType == DataType::BYTE || dataType == DataType::UNSIGNED_BYTE) return TextureInternalFormat::RED8;
			if (dataType == DataType::HALF_FLOAT) return TextureInternalFormat::RED16F;
			if (dataType == DataType::FLOAT) return TextureInternalFormat::RED32F;
			break;
		case 2:
			if (dataType == DataType::BYTE || dataType == DataType::UNSIGNED_BYTE) return TextureInternalFormat::RG8;
			if (dataType == DataType::HALF_FLOAT) return TextureInternalFormat::RG16F;
			if (dataType == DataType::FLOAT) return TextureInternalFormat::RG32F;
			break;
		case 3:
			if (dataType == DataType::BYTE || dataType == DataType::UNSIGNED_BYTE) return TextureInternalFormat::RGB8;
			if (dataType == DataType::HALF_FLOAT) return TextureInternalFormat::RGB16F;
			if (dataType == DataType::FLOAT) return TextureInternalFormat::RGB32F;
			break;
		case 4:
			if (dataType == DataType::BYTE || dataType == DataType::UNSIGNED_BYTE) return TextureInternalFormat::RGBA8;
			if (dataType == DataType::HALF_FLOAT) return TextureInternalFormat::RGBA16F;
			if (dataType == DataType::FLOAT) return TextureInternalFormat::RGBA32F;
			break;
		default: BEY_CORE_ASSERT(false, "Cannot determine format");
		}
		return TextureInternalFormat::NONE;
	}

	int getChannels(const aiTextureType texType, const bool srgb)
	{
		switch (texType)
		{
		case aiTextureType_HEIGHT:
		case aiTextureType_METALNESS:
		case aiTextureType_AMBIENT:
		case aiTextureType_AMBIENT_OCCLUSION:
		case aiTextureType_DISPLACEMENT:
		case aiTextureType_EMISSIVE:
		case aiTextureType_OPACITY:
		case aiTextureType_REFLECTION:
		case aiTextureType_SHININESS:
		case aiTextureType_SPECULAR:
		case aiTextureType_LIGHTMAP:
			return 1;
		case aiTextureType_DIFFUSE:
		case aiTextureType_BASE_COLOR:
			return srgb ? 3 : 4;
		case aiTextureType_NORMAL_CAMERA:
		case aiTextureType_NORMALS:
		case aiTextureType_EMISSION_COLOR:
			return 3;
		case aiTextureType_DIFFUSE_ROUGHNESS:
			return 4;
		case aiTextureType_NONE: [[fallthrough]];
		case aiTextureType_UNKNOWN: [[fallthrough]];
		case _aiTextureType_Force32Bit: [[fallthrough]];
		default: return 4;
		}
	}

	Texture2D::Texture2D(const std::string& path, bool srgb, const aiTextureType type, const DataType dataType, const bool transparent)
		: mDataType(dataType), mType(type), mFilePath(path)
	{
		BEY_PROFILE_FUNCTION();

		int width, height, channels;
		if (stbi_is_hdr(path.c_str()))
		{
			BEY_CORE_INFO("Loading HDR texture {0}, SRGB = {1}", path, srgb);
			mImageData.Data = reinterpret_cast<std::uint8_t*>(stbi_loadf(path.c_str(), &width, &height, &channels, 0));
			BEY_CORE_ASSERT(mImageData.Data, "Could not read image!");
			mIsHDR = true;

			mFormat = determineFormat(channels);
			mInternalFormat = determineInternalFormat(channels, dataType);
		}
		else
		{
			BEY_CORE_INFO("Loading texture {0}, SRGB = {1}", path, srgb);

			const int desired = getChannels(type, srgb);
			mImageData.Data = reinterpret_cast<std::uint8_t*>(stbi_load(path.c_str(), &width, &height, &channels, transparent ? 4 : desired));

			BEY_CORE_ASSERT(mImageData.Data, "Could not read image!");
			
			mFormat = determineFormat(transparent ? 4 : srgb ? 3 : desired);
			mInternalFormat = determineInternalFormat(transparent ? 4 : srgb ? 3 : desired, dataType);

		}

		if (!mImageData.Data)
			return;

		mLoaded = true;

		mWidth = width;
		mHeight = height;

		Ref<Texture2D> ptr = this;
		Renderer::submit([ptr, srgb, transparent]() mutable
			{
				BEY_PROFILE_SCOPE("glGenTextures: fromFile");

				if (srgb)
				{
					glCreateTextures(GL_TEXTURE_2D, 1, &ptr->mRendererID);
					const int levels = calculateMipMapCount(ptr->mWidth, ptr->mHeight);
					glTextureStorage2D(ptr->mRendererID, levels, transparent ? GL_SRGB8_ALPHA8 : GL_SRGB8, ptr->mWidth, ptr->mHeight);
					glTextureParameteri(ptr->mRendererID, GL_TEXTURE_MIN_FILTER, levels > 1 ? GL_LINEAR_MIPMAP_LINEAR : GL_LINEAR);
					glTextureParameteri(ptr->mRendererID, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
					glTextureParameterf(ptr->mRendererID, GL_TEXTURE_LOD_BIAS, -0.4f);
					glTextureParameteri(ptr->mRendererID, GL_TEXTURE_WRAP_S, ptr->mWrap == TextureWrap::CLAMP ? GL_CLAMP_TO_EDGE : GL_REPEAT);
					glTextureParameteri(ptr->mRendererID, GL_TEXTURE_WRAP_T, ptr->mWrap == TextureWrap::CLAMP ? GL_CLAMP_TO_EDGE : GL_REPEAT);

					glTextureSubImage2D(ptr->mRendererID, 0, 0, 0, ptr->mWidth, ptr->mHeight, transparent ? GL_RGBA : GL_RGB, toOpenGlTextureDataType(ptr->mDataType), ptr->mImageData.Data);
					glGenerateTextureMipmap(ptr->mRendererID);
				}
				else
				{
					glGenTextures(1, &ptr->mRendererID);
					glBindTexture(GL_TEXTURE_2D, ptr->mRendererID);

					glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
					glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
					glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, ptr->mWrap == TextureWrap::CLAMP ? GL_CLAMP_TO_EDGE : GL_REPEAT);
					glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, ptr->mWrap == TextureWrap::CLAMP ? GL_CLAMP_TO_EDGE : GL_REPEAT);
					glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_R, ptr->mWrap == TextureWrap::CLAMP ? GL_CLAMP_TO_EDGE : GL_REPEAT);
					glTextureParameterf(ptr->mRendererID, GL_TEXTURE_LOD_BIAS, -0.4f);

					const GLenum internalFormat = toOpenGlTextureInternalFormat(ptr->mInternalFormat);
					const GLenum format = toOpenGlTextureFormat(ptr->mFormat);
					glTexImage2D(GL_TEXTURE_2D, 0, internalFormat, ptr->mWidth, ptr->mHeight, 0, format, toOpenGlTextureDataType(ptr->mDataType), ptr->mImageData.Data);
					glGenerateMipmap(GL_TEXTURE_2D);

					glBindTexture(GL_TEXTURE_2D, 0);
				}
				{
					BEY_PROFILE_SCOPE("stbi_image_free");
					stbi_image_free(ptr->mImageData.Data);
				}
			});
	}




	Texture2D::~Texture2D()
	{
		BEY_PROFILE_FUNCTION();

		GLuint rendererID = mRendererID;
		Renderer::submit([rendererID]() {
			BEY_PROFILE_SCOPE("glDeleteTextures");
			glDeleteTextures(1, &rendererID);
			});
	}

	void Texture2D::bind(uint32_t slot) const
	{
		BEY_PROFILE_FUNCTION();
		Ref<const Texture2D> ptr = this;
		Renderer::submit([ptr, slot]() {
			BEY_PROFILE_SCOPE("Texture2D::bind");
			glBindTextureUnit(slot, ptr->mRendererID);
			});
	}

	void Texture2D::lock()
	{
		BEY_PROFILE_FUNCTION();
		mLocked = true;
	}

	void Texture2D::unlock()
	{
		BEY_PROFILE_FUNCTION();
		mLocked = false;
		Ref<Texture2D> ptr = this;
		Renderer::submit([ptr]() {
			BEY_PROFILE_SCOPE("Texture2D::unlock sub");

			glTextureSubImage2D(ptr->mRendererID, 0, 0, 0, ptr->mWidth, ptr->mHeight, toOpenGlTextureFormat(ptr->mFormat),
				toOpenGlTextureDataType(ptr->mDataType), ptr->mImageData.Data);
			});
	}

	void Texture2D::resize(const uint32_t width, const uint32_t height)
	{
		BEY_PROFILE_FUNCTION();
		BEY_CORE_ASSERT(mLocked, "Texture must be locked!");

		mImageData.allocate(width * height * getBPP(mInternalFormat));
#if BEY_DEBUG
		//mImageData.zeroInitialize();
#endif
	}

	Buffer Texture2D::getWritableBuffer() const
	{
		BEY_CORE_ASSERT(mLocked, "Texture must be locked!");
		return mImageData;
	}

	uint32_t Texture2D::getMipLevelCount() const
	{
		return calculateMipMapCount(mWidth, mHeight);
	}

	//////////////////////////////////////////////////////////////////////////////////
	// TextureCube
	//////////////////////////////////////////////////////////////////////////////////

	TextureCube::TextureCube(const TextureFormat format, const TextureInternalFormat internalFormat, const uint32_t width, const uint32_t height, const aiTextureType type)
		: mRendererID(0), mFormat(format), mInternalFormat(internalFormat), mWidth(width), mHeight(height), mType(type), mImageData(nullptr)
	{
		BEY_PROFILE_FUNCTION();
		uint32_t levels = calculateMipMapCount(width, height);
		Ref<TextureCube> instance = this;
		Renderer::submit([instance, levels]() mutable
			{
				BEY_PROFILE_SCOPE("glGenTextures: Cube NoData");

				glCreateTextures(GL_TEXTURE_CUBE_MAP, 1, &instance->mRendererID);
				glBindTexture(GL_TEXTURE_CUBE_MAP, instance->mRendererID);
				glTextureStorage2D(instance->mRendererID, levels, toOpenGlTextureInternalFormat(instance->mInternalFormat), instance->mWidth, instance->mHeight);
				glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MIN_FILTER, levels > 1 ? GL_LINEAR_MIPMAP_LINEAR : GL_LINEAR);
				glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
				glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
				glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
				glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_R, GL_CLAMP_TO_EDGE);

			});
	}

	// TODO: Revisit this, as currently env maps are being loaded as equirectangular 2D images
	//       so this is an old path
	TextureCube::TextureCube(const std::string& path, const aiTextureType type)
		: mRendererID(0), mFormat(TextureFormat::NONE), mType(type), mFilePath(path)
	{
		BEY_PROFILE_FUNCTION();

		int width, height, channels;
		stbi_set_flip_vertically_on_load(0);
		{
			BEY_PROFILE_SCOPE("TextureCube stbi_load");
			mImageData = stbi_load(path.c_str(), &width, &height, &channels, STBI_rgb);
		}

		mWidth = width;
		mHeight = height;

		uint32_t faceWidth = mWidth / 4;
		uint32_t faceHeight = mHeight / 3;
		BEY_CORE_ASSERT(faceWidth == faceHeight, "Non-square faces!");

		std::array<uint8_t*, 6> faces{};
		for (auto& face : faces)
			face = new uint8_t[uint64_t(faceWidth) * uint64_t(faceHeight) * 3]; // 3 BPP

		int faceIndex = 0;

		for (size_t i = 0; i < 4; i++)
		{
			for (size_t y = 0; y < faceHeight; y++)
			{
				const size_t yOffset = y + faceHeight;
				for (size_t x = 0; x < faceWidth; x++)
				{
					const size_t xOffset = x + i * faceWidth;
					faces[faceIndex][(x + y * faceWidth) * 3 + 0] = mImageData[(xOffset + yOffset * mWidth) * 3 + 0];
					faces[faceIndex][(x + y * faceWidth) * 3 + 1] = mImageData[(xOffset + yOffset * mWidth) * 3 + 1];
					faces[faceIndex][(x + y * faceWidth) * 3 + 2] = mImageData[(xOffset + yOffset * mWidth) * 3 + 2];
				}
			}
			faceIndex++;
		}

		for (size_t i = 0; i < 3; i++)
		{
			// Skip the middle one
			if (i == 1)
				continue;

			for (size_t y = 0; y < faceHeight; y++)
			{
				const size_t yOffset = y + i * faceHeight;
				for (size_t x = 0; x < faceWidth; x++)
				{
					const size_t xOffset = x + faceWidth;
					faces[faceIndex][(x + y * faceWidth) * 3 + 0] = mImageData[(xOffset + yOffset * mWidth) * 3 + 0];
					faces[faceIndex][(x + y * faceWidth) * 3 + 1] = mImageData[(xOffset + yOffset * mWidth) * 3 + 1];
					faces[faceIndex][(x + y * faceWidth) * 3 + 2] = mImageData[(xOffset + yOffset * mWidth) * 3 + 2];
				}
			}
			faceIndex++;
		}

		Ref<TextureCube> instance = this;
		Renderer::submit([instance, faceWidth, faceHeight, faces]() mutable
			{
				glGenTextures(1, &instance->mRendererID);
				glBindTexture(GL_TEXTURE_CUBE_MAP, instance->mRendererID);

				glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
				glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
				glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
				glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
				glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_R, GL_CLAMP_TO_EDGE);
				glTextureParameteri(instance->mRendererID, GL_TEXTURE_MAX_ANISOTROPY, RendererAPI::getCapabilities().MaxAnisotropy);

				const auto format = instance->mFormat;
				glTexImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_X, 0, toOpenGlTextureFormat(format), faceWidth, faceHeight, 0, toOpenGlTextureFormat(format), GL_UNSIGNED_BYTE, faces[2]);
				glTexImage2D(GL_TEXTURE_CUBE_MAP_NEGATIVE_X, 0, toOpenGlTextureFormat(format), faceWidth, faceHeight, 0, toOpenGlTextureFormat(format), GL_UNSIGNED_BYTE, faces[0]);

				glTexImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_Y, 0, toOpenGlTextureFormat(format), faceWidth, faceHeight, 0, toOpenGlTextureFormat(format), GL_UNSIGNED_BYTE, faces[4]);
				glTexImage2D(GL_TEXTURE_CUBE_MAP_NEGATIVE_Y, 0, toOpenGlTextureFormat(format), faceWidth, faceHeight, 0, toOpenGlTextureFormat(format), GL_UNSIGNED_BYTE, faces[5]);

				glTexImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_Z, 0, toOpenGlTextureFormat(format), faceWidth, faceHeight, 0, toOpenGlTextureFormat(format), GL_UNSIGNED_BYTE, faces[1]);
				glTexImage2D(GL_TEXTURE_CUBE_MAP_NEGATIVE_Z, 0, toOpenGlTextureFormat(format), faceWidth, faceHeight, 0, toOpenGlTextureFormat(format), GL_UNSIGNED_BYTE, faces[3]);

				glGenerateMipmap(GL_TEXTURE_CUBE_MAP);

				glBindTexture(GL_TEXTURE_2D, 0);

				for (auto& face : faces)
					delete[] face;

				stbi_image_free(instance->mImageData);
			});
	}

	TextureCube::~TextureCube()
	{
		BEY_PROFILE_FUNCTION();

		GLuint rendererID = mRendererID;
		Renderer::submit([rendererID]() {
			BEY_PROFILE_SCOPE("glDeleteTextures TextureCube");

			glDeleteTextures(1, &rendererID);
			});
	}

	void TextureCube::bind(uint32_t slot) const
	{
		BEY_PROFILE_FUNCTION();

		Ref<const TextureCube> instance = this;
		Renderer::submit([instance, slot]() {
			BEY_PROFILE_SCOPE("glBindTextureUnit TextureCube");
			glBindTextureUnit(slot, instance->mRendererID);
			});
	}

	uint32_t TextureCube::getMipLevelCount() const
	{
		BEY_PROFILE_FUNCTION();
		return calculateMipMapCount(mWidth, mHeight);
	}

	void TexturePool::add(const Ref<Texture>& texture)
	{
		mPool.push_back(texture);
	}
}
