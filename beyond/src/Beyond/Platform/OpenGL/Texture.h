#pragma once

#include <assimp/material.h>

#include "RendererAPI.h"
#include "Beyond/Core/Buffer.h"

#include <concepts>


namespace bey {


	enum class TextureFormat
	{
		NONE = 0,
		RED,
		REDGREEN,
		RGB,
		RGBA,
	};

	enum class TextureInternalFormat
	{
		NONE,
		RED8,
		RGB8,
		RG8,
		RGBA8,
		RED16F,
		RG16F,
		RGB16F,
		RGBA16F,
		RED32F,
		RG32F,
		RGB32F,
		RGBA32F,
	};

	enum class TextureWrap
	{
		NONE = 0,
		CLAMP = 1,
		REPEAT = 2
	};

	enum class DataType
	{
		None = 0,
		HALF_FLOAT,
		FLOAT,
		UNSIGNED_BYTE,
		BYTE,
	};
	
	class Texture : public RefCounted
	{
	public:
		virtual ~Texture() = default;

		virtual void bind(uint32_t slot = 0) const = 0;

		virtual TextureFormat getFormat() const = 0;
		virtual TextureInternalFormat getIntFormat() const = 0;

		virtual uint32_t getWidth() const = 0;
		virtual uint32_t getHeight() const = 0;
		virtual uint32_t getMipLevelCount() const = 0;

		virtual RendererID getRendererID() const = 0;
		virtual RendererID& getRendererID() = 0;
		virtual aiTextureType getType() const = 0;
		virtual const std::string& getPath() const = 0;

		constexpr static uint32_t getBPP(TextureInternalFormat format);
		constexpr static uint32_t calculateMipMapCount(uint32_t width, uint32_t height);

	};


	class Texture2D final : public Texture
	{
	public:
		Texture2D(TextureFormat format, TextureInternalFormat internalFormat, uint32_t width, uint32_t height,
		          DataType dataType, TextureWrap wrap, aiTextureType type);
		Texture2D(uint32_t width, uint32_t height);
		Texture2D(const std::string& path, bool srgb, aiTextureType type, DataType dataType, bool transparent);
		static Ref<Texture2D> create(TextureFormat format, TextureInternalFormat intFormat, uint32_t width, uint32_t height, const DataType dataType, aiTextureType
		                             type = aiTextureType_NONE, TextureWrap wrap = TextureWrap::CLAMP);
		static Ref<Texture2D> create(const std::string& path, const DataType dataType, bool srgb, bool transparent, aiTextureType type = aiTextureType_NONE);
		static Ref<Texture2D> create(uint32_t width, uint32_t height);
		~Texture2D() override;

		

		void bind(uint32_t slot = 0) const override;

		TextureFormat getFormat() const override { return mFormat; }
		TextureInternalFormat getIntFormat() const override { return mInternalFormat; }
		uint32_t getWidth() const override { return mWidth; }
		uint32_t getHeight() const override { return mHeight; }
		// This function currently returns the expected number of mips based on image size,
		// not present mips in data
		uint32_t getMipLevelCount() const override;

		void lock();
		void unlock();

		void resize(uint32_t width, uint32_t height);
		Buffer getWritableBuffer() const;

		const std::string& getPath() const override { return mFilePath; }

		bool loaded() const { return mLoaded; }

		RendererID getRendererID() const override { return mRendererID; }
		RendererID& getRendererID() override { return mRendererID; }

		aiTextureType getType() const override { return mType; }

		template <typename TextureT>
		bool operator==(const TextureT& other) const requires std::derived_from<TextureT, Texture>
		{
			return mRendererID == static_cast<Texture2D>(other).mRendererID;
		}
	private:
		RendererID mRendererID{};
		TextureFormat mFormat{};
		TextureInternalFormat mInternalFormat{};
		DataType mDataType{};
		TextureWrap mWrap = TextureWrap::REPEAT;
		uint32_t mWidth{}, mHeight{};
		aiTextureType mType{};


		Buffer mImageData;
		bool mIsHDR = false;

		bool mLocked = false;
		bool mLoaded = false;


		std::string mFilePath;
	};

	class TextureCube final : public Texture
	{
	public:
		TextureCube(TextureFormat format, TextureInternalFormat internalFormat, uint32_t width, uint32_t height, aiTextureType type);
		explicit TextureCube(const std::string& path, aiTextureType type);
		static Ref<TextureCube> create(TextureFormat format, TextureInternalFormat internalFormat, uint32_t width, uint32_t height, aiTextureType type =
			                               aiTextureType_NONE);
		static Ref<TextureCube> create(const std::string& path, aiTextureType type = aiTextureType_NONE);
		~TextureCube() override;

		void bind(uint32_t slot = 0) const override;

		TextureFormat getFormat() const override { return mFormat; }
		TextureInternalFormat getIntFormat() const override { return mInternalFormat; }
		uint32_t getWidth() const override { return mWidth; }
		uint32_t getHeight() const override { return mHeight; }
		// This function currently returns the expected number of mips based on image size,
		// not present mips in data
		uint32_t getMipLevelCount() const override;

		const std::string& getPath() const { return mFilePath; }

		RendererID getRendererID() const override { return mRendererID; }
		RendererID& getRendererID() override { return mRendererID; }
		aiTextureType getType() const override { return mType; }

		template <typename TextureT>
		bool operator==(const TextureT& other) const requires std::derived_from<TextureT, Texture>
		{
			return mRendererID == static_cast<TextureCube>(other).mRendererID;
		}
	private:
		RendererID mRendererID{};
		TextureFormat mFormat{};
		TextureInternalFormat mInternalFormat{};
		uint32_t mWidth{}, mHeight{};
		aiTextureType mType{};


		unsigned char* mImageData;

		std::string mFilePath;
	};


	class TexturePool final
	{
	public:

		void add(const Ref<Texture>& texture);

		[[nodiscard]] std::vector<Ref<Texture>>& getAll() { return mPool; }
		[[nodiscard]] const std::vector<Ref<Texture>>& getAll() const { return mPool; }
		
		[[nodiscard]] Ref<Texture> findAlreadyLoaded(const std::string& texturePath) const 
		{
			const auto found = std::ranges::find_if(mPool, [&texturePath](Ref<Texture> texture) { return texturePath == texture->getPath(); });
			
			return found != mPool.end() ? *found : nullptr;
		}

		static TexturePool* getGlobal() { return sInstance; }
	private:
		std::vector<Ref<Texture>> mPool;

		static TexturePool* sInstance;
	};
}
