#include "pchs.h"
#include "Shader.h"


#include "Beyond/Renderer/Renderer.h"
#include <glad/glad.h>


namespace bey {

#ifdef BEY_DEBUG 
#define UNIFORM_LOGGING
#endif

#ifdef UNIFORM_LOGGING
#define BEY_LOG_UNIFORM(...) BEY_CORE_WARN(__VA_ARGS__)
#else
#define BEY_LOG_UNIFORM(...)
#endif



	std::vector<Ref<Shader>> Shader::s_AllShaders;

	Ref<Shader> Shader::create(const std::string& filepath)
	{
		BEY_PROFILE_FUNCTION();


		Ref<Shader> result = Ref<Shader>::create(filepath);
		s_AllShaders.push_back(result);
		return result;
	}

	Ref<Shader> ShaderLibrary::add(const bey::Ref<Shader>& shader)
	{
		BEY_PROFILE_FUNCTION();
		const auto& name = shader->getName();
		//BEY_CORE_ASSERT(mShaders.find(name) == mShaders.end());
		mShaders[name] = shader;
		return shader;
	}

	void ShaderLibrary::load(const std::string& path)
	{
		BEY_PROFILE_FUNCTION();
		auto shader = Ref<Shader>(Shader::create(path));
		const auto& name = shader->getName();
		BEY_CORE_ASSERT(mShaders.find(name) == mShaders.end());
		mShaders[name] = shader;
	}

	void ShaderLibrary::load(const std::string& name, const std::string& path)
	{
		BEY_PROFILE_FUNCTION();
		BEY_CORE_ASSERT(mShaders.find(name) == mShaders.end());
		mShaders[name] = Ref<Shader>(Shader::create(path));
	}

	const Ref<Shader>& ShaderLibrary::get(const std::string& name) const
	{
		BEY_PROFILE_FUNCTION();
		BEY_CORE_ASSERT(mShaders.find(name) != mShaders.end());
		return mShaders.at(name);
	}

	Ref<Shader> ShaderLibrary::find(const std::string& name)
	{
		BEY_PROFILE_FUNCTION();
		if (mShaders.find(name) != mShaders.end())
			return mShaders.at(name);
		return Ref<Shader>();
	}


	Shader::Shader(const std::string& filepath)
		: mAssetPath(filepath)
	{
		BEY_PROFILE_FUNCTION();
		size_t found = filepath.find_last_of("/\\");
		mName = found != std::string::npos ? filepath.substr(found + 1) : filepath;
		found = mName.find_last_of('.');
		mName = found != std::string::npos ? mName.substr(0, found) : mName;

		reload();
	}



	void Shader::reload()
	{
		BEY_PROFILE_FUNCTION();
		mLocationCache.clear();
		load(readShaderFromFile(mAssetPath));
	}

	void Shader::load(const std::unordered_map<GLenum, std::string>& shaderSources)
	{
		BEY_PROFILE_FUNCTION();
		mShaderSource = shaderSources;
		if (mIsCompute && mName == "LightCullingShader" || !mIsCompute)
			parse();

		Ref<Shader> ptr = this;
		Renderer::submit([ptr]() mutable
			{
				BEY_PROFILE_SCOPE("loading shader");
				if (ptr->mRendererID)
					glDeleteProgram(ptr->mRendererID);

				ptr->compileAndUploadShader();
				if ((ptr->mIsCompute && ptr->mName == "LightCullingShader") || !ptr->mIsCompute)
				{
					ptr->resolveUniforms();
					ptr->validateUniforms();
				}

				if (ptr->mLoaded)
				{
					for (auto& callback : ptr->mShaderReloadedCallbacks)
						callback();
				}

				ptr->mLoaded = true;
			});
	}

	void Shader::addShaderReloadedCallback(const ShaderReloadedCallback& callback)
	{
		BEY_PROFILE_FUNCTION();
		mShaderReloadedCallbacks.push_back(callback);
	}

	void Shader::bind() const
	{
		BEY_PROFILE_FUNCTION();
		Ref<const Shader> ptr = this;
		Renderer::submit([ptr]() {
			BEY_PROFILE_SCOPE("binding shader");
			glUseProgram(ptr->mRendererID);
			});
	}

	const static std::array<std::string, 6> formats{ ".vert", ".frag", ".geom", ".comp", ".tesc", ".tese" };

	std::unordered_map<GLenum, std::string> Shader::readShaderFromFile(const std::string& filepath)
	{
		BEY_PROFILE_FUNCTION();
		const auto parentPath = std::filesystem::path(filepath).parent_path();

		std::unordered_map<GLenum, std::string> shaderSources;

		for (const auto& X : formats)
		{
			BEY_PROFILE_SCOPE("reading shader file");
			std::string result;

			std::ifstream in(filepath + X, std::ios::in | std::ios::binary);
			if (in.is_open())
			{
				in.seekg(0, std::ios::end);
				result.resize(in.tellg());
				in.seekg(0, std::ios::beg);
				in.read(&result[0], result.size());

				auto shaderType = shaderTypeFromString(X);
				BEY_CORE_ASSERT(shaderType != GL_NONE, "Shader could not be loaded!");

				shaderSources[shaderType] = result;

				if (shaderType == GL_COMPUTE_SHADER)
					mIsCompute = true;
				in.close();
			}
		}
		BEY_CORE_ASSERT(shaderSources.size(), "Shader could not be loaded!");

		return shaderSources;
	}


	// Parsing helper functions
	const char* findToken(const char* const str, const std::string& token)
	{
		BEY_PROFILE_FUNCTION();

		const char* t = str;
		while ((t = strstr(t, token.c_str())))
		{
			const bool left = str == t || isspace(t[-1]);
			const bool right = !t[token.size()] || isspace(t[token.size()]);
			if (left && right)
				return t;

			t += token.size();
		}
		return nullptr;
	}

	const char* skipLine(const char* const str, const std::string& token)
	{
		const char* t = str;
		while ((t = strstr(t, token.c_str())))
		{

		}
		return str;
	}

	const char* findToken(const std::string& string, const std::string& token)
	{
		BEY_PROFILE_FUNCTION();
		return findToken(string.c_str(), token);
	}

	std::vector<std::string> splitString(const std::string& string, const std::string& delimiters)
	{
		BEY_PROFILE_FUNCTION();

		size_t start = 0;
		size_t end = string.find_first_of(delimiters);

		std::vector<std::string> result;

		while (end <= std::string::npos)
		{
			std::string token = string.substr(start, end - start);
			
			if (!token.empty() && (token != "lowp" && token != "mediump" && token != "highp" && token != "writeonly"))
				result.push_back(token);

			if (end == std::string::npos)
				break;

			start = end + 1;
			end = string.find_first_of(delimiters, start);
		}

		return result;
	}

	std::vector<std::string> splitString(const std::string& string, const char delimiter)
	{
		BEY_PROFILE_FUNCTION();
		return splitString(string, std::string(1, delimiter));
	}

	std::vector<std::string> tokenize(const std::string& string)
	{
		BEY_PROFILE_FUNCTION();
		return splitString(string, " \t\n");
	}

	std::vector<std::string> getLines(const std::string& string)
	{
		BEY_PROFILE_FUNCTION();
		return splitString(string, "\n");
	}

	std::string getBlock(const char* str, const char** outPosition)
	{
		BEY_PROFILE_FUNCTION();
		const char* end = strchr(str, '}');
		if (!end)
			return str;

		if (outPosition)
			*outPosition = end;
		const auto length = uint64_t(end - str + 1);
		return std::string(str, length);
	}

	std::string getStatement(const char* str, const char** outPosition)
	{
		BEY_PROFILE_FUNCTION();

		const char* end = strchr(str, ';');
		if (!end)
			return str;

		if (outPosition)
			*outPosition = end;
		const auto length = unsigned(end - str + 1);
		return std::string(str, length);
	}

	bool startsWith(const std::string& string, const std::string& start)
	{
		BEY_PROFILE_FUNCTION();
		return string.find(start) == 0;
	}


	void removeStatements(const std::string& statement, std::string& source)
	{
		const std::string::size_type n = statement.length();

		for (std::string::size_type i = source.find(statement); i != std::string::npos; i = source.find(statement))
			source.erase(i, n);
	}

	void Shader::parse()
	{
		BEY_PROFILE_FUNCTION();
		const char* token;
		const char* vstr;
		const char* fstr;
		const char* tEstr;
		const char* tCstr;
		const char* cStr;

		mResources.clear();
		mStructs.clear();
		mVSMaterialUniformBuffer.reset();
		mPSMaterialUniformBuffer.reset();
		mTessESMaterialUniformBuffer.reset();
		mTessCSMaterialUniformBuffer.reset();
		mCompMaterialUniformBuffer.reset();

		auto vertexSource = mShaderSource[GL_VERTEX_SHADER];
		auto fragmentSource = mShaderSource[GL_FRAGMENT_SHADER];
		auto tessEvalSource = mShaderSource[GL_TESS_EVALUATION_SHADER];
		auto tessCtrlSource = mShaderSource[GL_TESS_CONTROL_SHADER];
		auto compSource = mShaderSource[GL_COMPUTE_SHADER];

		static std::array<std::string, 2> skipStatements = { "layout(std140, binding = 0) uniform light_data",
		"layout(std140, binding = 1) uniform light_matrices"};
		for (auto& statement : skipStatements)
			removeStatements(statement, vertexSource);
		for (auto& statement : skipStatements)
			removeStatements(statement, fragmentSource);
		for (auto& statement : skipStatements)
			removeStatements(statement, tessEvalSource);
		for (auto& statement : skipStatements)
			removeStatements(statement, tessCtrlSource);
		for (auto& statement : skipStatements)
			removeStatements(statement, compSource);

		// Vertex Shader
		vstr = vertexSource.c_str();
		while ((token = findToken(vstr, "struct")))
			parseUniformStruct(getBlock(token, &vstr), ShaderDomain::Vertex);

		vstr = vertexSource.c_str();
		while ((token = findToken(vstr, "uniform")))
			parseUniform(getStatement(token, &vstr), ShaderDomain::Vertex);

		// Fragment Shader
		fstr = fragmentSource.c_str();
		while ((token = findToken(fstr, "struct")))
			parseUniformStruct(getBlock(token, &fstr), ShaderDomain::Pixel);

		fstr = fragmentSource.c_str();
		while ((token = findToken(fstr, "uniform")))
			parseUniform(getStatement(token, &fstr), ShaderDomain::Pixel);

		// Tessellation Evaluation Shader
		tEstr = tessEvalSource.c_str();
		while ((token = findToken(tEstr, "struct")))
			parseUniformStruct(getBlock(token, &tEstr), ShaderDomain::TessES);

		tEstr = tessEvalSource.c_str();
		while ((token = findToken(tEstr, "uniform")))
			parseUniform(getStatement(token, &tEstr), ShaderDomain::TessES);

		// Tessellation Control Shader Shader
		tCstr = tessCtrlSource.c_str();
		while ((token = findToken(tCstr, "struct")))
			parseUniformStruct(getBlock(token, &tCstr), ShaderDomain::TessCS);

		tCstr = tessCtrlSource.c_str();
		while ((token = findToken(tCstr, "uniform")))
			parseUniform(getStatement(token, &tCstr), ShaderDomain::TessCS);

		// Tessellation Control Shader Shader
		cStr = compSource.c_str();
		while ((token = findToken(cStr, "struct")))
			parseUniformStruct(getBlock(token, &cStr), ShaderDomain::Comp);

		cStr = compSource.c_str();
		while ((token = findToken(cStr, "uniform")))
			parseUniform(getStatement(token, &cStr), ShaderDomain::Comp);

	}

	static bool isTypeStringResource(const std::string& type)
	{
		if (type == "sampler2D")		return true;
		if (type == "sampler2DArray")		return true;
		if (type == "sampler2DMS")		return true;
		if (type == "samplerCube")		return true;
		if (type == "samplerCubeShadow")		return true;
		if (type == "sampler2DShadow")	return true;
		if (type == "imageCube")	return true;
		if (type == "image2D")	return true;
		if (type == "image2DArray")	return true;
		return false;
	}

	ShaderStruct* Shader::findStruct(const std::string& name)
	{
		BEY_PROFILE_FUNCTION();
		for (ShaderStruct* s : mStructs)
			if (s->getName() == name)
				return s;
		return nullptr;
	}

	void Shader::parseUniform(const std::string& statement, const ShaderDomain domain)
	{
		BEY_PROFILE_FUNCTION();
		std::vector<std::string> tokens = tokenize(statement);
		uint32_t index = 0;

		index++; // "uniform"
		const std::string typeString = tokens[index++];
		std::string name = tokens[index++];
		// Strip ; from name if present
		if (const char* s = strchr(name.c_str(), ';'))
			name = std::string(name.c_str(), s - name.c_str());

		const std::string n(name);
		int32_t count = 1;
		const char* namestr = n.c_str();
		if (const char* s = strchr(namestr, '['))
		{
			name = std::string(namestr, s - namestr);

			const char* end = strchr(namestr, ']');
			const std::string c(s + 1, end - s);
			count = std::atoi(c.c_str());
		}

		if (isTypeStringResource(typeString))
		{
			auto* declaration = new ShaderResourceDeclaration(ShaderResourceDeclaration::stringToType(typeString), name, count);
			mResources.push_back(declaration);
		}
		else
		{
			const ShaderUniformDeclaration::Type t = ShaderUniformDeclaration::stringToType(typeString);
			ShaderUniformDeclaration* declaration;

			if (t == ShaderUniformDeclaration::Type::NONE)
			{
				// Find struct
				ShaderStruct* s = findStruct(typeString);
				BEY_CORE_ASSERT(s, "");
				declaration = new ShaderUniformDeclaration(domain, s, name, count);
			}
			else
			{
				declaration = new ShaderUniformDeclaration(domain, t, name, count);
			}

			if (startsWith(name, "r_"))
			{
				if (domain == ShaderDomain::Vertex)
					reinterpret_cast<ShaderUniformBufferDeclaration*>(mVSRendererUniformBuffers.front())->pushUniform(declaration);
				else if (domain == ShaderDomain::Pixel)
					reinterpret_cast<ShaderUniformBufferDeclaration*>(mPSRendererUniformBuffers.front())->pushUniform(declaration);
				else if (domain == ShaderDomain::TessES)
					reinterpret_cast<ShaderUniformBufferDeclaration*>(mTessESRendererUniformBuffers.front())->pushUniform(declaration);
				else if (domain == ShaderDomain::TessCS)
					reinterpret_cast<ShaderUniformBufferDeclaration*>(mTessCSRendererUniformBuffers.front())->pushUniform(declaration);
				else if (domain == ShaderDomain::Comp)
					reinterpret_cast<ShaderUniformBufferDeclaration*>(mCompRendererUniformBuffers.front())->pushUniform(declaration);
			}
			else
			{
				if (domain == ShaderDomain::Vertex)
				{
					if (!mVSMaterialUniformBuffer)
						mVSMaterialUniformBuffer.reset(new ShaderUniformBufferDeclaration("", domain));

					mVSMaterialUniformBuffer->pushUniform(declaration);
				}
				else if (domain == ShaderDomain::Pixel)
				{
					if (!mPSMaterialUniformBuffer)
						mPSMaterialUniformBuffer.reset(new ShaderUniformBufferDeclaration("", domain));

					mPSMaterialUniformBuffer->pushUniform(declaration);
				}
				else if (domain == ShaderDomain::TessES)
				{
					if (!mTessESMaterialUniformBuffer)
						mTessESMaterialUniformBuffer.reset(new ShaderUniformBufferDeclaration("", domain));

					mTessESMaterialUniformBuffer->pushUniform(declaration);
				}
				else if (domain == ShaderDomain::TessCS)
				{
					if (!mTessCSMaterialUniformBuffer)
						mTessCSMaterialUniformBuffer.reset(new ShaderUniformBufferDeclaration("", domain));

					mTessCSMaterialUniformBuffer->pushUniform(declaration);
				}
				else if (domain == ShaderDomain::Comp)
				{
					if (!mCompMaterialUniformBuffer)
						mCompMaterialUniformBuffer.reset(new ShaderUniformBufferDeclaration("", domain));

					mCompMaterialUniformBuffer->pushUniform(declaration);
				}
			}
		}
	}

	void Shader::parseUniformStruct(const std::string& block, const ShaderDomain domain)
	{
		BEY_PROFILE_FUNCTION();
		std::vector<std::string> tokens = tokenize(block);

		uint64_t index = 0;
		index++; // struct
		std::string name = tokens[index++];
		auto* uniformStruct = new ShaderStruct(name);
		index++; // {
		while (index < tokens.size())
		{
			if (tokens[index] == "}")
				break;

			std::string type = tokens[index++];
			name = tokens[index++];

			// Strip ; from name if present
			if (const char* s = strchr(name.c_str(), ';'))
				name = std::string(name.c_str(), s - name.c_str());

			const std::string n(name);
			int32_t count = 1;
			const char* namestr = n.c_str();
			if (const char* s = strchr(namestr, '['))
			{
				name = std::string(namestr, s - namestr);

				const char* end = strchr(namestr, ']');
				const std::string c(s + 1, end - s);
				count = std::atoi(c.c_str());
				
			}

			auto* field = new ShaderUniformDeclaration(domain, ShaderUniformDeclaration::stringToType(type), name, count);
			uniformStruct->addField(field);
		}
		mStructs.push_back(uniformStruct);
	}

	void Shader::resolveUniforms()
	{
		BEY_PROFILE_FUNCTION();
		glUseProgram(mRendererID);

		for (auto& vsRendererUniformBuffer : mVSRendererUniformBuffers)
		{
			auto* decl = reinterpret_cast<ShaderUniformBufferDeclaration*>(vsRendererUniformBuffer);
			const ShaderUniformList& uniforms = decl->getUniformDeclarations();

			for (auto* j : uniforms)
			{
				auto* uniform = static_cast<ShaderUniformDeclaration*>(j);
				if (uniform->getType() == ShaderUniformDeclaration::Type::STRUCT)
				{
					const ShaderStruct& s = uniform->getShaderUniformStruct();
					const auto& fields = s.getFields();
					if (uniform->isArray())
						for (uint32_t i = 0; i < uniform->getCount(); i++)
						{
							for (auto* k : fields)
							{
								auto* field = static_cast<ShaderUniformDeclaration*>(k);
								field->mLocation = getUniformLocation(fmt::format("{}[{}].{}", uniform->mName, i, field->mName));
							}
						}
					else
						for (auto* k : fields)
						{
							auto* field = static_cast<ShaderUniformDeclaration*>(k);
							field->mLocation = getUniformLocation(uniform->mName + "." + field->mName);
						}
				}
				else
				{
					uniform->mLocation = getUniformLocation(uniform->mName);
				}
			}
		}

		for (auto& psRendererUniformBuffer : mPSRendererUniformBuffers)
		{
			auto* decl = reinterpret_cast<ShaderUniformBufferDeclaration*>(psRendererUniformBuffer);
			const ShaderUniformList& uniforms = decl->getUniformDeclarations();
			for (auto* j : uniforms)
			{
				auto* uniform = static_cast<ShaderUniformDeclaration*>(j);
				if (uniform->getType() == ShaderUniformDeclaration::Type::STRUCT)
				{
					const ShaderStruct& s = uniform->getShaderUniformStruct();
					const auto& fields = s.getFields();
					if (uniform->isArray())
						for (uint32_t i = 0; i < uniform->getCount(); i++)
						{
							for (auto* k : fields)
							{
								auto* field = static_cast<ShaderUniformDeclaration*>(k);
								field->mLocation = getUniformLocation(fmt::format("{}[{}].{}", uniform->mName, i, field->mName));
							}
						}
					else
						for (auto* k : fields)
						{
							auto* field = static_cast<ShaderUniformDeclaration*>(k);
							field->mLocation = getUniformLocation(uniform->mName + "." + field->mName);
						}
				}
				else
				{
					uniform->mLocation = getUniformLocation(uniform->mName);
				}
			}
		}

		for (auto& psRendererUniformBuffer : mTessESRendererUniformBuffers)
		{
			auto* decl = reinterpret_cast<ShaderUniformBufferDeclaration*>(psRendererUniformBuffer);
			const ShaderUniformList& uniforms = decl->getUniformDeclarations();
			for (auto* j : uniforms)
			{
				auto* uniform = static_cast<ShaderUniformDeclaration*>(j);
				if (uniform->getType() == ShaderUniformDeclaration::Type::STRUCT)
				{
					const ShaderStruct& s = uniform->getShaderUniformStruct();
					const auto& fields = s.getFields();
					if (uniform->isArray())
						for (uint32_t i = 0; i < uniform->getCount(); i++)
						{
							for (auto* k : fields)
							{
								auto* field = static_cast<ShaderUniformDeclaration*>(k);
								field->mLocation = getUniformLocation(fmt::format("{}[{}].{}", uniform->mName, i, field->mName));
							}
						}
					else
						for (auto* k : fields)
						{
							auto* field = static_cast<ShaderUniformDeclaration*>(k);
							field->mLocation = getUniformLocation(uniform->mName + "." + field->mName);
						}
				}
				else
				{
					uniform->mLocation = getUniformLocation(uniform->mName);
				}
			}
		}

		{
			for (auto& psRendererUniformBuffer : mTessCSRendererUniformBuffers)
			{
				auto* decl = reinterpret_cast<ShaderUniformBufferDeclaration*>(psRendererUniformBuffer);
				const ShaderUniformList& uniforms = decl->getUniformDeclarations();
				for (auto* j : uniforms)
				{
					auto* uniform = static_cast<ShaderUniformDeclaration*>(j);
					if (uniform->getType() == ShaderUniformDeclaration::Type::STRUCT)
					{
						const ShaderStruct& s = uniform->getShaderUniformStruct();
						const auto& fields = s.getFields();
						if (uniform->isArray())
							for (uint32_t i = 0; i < uniform->getCount(); i++)
							{
								for (auto* k : fields)
								{
									auto* field = static_cast<ShaderUniformDeclaration*>(k);
									field->mLocation = getUniformLocation(fmt::format("{}[{}].{}", uniform->mName, i, field->mName));
								}
							}
						else
							for (auto* k : fields)
							{
								auto* field = static_cast<ShaderUniformDeclaration*>(k);
								field->mLocation = getUniformLocation(uniform->mName + "." + field->mName);
							}
					}
					else
					{
						uniform->mLocation = getUniformLocation(uniform->mName);
					}
				}
			}
		}

		{
			for (auto& compRendererUniformBuffer : mCompRendererUniformBuffers)
			{
				auto* decl = reinterpret_cast<ShaderUniformBufferDeclaration*>(compRendererUniformBuffer);
				const ShaderUniformList& uniforms = decl->getUniformDeclarations();
				for (auto* j : uniforms)
				{
					auto* uniform = static_cast<ShaderUniformDeclaration*>(j);
					if (uniform->getType() == ShaderUniformDeclaration::Type::STRUCT)
					{
						const ShaderStruct& s = uniform->getShaderUniformStruct();
						const auto& fields = s.getFields();
						if (uniform->isArray())
							for (uint32_t i = 0; i < uniform->getCount(); i++)
							{
								for (auto* k : fields)
								{
									auto* field = static_cast<ShaderUniformDeclaration*>(k);
									field->mLocation = getUniformLocation(fmt::format("{}[{}].{}", uniform->mName, i, field->mName));
								}
							}
						else
							for (auto* k : fields)
							{
								auto* field = static_cast<ShaderUniformDeclaration*>(k);
								field->mLocation = getUniformLocation(uniform->mName + "." + field->mName);
							}
					}
					else
					{
						uniform->mLocation = getUniformLocation(uniform->mName);
					}
				}
			}
		}

		{
			const auto& decl = mVSMaterialUniformBuffer;
			if (decl)
			{
				const ShaderUniformList& uniforms = decl->getUniformDeclarations();
				for (auto* j : uniforms)
				{
					auto* uniform = static_cast<ShaderUniformDeclaration*>(j);
					if (uniform->getType() == ShaderUniformDeclaration::Type::STRUCT)
					{
						const ShaderStruct& s = uniform->getShaderUniformStruct();
						const auto& fields = s.getFields();
						if (uniform->isArray())
							for (uint32_t i = 0; i < uniform->getCount(); i++)
							{
								for (auto* k : fields)
								{
									auto* field = static_cast<ShaderUniformDeclaration*>(k);
									field->mLocation = getUniformLocation(fmt::format("{}[{}].{}", uniform->mName, i, field->mName));
								}
							}
						else
							for (auto* k : fields)
							{
								auto* field = static_cast<ShaderUniformDeclaration*>(k);
								field->mLocation = getUniformLocation(uniform->mName + "." + field->mName);
							}
					}
					else
					{
						uniform->mLocation = getUniformLocation(uniform->mName);
					}
				}
			}
		}

		{
			const auto& decl = mPSMaterialUniformBuffer;
			if (decl)
			{
				const ShaderUniformList& uniforms = decl->getUniformDeclarations();
				for (auto* j : uniforms)
				{
					auto* uniform = static_cast<ShaderUniformDeclaration*>(j);
					if (uniform->getType() == ShaderUniformDeclaration::Type::STRUCT)
					{
						const ShaderStruct& s = uniform->getShaderUniformStruct();
						const auto& fields = s.getFields();
						if (uniform->isArray())
							for (uint32_t i = 0; i < uniform->getCount(); i++)
							{
								for (auto* k : fields)
								{
									auto* field = static_cast<ShaderUniformDeclaration*>(k);
									field->mLocation = getUniformLocation(fmt::format("{}[{}].{}", uniform->mName, i, field->mName));
								}
							}
						else
							for (auto* k : fields)
							{
								auto* field = static_cast<ShaderUniformDeclaration*>(k);
								field->mLocation = getUniformLocation(uniform->mName + '.' + field->mName);
							}
					}
					else
					{
						uniform->mLocation = getUniformLocation(uniform->mName);
					}
				}
			}
		}

		{
			const auto& decl = mTessESMaterialUniformBuffer;
			if (decl)
			{
				const ShaderUniformList& uniforms = decl->getUniformDeclarations();
				for (auto* j : uniforms)
				{
					auto* uniform = static_cast<ShaderUniformDeclaration*>(j);
					if (uniform->getType() == ShaderUniformDeclaration::Type::STRUCT)
					{
						const ShaderStruct& s = uniform->getShaderUniformStruct();
						const auto& fields = s.getFields();
						if (uniform->isArray())
							for (uint32_t i = 0; i < uniform->getCount(); i++)
							{
								for (auto* k : fields)
								{
									auto* field = static_cast<ShaderUniformDeclaration*>(k);
									field->mLocation = getUniformLocation(fmt::format("{}[{}].{}", uniform->mName, i, field->mName));
								}
							}
						else
							for (auto* k : fields)
							{
								auto* field = static_cast<ShaderUniformDeclaration*>(k);
								field->mLocation = getUniformLocation(uniform->mName + "." + field->mName);
							}
					}
					else
					{
						uniform->mLocation = getUniformLocation(uniform->mName);
					}
				}
			}
		}

		{
			const auto& decl = mTessCSMaterialUniformBuffer;
			if (decl)
			{
				const ShaderUniformList& uniforms = decl->getUniformDeclarations();
				for (auto* j : uniforms)
				{
					auto* uniform = static_cast<ShaderUniformDeclaration*>(j);
					if (uniform->getType() == ShaderUniformDeclaration::Type::STRUCT)
					{
						const ShaderStruct& s = uniform->getShaderUniformStruct();
						const auto& fields = s.getFields();
						if (uniform->isArray())
							for (uint32_t i = 0; i < uniform->getCount(); i++)
							{
								for (auto* k : fields)
								{
									auto* field = static_cast<ShaderUniformDeclaration*>(k);
									field->mLocation = getUniformLocation(fmt::format("{}[{}].{}", uniform->mName, i, field->mName));
								}
							}
						else
							for (auto* k : fields)
							{
								auto* field = static_cast<ShaderUniformDeclaration*>(k);
								field->mLocation = getUniformLocation(uniform->mName + "." + field->mName);
							}
					}
					else
					{
						uniform->mLocation = getUniformLocation(uniform->mName);
					}
				}
			}
		}

		{
			const auto& decl = mCompMaterialUniformBuffer;
			if (decl)
			{
				const ShaderUniformList& uniforms = decl->getUniformDeclarations();
				for (auto* j : uniforms)
				{
					auto* uniform = static_cast<ShaderUniformDeclaration*>(j);
					if (uniform->getType() == ShaderUniformDeclaration::Type::STRUCT)
					{
						const ShaderStruct& s = uniform->getShaderUniformStruct();
						const auto& fields = s.getFields();
						if (uniform->isArray())
							for (uint32_t i = 0; i < uniform->getCount(); i++)
							{
								for (auto* k : fields)
								{
									auto* field = static_cast<ShaderUniformDeclaration*>(k);
									field->mLocation = getUniformLocation(fmt::format("{}[{}].{}", uniform->mName, i, field->mName));
								}
							}
						else
							for (auto* k : fields)
							{
								auto* field = static_cast<ShaderUniformDeclaration*>(k);
								field->mLocation = getUniformLocation(uniform->mName + "." + field->mName);
							}
					}
					else
					{
						uniform->mLocation = getUniformLocation(uniform->mName);
					}
				}
			}
		}


		uint32_t sampler = 0;
		for (auto& resource : mResources)
		{
			auto* resourceDecl = static_cast<ShaderResourceDeclaration*>(resource);
			const int32_t location = getUniformLocation(resourceDecl->mName);

			if (resourceDecl->getCount() == 1)
			{
				resourceDecl->mRegister = sampler;
				if (location != -1)
					uploadUniformInt(location, sampler);

				sampler++;
			}
			else if (resourceDecl->getCount() > 1)
			{
				resourceDecl->mRegister = 0;
				const uint32_t count = resourceDecl->getCount();
				int* samplers = new int[count];
				for (uint32_t s = 0; s < count; s++)
					samplers[s] = s;
				uploadUniformIntArray(resourceDecl->getName(), samplers, count);
				delete[] samplers;
			}
		}
	}

	void Shader::validateUniforms()
	{

	}

	int32_t Shader::getUniformLocation(const std::string& name)
	{
		BEY_PROFILE_FUNCTION();
		if (mLocationCache.find(name) != mLocationCache.end())
			return mLocationCache[name];
		const int loc = glGetUniformLocation(mRendererID, name.c_str());
		if (loc == -1)
			BEY_CORE_WARN("Could not find uniform '{0}' in shader '{1}'", name, mName);
		else
			mLocationCache[name] = loc;
		return loc;
	}



	GLenum Shader::shaderTypeFromString(const std::string& type)
	{
		if (type == ".vert")
			return GL_VERTEX_SHADER;
		if (type == ".frag")
			return GL_FRAGMENT_SHADER;
		if (type == ".comp")
			return GL_COMPUTE_SHADER;
		if (type == ".geom")
			return GL_GEOMETRY_SHADER;
		if (type == ".tesc")
			return GL_TESS_CONTROL_SHADER;
		if (type == ".tese")
			return GL_TESS_EVALUATION_SHADER;
		return GL_NONE;
	}

	void Shader::compileAndUploadShader()
	{
		BEY_PROFILE_FUNCTION();
		std::vector<GLuint> shaderRendererIDs;

		const GLuint program = glCreateProgram();
		for (auto& kv : mShaderSource)
		{
			const auto& [type, source] = kv;

			if (source.empty())
				continue;
			GLuint shaderRendererID = glCreateShader(type);
			const auto* sourceCstr = static_cast<const GLchar*>(source.c_str());
			glShaderSource(shaderRendererID, 1, &sourceCstr, nullptr);

			glCompileShader(shaderRendererID);

			GLint isCompiled = 0;
			glGetShaderiv(shaderRendererID, GL_COMPILE_STATUS, &isCompiled);
			if (isCompiled == GL_FALSE)
			{
				GLint maxLength = 0;
				glGetShaderiv(shaderRendererID, GL_INFO_LOG_LENGTH, &maxLength);

				// The maxLength includes the NULL character
				std::vector<GLchar> infoLog(maxLength);
				glGetShaderInfoLog(shaderRendererID, maxLength, &maxLength, &infoLog[0]);

				BEY_CORE_ERROR("Shader compilation failed:\n{0}", &infoLog[0]);

				// We don't need the shader anymore.
				glDeleteShader(shaderRendererID);

				BEY_CORE_ASSERT(false, "Failed");
			}

			shaderRendererIDs.push_back(shaderRendererID);
			glAttachShader(program, shaderRendererID);
		}

		// Link our program
		glLinkProgram(program);

		// Note the different functions here: glGetProgram* instead of glGetShader*.
		GLint isLinked = 0;
		glGetProgramiv(program, GL_LINK_STATUS, static_cast<int*>(&isLinked));
		if (isLinked == GL_FALSE)
		{
			GLint maxLength = 0;
			glGetProgramiv(program, GL_INFO_LOG_LENGTH, &maxLength);

			// The maxLength includes the NULL character
			std::vector<GLchar> infoLog(maxLength);
			glGetProgramInfoLog(program, maxLength, &maxLength, &infoLog[0]);
			BEY_CORE_ERROR("Shader: \"{1}\" compilation failed:\n{0}", & infoLog[0], mName);

			// We don't need the program anymore.
			glDeleteProgram(program);
			// Don't leak shaders either.
			for (auto id : shaderRendererIDs)
				glDeleteShader(id);
		}

		// Always detach shaders after a successful link.
		for (auto id : shaderRendererIDs)
			glDetachShader(program, id);

		mRendererID = program;
	}

	void Shader::setVSMaterialUniformBuffer(Buffer buffer) const
	{
		BEY_PROFILE_FUNCTION();
		Renderer::submit([this, buffer]() {
			BEY_PROFILE_SCOPE("setVSMaterialUniformBuffer sub");
			resolveAndSetUniforms(mVSMaterialUniformBuffer, buffer);
			});
	}

	void Shader::setPSMaterialUniformBuffer(Buffer buffer) const
	{
		BEY_PROFILE_FUNCTION();
		Renderer::submit([this, buffer]() {
			BEY_PROFILE_SCOPE("setPSMaterialUniformBuffer sub");
			resolveAndSetUniforms(mPSMaterialUniformBuffer, buffer);
			});
	}

	void Shader::setTessESMaterialUniformBuffer(Buffer buffer) const
	{
		BEY_PROFILE_FUNCTION();
		Renderer::submit([this, buffer]() {
			BEY_PROFILE_SCOPE("setTessESMaterialUniformBuffer sub");
			resolveAndSetUniforms(mTessESMaterialUniformBuffer, buffer);
			});
	}

	void Shader::setTessCSMaterialUniformBuffer(Buffer buffer) const
	{
		BEY_PROFILE_FUNCTION();
		Renderer::submit([this, buffer]() {
			BEY_PROFILE_SCOPE("setTessCSMaterialUniformBuffer sub");
			resolveAndSetUniforms(mTessCSMaterialUniformBuffer, buffer);
			});
	}

	void Shader::setCompMaterialUniformBuffer(Buffer buffer) const
	{
		BEY_PROFILE_FUNCTION();
		Renderer::submit([this, buffer]() {
			BEY_PROFILE_SCOPE("setCompMaterialUniformBuffer sub");
			resolveAndSetUniforms(mCompMaterialUniformBuffer, buffer);
			});
	}

	void Shader::resolveAndSetUniforms(const Ref<ShaderUniformBufferDeclaration>& decl, const Buffer buffer) const
	{
		BEY_PROFILE_FUNCTION();
		const ShaderUniformList& uniforms = decl->getUniformDeclarations();
		for (ShaderUniformDeclaration* uniform : uniforms)
		{
			if (uniform->isArray())
				resolveAndSetUniformArray(uniform, buffer);
			else
				resolveAndSetUniform(uniform, buffer);
		}
	}

	void Shader::resolveAndSetUniform(ShaderUniformDeclaration* uniform, const Buffer buffer) const
	{
		BEY_PROFILE_FUNCTION();


		BEY_CORE_ASSERT(!(uniform->getLocation() == -1 && !uniform->isStruct()), "Uniform has invalid location!");

		const uint32_t offset = uniform->getOffset();
		using enum ShaderUniformDeclaration::Type;
		
		switch (uniform->getType())
		{
		case FLOAT32:
			uploadUniformFloat(uniform->getLocation(), *reinterpret_cast<float*>(&buffer.Data[offset]));
			break;
		case UINT32:
			uploadUniformUInt(uniform->getLocation(), *reinterpret_cast<uint32_t*>(&buffer.Data[offset]));
			break;
		case INT32:
			uploadUniformInt(uniform->getLocation(), *reinterpret_cast<int32_t*>(&buffer.Data[offset]));
			break;
		case BOOL:
			uploadUniformInt(uniform->getLocation(), *reinterpret_cast<bool*>(&buffer.Data[offset]));
			break;
		case VEC2:
			uploadUniformFloat2(uniform->getLocation(), *reinterpret_cast<glm::vec2*>(&buffer.Data[offset]));
			break;
		case IVEC2:
			uploadUniformInt2(uniform->getLocation(), *reinterpret_cast<glm::ivec2*>(&buffer.Data[offset]));
			break;
		case VEC3:
			uploadUniformFloat3(uniform->getLocation(), *reinterpret_cast<glm::vec3*>(&buffer.Data[offset]));
			break;
		case VEC4:
			uploadUniformFloat4(uniform->getLocation(), *reinterpret_cast<glm::vec4*>(&buffer.Data[offset]));
			break;
		case MAT3:
			uploadUniformMat3(uniform->getLocation(), *reinterpret_cast<glm::mat3*>(&buffer.Data[offset]));
			break;
		case MAT4:
			uploadUniformMat4(uniform->getLocation(), *reinterpret_cast<glm::mat4*>(&buffer.Data[offset]));
			break;
		case STRUCT:
			uploadUniformStruct(uniform, buffer.Data, offset);
			break;
		case NONE:
		default:
			BEY_CORE_ASSERT(false, "Unknown uniform type!");
			break;
		}
	}

	void Shader::resolveAndSetUniformArray(ShaderUniformDeclaration* uniform, const Buffer buffer) const
	{
		//BEY_CORE_ASSERT(uniform->GetLocation() != -1, "Uniform has invalid location!");
		BEY_PROFILE_FUNCTION();
		using enum ShaderUniformDeclaration::Type;
		switch (uniform->getType())
		{
		case FLOAT32:
			uploadUniformFloat(uniform->getLocation(), *reinterpret_cast<float*>(&buffer.Data[uniform->getOffset()]));
			break;
		case INT32:
			uploadUniformInt(uniform->getLocation(), *reinterpret_cast<int32_t*>(&buffer.Data[uniform->getOffset()]));
			break;
		case UINT32:
			uploadUniformUInt(uniform->getLocation(), *reinterpret_cast<uint32_t*>(&buffer.Data[uniform->getOffset()]));
			break;
		case BOOL:
			uploadUniformInt(uniform->getLocation(), *reinterpret_cast<bool*>(&buffer.Data[uniform->getOffset()]));
			break;
		case VEC2:
			uploadUniformFloat2(uniform->getLocation(), *reinterpret_cast<glm::vec2*>(&buffer.Data[uniform->getOffset()]));
			break;
		case IVEC2:
			uploadUniformInt2(uniform->getLocation(), *reinterpret_cast<glm::ivec2*>(&buffer.Data[uniform->getOffset()]));
			break;
		case VEC3:
			uploadUniformFloat3(uniform->getLocation(), *reinterpret_cast<glm::vec3*>(&buffer.Data[uniform->getOffset()]));
			break;
		case VEC4:
			uploadUniformFloat4(uniform->getLocation(), *reinterpret_cast<glm::vec4*>(&buffer.Data[uniform->getOffset()]));
			break;
		case MAT3:
			uploadUniformMat3(uniform->getLocation(), *reinterpret_cast<glm::mat3*>(&buffer.Data[uniform->getOffset()]));
			break;
		/*case MAT4:
			uploadUniformMat4Array(uniform->getLocation(), *reinterpret_cast<glm::mat4*>(&buffer.Data[uniform->getOffset()]), uniform->getCount());*/
			//break;
		case STRUCT:
			uploadUniformStruct(uniform, buffer.Data, uniform->getOffset());
			break;
		default:
			BEY_CORE_ASSERT(false, "Unknown uniform type!");
		}
	}

	void Shader::resolveAndSetUniformField(const ShaderUniformDeclaration& field, std::uint8_t* data, const int32_t offset) const
	{
		BEY_PROFILE_FUNCTION();

		using enum ShaderUniformDeclaration::Type;

		switch (field.getType())
		{
		case FLOAT32:
			uploadUniformFloat(field.getLocation(), *reinterpret_cast<float*>(&data[offset]));
			break;
		case INT32:
			uploadUniformInt(field.getLocation(), *reinterpret_cast<int32_t*>(&data[offset]));
			break;
		case UINT32:
			uploadUniformUInt(field.getLocation(), *reinterpret_cast<uint32_t*>(&data[offset]));
			break;
		case BOOL:
			uploadUniformInt(field.getLocation(), *reinterpret_cast<bool*>(&data[offset]));
			break;
		case VEC2:
			uploadUniformFloat2(field.getLocation(), *reinterpret_cast<glm::vec2*>(&data[offset]));
			break;
		case IVEC2:
			uploadUniformInt2(field.getLocation(), *reinterpret_cast<glm::ivec2*>(&data[offset]));
			break;
		case VEC3:
			uploadUniformFloat3(field.getLocation(), *reinterpret_cast<glm::vec3*>(&data[offset]));
			break;
		case VEC4:
			uploadUniformFloat4(field.getLocation(), *reinterpret_cast<glm::vec4*>(&data[offset]));
			break;
		case MAT3:
			uploadUniformMat3(field.getLocation(), *reinterpret_cast<glm::mat3*>(&data[offset]));
			break;
		case MAT4:
			uploadUniformMat4(field.getLocation(), *reinterpret_cast<glm::mat4*>(&data[offset]));
			break;
		default:
			BEY_CORE_ASSERT(false, "Unknown uniform type!");
		}
	}

	/*void Shader::uploadUniformBuffer(const UniformBufferDeclaration<5,5>& uniformBuffer)
	{
		BEY_PROFILE_FUNCTION();

		for (unsigned int i = 0; i < uniformBuffer.getUniformCount(); i++)
		{
			const UniformDecl& decl = uniformBuffer.getUniforms()[i];
			switch (decl.Type)
			{
			case UniformType::Float:
			{
				const std::string& name = decl.Name;
				const float value = *(float*)(uniformBuffer.getBuffer() + decl.Offset);
				Renderer::submit([this, name, value]() {
					uploadUniformFloat(name, value);
					});
				break;
			}
			case UniformType::Float3:
			{
				const std::string& name = decl.Name;
				glm::vec3& values = *(glm::vec3*)(uniformBuffer.getBuffer() + decl.Offset);
				Renderer::submit([this, name, values]() {
					uploadUniformFloat3(name, values);
					});
				break;
			}
			case UniformType::Float4:
			{
				const std::string& name = decl.Name;
				glm::vec4& values = *(glm::vec4*)(uniformBuffer.getBuffer() + decl.Offset);
				Renderer::submit([this, name, values]() {
					uploadUniformFloat4(name, values);
					});
				break;
			}
			case UniformType::Matrix4x4:
			{
				const std::string& name = decl.Name;
				glm::mat4& values = *(glm::mat4*)(uniformBuffer.getBuffer() + decl.Offset);
				Renderer::submit([this, name, values]() {
					uploadUniformMat4(name, values);
					});
				break;
			}

			default: BEY_ASSERT(false, "Tried to upload uniform with unknown type");
			}
		}
	}*/

	void Shader::setFloat(const std::string& name, const float value)
	{
		BEY_PROFILE_FUNCTION();

		Renderer::submit([this, name, value]() {
			BEY_PROFILE_SCOPE("setFloat");
			uploadUniformFloat(name, value);
			});
	}

	void Shader::setInt(const std::string& name, const int value)
	{
		BEY_PROFILE_FUNCTION();

		Renderer::submit([this, name, value]() {
			BEY_PROFILE_SCOPE("setInt");
			uploadUniformInt(name, value);
			});
	}

	void Shader::setInt2(const std::string& name, glm::ivec2 vec)
	{
		BEY_PROFILE_FUNCTION();

		Renderer::submit([this, name, vec]() {
			BEY_PROFILE_SCOPE("setInt2");
			uploadUniformInt2(name, vec);
			});
	}

	void Shader::setUInt(const std::string& name, const unsigned value)
	{
		BEY_PROFILE_FUNCTION();

		Renderer::submit([this, name, value]() {
			BEY_PROFILE_SCOPE("setUInt");
			uploadUniformUInt(name, value);
			});
	}

	void Shader::setFloat2(const std::string& name, const glm::vec2& value)
	{
		BEY_PROFILE_FUNCTION();

		Renderer::submit([this, name, value]() {
			BEY_PROFILE_SCOPE("setFloat2");
			uploadUniformFloat2(name, value);
			});
	}

	void Shader::setFloat3(const std::string& name, const glm::vec3& value)
	{
		BEY_PROFILE_FUNCTION();

		Renderer::submit([this, name, value]() {
			BEY_PROFILE_SCOPE("setFloat3");
			uploadUniformFloat3(name, value);
			});
	}

	void Shader::setFloat4(const std::string& name, const glm::vec4& value)
	{
		Renderer::submit([this, name, value]() {
			BEY_PROFILE_SCOPE("setFloat4");
			uploadUniformFloat4(name, value);
			});
	}

	void Shader::setMat4(const std::string& name, const glm::mat4& value)
	{
		BEY_PROFILE_FUNCTION();

		Renderer::submit([this, name, value]() {
			BEY_PROFILE_SCOPE("setMat4");
			uploadUniformMat4(name, value);
			});
	}

	void Shader::setMat4FromRenderThread(const std::string& name, const glm::mat4& value, const bool bind)
	{
		BEY_PROFILE_FUNCTION();

		if (bind)
			uploadUniformMat4(name, value);
		else
		{
			const int location = glGetUniformLocation(mRendererID, name.c_str());
			if (location != -1)
				uploadUniformMat4(location, value);
		}
	}

	void Shader::setIntArray(const std::string& name, int* values, const uint32_t size)
	{
		BEY_PROFILE_FUNCTION();

		Renderer::submit([this, name, values, size]() {
			BEY_PROFILE_SCOPE("setIntArray");
			uploadUniformIntArray(name, values, size);
			});
	}


	void Shader::setVec3Array(const std::string& name, const std::vector<glm::vec3>& values, const uint32_t size)
	{
		BEY_PROFILE_FUNCTION();
		Renderer::submit([this, name, values, size]() {
			BEY_PROFILE_SCOPE("setVec3Array");
			uploadUniformVec3Array(getUniformLocation(name), values, size);
			});
	}

	void Shader::setVec4Array(const std::string& name, const std::vector<glm::vec4>& values, uint32_t size)
	{
		BEY_PROFILE_FUNCTION();
		Renderer::submit([this, name, values, size]() {
			BEY_PROFILE_SCOPE("setVec4Array");
			uploadUniformVec4Array(getUniformLocation(name), values, size);
			});
	}

	void Shader::setMat4Vector(const std::string& name, const std::vector<glm::mat4>& values, const uint32_t size)
	{
		BEY_PROFILE_FUNCTION();
		Renderer::submit([this, name, values, size]() {
			BEY_PROFILE_SCOPE("setMat4Array");
			uploadUniformMat4Vector(getUniformLocation(name), values, size);
			});
	}

	void Shader::setMat4Array(const std::string& name, const glm::mat4 values[], const uint32_t size)
	{
		BEY_PROFILE_FUNCTION();
		Renderer::submit([this, name, values, size]() {
			BEY_PROFILE_SCOPE("setMat4Array");
			uploadUniformMat4Array(getUniformLocation(name), values, size);
			});
	}

	void Shader::uploadUniformInt(const uint32_t location, const int32_t value)
	{
		BEY_PROFILE_FUNCTION();
		glUniform1i(location, value);
	}

	void Shader::uploadUniformInt2(const int32_t location, const glm::ivec2 value)
	{
		BEY_PROFILE_FUNCTION();
		glUniform2i(location, value.x, value.y);
	}

	void Shader::uploadUniformUInt(const uint32_t location, const uint32_t value)
	{
		BEY_PROFILE_FUNCTION();
		glUniform1ui(location, value);
	}
	
	void Shader::uploadUniformIntArray(const uint32_t location, int32_t* values, const int32_t count)
	{
		BEY_PROFILE_FUNCTION();
		glUniform1iv(location, count, values);
	}

	void Shader::uploadUniformFloat(const uint32_t location, const float value)
	{
		BEY_PROFILE_FUNCTION();
		glUniform1f(location, value);
	}

	void Shader::uploadUniformFloat2(const uint32_t location, const glm::vec2& value)
	{
		BEY_PROFILE_FUNCTION();
		glUniform2f(location, value.x, value.y);
	}

	void Shader::uploadUniformFloat3(const uint32_t location, const glm::vec3& value)
	{
		BEY_PROFILE_FUNCTION();
		glUniform3f(location, value.x, value.y, value.z);
	}

	void Shader::uploadUniformFloat4(const uint32_t location, const glm::vec4& value)
	{
		BEY_PROFILE_FUNCTION();
		glUniform4f(location, value.x, value.y, value.z, value.w);
	}

	void Shader::uploadUniformMat3(const uint32_t location, const glm::mat3& value)
	{
		BEY_PROFILE_FUNCTION();
		glUniformMatrix3fv(location, 1, GL_FALSE, glm::value_ptr(value));
	}

	void Shader::uploadUniformMat4(const uint32_t location, const glm::mat4& value)
	{
		BEY_PROFILE_FUNCTION();
		glUniformMatrix4fv(location, 1, GL_FALSE, glm::value_ptr(value));
	}

	void Shader::uploadUniformVec3Array(const uint32_t location, const std::vector<glm::vec3>& values, const uint32_t count) const
	{
		BEY_PROFILE_FUNCTION();
		glUniform3fv(location, count, glm::value_ptr(*values.data()));
	}

	void Shader::uploadUniformVec4Array(const uint32_t location, const std::vector<glm::vec4>& values, const uint32_t count) const
	{
		BEY_PROFILE_FUNCTION();
		glUniform4fv(location, count, glm::value_ptr(*values.data()));
	}


	void Shader::uploadUniformMat4Vector(const uint32_t location, const std::vector<glm::mat4>& values, const uint32_t count) const
	{
		BEY_PROFILE_FUNCTION();
		glUniformMatrix4fv(location, count, GL_FALSE, glm::value_ptr(*values.data()));
	}

	void Shader::uploadUniformMat4Array(const uint32_t location, const glm::mat4 values[], const uint32_t count) const
	{
		BEY_PROFILE_FUNCTION();
		glUniformMatrix4fv(location, count, GL_FALSE, glm::value_ptr(values[0]));
	}

	void Shader::uploadUniformStruct(ShaderUniformDeclaration* uniform, std::uint8_t* buffer, uint32_t offset) const
	{
		BEY_PROFILE_FUNCTION();
		const ShaderStruct& s = uniform->getShaderUniformStruct();
		const auto& fields = s.getFields();
		for (auto* k : fields)
		{
			auto* field = static_cast<ShaderUniformDeclaration*>(k);
			resolveAndSetUniformField(*field, buffer, offset);
			offset += field->mSize;
		}
	}


	//////////////////////////////////////////////////////////////////////////////

	void Shader::resolveGLUniformBuffer(const std::string& bufferName, Ref<UniformBuffer> uniformBuffer) const noexcept
	{
		Ref<const Shader> ptr = this;
		Renderer::submit([ptr, bufferName, uniformBuffer]() mutable
			{
				uint32_t& index = uniformBuffer->getIndex();
			
				index = glGetUniformBlockIndex(ptr->mRendererID, bufferName.c_str());
				glBindBufferBase(GL_UNIFORM_BUFFER, index, uniformBuffer->getRendererID());
				glUniformBlockBinding(ptr->mRendererID, index, uniformBuffer->getBinding());
			});
	}

	void Shader::resolveGLUniformBuffer(const std::string& bufferName, Ref<StorageUniformBuffer> uniformBuffer) const noexcept
	{
		Ref<const Shader> ptr = this;
		Renderer::submit([ptr, bufferName, uniformBuffer]() mutable
			{
				uint32_t& index = uniformBuffer->getIndex();

				index = glGetProgramResourceIndex(ptr->mRendererID, GL_SHADER_STORAGE_BLOCK, bufferName.c_str());
				glBindBufferBase(GL_SHADER_STORAGE_BUFFER, index, uniformBuffer->getRendererID());
				glShaderStorageBlockBinding(ptr->mRendererID, index, uniformBuffer->getBinding());
			});
	}


	
	//////////////////////////////////////////////////////////////////////////////

	void Shader::uploadUniformInt(const std::string& name, const int32_t value)
	{
		BEY_PROFILE_FUNCTION();
		glUniform1i(getUniformLocation(name), value);
	}

	void Shader::uploadUniformInt2(const std::string& name, const glm::ivec2& value)
	{
		BEY_PROFILE_FUNCTION();
		glUniform2i(getUniformLocation(name), value.x, value.y);
	}

	void Shader::uploadUniformUInt(const std::string& name, const uint32_t value)
	{
		BEY_PROFILE_FUNCTION();
		glUniform1ui(getUniformLocation(name), value);
	}

	void Shader::uploadUniformIntArray(const std::string& name, int32_t* values, const uint32_t count)
	{
		BEY_PROFILE_FUNCTION();
		glUniform1iv(getUniformLocation(name), count, values);
	}

	void Shader::uploadUniformFloat(const std::string& name, const float value)
	{
		BEY_PROFILE_FUNCTION();
		glUniform1f(getUniformLocation(name), value);
	}

	void Shader::uploadUniformFloat2(const std::string& name, const glm::vec2& value)
	{
		BEY_PROFILE_FUNCTION();
		glUniform2f(getUniformLocation(name), value.x, value.y);
	}

	void Shader::uploadUniformFloat3(const std::string& name, const glm::vec3& value)
	{
		BEY_PROFILE_FUNCTION();
		glUniform3f(getUniformLocation(name), value.x, value.y, value.z);
	}

	void Shader::uploadUniformFloat4(const std::string& name, const glm::vec4& value)
	{
		BEY_PROFILE_FUNCTION();
		glUniform4f(getUniformLocation(name), value.x, value.y, value.z, value.w);
	}

	void Shader::uploadUniformMat4(const std::string& name, const glm::mat4& value)
	{
		BEY_PROFILE_FUNCTION();
		glUniformMatrix4fv(getUniformLocation(name), 1, GL_FALSE, reinterpret_cast<const float*>(&value));
	}

}