#pragma once
#include "Framebuffer.h"


namespace bey {

	struct RenderPassSpecification
	{
		Ref<FrameBuffer> TargetFramebuffer;
	};

	
	class RenderPass final : public RefCounted
	{
	public:

		explicit RenderPass(const RenderPassSpecification& spec);
		static Ref<RenderPass> create(const RenderPassSpecification& spec);

		~RenderPass();

		RenderPassSpecification& getSpecification()
		{
			return mSpecification;
		}
		const RenderPassSpecification& getSpecification() const { return mSpecification; }
	private:
		RenderPassSpecification mSpecification;
	};

}
