#pragma once

#include <pchs.h>

namespace bey {

	using RendererID = GLuint;

	// TODO: move into separate header
	enum class PrimitiveType
	{
		None = 0, Triangles, Lines,
		TriangleStrip
	};

	struct RenderAPICapabilities
	{
		std::string Vendor;
		std::string Renderer;
		std::string Version;

		int MaxSamples = 0;
		int MaxAnisotropy = 0;
		int MaxTextureUnits = 0;
		int MaxPatchVertices = 0;
	};

	class RendererAPI
	{
	public:
		static void init();
		static void shutdown();

		static void clear(float r, float g, float b, float a);
		static void clearDepth();
		static void setClearColor(float r, float g, float b, float a);

		static void drawArrays(uint32_t first, uint32_t count, PrimitiveType type, bool depthTest = true);
		static void drawIndexed(uint32_t count, PrimitiveType type, bool depthTest = true);
		static void setLineThickness(float thickness);

		static RenderAPICapabilities& getCapabilities()
		{
			static RenderAPICapabilities capabilities;
			return capabilities;
		}

	private:
		static void loadRequiredAssets();
	};

}