#pragma once
#include <GLFW/glfw3.h>


namespace bey {

	struct WindowProps
	{
		std::string Title;
		unsigned int Width;
		unsigned int Height;
		unsigned int RefreshRate;

		explicit WindowProps(std::string title = "Beyond Editor",
		                     const unsigned int refreshRate = 60,
		                     const unsigned int width = 1280,
		                     const unsigned int height = 720)
			: Title(std::move(title)), Width(width), Height(height), RefreshRate(refreshRate)
		{
		}
	};
	
	using EventCallbackFn = std::function<void(Event&)>;

	struct WindowData
	{
		std::string Title;
		uint32_t PosX = 0, PosY = 0;
		uint32_t Width = 0, Height = 0, RefreshRate = 0;
		bool VSync = false;
		bool Focused = true;
		bool FullScreen = false;

		EventCallbackFn EventCallback;
	};


	class Window final : public RefCounted
	{
	public:
		explicit Window(const WindowProps& props);
		static Window* create(const WindowProps& props = WindowProps());

		void onUpdate();

		[[nodiscard]] unsigned int getWidth() const { return mData.Width; }
		[[nodiscard]] unsigned int getHeight() const { return mData.Height; }
		[[nodiscard]] unsigned int getRefreshRate() const { return mData.RefreshRate; }
		[[nodiscard]] bool isFocused() const { return mData.Focused; }
		[[nodiscard]] bool isFullscreen() const { return mData.FullScreen; }
		GLFWwindow* getNativeWindow() const { return mWindow; }

		std::pair<uint32_t, uint32_t> getSize() const { return { mData.Width, mData.Height }; }
		std::pair<float, float> getWindowPos() const;

		// Window attributes
		void setEventCallback(const EventCallbackFn& callback) { mData.EventCallback = callback; }
		void setVSync(bool enabled);
		void toggleFullscreen();
		bool isVSync() const;

		// ReSharper disable once CppNonExplicitConversionOperator
		operator GLFWwindow* () const { return mWindow; }

	private:
		void init(const WindowProps& props);
		void shutdown();
	private:
		GLFWwindow* mWindow = nullptr;
		GLFWcursor* mImGuiMouseCursors[9] = { nullptr };

		
		WindowData mData;
		WindowData mBackupData;

		double mLastFrameTime = 0.0;
	};

}
