#include "pchs.h"
#include "Window.hpp"

#include "Beyond/Core/Events/ApplicationEvent.h"
#include "Beyond/Core/Events/KeyEvent.h"
#include "Beyond/Core/Events/MouseEvent.h"

#include <imgui.h>
#include "Beyond/Renderer/Renderer.h"

namespace bey {

	static void glfwErrorCallback(const int error, const char* description)
	{
		BEY_CORE_ERROR("GLFW Error ({0}): {1}", error, description);
	}

	static bool s_GLFWInitialized = false;

	Window* Window::create(const WindowProps& props)
	{
		BEY_PROFILE_FUNCTION();

		return new Window(props);
	}

	Window::Window(const WindowProps& props)
	{
		BEY_PROFILE_FUNCTION();
		init(props);

	}


	void Window::init(const WindowProps& props)
	{
		BEY_PROFILE_FUNCTION();

		mData.Title = props.Title;
		mData.Width = props.Width;
		mData.Height = props.Height;
		mData.RefreshRate = props.RefreshRate;


		BEY_CORE_INFO("Creating window {0} ({1}, {2})", props.Title, props.Width, props.Height);
		BEY_CORE_INFO("Thread ID {}", std::this_thread::get_id());

		if (!s_GLFWInitialized)
		{
			// TODO: glfwTerminate on system shutdown
			const int success = glfwInit();
			BEY_CORE_ASSERT(success == GLFW_TRUE, "Could not intialize GLFW!");
			glfwSetErrorCallback(glfwErrorCallback);

			s_GLFWInitialized = true;
		}

		glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4);
		glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 6);
		glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_COMPAT_PROFILE);

		mWindow = glfwCreateWindow(int(props.Width), int(props.Height), mData.Title.c_str(), nullptr, nullptr);
		glfwMakeContextCurrent(mWindow);
		glfwMaximizeWindow(mWindow);

		[[maybe_unused]] const int status = gladLoadGLLoader(GLADloadproc(glfwGetProcAddress));
		BEY_CORE_ASSERT(status, "Failed to initialize Glad!");

		glfwSetWindowUserPointer(mWindow, &mData);

		glfwSetInputMode(mWindow, GLFW_STICKY_MOUSE_BUTTONS, GLFW_TRUE);


		// Set GLFW callbacks
		glfwSetWindowSizeCallback(mWindow, [](GLFWwindow* window, const int width, const int height)
			{
				auto& data = *static_cast<WindowData*>(glfwGetWindowUserPointer(window));

				WindowResizeEvent event(static_cast<unsigned int>(width), static_cast<unsigned int>(height));
				data.EventCallback(event);
				data.Width = width;
				data.Height = height;
			});

		glfwSetWindowPosCallback(mWindow, [](GLFWwindow* window, const int posX, const int posY)
			{
				auto& data = *static_cast<WindowData*>(glfwGetWindowUserPointer(window));

				WindowMovedEvent event(static_cast<unsigned int>(posX), static_cast<unsigned int>(posY));
				data.EventCallback(event);
				data.PosX = posX;
				data.PosY = posY;
			});


		glfwSetWindowFocusCallback(mWindow, [](GLFWwindow* window, const int focused)
			{
				auto& data = *static_cast<WindowData*>(glfwGetWindowUserPointer(window));

				WindowFocusEvent event(focused);
				data.EventCallback(event);
				data.Focused = focused;
			});


		glfwSetWindowCloseCallback(mWindow, [](GLFWwindow* window)
			{
				auto& data = *static_cast<WindowData*>(glfwGetWindowUserPointer(window));

				WindowCloseEvent event;
				data.EventCallback(event);
			});

		glfwSetKeyCallback(mWindow, [](GLFWwindow* window, int key, int, const int action, int)
			{
				auto& data = *static_cast<WindowData*>(glfwGetWindowUserPointer(window));
				switch (action)
				{
				case GLFW_PRESS:
				{
					KeyPressedEvent event(KeyCode(key), 0);
					data.EventCallback(event);
					break;
				}
				case GLFW_RELEASE:
				{
					KeyReleasedEvent event(static_cast<KeyCode>(key));
					data.EventCallback(event);
					break;
				}
				case GLFW_REPEAT:
				{
					KeyPressedEvent event(KeyCode(key), 1);
					data.EventCallback(event);
					break;
				}
				default:;
				}
			});

		glfwSetCharCallback(mWindow, [](GLFWwindow* window, unsigned int codepoint)
			{

				auto& data = *static_cast<WindowData*>(glfwGetWindowUserPointer(window));
				KeyTypedEvent event(static_cast<KeyCode>(codepoint));
				data.EventCallback(event);

			});

		glfwSetMouseButtonCallback(mWindow, [](GLFWwindow* window, const int button, const int action, int mods)
			{
				auto& data = *static_cast<WindowData*>(glfwGetWindowUserPointer(window));

				switch (action)
				{
				case GLFW_PRESS:
				{
					MouseButtonPressedEvent event(button);
					data.EventCallback(event);
					break;
				}
				case GLFW_RELEASE:
				{
					MouseButtonReleasedEvent event(button);
					data.EventCallback(event);
					break;
				}
				default:;
				}

			});

		glfwSetScrollCallback(mWindow, [](GLFWwindow* window, const double xOffset, const double yOffset)
			{
				auto& data = *static_cast<WindowData*>(glfwGetWindowUserPointer(window));

				MouseScrolledEvent event(static_cast<float>(xOffset), static_cast<float>(yOffset));
				data.EventCallback(event);
			});

		glfwSetCursorPosCallback(mWindow, [](GLFWwindow* window, const double x, const double y)
			{
				auto& data = *static_cast<WindowData*>(glfwGetWindowUserPointer(window));

				MouseMovedEvent event(static_cast<float>(x), static_cast<float>(y));
				data.EventCallback(event);
			});



		mImGuiMouseCursors[ImGuiMouseCursor_Arrow] = glfwCreateStandardCursor(GLFW_ARROW_CURSOR);
		mImGuiMouseCursors[ImGuiMouseCursor_TextInput] = glfwCreateStandardCursor(GLFW_IBEAM_CURSOR);
		mImGuiMouseCursors[ImGuiMouseCursor_ResizeAll] = glfwCreateStandardCursor(GLFW_ARROW_CURSOR);   // FIXME: GLFW doesn't have this.
		mImGuiMouseCursors[ImGuiMouseCursor_ResizeNS] = glfwCreateStandardCursor(GLFW_VRESIZE_CURSOR);
		mImGuiMouseCursors[ImGuiMouseCursor_ResizeEW] = glfwCreateStandardCursor(GLFW_HRESIZE_CURSOR);
		mImGuiMouseCursors[ImGuiMouseCursor_ResizeNESW] = glfwCreateStandardCursor(GLFW_ARROW_CURSOR);  // FIXME: GLFW doesn't have this.
		mImGuiMouseCursors[ImGuiMouseCursor_ResizeNWSE] = glfwCreateStandardCursor(GLFW_ARROW_CURSOR);  // FIXME: GLFW doesn't have this.
		mImGuiMouseCursors[ImGuiMouseCursor_Hand] = glfwCreateStandardCursor(GLFW_HAND_CURSOR);

		// Update window size to actual size
		{
			int posX, posY;
			glfwGetWindowPos(mWindow, &posX, &posY);
			mData.PosX = posX;
			mData.PosY = posY;
			int width, height;
			glfwGetWindowSize(mWindow, &width, &height);
			mData.Width = width;
			mData.Height = height;
		}
	}

	void Window::shutdown()
	{

	}

	inline std::pair<float, float> Window::getWindowPos() const
	{
		BEY_PROFILE_FUNCTION();

		int x, y;
		glfwGetWindowPos(mWindow, &x, &y);
		return { float(x), float(y) };
	}

	void Window::onUpdate()
	{
		BEY_PROFILE_FUNCTION();
		{
			BEY_PROFILE_SCOPE("Polling events");
			glfwPollEvents();
		}
		{
			BEY_PROFILE_SCOPE("Swapping buffers");
			glfwSwapBuffers(mWindow);
		}
		const ImGuiMouseCursor imguiCursor = ImGui::GetMouseCursor();
		glfwSetCursor(mWindow, mImGuiMouseCursors[imguiCursor] ? mImGuiMouseCursors[imguiCursor] : mImGuiMouseCursors[ImGuiMouseCursor_Arrow]);

		mLastFrameTime = glfwGetTime();
	}

	void Window::setVSync(const bool enabled)
	{
		BEY_PROFILE_FUNCTION();

		glfwSwapInterval(enabled);
		mData.VSync = enabled;
	}

	void Window::toggleFullscreen()
	{
		if (mData.FullScreen)
		{
			glfwSetWindowMonitor(mWindow, nullptr,
				mBackupData.PosX, mBackupData.PosY, mBackupData.Width, mBackupData.Height, mBackupData.RefreshRate);
			
		}
		else
		{
			mBackupData = mData;
			const auto* videoMode = glfwGetVideoMode(glfwGetPrimaryMonitor());
			glfwSetWindowMonitor(mWindow, glfwGetPrimaryMonitor(),
				0, 0, videoMode->width, videoMode->height, videoMode->refreshRate);
		}
		setVSync(mData.VSync);
		mData.FullScreen = !mData.FullScreen;
	}

	bool Window::isVSync() const
	{
		return mData.VSync;
	}

}
