#pragma once

#ifdef BEY_PLATFORM_WINDOWS
#include "MyWinDef.hpp"
#endif


//GLM
#define GLM_ENABLE_EXPERIMENTAL
#include <glm/gtx/quaternion.hpp>
#include <glm/gtx/matrix_decompose.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/ext/matrix_transform.hpp>
#include <glm/gtc/quaternion.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <glm/gtx/vector_angle.hpp>
#include <glm/vec2.hpp>
#include <glm/vec3.hpp>
#include <glm/vec4.hpp>
#include <glm/common.hpp>



//Standard Libraries
#include <memory>
#include <vector>
#include <array>
#include <unordered_set>
#include <filesystem>
#include <functional>
#include <algorithm>
#include <random>
#include <unordered_map>
#include <fstream>
#include <string>
#include <sstream>
#include <filesystem>
#include <stack>
#include <future>
#include <atomic>
#include <thread>
#include <variant>




#include <fmt/ostream.h>

#include <dirent.h>


//Beyond
#include <Beyond/Core/Base.h>
#include <Beyond/Core/Log.h>
#include <Beyond/Core/Events/Event.h>
#include "Beyond/Core/Instrumentor.h"

// Math
#include <Beyond/Core/Math/Mat4.h>

#include <stb_image.h>
#include <glad/glad.h>
#include <GLFW/glfw3.h>

#include <imgui.h>

