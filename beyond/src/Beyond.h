//
// Note:	this file is to be included in client applications ONLY
//			NEVER include this file anywhere in the engine codebase
//
#pragma once

#include "Beyond/Core/Game.h"
#include "Beyond/Core/Log.h"
#include "Beyond/Core/Input.h"
#include "Beyond/Core/TimeStep.h"
#include "Beyond/Core/Timer.h"

#include "Beyond/Core/Events/Event.h"
#include "Beyond/Core/Events/ApplicationEvent.h"
#include "Beyond/Core/Events/KeyEvent.h"
#include "Beyond/Core/Events/MouseEvent.h"

#include "Beyond/Core/Math/AABB.h"
#include "Beyond/Core/Math/Ray.h"


// --- Beyond Render API ------------------------------
#include "Beyond/Renderer/Renderer.h"
#include "Beyond/Renderer/SceneRenderer.h"
#include "Beyond/Platform/OpenGL/RenderPass.h"
#include "Beyond/Platform/OpenGL/Framebuffer.h"
#include "Beyond/Platform/OpenGL/GLBuffer.h"
#include "Beyond/Platform/OpenGL/VertexArray.h"
#include "Beyond/Platform/OpenGL/Texture.h"
#include "Beyond/Platform/OpenGL/Shader.h"
#include "Beyond/Renderer/Mesh.h"
#include "Beyond/Renderer/Camera.h"
#include "Beyond/Renderer/Material.h"
// ---------------------------------------------------

// Scenes
#include "Beyond/Scene/Entity.h"
#include "Beyond/Scene/Scene.h"
#include "Beyond/Scene/SceneCamera.h"
#include "Beyond/Scene/SceneSerializer.h"
#include "Beyond/Scene/Components.h"

#include "Beyond/Core/Instrumentor.h"


#include <imgui.h>