![Beyond](bey.png)

**Beyond** is my final year project based on C++ and OpenGL. Beyond is a game engine that can be considered as a fork of [The Cherno](https://www.youtube.com/channel/UCQ-W1KE9EYfdxhL6S4twUNw)'s private repository **Hazel-dev**; but heavily modified. The new features are listed below.

---

## Motivation

Beyond was a way for me to learn all about graphics programming and game engine architecture in general.
It has helped me use many modern rendering techniques with it's deferred renderer(previously) and tile-based renderer(currently).

## New Features (compared to Hazel-dev)

- Deferred renderer (deprecated).
- Forward+ (tile-based) renderer.
- Point lights.
- SSR.
- SSAO (deprecated).
- HBAO.
- ImGui file browser mostly adopted from [ImGuiFileDialog](https://github.com/aiekick/ImGuiFileDialog).
- Fly camera.
- Tesselation.
- Parallax mapping.
- A fast profiler that uses C++17's pmr and can be viewed from any chromium based browser.

![Profiling](Screenshots/6.png)

## Libraries

- [Entt](https://github.com/skypjack/entt)
- [Assimp](https://github.com/assimp/assimp)
- [FMT](https://github.com/fmtlib/fmt)
- [glad](https://github.com/Dav1dde/glad)
- [GLFW](https://github.com/glfw/glfw)
- [GLM](https://github.com/g-truc/glm)
- [ImGui](https://github.com/ocornut/imgui)
- [spdlog](https://github.com/gabime/spdlog)
- [STBImage](https://github.com/nothings/stb)

## Pictures
![screenshot1](Screenshots/1.png)
![screenshot2](Screenshots/2.png)
![screenshot3](Screenshots/3.png)
![screenshot4](Screenshots/4.png)
![screenshot5](Screenshots/5.png)
